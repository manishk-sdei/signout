/*!
* Author: Antonio Dal Sie
* Name: lv-ripple
* Description: Material ripple effects
*/
(function($,exports){

	var lvRipple = (function(){

		var elements = {
			enable: [],
			disable: []
		};

		var rippleEventArray = []; //single init ripple

		var rippleConfig = {
			'rippleOpacity': .2,
			'rippleDelay': 100,
			'mobileTouch': false
		};

		function findElement(){
			elements.enable = $("ripple, *[ripple], .ripple").toArray();
		}

		function arrayRemove(array,elem){
			var index = array.indexOf(elem);
			if (index > -1) {
			    delete array[index];
			}
			return arrayClear(array);
		}

		function arrayClear(array){
			return array.filter(function(n){ return n != undefined });
		}

		function _destroyElements(){
			$(elements.enable).each(function(index, el) {
				delete elements.enable[index];
				$(el).trigger('r-destroy');
			});
			elements.enable = arrayClear(elements.enable);
		}

		function _disableElements(){
			$(elements.enable).each(function(index, el) {
				delete elements.enable[index];
				$(el).trigger('r-disable');
				elements.disable.push(el);
			});
			elements.enable = arrayClear(elements.enable);
		}

		function _enableElements(){
			$(elements.disable).each(function(index, el) {
				delete elements.disable[index];
				$(el).trigger('r-enable');
				elements.enable.push(el);
			});
			elements.disable = arrayClear(elements.disable);
		}

		function _updateElements(){
			$(elements.enable).each(function(index, el) {
				$(el).trigger('r-update');
			});
		}

		function destroyElement(elem){
			elements.enable = arrayRemove(elements.enable,$(elem)[0]);
			$(elem).trigger('r-destroy');
		}

		function disableElement(elem){
			elements.enable = arrayRemove(elements.enable,$(elem)[0]);
			$(elem).trigger('r-disable');
			elements.disable.push($(elem)[0]);
		}

		function enableElement(elem){
			elements.disable = arrayRemove(elements.disable,$(elem)[0]);
			$(elem).trigger('r-enable');
			elements.enable.push($(elem)[0]);
		}

		function updateElement(elem){
			$(elem).trigger('r-update');
		}

		function createMarkup(element){
			var content = $(element).html();
			var markup = $("<button></button>");
			var overink = $(element).hasClass('r-overink');

			if(overink){
				markup = $("<div></div>");
				var replacement = $("<button></button>");
			}


			if($(element).prop('nodeName').toLowerCase() != "ripple"){
				var cloneElement = $(element).clone();
				$(cloneElement).empty();
				$(cloneElement).removeClass('ripple');
				$(cloneElement).removeAttr('ripple');
				$(cloneElement).removeAttr('data-ripple');
				$(cloneElement).removeAttr('lv-ripple');
				
				if(overink){
					replacement = $(cloneElement);
				}else{
					markup = $(cloneElement);
				}
			}

			markup.addClass('ripple-cont');

			if(overink){
				replacement.addClass('ripple-content');
				replacement.html(content);

				markup.append(replacement);
			}else{
				markup.append("<div class='ripple-content'>"+content+"</div>");
			}

			markup.append("<div class='ink-content'></div>");
			return markup[0].outerHTML;
		}

		function replaceElement(element,newType) {
        	var attrs = {};

	        el = $(element);

	        $.each(el[0].attributes, function(idx, attr) {
	            attrs[attr.nodeName] = attr.nodeValue;
	        });

	        el = $("<" + newType + "/>", attrs);

	        return el;
	    }

		function createAllElements(){
			$(elements.enable).each(function(index, el) {
				var markup = createMarkup(el);
				var idmark = $.now();
				markup = $(markup);
				markup.addClass("ripple-idm-"+idmark);

				$(el).after(markup).remove();
				markup = $(".ripple-idm-"+idmark);
				markup.removeClass("ripple-idm-"+idmark);

				elements.enable[index] = markup[0];
				rippleInit(markup[0],index);
			});
		}

		function createElement(elem){
			if($(elem).length <= 1){
				var markup = createMarkup(elem);
				var idmark = $.now();
				markup = $(markup);
				markup.addClass("ripple-idm-"+idmark);

				$(elem).after(markup).remove();
				markup = $(".ripple-idm-"+idmark);
				markup.removeClass("ripple-idm-"+idmark);

				var index = elements.enable.push(markup[0]);
				return rippleInit(markup[0],index);
			}else{
				$(elem).each(function(index, el) {
					return createElement(el);
				});
			}
		}

		function rippleInit(element,index){

			var elem = null;
			var rippleCont = null;
			var inkLight = false;
			var inkColor = false;
			var customOpacity = null;
			var icon = false;
			var overInk = false;
			var preventInk = false;
			var index = index;
			var longTouch = null;
			var scrollTouch = null;
			
			elem = $(element);
			element = elem[0];

			if(typeof PointerEventsPolyfill !== "undefined"){
				PointerEventsPolyfill.initialize({
					'selector': elem,
					'mouseEvents': ['click','dblclick']
				});
			}

			var listenType = {
				"start" : ('ontouchstart' in document.documentElement) 
						? !!rippleConfig.mobileTouch 
							? 'touchstart'
							: 'click'
						: 'mousedown',
				"end" : ('ontouchend' in document.documentElement) ? 'touchend' : 'mouseup'
			};

			rippleEventArray[index] = [];

			elem.on("r-destroy",function(){
				elem.children(".ink-content").remove();
				var cont = elem.find(".ripple-content").html();
				elem.find(".ripple-content").remove();
				elem.append(cont);
				elem.removeClass('ripple-cont');
				elem.unbind(listenType.start,createRipple);
			});

			elem.on("r-disable",function(){
				elem.unbind(listenType.start,createRipple);
			});

			elem.on("r-enable",function(){
				elem.bind(listenType.start,createRipple);
			});

			elem.on("r-update",function(){
				_setValue();
			});

			elem.removeClass('ripple');
			rippleCont = elem.children(".ink-content");

			function _setValue(){
				icon = elem.hasClass('r-icon');
				overInk = elem.hasClass('r-overink');
				inkLight = typeof element.attributes['r-light'] !== "undefined";
				inkColor = typeof element.attributes['r-color'] !== "undefined" ? element.attributes['r-color'].nodeValue : false;
				customOpacity = typeof element.attributes['r-opacity'] !== "undefined" ? element.attributes['r-opacity'].nodeValue : null;
				preventInk = typeof element.attributes['r-prevent'] !== "undefined" ? element.attributes['r-prevent'].nodeValue : false;
			}

			_setValue();

			elem.bind(listenType.start,createRipple);

			function createRipple(event){
				var timeStamp = event.timeStamp;

				if(timeStamp == 0){
					var date = new Date();
					timeStamp = date.getTime();
				}

				if(elem.hasClass('r-childprevent')) return elem.removeClass('r-childprevent');
				elem.parents(".ripple-cont").addClass('r-childprevent');

				if(rippleEventArray[index].indexOf(event.timeStamp) != -1)return;
				rippleEventArray[index].push(event.timeStamp);

				var targetInk = $(event.target);

				if(typeof element.attributes['r-disabled'] != "undefined" || elem.hasClass('disabled'))return;
				if(targetInk.hasClass('r-noink') || !!targetInk.parents('.r-noink').length)return;
				if(!!preventInk && elem.is(preventInk))return;

				if(!!overInk)rippleCont.show(0);

				var inkWrapper = $("<div class='ink'><i></i></div>");
				var ink = inkWrapper.find("i");
				var incr = 0;
				var incrmax = 0;

				rippleCont.find(".ink").removeClass('new');
				inkWrapper.addClass('new');

				rippleCont.prepend(inkWrapper);

				//Set x and y position inside ripple content
				var x = event.type != "touchstart" ? 
					event.pageX - rippleCont.offset().left : 
					event.originalEvent.touches[0].pageX - rippleCont.offset().left;

				var y = event.type != "touchstart" ? 
					event.pageY - rippleCont.offset().top :
					event.originalEvent.touches[0].pageY - rippleCont.offset().top;

				// if icon set default position: 50% 50%
				if(!icon){
					inkWrapper.css({top: y+'px', left: x+'px'});
					
					//Set translate of user from center of ripple content
					x = x > rippleCont.outerWidth()/2 ? x - rippleCont.outerWidth()/2 : rippleCont.outerWidth()/2 - x;
					y = y > rippleCont.outerHeight()/2 ? y - rippleCont.outerHeight()/2 : rippleCont.outerHeight()/2 - y;
				}else{
					x = 0;
					y = 0;
				}

				//Set total translate
				var tr = (x*2) + (y*2);

				var h = rippleCont.outerHeight();
				var w = rippleCont.outerWidth();

				//Set diagonal of ripple container
				var d = Math.sqrt(w * w + h * h);
				//Set incremental diameter of ripple
				var incrmax = tr;
				
				incrmax = icon ? 0 : incrmax;

				inkWrapper.css({height: d, width: d});

				ink.css("opacity",0);
				
				var inkOpacity = customOpacity || rippleConfig.rippleOpacity;

				if(!!inkColor){
					ink.css("background-color",inkColor);
				}else if(!!inkLight){
					ink.css("background-color","rgb(255,255,255)");
				}

				if(!icon){
					rippleCont.css("background-color",'rgba(0,0,0,'+.098+')');

					if(!!inkColor){
						var rgba = hexToRGB(inkColor);
						rippleCont.css("background-color",'rgba('+rgba.r+','+rgba.g+','+rgba.b+','+.098+')');
					}else if(!!inkLight){
						rippleCont.css("background-color",'rgba(255,255,255,'+.098+')');
					}
				}

				setTimeout(function(){
					inkWrapper.addClass('animate');
				},1);

				// ink.css({height: d+incr, width: d+incr});

				ink.css({opacity: inkOpacity});
				
				var inkGrow = null;

				function hoverIncrement(){
					var incrStep = ((incrmax - incr)/100)*10
					inkGrow = setInterval(function(){
						if(incr < incrmax){
							incr += incrStep;
							inkWrapper.css({
								height: d+incr,
								width: d+incr
							});
						}else{
							clearInterval(inkGrow);
						}
					},50);
				}
				
				function listenerPress(){
					$(window).bind(listenType.end+' blur', removeInk);
					elem.bind('mouseleave',removeInk);
				}

				function removeInk(){
					$(window).unbind(listenType.end+' blur', removeInk);
					elem.unbind('mouseleave', removeInk);

					clearInterval(inkGrow);
					clearInterval(longTouch);
					clearInterval(scrollTouch);

					var delay = incr <= incrmax ? rippleConfig.rippleDelay : 1;
					incr = incr < incrmax ? incrmax : incr;
					inkWrapper.css({
						height: d+incr,
						width: d+incr
					});
					setTimeout(function(){
						ink.css({
							opacity:0
						});
						if(!!inkWrapper.hasClass('new') && !icon)rippleCont.css("background-color","");
						setTimeout(function(){
							inkWrapper.remove();
							if(!!overInk && !rippleCont.find(".ink").length)rippleCont.hide(0);
						},550);
					},delay);
				}

				hoverIncrement();

				if(event.type == "mousedown" && event.which !== 1){
					setTimeout(function(){
						removeInk();
					},100);
				}else if(event.type == "click"){
					setTimeout(function(){
						removeInk();
					},300);
				}else if(event.type == "touchstart"){
					longTouch = setTimeout(function(){
						removeInk();
					},1000);
					$(window).bind('scroll',forceRemoveInk);
					scrollTouch = setTimeout(function(){
						$(window).unbind('scroll',forceRemoveInk);
					},500);
					listenerPress();
				}else{
					listenerPress();
				}
			}

			function _destroySelf(){
				elements.enable = arrayRemove(elements.enable,$(elem)[0]);
				elem.children(".ink-content").remove();
				var cont = elem.find(".ripple-content").html();
				elem.find(".ripple-content").remove();
				elem.append(cont);
				elem.removeClass('ripple-cont');
				elem.unbind('mousedown touchstart',createRipple);
			}

			function _disableSelf(){
				elements.enable = arrayRemove(elements.enable,$(elem)[0]);
				elem.unbind('mousedown touchstart',createRipple);
				elements.disable.push($(elem)[0]);
			}

			function _enebleSelf(){
				elements.disable = arrayRemove(elements.disable,$(elem)[0]);
				elem.bind('mousedown touchstart',createRipple);
				elements.enable.push($(elem)[0]);
			}
			
			return {
				element: elem,
				destroy: _destroySelf,
				disable: _disableSelf,
				enable: _enebleSelf,
				update: _setValue
			}
		}
		
		function hexToRGB(hex){

			var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
			var matches = patt.exec(hex);

			return {
				r:parseInt(matches[1], 16),
				g:parseInt(matches[2], 16),
				b:parseInt(matches[3], 16)
			};
		}
		
		function _init(config){
			!! config ? _setConfig(config) : $.noop();
			findElement();
			createAllElements();
		}

		function _initElement(elem){
			return !! elem ? createElement(elem) : createElement(this);
		}

		function _getList(){
			return elements;
		}

		function _getRippleEventArray(){
			return rippleEventArray;
		}

		function _setConfig(config){
			$.extend(rippleConfig, config);
		}

		function _findCommand(cmd){
			switch(cmd){
				case "enable": enableElement(this)
							break;
				case "disable": disableElement(this)
							break;
				case "destroy": destroyElement(this)
							break;
				case "update": updateElement(this)
							break;
				default: return _initElement(this);
						break;
			}
		}

		return {
			init: _init,
			initElement: _initElement,
			list: _getList,
			eventHistory: _getRippleEventArray,
			config: _setConfig,
			destroy: _destroyElements,
			disable: _disableElements,
			enable: _enableElements,
			update: _updateElements,
			switch: _findCommand
		}
	})();

	// exports.lvRipple = exports.lvRipple || lvRipple;
	$.ripple = lvRipple;
	$.fn.ripple = lvRipple.switch;
	

})(jQuery,window);

/*
Add the Material Design Style Ripple Effect on Click / Touch.
*/
(function ($) {
    var transparent;
    //only run this once. this addes an empty elment in boody root and gets the defintion of the browsers transparent color scheme.
    if (typeof (transparent) == "undefined") {
        // Get this browser's definition of no back ground / transprent, rgba(0,0,0,0) etc..
        // Must be appended else Chrome etc return 'initial'
        var $temp = $('<div style="background:none;display:none;"/>').appendTo('body');
        //now we just get the value given back to use by the browser for its spcific transparent color scheme.
        transparent = $temp.css('backgroundColor');
        //remove the temp object since were done with it.
        $temp.remove();
    }


    //add Material desing style rippple effect to a given element that animates on click / touch.
    $.fn.rippleEffect = function (options) {
        // Extend our default options with those provided.
        var opts = $.extend({}, $.fn.rippleEffect.defaults, options);

        return this.each(function () {

            $(this).on("click", function (e) {
                var $thisElement = $(this),
                eventPageX,
                eventPageY,
                inkX,
                inkY,
                maxDiameter,
                eventType = e.type,
                rippleColor,
                $inkSpan,
                $inkparent;

                //default append
                $inkparent = $thisElement;

                function getRippleColorFromTraverse() {
                    if (opts.inkColor != "") {
                        return opts.inkColor;
                    } else {
                        //try and get from elemnt or parent what ever has a bg color and it will lighten or dark based on color.
                        return getInkColor($inkparent, opts.inkDefaultColor);
                    }
                }
                //check to see if were appending ink to a parent element other than the trigger
                //if you append to a parent item that item will have the ripple span and it may block access to links
                //all i had to do was give any of the siblings of the ripple a z-index of 1, some i had to set position to relative.
                if (opts.appendInkTo != "") {
                    $inkparent = $thisElement.closest(opts.appendInkTo);
                }
                // check to se if we have an ink, if not we need to add it in, we only need to do this once.
                if ($inkparent.find("." + opts.inkClass).length == 0) {
                    //add ink 
                    $inkparent.append('<span class="' + opts.inkContainerClass + '"><span class="' + opts.inkClass + '"></span></span>');
                }
                //set the ink var targeting the ink within the element to prevent dup ink animates.
                $inkSpan = $inkparent.find("." + opts.inkContainerClass + " > " + " ." + opts.inkClass);
                //incase of quick double click or animate is present. remove the animation
                $inkSpan.removeClass("animate");

                //now if the ink has no height set we need to add it in.
                if (!$inkSpan.height() && !$inkSpan.width()) {
                    //use $thisElement width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    maxDiameter = Math.max($inkparent.outerWidth(), $inkparent.outerHeight());
                    //set width and height and get bg color for the
                    //see if ripple color was provided via data attr.
                    if (typeof ($thisElement.data("ripple")) != "undefined" && $thisElement.data("ripple") != "") {
                        rippleColor = $thisElement.data("ripple");
                    } else {
                        //check if a target element id to get a ripple color from is provided
                        if (typeof ($thisElement.data("ripple-getcolorfromid")) != "undefined" && $thisElement.data("ripple-getcolorfromid") != "") {
                            var idToUse = $thisElement.data("ripple-getcolorfromid");
                            //make sure element is on page.
                            if ($(idToUse).length > 0) {
                                //set from given elmements bgcolor.
                                rippleColor = $(idToUse).css("background-color");
                            } else {
                                //get from default ways
                                rippleColor = getRippleColorFromTraverse();
                            }
                        } else {
                            //get from default ways
                            rippleColor = getRippleColorFromTraverse();
                        }
                    }
                    //set h and w and ripple color.
                    $inkSpan.css({ height: maxDiameter, width: maxDiameter, "background-color": rippleColor });
                }
                
                //now that the ink is taken care of we need to set the position where it starts, which if from click.
                //get event type.
                if(eventType === "click"){
                    eventPageX = e.pageX; 
                    eventPageY = e.pageY;
                } else if(eventType === "touchstart") {
                    var touch = (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]);
                    eventPageX = touch.pageX;
                    eventPageY = touch.pageY
                }
               
                inkX = (eventPageX - $inkparent.offset().left - $inkSpan.width() / 2);
                inkY = (eventPageY - $inkparent.offset().top - $inkSpan.height() / 2);
                $inkSpan.css({ top: inkY + 'px', left: inkX + 'px' }).addClass("animate");
                //remove animation after a little bit.
                setTimeout(function () {
                    $inkSpan.removeClass("animate")
                }, 800);

            });
        });
    };
    // Plugin defaults – added as a property on our plugin function.
    $.fn.rippleEffect.defaults = {
        inkContainerClass: "ripple",
        inkClass: "ink",
		
        //to avoid a global default you can add data-ripple="#ff00ff" to the element that will have the ripple and that color will be used.
        //or to use a tagrget elements background color for the ripple you can set the data-ripple="" and data-ripple-getcolorfromid="#elmentWIthBGColorToUSe"
        inkDefaultColor: "#F0F0F0", //falback color to use if using parent traversing to get a bg color.
        inkColor: "", //the ink color you want the element to use, this will override any bg checks for element and parent traversing, but if the ement has data-ripple="#fff" the #fff will be used above all else
        //if you append to a parent item that item will have the ripple span and it may block access to links and child elements
        //all i had to do was give any of the children of the appended element a z-index of 1, some i had to set position to relative.
        appendInkTo: "" //append ink to a diffrent element. will find closest element matching.
    };

    //Now users can include a line like this in their scripts:
    //$.fn.rippleEffect.defaults.inkDefaultColor = "#0000FF";

    // Define our get background color function.
    //this will try and get a background color for the ink color manipulation 
    //by starting with the element provided and going as far back to the body unless a value is found.
    //providing a falback value will make sure that an actual color is returned
    function getBackgroundColorForInk($element, fallback) {
        function getBgColor($elementToCheckForBg) {
            //here we check against the @transparent var that is set on load.
            if ($elementToCheckForBg.css('backgroundColor') == transparent) {
                //here we check if were not at body, if not, then run again on the elments parent, if were at body use fallback or the browers take on transparent.
                return !$elementToCheckForBg.is('body') ? getBgColor($elementToCheckForBg.parent()) : fallback || transparent;
            } else {
                //it was not transparent return this color.
                return $elementToCheckForBg.css('backgroundColor');
            }
        }
        //run the function which will repeate as long as it needs until it hits body to find a color to use.
        return getBgColor($element);
    };

    //create as a property so users can set make there own lum check.
    //returns the direction lumination should go for ink contrast.
    //get the lumination value for a given color.
    $.fn.rippleEffect.getLuminationValue = function (hexcolor) {
        var hexcolor = hexcolor.substring(1);      // strip #
        var rgb = parseInt(hexcolor, 16);   // convert rrggbb to decimal
        var r = (rgb >> 16) & 0xff;  // extract red
        var g = (rgb >> 8) & 0xff;  // extract green
        var b = (rgb >> 0) & 0xff;  // extract blue

        var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
        return luma;
    }

    //create as property, for user override. 
    //this was from a site point article.
    //get a new color luminance -0.15 is darker by 15% and 0.20 is lighter by 20%
    $.fn.rippleEffect.getColorLuminance = function (hexcolor, lum) {
        // validate hex string
        hexcolor = String(hexcolor).replace(/[^0-9a-f]/gi, '');
        if (hexcolor.length < 6) {
            hexcolor = hexcolor[0] + hexcolor[0] + hexcolor[1] + hexcolor[1] + hexcolor[2] + hexcolor[2];
        }
        //set a default lum value.
        lum = lum || 0;

        // convert to decimal and change luminosity
        var resultHex = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hexcolor.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            resultHex += ("00" + c).substr(c.length);
        }

        return resultHex;
    }

    //create property for overide if wanted, this converts an rgb value to hex.
    //it does not work right with rgba though it just returns the bas rgb part so the trans is not claculated in.
    $.fn.rippleEffect.colorToHex = function (rgb) {
        //first echeck to see if we already have a hex color, if so just return.
        if (rgb.substr(0, 1) === '#') {
            return color;
        }
        rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? "#" +
         ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
         ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
         ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
    }

    //this is the base function that does all of the magic, it gets a bkg color, 
    //then changes the colors lumanince so the ink is visiable when being animated.
    function getInkColor($element, fallBackColor) {
        var backgroundOrInhreitedBackGround,
            hex,
            luma,
            returnLumintion;
        //get back ground color of element / or parent with an actual color or use fallback of a lighter gray 
        // Call our getBackgroundColorForInk function.

        backgroundOrInhreitedBackGround = getBackgroundColorForInk($element, fallBackColor);
        //convert to hex, this returns hex if it is already hex.
        //call our colorToHex function
        hex = $.fn.rippleEffect.colorToHex(backgroundOrInhreitedBackGround);
        //now get the lumination value of this bg.
        //call our getLuminationValue function
        luma = $.fn.rippleEffect.getLuminationValue(hex);

        //239 for lighter lumination ok 
        if (luma <= 239) {
            //go lighter alwasy prefered.
            //we need to make sure we are light enough though
            if (luma <= 70) {
                returnLumintion = $.fn.rippleEffect.getColorLuminance(hex, 0.80);
            } else {
                returnLumintion = $.fn.rippleEffect.getColorLuminance(hex, 0.20);
            }
        } else {
            //go darker
            returnLumintion = $.fn.rippleEffect.getColorLuminance(hex, -0.15);
        }
        return returnLumintion;
    };


    //appply 
    $("[data-ripple]").rippleEffect();
})(jQuery);


 //side bar script
   $(document).ready(function() {

    $('#menu-toggle').click(function(e) {
      e.preventDefault();
      $('#wrapper').toggleClass('toggled');
      $('#overlay').fadeToggle( "slow", "swing" );
    });

    $('#overlay').click(function() {
      $('#overlay').fadeOut('slow');
      $('#wrapper').removeClass('toggled');
    });

  });

//side bar script
//pop up
		//(function($){
//			$(window).on("load",function(){
//				
//				$("#exampleModal .modal-body").mCustomScrollbar({
//					setHeight:450,
//					theme:"minimal-dark"
//				});
//				
//				
//			});
//		})(jQuery);

//pop up
//div conver accordian
$(function() {    
    var isXS = false,
        $accordionXSCollapse = $('.accordion-xs-collapse');

    // Window resize event (debounced)
    var timer;
    $(window).resize(function () {
        if (timer) { clearTimeout(timer); }
        timer = setTimeout(function () {
            isXS = Modernizr.mq('only screen and (max-width: 767px)');
            
            // Add/remove collapse class as needed
            if (isXS) {
                $accordionXSCollapse.addClass('collapse');               
            } else {
                $accordionXSCollapse.removeClass('collapse');
            }
        }, 100);
    }).trigger('resize'); //trigger window resize on pageload    
    
    // Initialise the Bootstrap Collapse
    $accordionXSCollapse.each(function () {
        $(this).collapse({ toggle: false });
    });      
    
    // Accordion toggle click event (live)
    $(document).on('click', '.accordion-xs-toggle', function (e) {
        e.preventDefault();
        
        var $thisToggle = $(this),
            $targetRow = $thisToggle.parent('.tr'),
            $targetCollapse = $targetRow.find('.accordion-xs-collapse');            
    	
        if (isXS && $targetCollapse.length) { 
            var $siblingRow = $targetRow.siblings('.tr'),
                $siblingToggle = $siblingRow.find('.accordion-xs-toggle'),
                $siblingCollapse = $siblingRow.find('.accordion-xs-collapse');
            
            $targetCollapse.collapse('toggle'); //toggle this collapse
            $siblingCollapse.collapse('hide'); //close siblings
            
            $thisToggle.toggleClass('collapsed'); //class used for icon marker
            $siblingToggle.removeClass('collapsed'); //remove sibling marker class
        }
    });
});
(function(exports){
	$.ripple.init();
})(window);



