/**
 * @class         :	AuthenticatedComponent
 * @description   : Handles application Login authentication
 * @Created by    : smartData
 */

import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router'

export function requireAuths(Component) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount () {
            this.checkAuth(this.props.isAuthenticated);
        }

        componentWillReceiveProps (nextProps) {
            this.checkAuth(nextProps.isAuthenticated);
        }

        checkAuth (isAuthenticated) {
            if (!isAuthenticated) {
                let token = localStorage.getItem('access_token', token);
                if(token){
                    this.props.isAuthenticated = true;
                    this.props.token = token;
                }else{
                    let redirectAfterLogin = this.props.location.pathname;
                    browserHistory.push('/login')
                }

            }
        }

        render () {
            return (
                <div>
                    {this.props.isAuthenticated === true
                        ? <Component {...this.props}/>
                        : null
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        token: state.login.token,
        userName: state.login.userName,
        isAuthenticated: state.login.isAuthenticated
    });

    return connect(mapStateToProps)(AuthenticatedComponent);
}
