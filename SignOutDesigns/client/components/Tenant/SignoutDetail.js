import React, { Component } from 'react';
import ModalWindow from '../Modals/SearchPatientModal';

export default class SignOutList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: false
        };
        this.open_modal = this.open_modal.bind(this);
    }

    componentDidMount() {

        document.title = "Signout - SignOutList";
    }

    open_modal(data) {
        this.setState({
            modal1: !this.state.modal1
        });
    }

    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar ">
                    <h1 className="page-title">Blue List</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><a href="signoutlist.html">Sign out</a></li>
                        <li className="active">Blue List</li>
                    </ul>
                </div>
                <ModalWindow modal_var={this.state.modal1} handle_modal = {this.open_modal} />
                <div className="container-fluid mrg-top30 pd-bt30">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="white-bg">
                                    <button type="button" onClick={this.open_modal} className="ripple r-round btn blue-btn add-btn" data-toggle="modal" data-target="#create-patient"><span className="hidden-xs-down">ADD PATIENT</span><img className="hidden-sm-up" src="../../assets/images/add-file.png" /></button>
                                    <div className="modal fade" id="create-patient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        {/*
                                        <!-- Modal -->*/}
                                        <div className="modal-dialog wd600" role="document">
                                            <div className="modal-content check-pop add-pt">
                                                <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel">Add Patient</h5>
                                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                </div>
                                                <div className="modal-body">
                                                    <div className="row">
                                                        <div className="search-bar">
                                                            <input className="inputMaterial" type="text" required />
                                                            <span className="highlight"></span> <span className="bar"></span>
                                                            <label>Search Patient Here...</label>
                                                            <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                                                        </div>
                                                        <div className="crt-patient">
                                                            <button className="link-popup" data-toggle="modal" data-target="#create-patient">Create Patient</button>
                                                        </div>
                                                        <div className="col-sm-12">
                                                            <div className="table-responsive">
                                                                <table className="table table-hover add-pt-table table-bordered">
                                                                    {/*
                                                                    <!--table start-->*/}
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="7%">&nbsp;</th>
                                                                            <th>Name</th>
                                                                            <th>Mrn</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div className="checkbox">
                                                                                    <label>
                                                                                        <input type="checkbox" value="" />
                                                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>Patient Name</td>
                                                                            <td>789</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div className="checkbox">
                                                                                    <label>
                                                                                        <input type="checkbox" value="" defaultChecked />
                                                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>Patient Name</td>
                                                                            <td>324</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div className="checkbox">
                                                                                    <label>
                                                                                        <input type="checkbox" value="" />
                                                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>Patient Name</td>
                                                                            <td>123</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div className="checkbox">
                                                                                    <label>
                                                                                        <input type="checkbox" value="" />
                                                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>Patient Name</td>
                                                                            <td>907</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div className="checkbox">
                                                                                    <label>
                                                                                        <input type="checkbox" value="" />
                                                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>Patient Name</td>
                                                                            <td>453</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                    <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="print-btn">
                                        <button><i className="zmdi zmdi-print"></i><span>Print</span></button>
                                    </div>
                                    <div className="table-responsive">
                                        <div className="divtable accordion-xs">
                                            <div className="tr headings">
                                                <div className="th firstname">Patient Name</div>
                                                <div className="th firstname">Consults</div>
                                                <div className="th firstname">PMH</div>
                                                <div className="th firstname">Summary</div>
                                                <div className="th firstname">RESULTS</div>
                                                <div className="th firstname">Treatments</div>
                                                <div className="th firstname">MEDI/DIET/ RESP</div>
                                                <div className="th firstname">LINES</div>
                                                <div className="th firstname">Follow-ups</div>
                                                <div className="th firstname">Action</div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5709-1</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy Protein S deficiency, chronic DVT on coumadian
                                                            <br/>
                                                            <br/> 10/24: 103 F fever this am, CT guided pelvic abscess drainage, cultures, c.w meropenem. FOBT positive for occult blood. </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone morphine nifedipime protonix cortisol cinacalcet hydrocortisone topical diphenydramine
                                                        </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u Procalcitonin and anti ds-DNA []F/U GI
                                                            <br/>
                                                            <br/> Night: []
                                                        </div>
                                                        <div className="td lastname action-click">
                                                            <button data-toggle="modal" data-target="#edit-editor" className="action-btn"><i className="zmdi zmdi-edit"></i></button>
                                                            <div className="modal fade" id="edit-editor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div className="modal-dialog modal-lg" role="document">
                                                                    <div className="modal-content check-pop edit-pt-editor">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title" id="exampleModalLabel">Blackwell Signout List</h5>
                                                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            <div className="row">
                                                                                <h6>Patient Name: 5709-1</h6>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Consults</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">PMH</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Summary</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Result</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Treatments</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Follow-ups</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                            <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                            <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5303</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy Protein S deficiency, chronic DVT on coumadian </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone morphine nifedipime protonix cortisol cinacalcet hydrocortisone topical diphenydramine
                                                        </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u Procalcitonin and anti ds-DNA []F/U GI </div>
                                                        <div className="td lastname action-click">
                                                            <button data-toggle="modal" data-target="#edit-editor1" className="action-btn"><i className="zmdi zmdi-edit"></i></button>
                                                            <div className="modal fade" id="edit-editor1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div className="modal-dialog modal-lg" role="document">
                                                                    <div className="modal-content check-pop edit-pt-editor">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title" id="exampleModalLabel">Blackwell Signout List</h5>
                                                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            <div className="row">
                                                                                <h6>Patient Name: 5709-1</h6>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Consults</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">PMH</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Summary</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Result</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Treatments</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Follow-ups</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                            <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                            <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5478</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy Protein S deficiency, chronic DVT on coumadian </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone morphine nifedipime </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u </div>
                                                        <div className="td lastname action-click">
                                                            <button data-toggle="modal" data-target="#edit-editor2" className="action-btn"><i className="zmdi zmdi-edit"></i></button>
                                                            <div className="modal fade" id="edit-editor2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div className="modal-dialog modal-lg" role="document">
                                                                    <div className="modal-content check-pop edit-pt-editor">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title" id="exampleModalLabel">Blackwell Signout List</h5>
                                                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            <div className="row">
                                                                                <h6>Patient Name: 5709-1</h6>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Consults</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">PMH</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Summary</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Result</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Treatments</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Follow-ups</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                            <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                            <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5303</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u Procalcitonin and anti ds-DNA []F/U GI </div>
                                                        <div className="td lastname action-click">
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5478</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone morphine nifedipime protonix cortisol </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u Procalcitonin and anti ds-DNA </div>
                                                        <div className="td lastname action-click">
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5303</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy Protein S deficiency, chronic DVT on coumadian </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone morphine nifedipime protonix cortisol cinacalcet hydrocortisone topical diphenydramine
                                                        </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u Procalcitonin and anti ds-DNA []F/U GI </div>
                                                        <div className="td lastname action-click">
                                                            <button data-toggle="modal" data-target="#edit-editor3" className="action-btn"><i className="zmdi zmdi-edit"></i></button>
                                                            <div className="modal fade" id="edit-editor3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div className="modal-dialog modal-lg" role="document">
                                                                    <div className="modal-content check-pop edit-pt-editor">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title" id="exampleModalLabel">Blackwell Signout List</h5>
                                                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            <div className="row">
                                                                                <h6>Patient Name: 5709-1</h6>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Consults</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">PMH</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Summary</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Result</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Treatments</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Follow-ups</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                            <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                            <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tr">
                                                <div className="td firstname accordion-xs-toggle">5478</div>
                                                <div className="accordion-xs-collapse">
                                                    <div className="inner">
                                                        <div className="td lastname"><span>Uro: <font>Whang</font></span> <span>Transplant surg: <font>Aitchison</font></span> <span>Vascular: <font>addis</font></span> <span>Id: <font>Jerome</font></span> <span>Rheumotology: <font>Paolina</font></span></div>
                                                        <div className="td username">ESRD 2/2 SLE s/p LRRT and rejection, HTN, protein S def, chronic DVT</div>
                                                        <div className="td lastname">SLE flair vs. endocarditis, Sepsis 2/2 transplant pyeionephriris s/p transpant nephrectomy Protein S deficiency, chronic DVT on coumadian
                                                            <br/>
                                                            <br/> 10/24: 103 F fever this am, CT guided pelvic abscess drainage, cultures, c.w meropenem. FOBT positive for occult blood. </div>
                                                        <div className="td username">CT: Probable hydronephrosis in RLQ transplant kidney, complex fluid w/in pelvis US pelvis: Rt paraovarian cyst 1.8cm, small amt of fluid in pelvis</div>
                                                        <div className="td lastname">meropenem prednisone morphine nifedipime protonix cortisol cinacalcet hydrocortisone topical diphenydramine
                                                        </div>
                                                        <div className="td username">D5 1/2NS at M Albuterol q4 Atc Orapred 2mg/kg/day</div>
                                                        <div className="td lastname">PIV</div>
                                                        <div className="td username">Day: []f/u cultures []f/u Procalcitonin and anti ds-DNA []F/U GI
                                                            <br/>
                                                            <br/> Night: []
                                                        </div>
                                                        <div className="td lastname action-click">
                                                            <button data-toggle="modal" data-target="#edit-editor4" className="action-btn"><i className="zmdi zmdi-edit"></i></button>
                                                            <div className="modal fade" id="edit-editor4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div className="modal-dialog modal-lg" role="document">
                                                                    <div className="modal-content check-pop edit-pt-editor">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title" id="exampleModalLabel">Blackwell Signout List</h5>
                                                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            <div className="row">
                                                                                <h6>Patient Name: 5709-1</h6>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Consults</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">PMH</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Summary</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Result</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Treatments</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                                <div className="col-sm-4">
                                                                                    <p className="editor-heading">Follow-ups</p>
                                                                                    <div className="editor-content"> <img src="../../assets/images/editor-pic.jpg" className="img-fluid" alt="" /> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                            <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>
                                                            <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/*
                                        <!--table End-->*/}
                                    </div>
                                    <div className="pagination-wrap">
                                        {/*
                                        <!--pagination-->*/}
                                        <nav aria-label="Page navigation example">
                                            <ul className="pagination">
                                                <li className="page-item">
                                                    <a className="page-link" href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span className="sr-only">Previous</span> </a>
                                                </li>
                                                <li className="page-item active"><a className="page-link" href="#">1</a></li>
                                                <li className="page-item"><a className="page-link" href="#">2</a></li>
                                                <li className="page-item"><a className="page-link" href="#">3</a></li>
                                                <li className="page-item"><a className="page-link" href="#">4</a></li>
                                                <li className="page-item"><a className="page-link" href="#">5</a></li>
                                                <li className="page-item">
                                                    <a className="page-link" href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span className="sr-only">Next</span> </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    {/*
                                    <!--pagination-->*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}
