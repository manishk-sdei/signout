import React, { Component } from 'react';
import ModalWindow from '../Modals/PatientModal';

export default class Patients extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: false
        };
        this.open_modal = this.open_modal.bind(this);
    }

    componentDidMount() {

        document.title = "Signout - Patients";
    }

    open_modal(data) {
        this.setState({
            modal1: !this.state.modal1
        });
    }

    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar ">
                    {/*<!-- welcome bar -->*/}
                    <h1 className="page-title">Patient List</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><a href="signoutlist.html">Sign out</a></li>
                        <li className="active">Patient List</li>
                    </ul>
                </div>
                <ModalWindow modal_var={this.state.modal1} handle_modal = {this.open_modal} />
                <div className="container-fluid mrg-top30 pd-bt30">
                    <div className="col pd-off-xs">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="white-bg">
                                    <button type="button" onClick={this.open_modal} className="ripple r-round btn blue-btn add-btn" data-toggle="modal" data-target="#create-patient"> <span className="hidden-xs-down">CREATE PATIENT</span> <img className="hidden-sm-up" src="../../assets/images/add-file.png" /> </button>
                                    <div className="modal fade" id="create-patient" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        {/*{/*<!-- Modal -->*/}*/}
                                        <div className="modal-dialog modal-lg" role="document">
                                            <div className="modal-content check-pop patientedit-form">
                                                <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                </div>
                                                <div className="modal-body">
                                                    <div className="row">
                                                        <form className="form-inline" action="" method="post">
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>First Name</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>Last Name</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>D.O.B</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group custom-box">
                                                                    <select className="selectpicker">
                                                                        <option>Sex</option>
                                                                        <option>Female</option>
                                                                        <option>Male</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>Mrn Id</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>Email</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-12 popup-title">Contact Info</div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>Contact No.</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group custom-box">
                                                                    <select className="selectpicker">
                                                                        <option>State</option>
                                                                        <option>Alabama</option>
                                                                        <option>Arizona</option>
                                                                        <option>California</option>
                                                                        <option>Florida</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group custom-box">
                                                                    <select className="selectpicker">
                                                                        <option>City/Town</option>
                                                                        <option>Georgia</option>
                                                                        <option>Illinois</option>
                                                                        <option>Indiana</option>
                                                                        <option>Missouri</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <input className="inputMaterial" type="text" required />
                                                                    <span className="highlight"></span> <span className="bar"></span>
                                                                    <label>Zip Code</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-12">
                                                                <div className="form-group">
                                                                    <textarea className="inputMaterial" type="text" required></textarea><span className="highlight"></span> <span className="bar"></span>
                                                                    <label>Address</label>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                    <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group search-bar">
                                        <input className="inputMaterial" type="text" required />
                                        <span className="highlight"></span> <span className="bar"></span>
                                        <label>Search Patient Name and MRN.</label>
                                        <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                                    </div>
                                    <div className="table-responsive">
                                        <table className="table table-hover patient-list">
                                            {/*<!--table start-->*/}
                                            <thead>
                                                <tr>
                                                    <th width="30%">Name<span className="set"><i className="zmdi zmdi-chevron-up"></i><i className="zmdi zmdi-chevron-down"></i></span></th>
                                                    <th width="15%">Age</th>
                                                    <th width="20%">Sex<span className="set"><i className="zmdi zmdi-chevron-up"></i><i className="zmdi zmdi-chevron-down"></i></span></th>
                                                    <th width="18%">Mrn</th>
                                                    <th width="17%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>25</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient1"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient1" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea><span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>25</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient2"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient2" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>25</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient3"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient3" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>35</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient4"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient4" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>40</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient5"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient5" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>32</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient6"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient6" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>66</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient7"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient7" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>27</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient7"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient7" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>33</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient8"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient8" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>25</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient9"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient9" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*{/*<!-- Modal -->*/}*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Patient Name</td>
                                                    <td>35</td>
                                                    <td>female</td>
                                                    <td>234</td>
                                                    <td>
                                                        <button className="action-btn" data-toggle="modal" data-target="#edit-patient10"><i className="zmdi zmdi-edit"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-eye"></i></button>
                                                        <button className="action-btn"><i className="zmdi zmdi-delete"></i></button>
                                                        <div className="modal fade" id="edit-patient10" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            {/*<!-- Modal -->*/}
                                                            <div className="modal-dialog modal-lg" role="document">
                                                                <div className="modal-content check-pop patientedit-form">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title" id="exampleModalLabel">Add/Edit Patient</h5>
                                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        <div className="row">
                                                                            <form className="form-inline" action="" method="post">
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>First Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Last Name</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>D.O.B</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>Sex</option>
                                                                                            <option>Female</option>
                                                                                            <option>Male</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Mrn Id</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Email</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12 popup-title">Contact Info</div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Contact No.</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>State</option>
                                                                                            <option>Alabama</option>
                                                                                            <option>Arizona</option>
                                                                                            <option>California</option>
                                                                                            <option>Florida</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group custom-box">
                                                                                        <select className="selectpicker">
                                                                                            <option>City/Town</option>
                                                                                            <option>Georgia</option>
                                                                                            <option>Illinois</option>
                                                                                            <option>Indiana</option>
                                                                                            <option>Missouri</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-6">
                                                                                    <div className="form-group">
                                                                                        <input className="inputMaterial" type="text" required />
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Zip Code</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-sm-12">
                                                                                    <div className="form-group">
                                                                                        <textarea className="inputMaterial" type="text" required></textarea>
                                                                                        <span className="highlight"></span> <span className="bar"></span>
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button" data-ripple="" className="btn blue-btn">Save</button>
                                                                        <button type="button" className="btn grey-btn mr-sm-4" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        {/*<!--table End-->*/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
