/**
 * @class         :	TanantDashboard
 * @description   : 
 * @Created by    : smartData
 */

import React, { Component } from 'react';

export default class TanantDashboard extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

        document.title = "Signout - Dashboard";
    }

    render() {
        return(
                <div id="page-content-wrapper">
                    <div className="col welcome-bar">
                        <h1 className="page-title">Dashboard</h1>
                        <ul className="breadcrumb pull-right hidden-xs-down">
                            <li className="active">Welcome!</li>
                        </ul>
                    </div>
                    <div className="container-fluid mrg-top30 pd-bt30">
                        <div className="col pd-off-xs">
                            <div className="row">
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">Total Hospital (Tenat)</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-sm-4 col-xs-4">
                                                    <div className="dashboard-icon green text-center"><img src="/client/assets/images/icon-1.png" alt="" /></div>
                                                </div>
                                                <div className="col-sm-8 text-right dash-content col-xs-8">
                                                    <h3>54</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">Total User</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-sm-4 col-xs-4">
                                                    <div className="dashboard-icon orange text-center"><i className="zmdi zmdi-account"></i></div>
                                                </div>
                                                <div className="col-sm-8 text-right dash-content col-xs-8">
                                                    <h3>1256</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">Total Patient</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-sm-4 col-xs-4">
                                                    <div className="dashboard-icon lightgrey text-center"><i className="zmdi zmdi-hotel"></i></div>
                                                </div>
                                                <div className="col-sm-8 text-right dash-content col-xs-8">
                                                    <h3>57</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">To be decided</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-sm-4 col-xs-4">
                                                    <div className="dashboard-icon pink text-center"><i className="zmdi zmdi-plus-square"></i></div>
                                                </div>
                                                <div className="col-sm-8 text-right dash-content col-xs-8">
                                                    <h3>16</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}
