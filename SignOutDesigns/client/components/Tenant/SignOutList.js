import React, { Component } from 'react';

export default class SignOutList extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

        document.title = "Signout - SignOutList";
    }

    render() {
        return(
                <div id="page-content-wrapper">                    
                    <div className="col welcome-bar">
                        <h1 className="page-title">Signout List</h1>
                        <ul className="breadcrumb pull-right hidden-xs-down">
                            <li className="active">Welcome!</li>
                        </ul>
                    </div>
                    <div className="container-fluid mrg-top30 pd-bt30">
                        <div className="col pd-off-xs">
                            <div className="row">
                                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                    <div className="sign-whtbox">
                                        <a href="/signout_detail">
                                            <h2>Blue List</h2>
                                            <h6>Medicine Floors</h6>
                                            <p>Last Updated: Jan 6, 2017 9:08PM</p>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                    <div className="sign-whtbox">
                                        <a href="/signout_detail">
                                            <h2>Green List</h2>
                                            <h6>Medicine Floors</h6>
                                            <p>Last Updated: Jan 6, 2017 9:08PM</p>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                    <div className="sign-whtbox">
                                        <a href="/signout_detail">
                                            <h2>Blackwell List</h2>
                                            <h6>Medicine Floors</h6>
                                            <p>Last Updated: Jan 6, 2017 9:08PM</p>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                    <div className="sign-whtbox">
                                        <a href="/signout_detail">
                                            <h2>ICU List</h2>
                                            <h6>Medicine Floors</h6>
                                            <p>Last Updated: Jan 6, 2017 9:08PM</p>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                    <div className="sign-whtbox">
                                        <a href="/signout_detail">
                                            <h2>CCU List</h2>
                                            <h6>Medicine Floors</h6>
                                            <p>Last Updated: Jan 6, 2017 9:08PM</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}
