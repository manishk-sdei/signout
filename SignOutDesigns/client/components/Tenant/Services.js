import React, { Component } from 'react';

export default class Services extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

        document.title = "Signout - Services";
    }

    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar">
                    <h1 className="page-title">Services</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><a href="signoutlist.html">Sign out</a></li>
                        <li className="active">Services</li>
                    </ul>
                </div>
                <div className="container-fluid mrg-top30 pd-bt30">
                    <div className="col pd-off-xs">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="white-bg pdoff-lt-rt">
                                    <h2 className="col content-title blue-text">My Services</h2>
                                    <div className="col-sm-12">
                                        <div className="row">
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>Medicine</h3>
                                                    <p>Blackwell, Stead , Osler, Weiss</p>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>ICU</h3>
                                                    <p>ICU</p>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>CCU</h3>
                                                    <p>CCU</p>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>Surgery</h3>
                                                    <p>Blue, Green</p>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>Pediatrics </h3>
                                                    <p>Peds</p>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>PICU</h3>
                                                    <p>PICU</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 className="col content-title blue-text mar-top35">Self Subscribe</h2>
                                    <div className="col-sm-12">
                                        <div className="row">
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>Medicine</h3>
                                                    <p>Blackwell, Stead , Osler, Weiss</p>
                                                    <button className="ripple btn grey-btn">UnSubscribe</button>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>ICU</h3>
                                                    <p>ICU</p>
                                                    <button className="ripple btn grey-btn active">Subscribe</button>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>CCU</h3>
                                                    <p>CCU</p>
                                                    <button className="ripple btn grey-btn">UnSubscribe</button>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>Surgery</h3>
                                                    <p>Blue, Green</p>
                                                    <button className="ripple btn grey-btn">UnSubscribe</button>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>Pediatrics </h3>
                                                    <p>Peds</p>
                                                    <button className="ripple btn grey-btn">UnSubscribe</button>
                                                </div>
                                            </div>
                                            <div className="col-sm-6 col-md-6 col-lg-4">
                                                <div className="service-box">
                                                    <h3>PICU</h3>
                                                    <p>PICU</p>
                                                    <button className="ripple btn grey-btn">UnSubscribe</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        );
    }
}
