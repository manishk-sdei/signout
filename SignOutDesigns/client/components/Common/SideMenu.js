/**
 * @class         :	SideMenu 
 * @description   : Dashbord SideMenu
 * @Created by    : smartData
 */

import React, { Component,PropTypes } from 'react';

export default class SideMenu extends Component {
            constructor(props) {
            super(props)
            this.state = {
                activeIndex: 0
            }
        }

        static contextTypes={
            
            router:PropTypes.object
        }

        handleClick(index) {

            this.setState({activeIndex: index});

            if(index===0){
                this.context.router.push('/signoutlist');
            }else if(index===1){
                this.context.router.push('/patients');
            }else if(index===2){
                this.context.router.push('/services');
            }else if(index===3){
                this.context.router.push('/directories');
            }
        }

        render() {

            return (
                 <div id="sidebar-wrapper">
                    <h2 className="hidden-sm-up menu-title"><span>Menu</span><i className="zmdi zmdi-caret-down"></i></h2>
                    <ul className="sidebar-nav">
                        <MySideMenuIcons name="Signout Lists"  icon="zmdi zmdi-power" index={0} isActive={this.state.activeIndex===0} onClick={this.handleClick.bind(this)}/>
                        <MySideMenuIcons name="Patients" icon="zmdi zmdi-hotel" index={1} isActive={this.state.activeIndex===1} onClick={this.handleClick.bind(this)}/>
                        <MySideMenuIcons name="Services" icon="zmdi zmdi-desktop-mac" index={2} isActive={this.state.activeIndex===2} onClick={this.handleClick.bind(this)}/>
                        <MySideMenuIcons name="Directory" icon="fa fa-folder-open" index={3} isActive={this.state.activeIndex===3} onClick={this.handleClick.bind(this)}/>      
                    </ul>
                  </div>
            );
        }
}

class MySideMenuIcons extends React.Component {

    handleClick() {
        
        this.props.onClick(this.props.index)
    }

    render () {
        return <li className={this.props.isActive ? 'active' : ''} onClick={this.handleClick.bind(this)}>
                <a>
                    <i className={this.props.icon} title="Signout Lists"></i><span className="nav-label">{this.props.name}</span>
                    {/*<!--<span className="badge bg-success">New</span>-->*/}
                </a>
            </li>
    }
}
