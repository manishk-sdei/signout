/**
 * @class         :	Header 
 * @description   : Dashboard Header
 * @Created by    : smartData
 */

import React, { Component ,PropTypes} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import CheckinModal from '../Modals/CheckinModal';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem,Button  } from 'reactstrap';
import { logoutUser } from '../../actions/index';
import { URL_SLUG_LOGIN } from '../../actions/Constants/ActionIndexConstants';
import Ink from 'react-ink';

class Header extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.open_modal = this.open_modal.bind(this);
        this.state = {
        dropdownOpen: false,
        toggleProp: 'toggled',
        wrapperToggle: '',
        modal1: false
        };
    }

    componentDidMount() {

        document.title = "Signout";
    }

    static contextTypes = {
        router: PropTypes.object
    }

    onClickHandler(e) {
        
       this.props.wrapperToggle('toggled');
    }



    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
 
    open_modal(data) {
        this.setState({
            modal1: !this.state.modal1
        });
    }

    logoutClick() {

        let response = this.props.logoutUser();
        this.context.router.push(URL_SLUG_LOGIN);
    }

    render() {
    return (
            <div>
                <header>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-8 col-sm-6 col-md-7 header-top col-xs-8">
                                <span onClick={()=>this.onClickHandler()} className="ham ripple" id="menu-toggle"><i className="zmdi zmdi-menu"><Ink /></i></span>
                                <Link to="/dashboard" className="logo"><img src="/client/assets/images/logo.png" alt=""/></Link>
                            </div>
                            <div className="col-4 col-sm-6 col-md-5 check-header col-xs-4">
                                <div className="dropdown btn-group user-btn user_btn1">
                                         <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                                            <DropdownToggle caret><i className="zmdi zmdi-account"></i>
                                             <span className="hidden-xs-down"> Johan Scarlett </span>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                              <DropdownItem> <i className="zmdi zmdi-account-box"></i>Account </DropdownItem>
                                              <DropdownItem onClick={this.logoutClick.bind(this)}> <i className="zmdi zmdi-power-setting"></i> <Link to="/login?id=smartdata" className="forgot-password">Logout</Link></DropdownItem>
                                            </DropdownMenu> 
                                          </ButtonDropdown>
                                </div>
                                <div className="checkin-btn"> 
                                    <button type="button" onClick={this.open_modal}><Ink /><i className="fa fa-check-square-o" aria-hidden="true"></i> <span className="hidden-xs-down">Check-in</span>

                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <CheckinModal modal_var={this.state.modal1} handle_modal = {this.open_modal} />
            </div>
        );
    }
}

export default connect(null , { logoutUser })(Header);