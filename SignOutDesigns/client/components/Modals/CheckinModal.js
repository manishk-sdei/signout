import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import Table from './table';
import { getcheckinInfo, getSignoutList } from '../../actions/index';

class CheckinModal extends Component {

    constructor(props){
    super(props);
        this.manage_modal = this.manage_modal.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    render() {
        return (
        <Modal isOpen={ this.props.modal_var } toggle={this.manage_modal} className="modal_data modal_data1 check-pop modal-lg">
            <ModalHeader toggle={this.manage_modal}>
                <span className="modal-title" id="exampleModalLabel">Check In<span> Verify the Group that you are assigned to today</span></span>
            </ModalHeader>
            <ModalBody>
                <div className="medi-floor">
                    <h6>Medicine Floors (Internal Medicine)</h6>
                    <ul>
                        <li>
                            <button>Group 1</button>
                        </li>
                        <li className="active">
                            <button>Group 2</button>
                        </li>
                        <li>
                            <button>Blackwell</button>
                        </li>
                        <li>
                            <button>Group Blue</button>
                        </li>
                        <li>
                            <button>Group Green</button>
                        </li>
                        <li>
                            <button>Night Float</button>
                        </li>
                    </ul>
                </div>
                <div className="medi-floor mar-top35">
                    <h6>On Call Groups:</h6>
                    <ul>
                        <li>
                            <button>OnCall 2345</button>
                        </li>
                        <li>
                            <button>OnCall 6789</button>
                        </li>
                        <li>
                            <button>OnCall 8888</button>
                        </li>
                    </ul>
                </div>
                <div className="medi-floor mar-top35">
                    <h6>ICU (Internal Medicine)</h6>
                    <ul>
                        <li>
                            <button>Group 1</button>
                        </li>
                        <li className="active">
                            <button>Group 2</button>
                        </li>
                        <li>
                            <button>Blackwell</button>
                        </li>
                        <li>
                            <button>Group Blue</button>
                        </li>
                        <li>
                            <button>Group Green</button>
                        </li>
                        <li>
                            <button>Night Float</button>
                        </li>
                    </ul>
                </div>
                <div className="medi-floor mar-top35">
                    <h6>2nd Floors (Internal Medicine)</h6>
                    <ul>
                        <li>
                            <button>OnCall 2345</button>
                        </li>
                    </ul>
                </div>
            </ModalBody>
            <ModalFooter>
                <p>Selected Groups: "Group Blue" <span>this will allow you access to the following list: "Blue List"</span></p>
                <button type="button" data-ripple="" className="btn blue-btn">Accept</button>
            </ModalFooter>
        </Modal>
        );
    }

}

export default connect(null, { getcheckinInfo, getSignoutList })(CheckinModal);
