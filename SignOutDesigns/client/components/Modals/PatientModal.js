import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class PatientModal extends Component {

    constructor(props){
    super(props);
        this.manage_modal = this.manage_modal.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    render() {
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
              <ModalHeader toggle={this.manage_modal}>
                 <h5 className="modal-title">Add/Edit Patient</h5>
              </ModalHeader>
              <ModalBody>
                 
                    
                          <form className="form-inline" action="" method="post">
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>First Name</label>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>Last Name</label>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>D.O.B</label>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group custom-box">
                                      <select className="selectpicker">
                                          <option>Sex</option>
                                          <option>Female</option>
                                          <option>Male</option>
                                      </select>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>Mrn Id</label>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>Email</label>
                                  </div>
                              </div>
                              <div className="col-sm-12 popup-title">Contact Info</div>
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>Contact No.</label>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group custom-box">
                                      <select className="selectpicker">
                                          <option>State</option>
                                          <option>Alabama</option>
                                          <option>Arizona</option>
                                          <option>California</option>
                                          <option>Florida</option>
                                      </select>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group custom-box">
                                      <select className="selectpicker">
                                          <option>City/Town</option>
                                          <option>Georgia</option>
                                          <option>Illinois</option>
                                          <option>Indiana</option>
                                          <option>Missouri</option>
                                      </select>
                                  </div>
                              </div>
                              <div className="col-sm-6">
                                  <div className="form-group">
                                      <input className="inputMaterial" type="text" required />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>Zip Code</label>
                                  </div>
                              </div>
                              <div className="col-sm-12">
                                  <div className="form-group">
                                      <textarea className="inputMaterial" type="text" required></textarea><span className="highlight"></span> <span className="bar"></span>
                                      <label>Address</label>
                                  </div>
                              </div>
                          </form>
                   
              </ModalBody>
              <ModalFooter>
			  
                <Button className="btn blue-btn " onClick={this.manage_modal}>Save</Button> 
                <Button className="btn grey-btn mr-sm-4" onClick={this.manage_modal}>Cancel</Button>               
              </ModalFooter>
          </Modal>
        );
    }
}
