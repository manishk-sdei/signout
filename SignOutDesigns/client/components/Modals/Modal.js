import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class CheckinModal extends Component {

    constructor(props){
    super(props);
        this.manage_modal = this.manage_modal.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    render() {
        return (
        <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal_data1">
            <ModalHeader toggle={this.manage_modal}>
               header
            </ModalHeader>
            <ModalBody>
                body
            </ModalBody>
            <ModalFooter>
                footer
            </ModalFooter>
        </Modal>
        );
    }
}
