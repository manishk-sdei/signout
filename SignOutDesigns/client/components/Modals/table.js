import React, { Component } from 'react';
    import axios from 'axios';

export default class TableRow extends React.Component {
	constructor(props){
		super(props);
		
		this.getdata = this.getdata.bind(this);
	}

	getdata(e,data,parentid){
		
		this.props.handleData(e,data,parentid);
	}

   render() {

      return (
      	    <section className="medi-floor mar-bot35">
                <h6>{this.props.data.name}(Internal Medicine)</h6>
                <ul>
                {this.props.data.groups.map((s, i) => <TableRow1 key = {i} data1 = {s} parentid = {this.props.data.id} getdata = {this.getdata} />)}
                </ul>
            </section>
      );
   }
}

class TableRow1 extends React.Component {
	constructor(props){
		super(props);
		
		this.returnData = this.returnData.bind(this);
	}
	returnData(event,data,parentid){
		
		event.preventDefault();
		this.props.getdata(event,data,parentid);
	}


   render() {  

      return (
        <li><button style={{'backgroundColor': this.props.data1.is_selected ? '#169589' : '#e3e3e3' , 'color': this.props.data1.is_selected ? '#fff' : '#686868' }} onClick={(event) => this.returnData(event,this.props.data1,this.props.parentid)} >{this.props.data1.name}</button></li>
      );
   }
}