import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class PatientModal extends Component {

    constructor(props){
    super(props);
        this.manage_modal = this.manage_modal.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    render() {
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data wd600 check-pop add-pt">
              <ModalHeader toggle={this.manage_modal}>
                 <h5 className="modal-title">Add Patient</h5>
              </ModalHeader>
              <ModalBody>
             
                    <div className="row">
                        <div className="search-bar">
                            <input className="inputMaterial" type="text" required />
                            <span className="highlight"></span> <span className="bar"></span>
                            <label>Search Patient Here...</label>
                            <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                        </div>
                        <div className="crt-patient">
                            <button className="link-popup" data-toggle="modal" data-target="#create-patient">Create Patient</button>
                        </div>
                        <div className="col-sm-12">
                            <div className="table-responsive">
                                <table className="table table-hover add-pt-table table-bordered">
                                    {/*
                                    <!--table start-->*/}
                                    <thead>
                                        <tr>
                                            <th width="7%">&nbsp;</th>
                                            <th>Name</th>
                                            <th>Mrn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" />
                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                </div>
                                            </td>
                                            <td>Patient Name</td>
                                            <td>789</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" defaultChecked />
                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                </div>
                                            </td>
                                            <td>Patient Name</td>
                                            <td>324</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" />
                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                </div>
                                            </td>
                                            <td>Patient Name</td>
                                            <td>123</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" />
                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                </div>
                                            </td>
                                            <td>Patient Name</td>
                                            <td>907</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" />
                                                        <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                </div>
                                            </td>
                                            <td>Patient Name</td>
                                            <td>453</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
               
              </ModalBody>
              <ModalFooter>
			   <Button className="btn blue-btn" onClick={this.manage_modal}>Save</Button>
                <Button className="btn grey-btn mr-sm-4" onClick={this.manage_modal}>Cancel</Button>
               
              </ModalFooter>
          </Modal>
        );
    }
}
