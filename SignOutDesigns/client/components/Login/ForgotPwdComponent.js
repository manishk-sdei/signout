/**
 * @class         :	ForgotPassword
 * @description   : 
 * @Created by    : smartData
 */

import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { forgotPassword } from '../../actions/index';
import { Link } from 'react-router';

class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.state = { show_error: 'hide' }
    }
    
    static contextTypes = {
		router:PropTypes.object
	}

    onSubmit(props) {

        let response = this.props.forgotPassword(props);
        this.context.router.push('/');        
    }

    render() {
        const { fields: { email }, handleSubmit } = this.props;
        return(
            <div className="login-bg">
                <div className="login-wrap">
                    <div className="login-box">
                        <h1><img src="/client/assets/images/login-logo.png" className="login-logo" alt=""/></h1>
                        <div className="login-form">
                          <h2>RESET PASSWORD</h2>  
                          
                          <div className="col-sm-12">
                                <div className="tab-pane show active" id="signin" role="tabpanel">
                                    
                                    <form action="signoutlist" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                                        <div className ={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                                            <input type="text" required className="form-control inputMaterial" {...email} />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>New Password</label>
                                           <i className="zmdi zmdi-lock-outline"></i>
                                         <div className="error_msg_danger">
                                                {email.touched ? email.error : '' }
                                        </div>
                                        </div>
										
										<div className ={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                                            <input type="text" required className="form-control inputMaterial" {...email} />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>Confirm Password</label>
                                           <i className="zmdi zmdi-lock-outline"></i>
                                         <div className="error_msg_danger">
                                                {email.touched ? email.error : '' }
                                        </div>
                                        </div>
                                         
                                        <button className="btn blue-btn sign-btn">Save Password</button>
                                        <div className="frgt-password">
                                            <Link to="/login?id=smartdata" className="forgot-password">click here to Login!</Link>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function validate(values) {
    const error = {};

    if(!values.email) {
        error.email = 'Please enter email address';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(!values.email) {
            error.email = 'Please enter email address';
        }else {
            error.email = 'Please enter a valid email address';
        }        
    }
    return error;
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
// reduxForm: first is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps

export default reduxForm({
    form: 'ForgotPasswordForm',
    fields: ['email'],
    validate
}, null, { forgotPassword })(ForgotPassword);