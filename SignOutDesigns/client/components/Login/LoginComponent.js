/**
 * @class         :	Login
 * @description   :
 * @Created by    : smartData
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import LoginSlugCheck from './LoginSlugComponent';
import LoginForm from './LoginFormComponent';
import { checkGlobalSlug } from '../../actions/index';

class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.state = {slugCheck: false, loading: true};
    }

    static contextTypes={
  		router:PropTypes.object
  	}

    componentDidMount() { // Run only once on page load

        document.title = "Signout - Login";
        let accessToken = localStorage.getItem( 'access_token' );
        if( accessToken ) {
            this.context.router.push( '/dashboard' );
        }

         //https://myapp.domain.com/login?id=COMPANYID
         //http://localhost:3000/login?id=smartdata
        //  let tenantSlug = ( this.props.location.query.id == undefined ) ? '' : this.props.location.query.id;
         //
        //  this.props.checkGlobalSlug( tenantSlug )
        //                             .then((response) => {
         //
        //                                 const tId = response.payload.data.t_id;
        //                                 if(tId!=undefined) {
        //                                     localStorage.setItem( 'tid', tId );
        //                                     localStorage.setItem( 'tenant_slug', tenantSlug );
        //                                     this.setState( { slugCheck : true } );
        //                                 }
        //                                 this.setState( { loading : false } );
         //
        //                             }).catch(function ( error ) {
         //
        //                                     this.state( err )
        //                                     this.setState( { loading : false } );
        //                             });
    }

    render() {

        // Render slug if login slug not found
        // if(this.state.loading===true) {
        //    return <div className="spinner"></div>;
        // }
        //
        // if(this.state.slugCheck===false) {
        //    return <LoginSlugCheck />;
        // }

        // Render login if login slug found
        return(
            <LoginForm />
        );
    }
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
export default connect(null, { checkGlobalSlug })(LoginPage);
