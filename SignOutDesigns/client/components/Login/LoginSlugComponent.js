/**
 * @class         :	Login
 * @description   : if Login slug not found for the tenent then 
 *                  this page will be visible otherwise Login.
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { Card, CardBlock, CardTitle, CardText, CardImg } from 'reactstrap';

export default class LoginSlugCheck extends Component {

    render() {
        return(            
            <div className="login-bg">
            <div className="login-wrap">
            <div className="login-box">
                <h1><img src="/client/assets/images/login-logo.png" className="login-logo" alt=""/></h1>
                <div className="login-slug">
                <Card block inverse color="danger" className="login-slug-card">
                    <CardImg top width="100%" src="https://blog.hubspot.com/hs-fs/hubfs/gog-404-page.png?t=1486468536627&width=671&height=409&name=gog-404-page.png" alt="Card image cap" />
                    <CardBlock>
                        <CardTitle>Page Not Found</CardTitle>
                        <CardText>The page you were looking for could not be found. If you followed a link on this site to get here, please contact the administrator so it can be corrected.</CardText>        
                    </CardBlock>
                </Card>
                </div>
            </div>
            </div>
            </div>
        );
    }
}
