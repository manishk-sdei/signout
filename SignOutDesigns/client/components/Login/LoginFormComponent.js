/**
 * @class         :	Login
 * @description   :
 * @Created by    : smartData
 */

import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { loginRequest } from '../../actions/index';
import { Link } from 'react-router';
import { Alert } from 'reactstrap';

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = { show_error: 'hide' }
        this.state = { email: '', password: ''};
    }

    static contextTypes={
		router:PropTypes.object
	}

    onSubmit(props) {

        let This = this;
        this.props.loginRequest( props ).then(( response ) => {

            // Login Success - Navigate the user to Dashboard
            // We Navigate by calling this.context.router.push with the new path to navigate to
console.log("api response  : ", response);
            if( response.payload.data.access_token != undefined ) {
                localStorage.setItem( 'access_token', response.payload.data.access_token );
                this.context.router.push('/dashboard' );
            }
        }).catch(( error ) => {

                console.log( "error", error );
                This.setState( { show_error : 'show' } );
        });
    }

    render() {

        const { fields: { email, password }, handleSubmit } = this.props;
        return(
            <div className="login-bg">
                <div className="login-wrap">
                    <div className="login-box">
                        <h1><img src="/client/assets/images/login-logo.png" className="login-logo" alt=""/></h1>
                        <div className="login-form">
                          <h2>SIGN IN</h2>
                            {
                            this.state.show_error =='show'?
                            <Alert color="danger login_show_error"><strong>Oh snap!</strong> Invalid email and password.</Alert>:""
                            }
                          <div className="col-sm-12">
                                <div className="tab-pane show active" id="signin" role="tabpanel">

                                    <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                                        <div className ={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                                            <input required type="text" className="form-control inputMaterial" {...email} value={this.state.email} onChange={(e)=>this.setState({email:e.target.value})} />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>Email</label>
                                            <i className="zmdi zmdi-account-o"></i>
											 <div className="error_msg_danger">
                                            {email.touched ? email.error : '' }
                                        </div>
                                        </div>
                                       

                                        <div className={`form-group ${password.touched && password.invalid ? 'has-danger' : ''}`}>
                                            <input required type="password" className="form-control inputMaterial" {...password} value={this.state.password} onChange={(e)=>this.setState({password:e.target.value})} />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>Password</label>
                                            <i className="zmdi zmdi-lock-outline"></i>
											<div className="error_msg_danger">
                                            {password.touched ? password.error : '' }
                                        </div>
                                        </div>
                                        
                                        <button className="btn blue-btn sign-btn" data-ripple="">SIGN IN</button>
                                        <div className="frgt-password">
                                            <Link to="/forgot_password" className="forgot-password">Forgot Password?</Link>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.email) {
        error.email = 'Please enter email address';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(!values.email) {
            error.email = 'Please enter email address';
        }else {
            error.email = 'Please enter a valid email address';
        }
    }

    if(!values.password) {
        error.password = 'Please enter a password';
    }
    return error;
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
// reduxForm: first is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps

export default reduxForm({
    form: 'LoginForm',
    fields: ['email', 'password'],
    validate
}, null, { loginRequest })(LoginForm);
