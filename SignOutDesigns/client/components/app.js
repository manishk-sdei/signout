/**
 * @class         :	Application Portal
 * @description   : This class handles application header footer and content section
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import {Link} from 'react-router';
import Header from './Common/Header';
import SideMenu from './Common/SideMenu';


export default class App extends Component {
  constructor(props) {
        super(props);
        this.state = {toggleState: ''};
    }

    toggleState() {

      if(this.state.toggleState=='') {
        this.setState({toggleState: 'toggled'});
      } else {
        this.setState({toggleState: ''});
      }

    }
  render() {

    return (
      <div>
          <Header wrapperToggle={()=>this.toggleState()} />
          <div id="wrapper" className={this.state.toggleState}>
            <SideMenu />
            {this.props.children}
          </div>
      </div>
    );
  }
}
