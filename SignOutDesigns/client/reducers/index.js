/**
 * @reducer       : index reducer
 * @description   : 
 * @Created by    : smartData
 */

import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer/LoginReducer';
import LogoutReducer from './LoginReducer/LogoutReducer';
import GlobalSlugReducer from './LoginReducer/GlobalSlugReducer';

import { reducer as formReducer } from 'redux-form';   //SAYING use redux form reducer as reducer

const rootReducer = combineReducers({

  form: formReducer,
  login: LoginReducer,
  slug: GlobalSlugReducer,
  logout: LogoutReducer
});

export default rootReducer;
