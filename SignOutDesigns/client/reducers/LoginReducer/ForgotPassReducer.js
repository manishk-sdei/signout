/**
 * @reducer       : forgot password reducer
 * @description   : 
 * @Created by    : smartData
 */

import { FORGOTPASSWORD_REQUEST } from '../../actions/index';

export default function(state=[],action) {
    
    switch (action.type){
        case FORGOTPASSWORD_REQUEST:        
            return [action.payload, ...state];
        default:
            return state;
    }
}
