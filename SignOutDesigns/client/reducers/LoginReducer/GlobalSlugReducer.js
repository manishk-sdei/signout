/**
 * @reducer       : forgot password reducer
 * @description   : 
 * @Created by    : smartData
 */

import { GLOBAL_SLUG_CHECK } from '../../actions/index';

export default function(state=[],action) {
    
    switch (action.type){
        case GLOBAL_SLUG_CHECK:        
            return [action.payload, ...state];
        default:
            return state;
    }
}
