/**
 * @reducer       : login reducer
 * @description   :
 * @Created by    : smartData
 */

import { LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE  } from '../../actions/index';
const INITIAL_STATE = {
    token: null,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null
};

export default function(state = INITIAL_STATE, action) {

    switch (action.type){
        case LOGIN_USER_REQUEST:
            return [action.payload, ...state, isAuthenticating: true, statusText: null];
        case LOGIN_USER_SUCCESS:
            return [action.payload, ...state, isAuthenticating: false, isAuthenticated: true, token: action.payload.token, statusText: 'You have been successfully logged in.'];
        case LOGIN_USER_FAILURE:
            return [action.payload, ...state, isAuthenticating: false, isAuthenticated: false, token: null, statusText: 'You have been successfully logged in.'];
        default:
            return state;
    }
    return state
}
