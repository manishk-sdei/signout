/**
 * @Router        :	Index Router
 * @description   : Application url routing.
 * @Created by    : smartData
 */

import React from 'react';
import { Router, Route, Link, browserHistory,IndexRoute } from 'react-router'

// Import components to route
import App from '../components/app';
import LoginComponent from '../components/Login/LoginComponent';
import ForgotPwdComponent from '../components/Login/ForgotPwdComponent';
import TenantDashboard from '../components/Tenant/TenantDashboard';
import SignOutList from '../components/Tenant/SignOutList';
import SignoutDetail from '../components/Tenant/SignoutDetail';
import Patients from '../components/Tenant/Patients';
import Services from '../components/Tenant/Services';
import Directories from '../components/Tenant/Directories';

// Import AuthsComponent - To check login authentication
import { requireAuths } from '../components/AuthsComponent';

export default(
<Router history={browserHistory}>
    <Route path="/" component={LoginComponent}></Route>
    <Route path="/forgot_password" component={ForgotPwdComponent}></Route>
     <Route  component={App}>
           <Route path="/dashboard" component={ TenantDashboard } />
           <Route path="/signoutlist" component={ SignOutList } />
           <Route path="/signout_detail" component={ SignoutDetail } />
           <Route path="/patients" component={ Patients } />
           <Route path="/services" component={ Services } />
           <Route path="/directories" component={ Directories } />
    </Route>
    <Route path="*" component={ LoginComponent } />
 </Router>
);
