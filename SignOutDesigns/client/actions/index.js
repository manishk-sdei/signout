/**
 * @action        : index.js
 * @description   : Handles all actions and get data from apis
 * @Created by    : smartData
 */

import {  GLOBAL_SLUG_API,
          LOGIN_API,
          AUTH_CONST } from './Constants/ActionIndexConstants';
import React from 'react';
import axios from 'axios';

export function checkGlobalSlug( props ) {

	let postBody = { 'tenant_slug' : props };
	const request = axios.post( GLOBAL_SLUG_API, postBody );

	return {
		type: AUTH_CONST.GLOBAL_SLUG_CHECK,
		payload: request
	};
}

/**
 * Login Process  - Start
 * @todo refresh_token has yet to be implemented
 * @todo postdata static content used make dynamic
 * @param props object email and password
 */
 
export function loginRequest( props ) {

  	const tId = localStorage.getItem('tid');
  	let postData = {'Authorization': 'Basic UVNCbDlQaUlMY3I2U3JsYWRUdDZOVktpdWtKbEp5R2NSNl9wTEg3NjpDaWVvTmJNZUlOS1VTVEtOdVZOSFNLb3YzYUZsaEpFVXBqY2JhbXRM',
              		'grant_type': 'password',
              		'username': 'billy.walters@test.com',
              		'password': 'B@conator',
              		'tenant_slug': 'demo1',
              		'scope': 'epihealth-web'};

    const instance = axios.create();
    instance.defaults.headers.post['Content-Type'] = 'application/json';
    instance.defaults.headers.common['Accept'] = 'application/json';
    let requestAPI = instance.post( LOGIN_API, postData );
  	// let requestAPI = axios.post( LOGIN_API, postData ).then((response) => {
    //         console.log("api-hit-response : ", response);
    // }).catch(function ( error ) {
    //         this.state( err )
    // });

    console.log("api hit : ", requestAPI);

    return {
  		type: AUTH_CONST.LOGIN_USER_REQUEST,
  		payload:requestAPI
  	};
}

export function getAuthToken() {

	let accessToken = localStorage.getItem('access_token');
	return {
		type: AUTH_CONST.GET_AUTHTOKEN,
		payload:request
	};
}

export function logoutUser() {

	localStorage.removeItem('access_token');
  // localStorage.removeItem('tid');
  // localStorage.removeItem('tenant_slug');

	return {
		type: AUTH_CONST.LOGOUT_USER
	};
}

export function forgotPassword() {

	let request = {'email':'example@mail.com'};

	return {
		type: AUTH_CONST.FORGOTPASSWORD_REQUEST,
		payload:request
	};
}
