/**
 * @constants
 * @description   : Hold CONSTANTS and APIs
 * @Created by    : smartData
 */

// Define and Export Constants
export const AUTH_CONST = {
                GLOBAL_SLUG_CHECK : 'GLOBAL_SLUG_CHECK',
                LOGIN_USER_REQUEST : 'LOGIN_USER_REQUEST',
                LOGIN_USER_SUCCESS : 'LOGIN_USER_SUCCESS',
                LOGIN_USER_FAILURE : 'LOGIN_USER_FAILURE',
                LOGOUT_USER : 'LOGOUT_USER',
                GET_AUTHTOKEN : 'GET_AUTHTOKEN',
                FORGOTPASSWORD_REQUEST : 'FORGOTPASSWORD_REQUEST'
};

// API Server URL
export const SERVER_URL="https://api.epihealthstg.com/v1/";
const tSlug = localStorage.getItem( 'tenant_slug' );
export const LoginUrl = '/login?id=';
const URL_SLUG_LOGIN = LoginUrl + tSlug;

// Define APIS
export const GLOBAL_SLUG_API = SERVER_URL + 'global/checkslug';
export const LOGIN_API = SERVER_URL + 'oauth/token';
