/**
 * @reducer       : index reducer
 * @description   :
 * @Created by    : smartData
 */

import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer/LoginReducer';
import TenantsReducer from './TenantsReducer';
import UsersReducer from './UsersReducer';
import SuperAdminsReducer from './SuperAdminsReducer';
import TermsReducer from './TermsReducer';

import { reducer as formReducer } from 'redux-form';   //SAYING use redux form reducer as reducer

const rootReducer = combineReducers({
  form        : formReducer,
  login       : LoginReducer,
  tenants     : TenantsReducer,
  users       : UsersReducer,
  terms       : TermsReducer,
  superAdmins : SuperAdminsReducer
});

export default rootReducer;
