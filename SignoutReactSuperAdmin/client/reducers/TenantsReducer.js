/**
 * @reducer       : UsersReducer
 * @description   : handle currently login user data
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { TENANTS_CONST } from '../actions/Constants';

const initialState = {
    data : [],
    dataCount : null,
    tenantById : [],
    tenantUsers : [],
    isFetching : false,
    isAuthenticated : false,
    isAuthenticating : false,
    statusCode : null,
    statusText : null,

      name: null,
      slug: null,
      is_disabled: false,
      db_host: null,
      db_port: null,
      db_name: null,
      db_user: null,
      db_pass: null

};

export default createReducer(initialState, {

    [TENANTS_CONST.GET_TENANTS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TENANTS_CONST.GET_TENANTS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [TENANTS_CONST.GET_TENANT_USERS_COUNT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'dataCount': payload,
            'isFetching': false
        });
    },
    [TENANTS_CONST.GET_TENANTS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TENANTS_CONST.GET_TENANT_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TENANTS_CONST.GET_TENANT_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'tenantById': payload,
            'isFetching': false
        });
    },
    [TENANTS_CONST.GET_TENANT_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TENANTS_CONST.GET_TENANT_USERS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TENANTS_CONST.GET_TENANT_USERS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'tenantUsers': payload,
            'isFetching': false
        });
    },
    [TENANTS_CONST.GET_TENANT_USERS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TENANTS_CONST.PUT_TENANT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [TENANTS_CONST.PUT_TENANT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });
    },
    [TENANTS_CONST.PUT_TENANT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusCode': payload.status,
            'statusText': payload.statusText
        });
    },
    [TENANTS_CONST.UPDATE_TENANT_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'name': payload!==undefined ? payload.name : null,
            'slug': payload!==undefined ? payload.slug : null,
            'is_disabled': payload!==undefined ? payload.is_disabled : null,
            'db_host': payload!==undefined ? payload.db_host : null,
            'db_port': payload!==undefined ? payload.db_port : null,
            'db_name': payload!==undefined ? payload.db_name : null,
            'db_user': payload!==undefined ? payload.db_user : null,
            'db_pass': payload!==undefined ? payload.db_pass : null
        });
    },
    [TENANTS_CONST.DELETE_TENANT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [TENANTS_CONST.DELETE_TENANT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true
        });
    },
    [TENANTS_CONST.DELETE_TENANT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false
        });
    }
});
