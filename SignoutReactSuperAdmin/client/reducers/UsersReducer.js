/**
 * @reducer       : UsersReducer
 * @description   : handle currently login user data
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { GET_USER_CONST } from '../actions/Constants';

const initialState = {
    data : null,
    userById : [],
    usersList : [],
    isFetching : true,
    isAuthenticated : false,
    isAuthenticating : false,
    statusCode : null,
    statusText : null,
    details : []
};

export default createReducer(initialState, {

    [GET_USER_CONST.GET_USER_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [GET_USER_CONST.GET_USER_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [GET_USER_CONST.GET_USER_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [GET_USER_CONST.GET_USER_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [GET_USER_CONST.GET_USER_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'userById': payload,
            'isFetching': false
        });
    },
    [GET_USER_CONST.GET_USER_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [GET_USER_CONST.USERS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [GET_USER_CONST.USERS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'usersList': payload,
            'isFetching': false
        });
    },
    [GET_USER_CONST.USERS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [GET_USER_CONST.PUT_USER_PASSWORD_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [GET_USER_CONST.PUT_USER_PASSWORD_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });
    },
    [GET_USER_CONST.PUT_USER_PASSWORD_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusCode': payload.status,
            'statusText': payload.statusText,
            'details' : payload.details
        });
    }
});
