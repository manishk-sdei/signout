/**
 * @reducer       : SuperAdminsReducer
 * @description   : handle currently login user data
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { GET_SUPER_ADMIN_CONST } from '../actions/Constants';

const initialState = {
    data : [],
    totalSuperAdmins : null,
    superAdminsById : [],
    isFetching : true,
    isAuthenticated : false,
    isAuthenticating : false,
    statusCode : null,
    statusText : null,

    initialValues : {
      first_name: null,
      last_name: null,
      email: null,
      status: 'inactive',
      timezone: null
    }
};

export default createReducer(initialState, {

    [GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [GET_SUPER_ADMIN_CONST.GET_TOTAL_SUPER_ADMINS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'totalSuperAdmins': payload,
            'isFetching': false
        });
    },
    [GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'userById': payload,
            'isFetching': false
        });
    },
    [GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [GET_SUPER_ADMIN_CONST.POST_SUPER_ADMIN_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [GET_SUPER_ADMIN_CONST.POST_SUPER_ADMIN_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });
    },
    [GET_SUPER_ADMIN_CONST.POST_SUPER_ADMIN_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false
        });
    },
    [GET_SUPER_ADMIN_CONST.UPDATE_SUPER_ADMIN_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'first_name': payload!==undefined ? payload.first_name : null,
            'last_name': payload!==undefined ? payload.last_name : null,
            'email': payload!==undefined ? payload.email : null,
            'status': payload!==undefined ? payload.status : null,
            'timezone': payload!==undefined ? payload.timezone : null
        });
    }
});
