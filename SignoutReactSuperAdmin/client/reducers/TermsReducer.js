/**
 * @reducer       : TermsReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { TERM_CONST } from '../actions/Constants';

const initialState = {
    countries : null,
    languages : null,
    meritalstatus : null,
    race : null,
    sex : null,
    timezones : null,
    isFetching : true,
    statusText : null
};

export default createReducer(initialState, {
    [TERM_CONST.GET_COUNTRIES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TERM_CONST.GET_COUNTRIES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'countries': payload,
            'isFetching': false
        });
    },
    [TERM_CONST.GET_COUNTRIES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TERM_CONST.GET_LANGUAGES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TERM_CONST.GET_LANGUAGES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'languages': payload,
            'isFetching': false
        });
    },
    [TERM_CONST.GET_LANGUAGES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TERM_CONST.GET_MERITALSTATUS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TERM_CONST.GET_MERITALSTATUS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'meritalstatus': payload,
            'isFetching': false
        });
    },
    [TERM_CONST.GET_MERITALSTATUS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TERM_CONST.GET_RACE_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TERM_CONST.GET_RACE_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'race': payload,
            'isFetching': false
        });
    },
    [TERM_CONST.GET_RACE_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TERM_CONST.GET_SEX_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TERM_CONST.GET_SEX_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'sex': payload,
            'isFetching': false
        });
    },
    [TERM_CONST.GET_SEX_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [TERM_CONST.GET_TIMEZONES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [TERM_CONST.GET_TIMEZONES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'timezones': payload,
            'isFetching': false
        });
    },
    [TERM_CONST.GET_TIMEZONES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    }
});
