/**
 * @Router        :	Index Router
 * @description   : Application url routing.
 * @Created by    : smartData
 */

import React from 'react';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'

// Import components to route
import App from '../components/app';
import LoginComponent from '../components/Login/LoginComponent';
import ForgotPwdComponent from '../components/Login/ForgotPwdComponent';
import ResetPwdComponent from '../components/Login/ResetPassword';

import TenantDashboard from '../components/Tenants/TenantDashboard';
import Tenants from '../components/Tenants';
import Users from '../components/Tenants/Users';

import Pagination from '../components/Common/Pagination';
import ComingSoon from '../components/Common/ComingSoon';
import Account from '../components/SuperAdmins/Account';
import SuperAdmins from  '../components/SuperAdmins';

// Import AuthsComponent - To check login authentication
import { requireAuths } from '../components/AuthsComponent';

export default(
<Router history={browserHistory}>
    <Route path="/" component={LoginComponent}></Route>
    <Route path="/forgot-password" component={ForgotPwdComponent}></Route>
    <Route path="/reset-password" component={ResetPwdComponent}></Route>
     <Route  component={App}>

           <Route path="/comingsoon" component={ ComingSoon } />
           <Route path="/pagination" component={ requireAuths(Pagination) } />

           <Route path="/dashboard" component={ requireAuths(TenantDashboard) } />
           <Route path="/tenants" component={ requireAuths(Tenants) } />
           <Route path="/users" component={ requireAuths(Users) } >
             <Route path="/users" component={requireAuths(Users)} />
             <Route path=":tenantId" component={requireAuths(Users)} />
           </Route>
           <Route path="/account" component={ requireAuths(Account) } />
           <Route path="/super-admins" component={ requireAuths(SuperAdmins) } />

    </Route>
  <Route path="*" component={LoginComponent}/>
 </Router>
);
