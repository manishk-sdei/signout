import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import moment from 'moment';
import ModalWindow from '../Modals/AddEditSuperAdminModal';

import ButtonM from 'muicss/lib/react/button';
import { getLoginUser } from '../../actions/UsersActions';

import { getSuperAdmins, countSuperAdmins, setSuperAdminFormValues } from '../../actions/SuperAdminActions';
import { getTimeZones } from '../../actions/TermsActions';

import Pagination from '../Common/Pagination';
import { PAGINATION_DEFAULT_LIMIT } from '../../actions/Constants';

class SuperAdmins extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: null,
            modal1: false,
            superAdmins: [],
            editSuperAdmin: [],
            totalSuperAdmins: null
        };

        this.open_modal = this.open_modal.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Tenants";
        this.setState({ token: this.props.token });
    }

    componentWillMount() {
        this.setState({ token: this.props.token });
        this.props.getLoginUser(this.props.token);
        this.props.getTimeZones(this.props.token);
        this.props.countSuperAdmins(this.props.token);
        let limit = PAGINATION_DEFAULT_LIMIT;
        let orderBy = 'id';
        let order = 'desc';
        let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
        this.props.getSuperAdmins(this.props.token, query);
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.superAdmins && this.props.superAdmins !== nextProps.superAdmins ) {
            let superAdmins = nextProps.superAdmins;
            this.setState( { superAdmins : superAdmins } );
        }

        if ( nextProps.totalSuperAdmins ) {
            this.setState( { totalSuperAdmins : nextProps.totalSuperAdmins } );
        }
    }

    // Open modal on add and update superadmins
    open_modal(e, data) {

      if (data === undefined) {
          let limit = PAGINATION_DEFAULT_LIMIT;
          let orderBy = 'id';
          let order = 'desc';
          let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
          this.props.getSuperAdmins(this.props.token, query);
      }

      if( e===true ) {
          e.preventDefault();
      } else {

        if(data) {
          this.props.setSuperAdminFormValues(data); // Update row
        }
        this.setState({
            modal1: !this.state.modal1,
            editSuperAdmin: data,
        });
      }
    }

    renderRow() {

        if(this.props.isFetching) {
          return(
            <tr>
              <td>
                <div className="loader" id="loader-4">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </td>
            </tr>
          );
        }

        if(this.state.superAdmins.length <= 0) {
          return (
            <tr>
              <td colSpan="5">
                  No super admins.
              </td>
            </tr>
          );
        }

        if( this.state.superAdmins ) {
          return this.state.superAdmins.map(( admin ) => {
            return (
              <tr key= { admin.id }>
                <td>{ admin.first_name } { admin.last_name }</td>
                <td>{ admin.email }</td>
                <td>{ admin.status }</td>
                <td>{ admin.timezone }</td>
                <td>{ admin.created_at ? moment(admin.created_at).format('MM/DD/YYYY') : 'n/a' }</td>
                <td>
                  <button
                        onClick={ (e)=>this.open_modal(e, admin) }
                        className="action-btn tenant-usr-icon"
                        data-toggle="modal"
                        data-target="#edit-patient1">
                        <i className="zmdi zmdi-edit"></i>
                  </button>
                </td>
              </tr>
            );
          });
        }
    }

    render() {
        return(
          <div id="page-content-wrapper">
              {/*
              <!-- Page Content -->*/}
              <div className="col welcome-bar ">
                  {/*
                  <!-- welcome bar -->*/}
                  <h1 className="page-title">Super Admins</h1>
                  <ul className="breadcrumb pull-right hidden-xs-down">
                      <li><Link to={`/dashboard`}>Sign out</Link></li>
                      <li className="active">Super-admins</li>
                  </ul>
              </div>

              {/** ModalWindow - with props **/}

              <ModalWindow
                          modal_var = {this.state.modal1}
                          handle_modal = { this.open_modal }
                          token = { this.state.token }
                          editSuperAdmin = { this.state.editSuperAdmin } />

              <div className="container-fluid mrg-top30 pd-bt30">
                  <div className="col pd-off-xs">
                      <div className="row">
                          <div className="col-sm-12">
                              <div className="white-bg">
                                <ButtonM type="button"
                                           onClick={(e)=>this.open_modal(e, null)}
                                           className="add-deprt btn blue-btn"
                                           data-toggle="modal"
                                           data-target="#create-patient">
                                           <span>Create Super Admin</span>
                                  </ButtonM>
                                  <div className="table-responsive">
                                      <table className="table patient-list">
                                          <thead>
                                              <tr>
                                                  <th width="20%">Name</th>
                                                  <th width="30">Email</th>
                                                  <th width="10%">Status</th>
                                                  <th width="15%">Timezone</th>
                                                  <th width="15%">Created</th>
                                                  <th width="10%">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              { this.renderRow() }
                                            </tbody>
                                      </table>
                                  </div>
                                  {(this.state.superAdmins.length > 0) ?
                                  <Pagination
                                            token = { this.state.token }
                                            pageName = "SUPER_ADMINS"
                                            totalPages ={ this.state.totalSuperAdmins } /> : ''}
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

function mapStateToProps(state) {
     return {
       isFetching : state.users.isFetching,
       currentLoginUser: state.users.data,
       isFetching : state.superAdmins.isFetching,
       superAdmins  : state.superAdmins.data,
       totalSuperAdmins : state.superAdmins.totalSuperAdmins,
       timezones : state.terms.getTimeZones
     }
}

export default connect(mapStateToProps, {  setSuperAdminFormValues, getTimeZones, getLoginUser, getSuperAdmins, countSuperAdmins })(SuperAdmins);
