import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router';
import { Badge } from 'reactstrap';

import { getLoginUser,
         updateSuperAdminsPassword,
         createUser,
         updateUserFormInitialValues } from '../../actions/UsersActions';

class Account extends Component {

    constructor(props) {
        super(props);
        this.state = { userId : null,
                       first_name : null,
                       last_name : null,
                       email : null,
                       warning : null,
                       suggestions : []
                     };
    }

    componentDidMount() {
        document.title = "Signout - Account Setting";
    }

    componentWillMount() {
      this.props.getLoginUser(this.props.token);

    }

    componentWillReceiveProps(nextProps) {//createUser( token, tenantId, userId, formData )

      if (nextProps.currentLoginUser && this.props.currentLoginUser !== nextProps.currentLoginUser) {

          this.setState({
                          userId : nextProps.currentLoginUser.id,
                          first_name : nextProps.currentLoginUser.first_name,
                          last_name : nextProps.currentLoginUser.last_name,
                          email : nextProps.currentLoginUser.email });

          this.props.updateUserFormInitialValues(nextProps.currentLoginUser);
      }

    }

    onSubmit(formData) {

      this.props.updateSuperAdminsPassword(this.props.token, this.state.userId, formData);
      let This = this;
      setTimeout(function(){
          This.props.handle_modal('false');
      }, 1000);

      if(this.props.details && this.props.details.warning !== undefined) {
          this.setState({ warning : this.props.details.warning });
      }
      if(this.props.details && this.props.details.suggestions !== undefined) {
          this.setState({ suggestions : this.props.details.suggestions });
      }
    }

    renderPwdWarning() {
            return (<p><Badge color="warning">{ (this.props.details && this.props.details.warning !== undefined) ? this.props.details.warning : '' }</Badge></p>);
    }

    renderPwdSuggestions() {

        return (this.props.details && this.props.details.suggestions !== undefined) ?
          this.props.details.suggestions.map(( suggestionPoint ) => {
                return (
                    <p><Badge color="danger">{suggestionPoint}</Badge></p>
                )
        }) : '';
    }

    render() {
      const { fields: { current_password,
                        new_password,
                        confirm_password }, handleSubmit } = this.props;
        return(
          <div id="page-content-wrapper">
            <div className="col welcome-bar ">
                <h1 className="page-title">Account Setting</h1>
                <ul className="breadcrumb pull-right hidden-xs-down">
                    <li><a href="signoutlist.html">Sign out</a></li>
                    <li className="active">Account Setting</li>
                </ul>
            </div>
            <div className="container-fluid mrg-top30 pd-bt30">
                <div className="col pd-off-xs">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="white-bg">
                                <div className="account-form">
                                    <h3 className="sub-title">Update Your Account</h3>

                                    <form noValidate onSubmit={handleSubmit(this.onSubmit.bind(this))}>

                                        <div className="row">

                                          <div className="col-sm-6">
                                              <div className="form-group">
                                                  <input className="inputMaterial disabled" type="text" disabled />
                                                  <span className="highlight"></span> <span className="bar"></span>
                                                  <label for="disabled">{ this.state.first_name }</label>
                                              </div>
                                          </div>

                                          <div className="col-sm-6">
                                              <div className="form-group">
                                                  <input className="inputMaterial disabled" type="text" disabled />
                                                  <span className="highlight"></span> <span className="bar"></span>
                                                  <label for="disabled">{ this.state.last_name }</label>
                                              </div>
                                          </div>
                                        </div>

                                        <div className="row">
                                          <div className="col-sm-6">
                                              <div className="form-group">
                                                  <input className="inputMaterial disabled" type="text" disabled />
                                                  <span className="highlight"></span> <span className="bar"></span>
                                                  <label for="disabled">{ this.state.email }</label>
                                              </div>
                                          </div>
                                          <div className="col-sm-6">
                                              <div className ={`form-group ${current_password.touched && current_password.invalid ? 'has-danger' : ''}`}>
                                                  <input
                                                        className="inputMaterial"
                                                        type="password"
                                                        required
                                                        {...current_password}
                                                        maxLength="50"
                                                        value={this.state.current_password}
                                                        onChange={()=>this.setState({current_password: event.target.value})} />
                                                  <span className="highlight"></span>
                                                  <span className="bar"></span>
                                                  <label>Current password <i className="reqfld">*</i></label>
                                                  <div className="error_msg_danger">
                                                      {current_password.touched ? current_password.error : '' }
                                                      { this.props.details === null ? this.props.statusText : '' }
                                                  </div>
                                              </div>
                                          </div>
                                        </div>

                                        <div className="row">
                                          <div className="col-sm-6">
                                              <div className ={`form-group ${new_password.touched && new_password.invalid ? 'has-danger' : ''}`}>
                                                  <input
                                                        {...new_password} required
                                                        className="inputMaterial"
                                                        type="password"
                                                        maxLength="50"
                                                        value={this.state.new_password}
                                                        onChange={(event)=>this.setState({new_password: event.target.value})} />
                                                  <span className="highlight"></span>
                                                  <span className="bar"></span>
                                                  <label>New password <i className="reqfld">*</i></label>
                                                  <div className="error_msg_danger">
                                                      {new_password.touched ? new_password.error : '' }
                                                  </div>
                                              </div>
                                          </div>
                                          <div className="col-sm-6">
                                              <div className ={`form-group ${confirm_password.touched && confirm_password.invalid ? 'has-danger' : ''}`}>
                                                  <input
                                                        className="inputMaterial"
                                                        type="password"
                                                        required
                                                        {...confirm_password}
                                                        maxLength="50"
                                                        value={this.state.confirm_password}
                                                        onChange={()=>this.setState({confirm_password: event.target.value})} />
                                                  <span className="highlight"></span>
                                                  <span className="bar"></span>
                                                  <label>Confirm password <i className="reqfld">*</i></label>
                                                  <div className="error_msg_danger">
                                                      {confirm_password.touched ? confirm_password.error : '' }
                                                  </div>
                                              </div>
                                          </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-sm-6">
                                                <div className="form-group mar-offbt">
                                                  { this.renderPwdWarning() }
                                                  { this.renderPwdSuggestions() }
                                                </div>
                                            </div>

                                            <div className="col-sm-6">
                                                <div className="form-group mar-offbt">
                                                  <button
                                                      type="submit"
                                                      className="btn blue-btn sign-btn">
                                                      <span>{ this.props.isAuthenticating === true ? 'Updating...' : 'Update' }</span></button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.current_password) {
        error.current_password = 'Please enter current password ';
    }

    if(!values.new_password) {
        error.new_password = 'Please enter new password ';
    }

    if(!values.confirm_password) {
        error.confirm_password = 'Please enter confirm password ';
    }

    if(values.new_password !== values.confirm_password) {
      error.confirm_password = 'New password does\'t match with confirm password ';
    }
    return error;
}

function mapStateToProps(state) {

  return {
      isAuthenticating : state.users.isAuthenticating,
      isFetching : state.users.isFetching,
      currentLoginUser: state.users.data,
      statusCode : state.users.statusCode,
      statusText : state.users.statusText,
      details : state.users.details
  }
}

export default reduxForm({
    form: 'UpdatePasswordForm',
    fields: ['current_password',
             'new_password',
             'confirm_password'],
    validate
}, mapStateToProps, { getLoginUser, updateSuperAdminsPassword, createUser, updateUserFormInitialValues })( Account );
