'use strict'
/**
 * @class         :	Application Portal
 * @description   : This class handles application header footer and content section
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import {Link} from 'react-router';
import Header from './Common/Header';
import SideMenu from './Common/SideMenu';
import Alert from 'react-s-alert';
// mandatory
//import 'react-s-alert/dist/s-alert-default.css';

//import 'bootstrap-css';
export default class App extends Component {
  constructor(props) {
        super(props);
        this.state = {toggleState: ''};
    }

    toggleState() {

      if(this.state.toggleState=='') {
        this.setState({toggleState: 'toggled'});
      } else {
        this.setState({toggleState: ''});
      }

    }
  render() {

    return (
      <div>
          <Header wrapperToggle={()=>this.toggleState()} />
          <div id="wrapper" className={this.state.toggleState}>
            <SideMenu />
            {this.props.children}
            <Alert stack={{ limit: 3}} position="top-right" timeout={3000} effect="slide" />
          </div>
          <div id="overlay" className="show" onClick={(e) => this.setState({ toggleState : '' })}></div>
      </div>
    );
  }
}
