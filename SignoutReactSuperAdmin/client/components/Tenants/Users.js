/**
 * @Component : UsersList Component
 * @description
 * @Created   : smartData
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';
import { getTenants, getTenantUsers, countTenantUsers } from '../../actions/TenantsActions';
import _ from 'lodash';
import Pagination from '../Common/Pagination';
import { PAGINATION_DEFAULT_LIMIT } from '../../actions/Constants';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';

class UsersList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token : null,
            tenantId : null,
            usersData : [],
            totalUsers : null,
            tenantsData : [],
            tenant : null,
            rowOrder: 'desc'
        };
        this.open_modal = this.open_modal.bind(this);
    }

    open_modal(data) {
        this.setState({
            modal1: !this.state.modal1
        });
    }

    componentDidMount() {
        document.title = "Signout - Users";
    }

    componentWillMount() {

      let limit = PAGINATION_DEFAULT_LIMIT;
      let orderBy = 'id';
      let order = 'desc';
      let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
      this.props.getTenants(this.props.token, query);

      if(this.props.params.tenantId) {
        let tenantId = this.props.params.tenantId;
        this.props.countTenantUsers(this.props.token, tenantId);

        let limit = PAGINATION_DEFAULT_LIMIT;
        let orderBy = 'id';
        let order = 'desc';
        let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
        this.props.getTenantUsers(this.props.token, tenantId, query);
        this.setState({ tenantId : tenantId });
      }

      this.setState({token: this.props.token });
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.tenants && this.props.tenants !== nextProps.tenants ) {
            let tenants = nextProps.tenants;
            this.setState( { tenantsData : tenants } );
        }

        if ( nextProps.tenantUsers && this.props.tenantUsers !== nextProps.tenantUsers ) {
            let tenantUsers = nextProps.tenantUsers;
            this.setState( { usersData : tenantUsers } );
        }

        if ( nextProps.countUsers ) {
            let countUsers = nextProps.countUsers;
            this.setState( { totalUsers : countUsers } );
        }
        //this.setState( { totalUsers : nextProps.countUsers } );
    }

    sortRowByFieldName(e, fieldName) {

      let orderType = this.state.rowOrder === 'desc' ? 'asc' : 'desc';
      this.setState({ rowOrder : orderType });

      let limit = PAGINATION_DEFAULT_LIMIT;
      let orderBy = fieldName;
      let order = this.state.rowOrder;
      let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
      this.props.getTenantUsers(this.props.token, this.state.tenantId, query);
    }

    renderRow() {

      if(this.props.isFetching) {
        return(
          <tr>
            <td colSpan="5">
              <div className="loader" id="loader-4">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </td>
          </tr>
        );
      }

      if(this.state.usersData.length === 0) {
        return (
          <tr>
            <td colSpan="5">
                No User.
            </td>
          </tr>
        );
      }

      if(this.state.usersData) {
        return this.state.usersData.map((user) => {
           return(
             <tr key= { user.id }>
                 <td>{ user.first_name } { user.last_name }</td>
                 <td>{ user.email }</td>
                 <td>
                   { user.phone_mobile ? user.phone_mobile : '(xxx) xxx-xxxx' } <br />
                   { user.phone_work ? user.phone_work : '(xxx) xxx-xxxx' }
                 </td>
                 <td>{ user.role.name ? user.role.name : 'n/a' }</td>
                 <td>{ user.department.name ? user.department.name : 'n/a' }</td>
             </tr>
           );
        })
      }
    }

    onChangeSelectBox(e, tid) {
      e.preventDefault();
      if(tid === 'null') {
          this.setState({ usersData : [] });
          this.setState({ tenantId : tid });
      } else {
        this.props.countTenantUsers(this.props.token, tid);

        let limit = PAGINATION_DEFAULT_LIMIT;
        let orderBy = 'id';
        let order = 'desc';
        let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
        this.props.getTenantUsers(this.state.token, tid, query);
        this.setState({ tenantId : tid });
      }
    }

    searchUser(e, searchValue) {
      e.preventDefault();
      let limit = PAGINATION_DEFAULT_LIMIT;
      let orderBy = 'id';
      let order = 'desc';
      let search = searchValue;
      let query = `?search=${searchValue}&limit=${limit}&orderBy=${orderBy}&order=${order}`;
      this.props.getTenantUsers(this.state.token, this.state.tenantId, query);
    }

    render() {
        return(

          <div id="page-content-wrapper">
              {/*<!-- Page Content -->*/}
              <div className="col welcome-bar ">
                  {/*<!-- welcome bar -->*/}
                  <h1 className="page-title">Users List</h1>
                  <ul className="breadcrumb pull-right hidden-xs-down">
                      <li><Link to={`/dashboard`}>Sign out</Link></li>
                      <li className="active">Users</li>
                  </ul>
              </div>

              <div className="container-fluid mrg-top30 pd-bt30">

                  <div className="col pd-off-xs loader-cover">

                      <div className="row">
                          <div className="col-sm-12">
                              <div className="white-bg">
                                    <div className="form-group col-sm-6 tenant-select-box">
                                      <Select name="tenant"
                                              value = { this.state.tenantId }
                                              onChange = { (e) => this.onChangeSelectBox(e, e.target.value) }>
                                          <Option value="null" label="Hospital (Tenant)" />
                                          { this.state.tenantsData.map( tenant => <option key={ tenant.id } value={ tenant.id }>{ tenant.name }</option>) }
                                      </Select>
                                      </div>

                                  <div className="form-group search-bar">
                                      <input className="inputMaterial"
                                              type="text" required
                                               onChange={ (e) => this.searchUser(e, e.target.value) } />
                                      <span className="highlight"></span> <span className="bar"></span>
                                      <label>Search Users by first name, last name & email</label>
                                      <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                                  </div>
                                  <div className="table-responsive">
                                      <table className="table table-hover patient-list user_lt">
                                          {/*<!--table start-->*/}
                                          <thead>
                                              <tr>
                                                  <th width="20%" onClick={ (e) => this.sortRowByFieldName(e, 'first_name') }>Name<span className="set"><i className="zmdi zmdi-chevron-up"></i><i className="zmdi zmdi-chevron-down"></i></span></th>
                                                  <th width="20%">Email</th>
                                                  <th width="15%">Phone</th>
                                                  <th width="15%" onClick={ (e) => this.sortRowByFieldName(e, 'role') }>Roles<span className="set"><i className="zmdi zmdi-chevron-up"></i><i className="zmdi zmdi-chevron-down"></i></span></th>
                                                  <th width="20%" onClick={ (e) => this.sortRowByFieldName(e, 'department') }>Department<span className="set"><i className="zmdi zmdi-chevron-up"></i><i className="zmdi zmdi-chevron-down"></i></span></th>
                                                  {/**<th width="10%">Action</th>**/}
                                              </tr>
                                          </thead>
                                          <tbody>
                                              { this.renderRow() }
                                          </tbody>
                                      </table>
                                      {/*<!--table End-->*/}
                                  </div>
                                  {(this.state.usersData.length > 0) ?
                                  <Pagination
                                            token = { this.state.token }
                                            pageName = "TENANT_USERS"
                                            totalPages ={ this.state.totalUsers }
                                            tenantId = { this.state.tenantId } /> : ''
                                  }
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
		      );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.tenants.isFetching,
         tenants  : state.tenants.data,
         tenantUsers: state.tenants.tenantUsers,
         countUsers: state.tenants.dataCount
     }
}

export default connect(mapStateToProps, { getTenants, getTenantUsers, countTenantUsers })(UsersList);
