import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import moment from 'moment';
import ModalWindow from '../Modals/UpdateTenantModal';
import ModalWindowConfirmDelete from '../Modals/ConfirmDeleteModal';
import ButtonM from 'muicss/lib/react/button';
import { getLoginUser } from '../../actions/UsersActions';
import { getTenants, setTenantInitialValues } from '../../actions/TenantsActions';
import Pagination from '../Common/Pagination';
import { PAGINATION_DEFAULT_LIMIT } from '../../actions/Constants';

class Tenants extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: null,
            modal1: false,
            modal2: false,
            data: [],
            tenantsData: [],
            deleteTenant: null,
            editTenantData: [],
            totalTenants: null
        };

        this.open_modal = this.open_modal.bind(this);
        this.open_modal_delete = this.open_modal_delete.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Tenants";
        this.setState({ token: this.props.token });
    }

    componentWillMount() {
        this.setState({ token: this.props.token });
        this.props.getLoginUser(this.props.token);
        let limit = PAGINATION_DEFAULT_LIMIT;
        let orderBy = 'id';
        let order = 'desc';
        let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
        this.props.getTenants(this.props.token, query);
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.tenants && this.props.tenants !== nextProps.tenants ) {
            let tenants = nextProps.tenants;
            this.setState( { tenantsData : tenants, totalTenants : tenants.length } );
        }
    }

    // Open modal on add and update Patient
    open_modal(e, data) {

      if (data === undefined) {
          let limit = PAGINATION_DEFAULT_LIMIT;
          let orderBy = 'id';
          let order = 'desc';
          let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
          this.props.getTenants(this.props.token, query);
      }

      this.props.setTenantInitialValues(data); // Update row

      //e.preventDefault();

          this.setState({
              modal1: !this.state.modal1,
              editTenantData: data,
          });

    }

    open_modal_delete( e, tenantId ) {

      // if(this.state.deletePatient) {
      //     this.props.getPatients(this.state.token); // update row
      // }

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal2: !this.state.modal2,
          deleteTenant: tenantId,
      });
    }

    renderRow() {

        if(this.props.isFetching) {
          return(
            <tr>
              <td>
                <div className="loader" id="loader-4">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </td>
            </tr>
          );
        }

        if(this.state.tenantsData.length <= 0) {
          return (
            <tr>
              <td colSpan="5">
                  No tenant.
              </td>
            </tr>
          );
        }

        if( this.state.tenantsData ) {
          return this.state.tenantsData.map(( tenant ) => {
            return (
              <tr key= { tenant.id }>
                <td>{ tenant.name}</td>
                <td>{ tenant.slug }</td>
                <td>{ tenant.db_host }</td>
                <td>{ tenant.db_port }</td>
                <td>{ tenant.db_name ? tenant.db_name : 'n/a' }</td>
                {/**<td>{ tenant.db_user ? tenant.db_user : 'n/a' }</td>
                <td>{ tenant.db_pass ? tenant.db_pass : 'n/a' }</td>
                <td>{ tenant.db_ssl ? tenant.db_ssl : 'n/a' }</td>**/}
                <td>{ moment(tenant.created_at).format('MM/DD/YYYY') }</td>
                <td>{ tenant.updated_at ? moment(tenant.updated_at).format('MM/DD/YYYY') : 'n/a' }</td>
                <td>{ tenant.deleted_at ? moment(tenant.deleted_at).format('MM/DD/YYYY') : 'n/a' }</td>
                <td>

                  <Link to={`/users/${tenant.id}`}><i className="zmdi zmdi-accounts-alt zmdi-hc-lg"></i></Link>

                  <button
                        onClick={ (e)=>this.open_modal(e, tenant) }
                        className="action-btn tenant-usr-icon"
                        data-toggle="modal"
                        data-target="#edit-patient1">
                        <i className="zmdi zmdi-edit"></i>
                  </button>
                  <button
                        onClick={ (e) => this.open_modal_delete( e, tenant.id )}
                        className="action-btn">
                        <i className="zmdi zmdi-delete"></i></button>
                </td>
              </tr>
            );
          });
        }
    }

    render() {
        return(
          <div id="page-content-wrapper">
              {/*
              <!-- Page Content -->*/}
              <div className="col welcome-bar ">
                  {/*
                  <!-- welcome bar -->*/}
                  <h1 className="page-title">Hospital (Tenants)</h1>
                  <ul className="breadcrumb pull-right hidden-xs-down">
                      <li><Link to={`/dashboard`}>Sign out</Link></li>
                      <li className="active">Hospital (Tenants)</li>
                  </ul>
              </div>

              {/** ModalWindow - with props **/}

              <ModalWindow
                          modal_var = {this.state.modal1}
                          handle_modal = { this.open_modal }
                          token = { this.state.token }
                          editTenantData = { this.state.editTenantData } />

              <ModalWindowConfirmDelete
                          modal_var = {this.state.modal2 }
                          handle_modal = { this.open_modal_delete }
                          token = { this.state.token }
                          confirmPage = { 'TENANTS_LISTS' }
                          tenantId = { this.state.deleteTenant } />

              <div className="container-fluid mrg-top30 pd-bt30">
                  <div className="col pd-off-xs">
                      <div className="row">
                          <div className="col-sm-12">
                              <div className="white-bg">

                                  <div className="table-responsive">
                                      <table className="table patient-list">
                                          <thead>
                                              <tr>
                                                  <th width="15%">Hospital Name</th>
                                                  <th width="5%">Slug</th>
                                                  <th width="20">Db host</th>
                                                  <th width="7%">Db port</th>
                                                  <th width="8%">Db name</th>
                                                  {/**<th width="10%">db_user</th>
                                                  <th width="10%">db_pass</th>
                                                  <th width="5%">db_ssl</th>**/}
                                                  <th width="5%">Created</th>
                                                  <th width="10%">Updated</th>
                                                  <th width="10%">Deleted</th>
                                                  <th width="10%">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              { this.renderRow() }
                                            </tbody>
                                      </table>
                                  </div>
                                  {(this.state.tenantsData.length > 0) ?
                                  <Pagination
                                            token = { this.state.token }
                                            pageName = "HOSPITAL_TENANTS"
                                            totalPages ={ this.state.totalTenants }
                                            tenantsData = { this.state.tenantsData } /> : ''
                                  }
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

function mapStateToProps(state) {
     return {
       isFetching : state.users.isFetching,
       currentLoginUser: state.users.data,
       isFetching : state.tenants.isFetching,
       tenants  : state.tenants.data
     }
}

export default connect(mapStateToProps, {  getLoginUser, getTenants, setTenantInitialValues })(Tenants);
