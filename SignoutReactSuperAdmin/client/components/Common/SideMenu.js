/**
 * @class         :	SideMenu
 * @description   : Dashbord SideMenu
 * @Created by    : smartData
 */

import React, { Component,PropTypes } from 'react';
import Ink from 'react-ink';

export default class SideMenu extends Component {
            constructor(props) {
            super(props)
            this.state = {
                activeIndex: 0
            }
        }

        static contextTypes={
            router:PropTypes.object
        }

        handleClick(index) {

            localStorage.setItem('index', index);

            let refreshIndex = localStorage.getItem(index);
            this.setState({activeIndex: index});

            if(index===0) {
                this.context.router.push('/dashboard');
            }else if(index===1) {
                this.context.router.push('/tenants');
            }else if(index===2) {
                this.context.router.push('/users');
            }else if(index===3) {
                this.context.router.push('/super-admins');
            }
        }

        render() {

            return (
                 <div id="sidebar-wrapper">
                   <h2 className="hidden-sm-up menu-title"><span>Menu</span><i className="zmdi zmdi-caret-down"></i></h2>
                   <ul className="sidebar-nav">
                      <MySideMenuIcons name="Dashbord"  icon="zmdi zmdi-assignment" index={0} isActive={this.state.activeIndex===0}
                      onClick={this.handleClick.bind(this)} />
                      <MySideMenuIcons name="Hospital(Tenant)" icon="zmdi zmdi-view-dashboard" index={1} isActive={this.state.activeIndex===1}
                      onClick={this.handleClick.bind(this)} />
                      <MySideMenuIcons name="Users" icon="zmdi zmdi-accounts-alt" index={2} isActive={this.state.activeIndex===2}
                      onClick={this.handleClick.bind(this)} />
                    <MySideMenuIcons name="SuperAdmins" icon="zmdi zmdi-accounts-outline" index={3} isActive={this.state.activeIndex===3}
                      onClick={this.handleClick.bind(this)} />
                    </ul>
                  </div>
            );
        }
}

class MySideMenuIcons extends React.Component {

    handleClick() {
        this.props.onClick(this.props.index)
    }

    render () {
        return <li className={this.props.isActive ? 'active' : ''} onClick={this.handleClick.bind(this)}>
               <a>
                    <Ink /><i className={this.props.icon} title="Signout Lists"></i><span className="nav-label">{this.props.name}</span>
                    {/*<!--<span className="badge bg-success">New</span>-->*/}
                </a>
            </li>
    }
}
