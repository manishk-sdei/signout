/**
 * @class         :	Footer 
 * @description   : Dashboard Footer
 * @Created by    : smartData
 */

import React, { Component } from 'react';
//import Moment from 'react-moment';

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer footer_default">
                {/*<Moment format="YYYY">{new Date()}</Moment>*/} © smartData. All Rights Reserved.
            </footer>
        );
    }
}
