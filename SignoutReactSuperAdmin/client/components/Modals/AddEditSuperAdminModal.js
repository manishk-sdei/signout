/**
 * @TODO Add patient copy functionality to copy a patient data and add as another patient.
 */

import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { reduxForm } from 'redux-form';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import Radio from 'muicss/lib/react/radio';
import ButtonM from 'muicss/lib/react/button';
import { createSuperAdmin } from '../../actions/SuperAdminActions';

class CreateSuperAdmins extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
            tenantsData: [],

            id: null,
            first_name: null,
            last_name: null,
            email: null,
            status: 'inactive',
            timezone: null,
            timezoneLists : []
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.editSuperAdmin && this.props.editSuperAdmin !== nextProps.editSuperAdmin ) {
            let admin = nextProps.editSuperAdmin;

            this.setState( { id : admin.id,
                             first_name : admin.first_name,
                             last_name : admin.last_name,
                             email : admin.email,
                             status : admin.status,
                             timezone : admin.timezone } );
        }

        if ( nextProps.timezones && this.props.timezones !== nextProps.timezones ) {
            let timezones = nextProps.timezones;
            this.setState( { timezoneLists : timezones.list } );
        }
    }

    onSubmit(formData) {

      formData.timezone = this.state.timezone;
      formData.status = this.state.status;

        this.props.createSuperAdmin(this.props.token, this.state.id, formData);
        let This = this;
        setTimeout(function(){
            This.props.handle_modal('false');
        }, 1000);
    }

    render() {

        const { fields: { first_name, last_name, email, status, timezone }, handleSubmit } = this.props;

        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Patient</span>
              </ModalHeader>
              <ModalBody>
                <div className="row">

                    <div className ={`col-sm-6 form-group ${first_name.touched && first_name.invalid ? 'col-sm-6 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text"
                            maxLength="200"
                            required {...first_name}
                            value={  this.state.first_name }
                            onChange={(event)=>this.setState({first_name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>First name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {first_name.touched && first_name.error ? first_name.error : false }
                        </div>
                    </div>

                    <div className ={`col-sm-6 form-group ${last_name.touched && last_name.invalid ? 'col-sm-6 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text"
                            maxLength="100"
                            required {...last_name}
                            value={  this.state.last_name }
                            onChange={(event)=>this.setState({last_name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Last name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {last_name.touched && last_name.error ? last_name.error : false }
                        </div>
                    </div>
                  </div>

                  <div className="row">
                      <div className ={`col-sm-12 form-group ${email.touched && email.invalid ? 'col-sm-12 has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              required {...email}
                              value={  this.state.email }
                              onChange={(event)=>this.setState({email: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Email <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {email.touched && email.error ? email.error : false }
                          </div>
                      </div>
                  </div>

                  <div className="row">

                      <div className="col-sm-6 form-group">
                          <div className="custom-box">
                          <Select name="timezone" {...timezone}
                                  value={ this.state.timezone }
                                  onChange={(event)=>this.setState({timezone: event.target.value})}>
                              <Option label="Timezone" />
                              { this.state.timezoneLists.map( val =>
                                  <option key={ val.zone }
                                          value={ val.zone }>
                                  { val.name }
                              </option>) }
                          </Select>
                          </div>
                      </div>

                      <div className="col-sm-6 form-group">
                          Status
                          <div
                            {...status}
                            className="radio hgt"
                            onChange={ (event) => this.setState({ status : event.target.value }) }>
                            <input type="radio" value="inactive" name="status" checked={this.state.status === 'inactive'}  /> Inactive &nbsp;
                            <input type="radio" value="active" name="status" checked={this.state.status === 'active'}  /> Active
                          </div>
                      </div>
                  </div>

              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.first_name) {
        error.first_name = 'Please enter first name';
    }

    if(!values.last_name) {
        error.last_name = 'Please enter last name';
    }

    if(!values.email) {
        error.email = 'Please enter email';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(!values.email) {
            error.email = 'Please enter email address';
        }else {
            error.email = 'Please enter a valid email address';
        }
    }

    return error;
}

function mapStateToProps(state) {
     return {
         isFetching : state.superAdmins.isFetching,
         superAdmins  : state.superAdmins.data,
         isAuthenticating : state.superAdmins.isAuthenticating,
         initialValues : {
           first_name: state.superAdmins.first_name,
           last_name: state.superAdmins.last_name,
           email: state.superAdmins.email,
           status: state.superAdmins.status,
           timezone: state.superAdmins.timezone
         },
         timezones : state.terms.timezones
     }
}

export default reduxForm({
    form: 'AddEditSuperAdminsForm',
    fields: ['first_name', 'last_name', 'email', 'status', 'timezone'],
    validate
}, mapStateToProps, { createSuperAdmin })( CreateSuperAdmins );
