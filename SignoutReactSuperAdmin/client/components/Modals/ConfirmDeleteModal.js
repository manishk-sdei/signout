import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';

import { deleteTenant } from '../../actions/TenantsActions';

class ConfirmDeleteModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          id: null,
          actionButton: null
        };
        this.deleteRow = this.deleteRow.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps(nextProps) {


        this.setState({ actionButton : 'Delete' });
    }

    deleteRow(e) {
      e.preventDefault();      

      if (this.props.confirmPage === 'TENANTS_LISTS') {

        if(this.props.tenantId) {
          this.props.deleteTenant( this.props.token, this.props.tenantId );
        }
      }

      this.closeModal();
    }

    closeModal() {
      // Close Modal Window
      let This = this;
      setTimeout(function(){
          This.props.handle_modal('false');
      }, 1000);
    }

    render() {
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">

              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Confirm</span>
              </ModalHeader>
              <ModalBody>
                Are you sure?
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  disabled={
                    this.props.isDelete ? true : false}
                  onClick={ (e) => this.deleteRow(e) }
                  type="submit"
                  className="blue-btn">{ this.state.actionButton }</ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>

          </Modal>
        );
    }
}

function mapStateToProps(state) {
     return {
         isDelete: state.tenants.isAuthenticating,
     }
}

export default connect(mapStateToProps, { deleteTenant })( ConfirmDeleteModal );
