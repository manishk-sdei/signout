/**
 * @TODO Add patient copy functionality to copy a patient data and add as another patient.
 */

import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { reduxForm } from 'redux-form';
import ButtonM from 'muicss/lib/react/button';
import { updateTenant } from '../../actions/TenantsActions';

class UpdateTenant extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
            tenantsData: [],

            id: null,
            name: null,
            slug: null,
            is_disabled: false,
            db_host: null,
            db_port: null,
            db_name: null,
            db_user: null,
            db_pass: null
        };
        this._onChange = this._onChange.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.editTenantData && this.props.editTenantData !== nextProps.editTenantData ) {
            let tenant = nextProps.editTenantData;

            this.setState( { id : tenant.id,
                             name : tenant.name,
                             slug : tenant.slug,
                             is_disabled : tenant.is_disabled,
                             db_host : tenant.db_host,
                             db_port : tenant.db_port,
                             db_name : tenant.db_name,
                             db_user : tenant.db_user,
                             db_pass : tenant.db_pass } );
        }
    }

    onSubmit(formData) {
      console.log(formData);

        this.props.updateTenant(this.props.token, this.state.id, formData);

        let This = this;
        setTimeout(function(){
            This.props.handle_modal('false');
        }, 1000);
    }

    _onChange(e) {
        let stateChange = {};
        stateChange[e.target.name] = e.target.value;
        this.setState(stateChange);
      }

    render() {

        const { fields: { name, slug, is_disabled, db_host, db_port, db_name, db_user, db_pass }, handleSubmit } = this.props;

        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Patient</span>
              </ModalHeader>
              <ModalBody>
                <div className="row">

                    <div className ={`col-sm-6 form-group ${name.touched && name.invalid ? 'col-sm-6 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text"
                            maxLength="200"
                            required {...name}
                            value={  this.state.name }
                            onChange={()=>this.setState({name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {name.touched && name.error ? name.error : false }
                        </div>
                    </div>

                    <div className ={`col-sm-6 form-group ${slug.touched && slug.invalid ? 'col-sm-6 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text"
                            maxLength="100"
                            required {...slug}
                            value={  this.state.slug }
                            onChange={()=>this.setState({slug: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Slug <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {slug.touched && slug.error ? slug.error : false }
                        </div>
                    </div>
                  </div>

                  <div className="row">
                      <div className ={`col-sm-12 form-group ${db_host.touched && db_host.invalid ? 'col-sm-12 has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              required {...db_host}
                              value={  this.state.db_host }
                              onChange={()=>this.setState({db_host: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Db host <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {db_host.touched && db_host.error ? db_host.error : false }
                          </div>
                      </div>
                  </div>

                  <div className="row">

                      <div className ={`col-sm-6 form-group ${db_name.touched && db_name.invalid ? 'col-sm-6 has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              maxLength="100"
                              required {...db_name}
                              value={  this.state.db_name }
                              onChange={()=>this.setState({db_name: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Db name <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {db_name.touched && db_name.error ? db_name.error : false }
                          </div>
                      </div>

                      <div className ={`col-sm-6 form-group ${db_port.touched && db_port.invalid ? 'col-sm-6 has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              maxLength="50"
                              required {...db_port}
                              value={  this.state.db_port }
                              onChange={()=>this.setState({db_port: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Db port <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {db_port.touched && db_port.error ? db_port.error : false }
                          </div>
                      </div>
                  </div>

                  <div className="row">

                      <div className ={`col-sm-6 form-group ${db_user.touched && db_user.invalid ? 'col-sm-6 has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              maxLength="100"
                              required {...db_user}
                              value={  this.state.db_user }
                              onChange={()=>this.setState({db_user: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Db user <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {db_user.touched && db_user.error ? db_user.error : false }
                          </div>
                      </div>

                      <div className ={`col-sm-6 form-group ${db_pass.touched && db_pass.invalid ? 'col-sm-6 has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              maxLength="100"
                              required {...db_pass}
                              value={  this.state.db_pass }
                              onChange={()=>this.setState({db_pass: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Db password <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {db_pass.touched && db_pass.error ? db_pass.error : false }
                          </div>
                      </div>
                  </div>

                  <div className="row">
                    <div className="col-sm-12 form-group">
                          <div className="checkbox">
                              <label>
                                <input
                                    {...is_disabled}
                                    type="checkbox"
                                    value = { this.state.is_disabled }
                                    onClick={(e) => this.setState({ is_disabled : e.target.value === false ? true : false }) } />
                                  <span className="cr"><i className="cr-icon fa fa-check"></i></span> is disabled </label>
                        </div>
    							  </div>

                  </div>
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.name) {
        error.name = 'Please enter name';
    }

    if(!values.slug) {
        error.slug = 'Please enter slug';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isFetching : state.tenants.isFetching,
         tenants  : state.tenants.data,
         isAuthenticating : state.tenants.isAuthenticating,
         initialValues : {
           name: state.tenants.name,
           slug: state.tenants.slug,
           is_disabled: state.tenants.is_disabled,
           db_host: state.tenants.db_host,
           db_port: state.tenants.db_port,
           db_name: state.tenants.db_name,
           db_user: state.tenants.db_user,
           db_pass: state.tenants.db_pass
         }
     }
}

export default reduxForm({
    form: 'AddEditPatientForm',
    fields: ['name', 'slug', 'is_disabled', 'db_host', 'db_port', 'db_name', 'db_user', 'db_pass'],
    validate
}, mapStateToProps, { updateTenant })( UpdateTenant );
