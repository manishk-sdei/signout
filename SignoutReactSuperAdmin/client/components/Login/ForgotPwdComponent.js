/**
 * @class         :	ForgotPassword
 * @description   :
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { forgotPassword, statusUpdate } from '../../actions/LoginActions';
import { Link } from 'react-router';
import { Alert } from 'reactstrap';

class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.state = { show_error: 'hide' }
    }

    componentDidMount() {
        document.title = "Signout - Forgot Password";
        this.props.statusUpdate();
    }

    componentDidMount() {
        this.props.statusUpdate();
    }

    onSubmit(formData) {

        this.props.forgotPassword(formData);
    }

    render() {
        const { fields: { email }, handleSubmit } = this.props;
        return(
            <div className="login-bg">
                <div className="login-wrap">
                    <div className="login-box">
                        <h1><img src="/client/assets/images/login-logo.png" className="login-logo" alt=""/></h1>
                        <div className="login-form">
                          <h2>FORGOT PASSWORD</h2>
                            {
                              (this.props.statusText!=null)?
                              <Alert color="success">{this.props.statusText}</Alert>
                              :""
                            }
                          <div className="col-sm-12">
                                <div className="tab-pane show active" id="signin" role="tabpanel">

                                    <form noValidate action="signoutlist" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                                          <div className ={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                                              <input type="text" required className="form-control inputMaterial" {...email} />
                                              <span className="highlight"></span> <span className="bar"></span>
                                              <label>Email</label>
                                              <i className="zmdi zmdi-lock-outline"></i>
                                              <div className="error_msg_danger">
                                                    {email.touched ? email.error : '' }
                                              </div>
                                          </div>

                                        <button className="btn blue-btn sign-btn">
                                          { this.props.isAuthenticating === true ? 'Sending...' : 'Send' }</button>
                                        <div className="frgt-password">
                                            <Link to="/" className="forgot-password">click here to Login!</Link>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function validate(values) {
    const error = {};

    if(!values.email) {
        error.email = 'Please enter email address';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(!values.email) {
            error.email = 'Please enter email address';
        }else {
            error.email = 'Please enter a valid email address';
        }
    }
    return error;
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
// reduxForm: first is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps
const mapStateToProps = (state) => ({
  isAuthenticating   : state.login.isAuthenticating,
  statusText         : state.login.statusText
});


export default reduxForm({
    form: 'ForgotPasswordForm',
    fields: ['email'],
    validate
}, mapStateToProps, { forgotPassword, statusUpdate })(ForgotPassword);
