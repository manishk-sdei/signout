/**
 * @class         :	Login
 * @description   :
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { loginUser, statusUpdate } from '../../actions/LoginActions';
import { LOCALS_STORAGE_AUTHTOKEN } from '../../actions/Constants';
import { Link } from 'react-router';
import { Alert } from 'reactstrap';

class LoginForm extends Component {

    constructor(props) {
        super(props);
        const redirectRoute = this.props.location.query.next || '/';
        this.state = { email: null, password: null}
    }

    componentDidMount() {
        document.title = "Signout - Login";
        this.props.actions.statusUpdate();
    }

    componentWillMount() {
        this.props.actions.statusUpdate();
    }

    onSubmit(creds) {
          this.props.actions.loginUser(creds, this.state.redirectTo);
    }

    render() {

        const { fields: { email, password }, handleSubmit } = this.props;
        return(
            <div className="login-bg">
                <div className="login-wrap">
                    <div className="login-box">
                        <h1><img src="/client/assets/images/login-logo.png" className="login-logo" alt=""/></h1>
                        <div className="login-form">
                          <h2>SIGN IN</h2>
                          {
                            //<strong>Oh snap!</strong> <br />
                            (this.props.statusText!=null)?
                            <Alert color="danger">{this.props.statusText}</Alert>
                            :""
                          }
                          <div className="col-sm-12">
                                <div className="tab-pane show active" id="signin" role="tabpanel">

                                    <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                                        <div className ={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                                            <input
                                              {...email} required
                                              type="text"
                                              className="form-control inputMaterial" />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>Email</label>
                                            <i className="zmdi zmdi-account-o"></i>
    											                  <div className="error_msg_danger">
                                                {email.touched ? email.error : '' }
                                            </div>
                                        </div>


                                        <div className={`form-group ${password.touched && password.invalid ? 'has-danger' : ''}`}>
                                            <input
                                              {...password} required
                                              type="password"
                                              className="form-control inputMaterial" />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>Password</label>
                                            <i className="zmdi zmdi-lock-outline"></i>
    											                  <div className="error_msg_danger">
                                                {password.touched ? password.error : '' }
                                            </div>
                                        </div>

                                          <button
                                            type="submit"
                                            className="btn blue-btn sign-btn">
                                            { this.props.isAuthenticating === true ? 'Authenticating...' : 'SIGN IN' }
                                          </button>
                                      <div className="frgt-password">
                                            <Link to="/forgot-password" className="forgot-password">Forgot Password?</Link>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.email) {
        error.email = 'Please enter email address';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(!values.email) {
            error.email = 'Please enter email address';
        }else {
            error.email = 'Please enter a valid email address';
        }
    }

    if(!values.password) {
        error.password = 'Please enter a password';
    }
    return error;
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
// reduxForm: first is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps

const mapStateToProps = (state) => ({
  isAuthenticating   : state.login.isAuthenticating,
  statusText         : state.login.statusText,
  isAuthenticated    : state.login.isAuthenticated
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators({ loginUser, statusUpdate }, dispatch)
});

export default reduxForm({
    form: 'LoginForm',
    fields: ['email', 'password'],
    validate
}, mapStateToProps, mapDispatchToProps)( LoginForm );
