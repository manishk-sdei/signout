/**
 * @constants
 * @description   : Hold CONSTANTS and APIs
 * @Created by    : smartData
 */

import axios from 'axios';

/******************************* Define Pagination Constants *****************************/

export const PAGINATION_DEFAULT_LIMIT = '10';

/******************************* Define and Export Constants ****************************/

export const TENANTS_CONST = {

                GET_TENANTS_REQUEST : 'GET_TENANTS_REQUEST',
                GET_TENANTS_SUCCESS : 'GET_TENANTS_SUCCESS',
                GET_TENANT_USERS_COUNT_SUCCESS : 'GET_TENANT_USERS_COUNT_SUCCESS',
                GET_TENANTS_FAILURE : 'GET_TENANTS_FAILURE',

                GET_TENANT_BYID_REQUEST : 'GET_TENANT_BYID_REQUEST',
                GET_TENANT_BYID_SUCCESS : 'GET_TENANT_BYID_SUCCESS',
                GET_TENANT_BYID_FAILURE : 'GET_TENANT_BYID_FAILURE',

                GET_TENANT_USERS_REQUEST : 'GET_TENANT_USERS_REQUEST',
                GET_TENANT_USERS_SUCCESS : 'GET_TENANT_USERS_SUCCESS',
                GET_TENANT_USERS_FAILURE : 'GET_TENANT_USERS_FAILURE',

                PUT_TENANT_REQUEST : 'PUT_TENANT_REQUEST',
                PUT_TENANT_SUCCESS : 'PUT_TENANT_SUCCESS',
                PUT_TENANT_FAILURE : 'PUT_TENANT_FAILURE',

                UPDATE_TENANT_FORM_VALUES : 'UPDATE_TENANT_FORM_VALUES',

                DELETE_TENANT_REQUEST : 'DELETE_TENANT_REQUEST',
                DELETE_TENANT_SUCCESS : 'DELETE_TENANT_SUCCESS',
                DELETE_TENANT_FAILURE : 'DELETE_TENANT_FAILURE'
};

export const AUTH_CONST = {
                LOGIN_USER_REQUEST : 'LOGIN_USER_REQUEST',
                LOGIN_USER_SUCCESS : 'LOGIN_USER_SUCCESS',
                LOGIN_USER_FAILURE : 'LOGIN_USER_FAILURE',

                LOGOUT_USER : 'LOGOUT_USER',
                TOKEN_EXPIRED : 'TOKEN_EXPIRED',

                FORGOT_PASSWORD_REQUEST : 'FORGOT_PASSWORD_REQUEST',
                FORGOT_PASSWORD_SUCCESS : 'FORGOT_PASSWORD_SUCCESS',
                FORGOT_PASSWORD_FAILURE : 'FORGOT_PASSWORD_FAILURE',

                RESET_PASSWORD_REQUEST : 'RESET_PASSWORD_REQUEST',
                RESET_PASSWORD_SUCCESS : 'RESET_PASSWORD_SUCCESS',
                RESET_PASSWORD_FAILURE : 'RESET_PASSWORD_FAILURE',

                STATUS_UPDATE : 'STATUS_UPDATE'
};

export const GET_USER_CONST = {
                            GET_USER_REQUEST : 'GET_USER_REQUEST',
                            GET_USER_SUCCESS : 'GET_USER_SUCCESS',
                            GET_USER_FAILURE : 'GET_USER_FAILURE',

                            GET_USER_BYID_REQUEST : 'GET_USER_BYID_REQUEST',
                            GET_USER_BYID_SUCCESS : 'GET_USER_BYID_SUCCESS',
                            GET_USER_BYID_FAILURE : 'GET_USER_BYID_FAILURE',

                            UPDATE_USER_FORM_VALUES : 'UPDATE_USER_FORM_VALUES',

                            USERS_REQUEST : 'USERS_REQUEST',
                            USERS_SUCCESS : 'USERS_SUCCESS',
                            USERS_FAILURE : 'USERS_FAILURE',

                            PUT_USER_PASSWORD_REQUEST : 'PUT_USER_PASSWORD_REQUEST',
                            PUT_USER_PASSWORD_SUCCESS : 'PUT_USER_PASSWORD_SUCCESS',
                            PUT_USER_PASSWORD_FAILURE : 'PUT_USER_PASSWORD_FAILURE',


};

export const TERM_CONST = {
                GET_COUNTRIES_REQUEST : 'GET_COUNTRIES_REQUEST',
                GET_COUNTRIES_SUCCESS : 'GET_COUNTRIES_SUCCESS',
                GET_COUNTRIES_FAILURE : 'GET_COUNTRIES_FAILURE',

                GET_LANGUAGES_REQUEST : 'GET_LANGUAGES_REQUEST',
                GET_LANGUAGES_SUCCESS : 'GET_LANGUAGES_SUCCESS',
                GET_LANGUAGES_FAILURE : 'GET_LANGUAGES_FAILURE',

                GET_MERITALSTATUS_REQUEST : 'GET_MERITALSTATUS_REQUEST',
                GET_MERITALSTATUS_SUCCESS : 'GET_MERITALSTATUS_SUCCESS',
                GET_MERITALSTATUS_FAILURE : 'GET_MERITALSTATUS_FAILURE',

                GET_RACE_REQUEST : 'GET_RACE_REQUEST',
                GET_RACE_SUCCESS : 'GET_RACE_SUCCESS',
                GET_RACE_FAILURE : 'GET_RACE_FAILURE',

                GET_SEX_REQUEST : 'GET_SEX_REQUEST',
                GET_SEX_SUCCESS : 'GET_SEX_SUCCESS',
                GET_SEX_FAILURE : 'GET_SEX_FAILURE',

                GET_TIMEZONES_REQUEST : 'GET_TIMEZONES_REQUEST',
                GET_TIMEZONES_SUCCESS : 'GET_TIMEZONES_SUCCESS',
                GET_TIMEZONES_FAILURE : 'GET_TIMEZONES_FAILURE'
}

export const GET_SUPER_ADMIN_CONST = {
                          GET_SUPER_ADMIN_REQUEST : 'GET_SUPER_ADMIN_REQUEST',
                          GET_SUPER_ADMIN_SUCCESS : 'GET_SUPER_ADMIN_SUCCESS',
                          GET_SUPER_ADMIN_FAILURE : 'GET_SUPER_ADMIN_FAILURE',
                          GET_TOTAL_SUPER_ADMINS_SUCCESS : 'GET_TOTAL_SUPER_ADMINS_SUCCESS',

                          POST_SUPER_ADMIN_REQUEST : 'POST_SUPER_ADMIN_REQUEST',
                          POST_SUPER_ADMIN_SUCCESS : 'POST_SUPER_ADMIN_SUCCESS',
                          POST_SUPER_ADMIN_FAILURE : 'POST_SUPER_ADMIN_FAILURE',

                          GET_SUPER_ADMIN_BYID_REQUEST : 'GET_SUPER_ADMIN_BYID_REQUEST',
                          GET_SUPER_ADMIN_BYID_SUCCESS : 'GET_SUPER_ADMIN_BYID_SUCCESS',
                          GET_SUPER_ADMIN_BYID_FAILURE : 'GET_SUPER_ADMIN_BYID_FAILURE',

                          UPDATE_SUPER_ADMIN_FORM_VALUES : 'UPDATE_SUPER_ADMIN_FORM_VALUES',
};

/************************************* API CONSTANTS ***************************************/

// creating global instance for the axios to call apis
export const AXIOS_INSTANCE = axios.create();
AXIOS_INSTANCE.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
AXIOS_INSTANCE.defaults.headers.common['Accept'] = 'application/json';

export const BASIC_AUTHTOKEN = 'Basic UVNCbDlQaUlMY3I2U3JsYWRUdDZOVktpdWtKbEp5R2NSNl9wTEg3NjpDaWVvTmJNZUlOS1VTVEtOdVZOSFNLb3YzYUZsaEpFVXBqY2JhbXRM';

export const LOCALS_STORAGE_AUTHTOKEN = localStorage.getItem('authToken');
//export const AUTHTOKEN = `Bearer ` + LOCALS_STORAGE_AUTHTOKEN;
export const TOKEN_BEARER = `Bearer `;

//--------------------------------------------------------------------------------------------

// Define APIS
const SERVER_URL = `http://203.129.220.75`;
const API_SLUG = SERVER_URL + `/v1`;
export const LOGIN_API = `${API_SLUG}/oauth/token`; // POST - Get JWT access token for the application
export const RESET_PASSWORD_API = `${API_SLUG}/oauth/password-reset`; //POST /oauth/password-reset

export const GET_LOGIN_USER_API = `${API_SLUG}/userinfo`; // GET - Returns the 'me' data for the currently logged in user
export const GET_TENANTS_API = `${API_SLUG}/system/db-tenants`; // GET /system/db-tenants - Returns a list of DB Tenants
export const SUPER_ADMINS_API = `${API_SLUG}/system/superadmins`; // GET /system/superadmins

export const GET_COUNTRIES_API = `${API_SLUG}/terms/countries`;
export const GET_LANGUAGES_API = `${API_SLUG}/terms/languages`;
export const GET_MERITALSTATUS_API = `${API_SLUG}/terms/marital-status`;
export const GET_RACE_API = `${API_SLUG}/terms/race`;
export const GET_SEX_API = `${API_SLUG}/terms/sex`;
export const GET_TIMEZONES_API = `${API_SLUG}/terms/timezones`;
