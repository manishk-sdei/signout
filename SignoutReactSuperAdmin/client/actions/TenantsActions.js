/**
 * @action        : TenantsActions
 * @description   : Handles all users
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         TENANTS_CONST,
         TOKEN_BEARER,
         GET_TENANTS_API
       } from './Constants';

  /**
   * [getUserRequest user request to api]
   * @return {[OBJECT]} [description]
   */
  export function getUserRequest(REQUEST) {
  	return {
      type: REQUEST,
    	}
  }

  /**
   * [getUserSuccess user api response success]
   * @param  {[type]} data [json data]
   * @return {[OBJECT]}      [json]
   */
  export function getUserSuccess( SUCCESS, data ) {
  	return {
      	type: SUCCESS,
      	payload: data
    	}
  }

  /**
   * [getUserFailure user api response failure]
   * @return {[OBJECT]}      [json]
   */
  export function getUserFailure(FAILURE, error) {
      return {
        type: FAILURE,
        payload: {
          status: error.response.status,
          statusText: error.response.statusText
        }
      }
  }

   /**
    * [getTenants Returns a single tenant by id or slug]
    * @param  {[type]} token    [description]
    * @return {[api]}          [GET /tenants/{id}]
    */
   export function getTenants( token, query ) {

       return function(dispatch) {

         dispatch(getUserRequest(TENANTS_CONST.GET_TENANTS_REQUEST));

         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

          AXIOS_INSTANCE.get(`${GET_TENANTS_API}${query}`, config)
                                         .then(function (response) {
                                           try {
                                             dispatch(getUserSuccess(TENANTS_CONST.GET_TENANTS_SUCCESS, response.data.data));
                                           } catch (e) {
                                             //console.log('getUsers', e);
                                           }
                                         })
                                         .catch(function (error) {
                                            // Handle expire token
                                            if(error.response.data.statusCode===401) {// Handle expire token
                                              dispatch(tokenExpired());
                                            } else {// Handle other errors
                                              dispatch(errorHandler(TENANTS_CONST.GET_TENANTS_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                         });
       }
   }


   export function getTenantBYID( token, tenantId ) {

       return function(dispatch) {

         dispatch(getUserRequest(TENANTS_CONST.GET_TENANT_BYID_REQUEST));

         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

          AXIOS_INSTANCE.get(GET_TENANTS_API, config)
                                         .then(function (response) {
                                           try {
                                             dispatch(getUserSuccess(TENANTS_CONST.GET_TENANT_BYID_SUCCESS, response.data.data));
                                           } catch (e) {
                                             //console.log('getUsers', e);
                                           }
                                         })
                                         .catch(function (error) {
                                            // Handle expire token
                                            if(error.response.data.statusCode===401) {// Handle expire token
                                              dispatch(tokenExpired());
                                            } else {// Handle other errors
                                              dispatch(errorHandler(TENANTS_CONST.GET_TENANT_BYID_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                         });
       }
   }

   //countTenant

   export function countTenantUsers( token, tenantId ) {

       return function(dispatch) {

         dispatch(getUserRequest(TENANTS_CONST.GET_TENANT_USERS_REQUEST));

         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

          AXIOS_INSTANCE.get(`${GET_TENANTS_API}/${tenantId}/users`, config)
                                         .then(function (response) {
                                           try {
                                             dispatch(getUserSuccess(TENANTS_CONST.GET_TENANT_USERS_COUNT_SUCCESS, response.data.data.length));
                                           } catch (e) {
                                             //console.log('getUsers', e);
                                           }
                                         })
                                         .catch(function (error) {
                                            // Handle expire token
                                            if(error.response.data.statusCode===401) {// Handle expire token
                                              dispatch(tokenExpired());
                                            } else {// Handle other errors
                                              dispatch(errorHandler(TENANTS_CONST.GET_TENANT_USERS_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                         });
       }
   }

   export function getTenantUsers( token, tenantId, query ) {

       return function(dispatch) {

         dispatch(getUserRequest(TENANTS_CONST.GET_TENANT_USERS_REQUEST));

         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

          AXIOS_INSTANCE.get(`${GET_TENANTS_API}/${tenantId}/users${query}`, config)
                                         .then(function (response) {
                                           try {
                                             dispatch(getUserSuccess(TENANTS_CONST.GET_TENANT_USERS_SUCCESS, response.data.data));
                                           } catch (e) {
                                             //console.log('getUsers', e);
                                           }
                                         })
                                         .catch(function (error) {
                                            // Handle expire token
                                            if(error.response.data.statusCode===401) {// Handle expire token
                                              dispatch(tokenExpired());
                                            } else {// Handle other errors
                                              dispatch(errorHandler(TENANTS_CONST.GET_TENANT_USERS_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                         });
       }
   }

   /**
    * [updateTenant Update a DbTenant]
    * @param  {[type]} token    [description]
    * @param  {[type]} tenantId [description]
    * @param  {[type]} formData [description]
    * @return {[api]}          [PUT /system/db-tenants/{id}]
    */
   export function updateTenant( token, tenantId, formData ) {

       return function( dispatch ) {

         dispatch( getUserRequest( TENANTS_CONST.PUT_TENANT_REQUEST ) );
         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         const postData = qs.stringify({
                               id: tenantId,
                               name: formData.name,
                               slug: formData.slug,
                               is_disabled: formData.is_disabled,
                               db_host: formData.db_host,
                               db_port: formData.db_port,
                               db_name: formData.db_name,
                               db_user: formData.db_user,
                               db_pass: formData.db_pass
                           });

        AXIOS_INSTANCE.put(`${GET_TENANTS_API}/${tenantId}`, postData, config)
                                        .then(checkHttpStatus)
                                        .then(parseJSON)
                                         .then(function ( response ) {
                                           try {
                                             dispatch(getUserSuccess(TENANTS_CONST.PUT_TENANT_SUCCESS, response));
                                             dispatch(successHandler('Success', {
                                               response: {
                                                 status: 200, //success
                                                 statusText: 'Tenant saved successfully.',
                                                 data: null
                                               }
                                             }));
                                           } catch (e) {
                                             //console.log("createUser ");
                                           }
                                         })
                                         .catch(function ( error ) {
                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch(getUserFailure(TENANTS_CONST.PUT_TENANT_FAILURE, error));
                                               dispatch(errorHandler('Failure', {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: error.response.data.message
                                                 }
                                               }));
                                             }
                                         });
       }
   }


   export function deleteTenant( token, tenantId ) {

       return function( dispatch ) {

         dispatch( getUserRequest( TENANTS_CONST.DELETE_TENANT_REQUEST ) );
         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         const postData = qs.stringify({
                               is_disabled: true
                           });

        AXIOS_INSTANCE.put(`${GET_TENANTS_API}/${tenantId}`, postData, config)
                                        .then(checkHttpStatus)
                                        .then(parseJSON)
                                         .then(function ( response ) {
                                           try {
                                             dispatch(getUserSuccess(TENANTS_CONST.DELETE_TENANT_SUCCESS, response));
                                             dispatch(successHandler('Success', {
                                               response: {
                                                 status: 200, //success
                                                 statusText: 'DbTenant successfully updated.',
                                                 data: null
                                               }
                                             }));
                                           } catch (e) {
                                             //console.log("createUser ");
                                           }
                                         })
                                         .catch(function ( error ) {
                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch(getUserFailure(TENANTS_CONST.DELETE_TENANT_FAILURE, error));
                                               dispatch(errorHandler('Failure', {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: error.response.data.message
                                                 }
                                               }));
                                             }
                                         });
       }
   }

   export function getUpdate( REQUEST, data ) {
   	return {
         type : REQUEST,
         payload: data
     	}
   }

   /**
    * [setTenantInitialValues description]
    * @param {[type]} data [description]
    */
   export function setTenantInitialValues( data ) {
     return function( dispatch ) {
       dispatch( getUpdate( TENANTS_CONST.UPDATE_TENANT_FORM_VALUES, data ) );
     }
   }
