/**
 * @action        : UsersActions
 * @description   : Handles all users
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         GET_SUPER_ADMIN_CONST,
         TOKEN_BEARER,
         SUPER_ADMINS_API
       } from './Constants';

  /**
   * [getRequest user request to api]
   * @return {[OBJECT]} [description]
   */
  export function getRequest(REQUEST) {
  	return {
      type: REQUEST,
    	}
  }

  /**
   * [getSuccess user api response success]
   * @param  {[type]} data [json data]
   * @return {[OBJECT]}      [json]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type: SUCCESS,
      	payload: data
    	}
  }

  /**
   * [getFailure user api response failure]
   * @return {[OBJECT]}      [json]
   */
  export function getFailure(FAILURE, error) {
      return {
        type: FAILURE,
        payload: {
          status: error.response.status,
          statusText: error.response.statusText
        }
      }
  }

  /**
   * [getSuperAdmins Returns a list of Superadmins]
   * @param  {[type]} token [description]
   * @return {[api]}       [GET /system/superadmins]
   */
   export function getSuperAdmins( token, query ) {

       return function(dispatch) {

         dispatch(getRequest(GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_REQUEST));

         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

          AXIOS_INSTANCE.get(`${SUPER_ADMINS_API}${query}`, config)
                                         .then(function (response) {
                                           try {
                                             dispatch(getSuccess(GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_SUCCESS, response.data.data));
                                           } catch (e) {
                                             //console.log('getSuperAdmins', e);
                                           }
                                         })
                                         .catch(function (error) {
                                            // Handle expire token
                                            if(error.response.data.statusCode===401) {// Handle expire token
                                              dispatch(tokenExpired());
                                            } else {// Handle other errors
                                              dispatch(errorHandler(GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                         });
       }
   }

   export function countSuperAdmins( token ) {

       return function(dispatch) {

         dispatch(getRequest(GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_REQUEST));

         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

          AXIOS_INSTANCE.get(SUPER_ADMINS_API, config)
                                         .then(function (response) {
                                           try {
                                             dispatch(getSuccess(GET_SUPER_ADMIN_CONST.GET_TOTAL_SUPER_ADMINS_SUCCESS, response.data.data.length));
                                           } catch (e) {
                                             //console.log('getSuperAdmins', e);
                                           }
                                         })
                                         .catch(function (error) {
                                            // Handle expire token
                                            if(error.response.data.statusCode===401) {// Handle expire token
                                              dispatch(tokenExpired());
                                            } else {// Handle other errors
                                              dispatch(errorHandler(GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                         });
       }
   }

   /**
    * [createSuperAdmin Create a new Superadmin and Update a Superadmin]
    * @param  {[type]} token    [description]
    * @param  {[type]} adminId  [description]
    * @param  {[type]} formData [description]
    * @return {[api]}          [POST /system/superadmins   and   PUT /system/superadmins/{id}]
    */
   export function createSuperAdmin( token, adminId, formData ) {

       return function( dispatch ) {

         dispatch( getRequest( GET_SUPER_ADMIN_CONST.POST_SUPER_ADMIN_REQUEST ) );
         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         const postData = qs.stringify({
                               id: adminId,
                               first_name: formData.first_name,
                               last_name: formData.last_name,
                               email: formData.email,
                               status: formData.status,
                               timezone: formData.timezone
                           });


        const url = (adminId===null) ? SUPER_ADMINS_API : `${SUPER_ADMINS_API}/${adminId}`;
        let callAdminsAPI = (adminId===null) ? AXIOS_INSTANCE.post(url, postData, config):  AXIOS_INSTANCE.put(url, postData, config);
        callAdminsAPI.then(checkHttpStatus)
                                        .then(parseJSON)
                                         .then(function ( response ) {
                                           try {
                                             dispatch(getSuccess(GET_SUPER_ADMIN_CONST.POST_SUPER_ADMIN_SUCCESS, response));
                                             dispatch(successHandler('Success', {
                                               response: {
                                                 status: 200, //success
                                                 statusText: 'Super admin saved successfully.',
                                                 data: null
                                               }
                                             }));
                                           } catch (e) {
                                             //console.log("createUser ");
                                           }
                                         })
                                         .catch(function ( error ) {
                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch(getFailure(GET_SUPER_ADMIN_CONST.POST_SUPER_ADMIN_FAILURE, error));
                                               dispatch(errorHandler('Failure', {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: error.response.data.message
                                                 }
                                               }));
                                             }
                                         });
       }
   }

   export function getUpdate( REQUEST, data ) {
   	return {
         type : REQUEST,
         payload: data
     	}
   }


   export function setSuperAdminFormValues( data ) {
     return function( dispatch ) {
       dispatch( getUpdate( GET_SUPER_ADMIN_CONST.UPDATE_SUPER_ADMIN_FORM_VALUES, data ) );
     }
   }

   /**
    * [getSuperAdminById Returns a single Superadmin by id]
    * @param  {[type]} token   [description]
    * @param  {[type]} adminId [description]
    * @return {[api]}         [GET /system/superadmins/{id}]
    */
   export function getSuperAdminById( token, adminId ) {

     return function( dispatch ) {

         dispatch( getRequest( GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_BYID_REQUEST ) );
         let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         AXIOS_INSTANCE.get( `${SUPER_ADMINS_API}/${adminId}`, config )
                                         .then(function ( response ) {
                                           try {
                                             dispatch( getSuccess( GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_BYID_SUCCESS, response.data.data ) );
                                           } catch (e) {
                                             //console.log("catch : getDepartmenById");
                                           }
                                         })
                                         .catch(function ( error ) {
                                               if(error.response.data.statusCode===401) {
                                                 dispatch(tokenExpired());
                                               } else {
                                                 dispatch( getFailure( GET_SUPER_ADMIN_CONST.GET_SUPER_ADMIN_BYID_FAILURE, error ) );
                                                 dispatch(errorHandler('Failure', {
                                                   response: {
                                                     status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                     errorType: error.response.data.error,        //Forbidden
                                                     statusText: error.response.data.message
                                                   }
                                                 }));
                                               }
                                         });
       }
   }
