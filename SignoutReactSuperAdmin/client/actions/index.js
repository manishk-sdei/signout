/**
 * @action        : index.js
 * @description   : Handles all actions and get data from apis
 * @Created by    : smartData
 */

import { AUTH, LOGIN_API } from './Constants';
import React from 'react';
import axios from 'axios';

export function getAuthToken() {

	let accessToken = localStorage.getItem('access_token');
	return {
		type: AUTH_CONST.GET_AUTHTOKEN,
		payload:request
	};
}
