/**
 * @action        : SignoutActions
 * @description   : Handles all service signout actions
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         SIGNOUT_CONST,
         TOKEN_BEARER,
         GET_SIGNOUT_API,
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE ) {
  	return {
      	type : FAILURE,
        payload : []
    	}
  }

  /**
   * [getSignoutList Returns a list of signout-lists]
   * @param  {[type]} token     [description]
   * @param  {[type]} serviceId [description]
   * @return {[type]}           [description]
   */
  export function getSignoutList( token, serviceId, userId ) {

       return function( dispatch ) {

         dispatch( getRequest( SIGNOUT_CONST.GET_SIGNOUT_LIST_REQUEST ) );
         let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          let serviceFlag = (serviceId === null || serviceId === undefined) ? `` : `serviceId=${serviceId}`;
          let userFlag = (userId === null || userId === undefined) ? `` : `userId=${userId}`;
          let togatherFlag = (serviceId !== null && (userId !== null)) ? `/` : ``;

          let url = `${GET_SIGNOUT_API}?${serviceFlag}${userFlag}`;

          AXIOS_INSTANCE.get(url, config)
                                         .then(function ( response ) {
                                           try {
                                             dispatch( getSuccess( SIGNOUT_CONST.GET_SIGNOUT_LIST_SUCCESS, response.data.data ) );
                                           } catch (e) {
                                             console.log('getSignoutList');
                                           }
                                         })
                                         .catch(function ( error ) {

                                           if(error.response.data.statusCode===401) {
                                             dispatch(tokenExpired());
                                           } else {
                                             dispatch( getFailure( SIGNOUT_CONST.GET_SIGNOUT_LIST_FAILURE ) );
                                             dispatch(errorHandler(SIGNOUT_CONST.GET_SIGNOUT_LIST_FAILURE, {
                                               response: {
                                                 status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                 errorType: error.response.data.error,        //Forbidden
                                                 statusText: 'No data'
                                               }
                                             }));
                                           }
                                         });
       }
   }


   export function createServiceSignouts( token, serviceId, signoutId, formData ) {

       return function( dispatch ) {

         dispatch( getRequest( SIGNOUT_CONST.POST_SIGNOUT_REQUEST ) );
         let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         const postData = qs.stringify({
                               id: signoutId,
                               service_id: serviceId,
                               name: formData.name,
                               description: formData.description
                           });

         const url = (signoutId===null) ? GET_SIGNOUT_API : GET_SIGNOUT_API+'/'+signoutId;
         (signoutId===null) ? AXIOS_INSTANCE.post(url, postData, config):  AXIOS_INSTANCE.put(url, postData, config)
                                        .then(checkHttpStatus)
                                        .then(parseJSON)
                                         .then(function ( response ) {
                                           try {
                                             dispatch( getSuccess( SIGNOUT_CONST.POST_SIGNOUT_SUCCESS, response ) );
                                             dispatch(successHandler(SIGNOUT_CONST.POST_SIGNOUT_SUCCESS, {
                                               response: {
                                                   status: 200, //success
                                                   statusText: 'Signout saved successfully.',
                                                 //data: response.data
                                               }
                                             }));
                                           } catch (e) {
                                             console.log("createServiceSignouts");
                                           }
                                         })
                                         .catch(function ( error ) {
                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch(errorHandler(SIGNOUT_CONST.POST_SIGNOUT_FAILURE, {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: error.response.data.message
                                                 }
                                               }));
                                             }
                                         });
       }
   }


   export function deleteServiceSignouts( token, signoutId ) {
     return function( dispatch ) {

       dispatch( getRequest( SIGNOUT_CONST.DELETE_SIGNOUT_REQUEST ) );
       let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

       const url = GET_SIGNOUT_API+'/'+signoutId;
       AXIOS_INSTANCE.delete(url, config)
                                      .then(checkHttpStatus)
                                      .then(parseJSON)
                                       .then(function ( response ) {
                                         try {
                                           dispatch(getSuccess(response.data));
                                           dispatch(successHandler(SIGNOUT_CONST.DELETE_SIGNOUT_SUCCESS, {
                                             response: {
                                               status: 200, //success
                                               statusText: 'Signout deleted successfully.',
                                               data: response.data
                                             }
                                           }));
                                         } catch (e) {
                                           console.log("createRole catch error : ");
                                         }
                                       })
                                       .catch(function ( error ) {
                                           if(error.response.data.statusCode===401) {
                                             dispatch(tokenExpired());
                                           } else {
                                             dispatch(errorHandler(SIGNOUT_CONST.DELETE_SIGNOUT_FAILURE, {
                                               response: {
                                                 status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                 errorType: error.response.data.error,        //Forbidden
                                                 statusText: error.response.data.message
                                               }
                                             }));
                                           }
                                       });
     }
   }


    export function getUpdate( REQUEST, data ) {
     return {
         type : REQUEST,
         payload: data
       }
    }

    export function updateSignoutInitialState( data ) {
      return function( dispatch ) {
        dispatch( getUpdate( SIGNOUT_CONST.UPDATE_SIGNOUT_FORM_VALUES, data ) );
      }
    }

  /**
   * [addSignoutGroups Add a group to a signout list]
   * @param {[type]} token     [description]
   * @param {[type]} signoutId [description]
   * @param {[type]} groupId   [description]
   * @param {[api]} groupName [POST /signout-lists/{id}/groups]
   */
  export function addSignoutGroups( token, signoutId, groupId, groupName ) {

      return function( dispatch ) {

        dispatch( getRequest( SIGNOUT_CONST.POST_SIGNOUT_GROUP_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

        const postData = {
                              group_id: groupId,
                              signout_list_id: signoutId
                          };

        AXIOS_INSTANCE.post(`${GET_SIGNOUT_API}/${signoutId}/groups`, postData, config)
                                       .then(checkHttpStatus)
                                       .then(parseJSON)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( SIGNOUT_CONST.POST_SIGNOUT_GROUP_SUCCESS, response ) );
                                            dispatch(successHandler(SIGNOUT_CONST.POST_SIGNOUT_GROUP_SUCCESS, {
                                              response: {
                                                  status: 200, //success
                                                  statusText: groupName + ' group saved successfully for this Signout List.',
                                                //data: response.data
                                              }
                                            }));
                                          } catch (e) {
                                            console.log("createServiceSignouts");
                                          }
                                        })
                                        .catch(function ( error ) {
                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch(errorHandler(SIGNOUT_CONST.POST_SIGNOUT_GROUP_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                        });
        }
    }

    /**
     * [getSignoutListById Returns a single signout list by id]
     * @param  {[type]} token     [description]
     * @param  {[type]} signoutId [description]
     * @return {[api]}           [GET /signout-lists/{id}]
     */
    export function getSignoutListById( token, signoutId ) {

         return function( dispatch ) {

           dispatch( getRequest( SIGNOUT_CONST.GET_SIGNOUT_LIST_BYID_REQUEST ) );
           let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

            AXIOS_INSTANCE.get(`${GET_SIGNOUT_API}/${signoutId}`, config)
                                           .then(function ( response ) {
                                             try {
                                               dispatch( getSuccess( SIGNOUT_CONST.GET_SIGNOUT_LIST_BYID_SUCCESS, response.data.data ) );
                                             } catch (e) {
                                               console.log('getSignoutListById');
                                             }
                                           })
                                           .catch(function ( error ) {

                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch( getFailure( SIGNOUT_CONST.GET_SIGNOUT_LIST_BYID_FAILURE ) );
                                               dispatch(errorHandler('Failure', {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: 'No data'
                                                 }
                                               }));
                                             }
                                           });
         }
     }

     /**
      * [deleteSignoutGroup Remove a group from a signout list]
      * @param  {[type]} token     [description]
      * @param  {[type]} signoutId [description]
      * @param  {[type]} groupId   [description]
      * @return {[api]}           [DELETE /signout-lists/{id}/groups/{groupId}]
      */
     export function deleteSignoutGroup( token, signoutId, groupId ) {
       return function( dispatch ) {

       dispatch( getRequest( SIGNOUT_CONST.DELETE_SIGNOUT_GROUP_REQUEST ) );
       let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

       AXIOS_INSTANCE.delete(`${GET_SIGNOUT_API}/${signoutId}/groups/${groupId}`, config)
                                      .then(checkHttpStatus)
                                      .then(parseJSON)
                                       .then(function ( response ) {
                                         try {
                                           dispatch(getSuccess(SIGNOUT_CONST.DELETE_SIGNOUT_GROUP_SUCCESS, response.data));
                                           dispatch(successHandler('Success', {
                                             response: {
                                               status: 200, //success
                                               statusText: 'Group deleted successfully.',
                                               data: response.data
                                             }
                                           }));
                                         } catch (e) {
                                           console.log("createRole catch error : ");
                                         }
                                       })
                                       .catch(function ( error ) {
                                           if(error.response.data.statusCode===401) {
                                             dispatch(tokenExpired());
                                           } else {
                                             dispatch(getSuccess(SIGNOUT_CONST.DELETE_SIGNOUT_GROUP_FAILURE, error));
                                             dispatch(errorHandler('Failure', {
                                               response: {
                                                 status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                 errorType: error.response.data.error,        //Forbidden
                                                 statusText: error.response.data.message
                                               }
                                             }));
                                           }
                                       });
     }
   }

    /**
     * [createSignoutEncounters Add a patient encounter to a signout list]
     * @param  {[type]} token     [description]
     * @param  {[type]} signoutId [description]
     * @param  {[type]} formData  [description]
     * @return {[api]}           [POST /signout-lists/{id}/encounters]
     */
    export function createSignoutEncounters( token, signoutId, formData ) {

        return function( dispatch ) {

          dispatch( getRequest( SIGNOUT_CONST.POST_SIGNOUT_ENCOUNTER_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          // "patient_id": "41419488-c4cd-4d8a-af3a-9ab22692d7fa",
          //   "signout_list_id": "ad8b299f-578c-457c-883c-053ff46a687f",
          //   "external_encounter_id": "12345",
          //   id: formData.id,
          const postData = {
                              patient_id: formData.patient_id,
                              signout_list_id: signoutId
                            };

          AXIOS_INSTANCE.post(`${GET_SIGNOUT_API}/${signoutId}/encounters`, postData, config)
                                         .then(checkHttpStatus)
                                         .then(parseJSON)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( SIGNOUT_CONST.POST_SIGNOUT_ENCOUNTER_SUCCESS, response ) );
                                              dispatch(successHandler('Success', {
                                                response: {
                                                    status: 200, //success
                                                    statusText: 'Patient encounter successfully added to signout list'
                                                }
                                              }));
                                            } catch (e) {
                                              console.log("createServiceSignouts");
                                            }
                                          })
                                          .catch(function ( error ) {
                                              if(error.response.data.statusCode===401) {
                                                dispatch(tokenExpired());
                                              } else {
                                                dispatch(errorHandler(SIGNOUT_CONST.POST_SIGNOUT_ENCOUNTER_FAILURE, {
                                                  response: {
                                                    status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                    errorType: error.response.data.error,        //Forbidden
                                                    statusText: error.response.data.message
                                                  }
                                                }));
                                              }
                                          });
        }
    }

    export function getEditorSuccess( SUCCESS, data ) {
    	return {
        	type : SUCCESS,
        	payload : data
      	}
    }

    export function setDraftEditorValues(values) {
      return function( dispatch ) {
        dispatch( getEditorSuccess( SIGNOUT_CONST.GET_DRAFTJS_EDITOR_VALUES, values ) );
      }
    }


    /**
     * [updateSignoutEncounters Update a patient encounter on signout list]
     * @param  {[type]} token     [description]
     * @param  {[type]} signoutId [description]
     * @param  {[type]} formData  [description]
     * @return {[api]}           [PATCH /signout-lists/{id}/encounters/{encounterId}]
     */
    export function updateSignoutEncounters( token, patientData, version, editorValues, formData ) {

        return function( dispatch ) {

          dispatch( getRequest( SIGNOUT_CONST.PUT_SIGNOUT_ENCOUNTER_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          // "patient_id": "41419488-c4cd-4d8a-af3a-9ab22692d7fa",
          //   "signout_list_id": "ad8b299f-578c-457c-883c-053ff46a687f",
          //   "external_encounter_id": "12345",
          //   id: formData.id,

          // {
          //     "patient_id": "41419488-c4cd-4d8a-af3a-9ab22692d7fa",
          //     "signout_list_id": "ad8b299f-578c-457c-883c-053ff46a687f",
          //     "external_encounter_id": "11122",
          //     "version": 1,
          //     "field_data": [
          //       {
          //         "signout_list_field_template_id": 100000,
          //         "field_value": "No consults"
          //       },
          //       {
          //         "signout_list_field_template_id": 100001,
          //         "field_value": "No PMH"
          //       }
          //     ]
          //   }

          //console.log("version--", version);

          const postData = {
                              patient_id: patientData.patient_id,
                              signout_list_id: patientData.signout_list_id,
                              external_encounter_id : formData.external_encounter_id,
                              last_version: version,
                              field_data: editorValues
                            };

          AXIOS_INSTANCE.put(`${GET_SIGNOUT_API}/${patientData.signout_list_id}/encounters/${patientData.id}`, postData, config)
                                         .then(checkHttpStatus)
                                         .then(parseJSON)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( SIGNOUT_CONST.PUT_SIGNOUT_ENCOUNTER_SUCCESS, response ) );
                                              dispatch(successHandler('Success', {
                                                response: {
                                                    status: 200, //success
                                                    statusText: 'Patient encounter successfully added to signout list'
                                                }
                                              }));

                                              dispatch( getSignoutListById(token, patientData.signout_list_id) );
                                            } catch (e) {
                                              console.log("createServiceSignouts");
                                            }
                                          })
                                          .catch(function ( error ) {
                                              if(error.response.data.statusCode===401) {
                                                dispatch(tokenExpired());
                                              } else {
                                                dispatch(errorHandler(SIGNOUT_CONST.PUT_SIGNOUT_ENCOUNTER_FAILURE, {
                                                  response: {
                                                    status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                    errorType: error.response.data.error,        //Forbidden
                                                    statusText: error.response.data.message
                                                  }
                                                }));
                                              }
                                          });
        }
    }

    /**
     * [getSignoutEncounters Returns a single signout list patient encounter by id]
     * @param  {[type]} token     [description]
     * @param  {[type]} signoutId [description]
     * @return {[api]}           [GET /signout-lists/{id}/encounters/{encounterId]
     */
    export function getSignoutEncounters( token, signoutId, encounterId ) {

         return function( dispatch ) {

          dispatch( getRequest( SIGNOUT_CONST.GET_SIGNOUT_ENCOUNTERS_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          AXIOS_INSTANCE.get(`${GET_SIGNOUT_API}/${signoutId}/encounters/${encounterId}`, config)
                                           .then(function ( response ) {
                                             try {
                                               dispatch( getSuccess( SIGNOUT_CONST.GET_SIGNOUT_ENCOUNTERS_SUCCESS, response.data.data ) );
                                             } catch (e) {
                                               console.log('getSignoutListById');
                                             }
                                           })
                                           .catch(function ( error ) {

                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch( getFailure( SIGNOUT_CONST.GET_SIGNOUT_ENCOUNTERS_FAILURE ) );
                                               dispatch(errorHandler('Failure', {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: 'No data'
                                                 }
                                               }));
                                             }
                                           });
         }
     }

     /**
      * [deleteSignoutEncounters Remove a patient encounter from a signout list]
      * @param  {[type]} token       [description]
      * @param  {[type]} signoutId   [description]
      * @param  {[type]} encounterId [description]
      * @return {[api]}             [DELETE /signout-lists/{id}/encounters/{encounterId}]
      */
     export function deleteSignoutEncounters( token, signoutId, encounterId ) {
       return function( dispatch ) {

         dispatch( getRequest( SIGNOUT_CONST.DELETE_SIGNOUT_ENCOUNTERS_REQUEST ) );
         let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.delete(`${GET_SIGNOUT_API}/${signoutId}/encounters/${encounterId}`, config)
                                        .then(checkHttpStatus)
                                        .then(parseJSON)
                                         .then(function ( response ) {
                                           try {
                                             dispatch(getSuccess(response.data));
                                             dispatch(successHandler(SIGNOUT_CONST.DELETE_SIGNOUT_ENCOUNTERS_SUCCESS, {
                                               response: {
                                                 status: 200, //success
                                                 statusText: 'Signout deleted successfully.',
                                                 data: response.data
                                               }
                                             }));
                                           } catch (e) {
                                             console.log("createRole catch error : ");
                                           }
                                         })
                                         .catch(function ( error ) {
                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch(errorHandler(SIGNOUT_CONST.DELETE_SIGNOUT_ENCOUNTERS_FAILURE, {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: error.response.data.message
                                                 }
                                               }));
                                             }
                                         });
       }
     }
