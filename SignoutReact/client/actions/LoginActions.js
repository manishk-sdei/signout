/**
 * @action        : LoginActions
 * @description   : Handles all login actions
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import Alert from 'react-s-alert';
import { successHandler, errorHandler } from './ErrorHandler';

import { AUTH_CONST,
         AXIOS_INSTANCE,
         BASIC_AUTHTOKEN,
         LOGIN_API,
         RESET_PASSWORD_API,
         LOCALS_STORAGE_AUTHTOKEN
       } from './Constants';

//handle state and redirection if user is successfully logged in
export function loginUserSuccess(token) {
  localStorage.setItem('authToken', token);
  return {
    type: AUTH_CONST.LOGIN_USER_SUCCESS,
    payload: {
      token: token
    }
  }
}

//handle state in case of failure of user login
export function loginUserFailure(error) {

  Alert.error(error.response.statusText, {
       position: 'top-right',
       effect: 'jelly',
       timeout: 500
  });
  return {
    type: AUTH_CONST.LOGIN_USER_FAILURE,
    payload: {
      status: error.response.status,
      statusText: error.response.statusText
    }
  }
}

//handle state when request is send and resposen is awaited
export function loginUserRequest() {
  return {
    type: AUTH_CONST.LOGIN_USER_REQUEST
  }
}

// Login
export function loginUser( creds, redirect="/" ) {

    return function(dispatch) {

      dispatch(loginUserRequest());

      let config = { 'headers' : { 'Authorization': BASIC_AUTHTOKEN } };

      const postData = qs.stringify({
                            grant_type: 'password',
                            username: creds.email,//'billy.walters@test.com',
                            password: creds.password,//'B@conator',
                            tenant_slug: 'demo1',
                            scope: 'epihealth-web'
                        });

      AXIOS_INSTANCE.post(LOGIN_API, postData, config)
      .then(checkHttpStatus)
      .then(parseJSON)
      .then(function (response) {
          dispatch(loginUserSuccess(response.access_token));
          browserHistory.push('/signoutlist');
      })
      .catch(function (error) {
          dispatch(loginUserFailure({
            response: {
              status: 403,
              statusText: 'Invalid email and password, try again!'
            }
          }));
      });
    }
}


//----------------------------------------

export function forgotPasswordRequest() {
  return {
    type: AUTH_CONST.FORGOT_PASSWORD_REQUEST
  }
}

export function forgotPasswordSuccess(success) {//console.log("rrrrrr", response);

  return {
    type: AUTH_CONST.FORGOT_PASSWORD_SUCCESS,
    payload: {
      statusCode: success.response.statusCode,
      statusText: success.response.statusText
    }
  }
}

export function forgotPasswordFailure(error) {

  return {
    type: AUTH_CONST.FORGOT_PASSWORD_FAILURE,
    payload: {
      statusCode: error.response.statusCode,
      statusText: error.response.statusText
    }
  }
}

/**
 * [forgotPassword Request a password reset]
 * @param  {[type]} formData       [email]
 * @param  {String} [redirect="/"] [description]
 * @return {[type]}                [description]
 */
export function forgotPassword( formData, redirect="/" ) {

    return function(dispatch) {

      dispatch(forgotPasswordRequest());

      let config = { 'headers' : { 'Authorization': BASIC_AUTHTOKEN } };

      const postData = qs.stringify({
                            email: formData.email,
                            tenant_slug: 'demo1',
                        });

      AXIOS_INSTANCE.post(RESET_PASSWORD_API, postData, config)
      .then(checkHttpStatus)
      .then(parseJSON)
      .then(function (success) {

            dispatch(forgotPasswordSuccess({
              response: {
                statusCode: success.statusCode,
                statusText: success.message
              }
            }));

          //browserHistory.push('/login');
      })
      .catch(function (error) {//console.log(error);
          dispatch(forgotPasswordFailure({
            response: {
              statusCode: error.statusCode,
              statusText: error.statusText
            }
          }));
      });
    }
}

//--------------------------------------------------------------------

export function resetPasswordRequest() {
  return {
    type: AUTH_CONST.RESET_PASSWORD_REQUEST
  }
}

export function resetPasswordSuccess(success) {//console.log("rrrrrr", response);

  return {
    type: AUTH_CONST.RESET_PASSWORD_SUCCESS,
    payload: {
      statusCode: success.response.statusCode,
      statusText: success.response.statusText
    }
  }
}

export function resetPasswordFailure(error) {

  return {
    type: AUTH_CONST.RESET_PASSWORD_FAILURE,
    payload: {
      statusCode: error.response.statusCode,
      statusText: error.response.statusText,
      details: error.response.details
    }
  }
}

//here: /reset-password?reset_token=0QplxhWG34eBQdftBDNeeK_QdnpEXCWVJRdlL01CazzmSLeO
//http://203.129.220.75/reset-password?reset_token=0QplxhWG34eBQdftBDNeeK_QdnpEXCWVJRdlL01CazzmSLeO

export function resetPassword( formData, resetToken ) {console.log(resetToken);

    return function(dispatch) {

      dispatch(resetPasswordRequest());

      let config = { 'headers' : { 'Authorization': BASIC_AUTHTOKEN } };

      const postData = qs.stringify({
                            reset_token: resetToken,
                            new_password: formData.new_password,
                            tenant_slug: 'demo1',
                        });

      AXIOS_INSTANCE.put(RESET_PASSWORD_API, postData, config)
      .then(checkHttpStatus)
      .then(parseJSON)
      .then(function (success) {

            dispatch(resetPasswordSuccess({
              response: {
                statusCode: success.statusCode,
                statusText: success.message
              }
            }));

          //browserHistory.push('/login');
      })
      .catch(function (error) {
          dispatch(resetPasswordFailure({
            response: {
              statusCode: error.response.data.statusCode,
              statusText: error.response.data.message,
              details : error.response.data.details
            }
          }));
      });
    }
}

//logout and redirect the user to login page
export function logoutAndRedirect() {

    return (dispatch, state) => {
        //dispatch(logout());
          dispatch(logout());
          browserHistory.push('/');
    }
}

//remove any login data saved
export function logout() {
    localStorage.removeItem('authToken');
    return {
        type: AUTH_CONST.LOGOUT_USER,
        payload: {
          status: 200,
          statusText: 'You have been successfully logged out.'
        }
    }
}

// remove any login data saved on expire token and redirect to login
export function tokenExpired() {

      return (dispatch, state) => {
        dispatch(tokenExpiredStatus({
          response: {
            status: 401,
            statusText: 'Unauthorized! Unable to complete authentication!'
          }
        }));
        //setTimeout(function(){
        //browserHistory.push('/');
        //}, 3000);
      }

}

export function tokenExpiredStatus(error) {

  localStorage.removeItem('authToken');
  localStorage.removeItem('index');
  Alert.error(error.response.statusText, {
       position: 'top-right',
       effect: 'jelly',
       timeout: 500
  });
  browserHistory.push('/');

  return {
    type: AUTH_CONST.TOKEN_EXPIRED,
    payload: {
      status: error.response.status,
      statusText: error.response.statusText
    }
  }
}


export function getUpdate( REQUEST ) {
 return {
     type : REQUEST
   }
}

export function statusUpdate() {
  return function( dispatch ) {
    if(localStorage.getItem('authToken') !== null) {
      browserHistory.push('/signoutlist');
    }
    dispatch( getUpdate( AUTH_CONST.STATUS_UPDATE ) );
  }
}
