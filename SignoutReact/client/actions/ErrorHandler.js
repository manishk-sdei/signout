/**
 * @action        : LoginActions
 * @description   : Handles all login actions
 * @Created by    : smartData
 */

import React from 'react';
import { browserHistory } from 'react-router';
import Alert from 'react-s-alert';

  //@TODO requestHandler for common handler for all request actions
  export function requestHandler( REQUEST, response ) {
      type: REQUEST
  }

  //@TODO check for props passing
  export function successHandler( SUCCESS, success ) {
    
    let alertText = success.response.statusText;

    Alert.success(alertText, {
        timeout: 1000,
    });
  }

  /**
   * [errorHandler description]
   * @param  {[type]} FAILURE [description]
   * @param  {[type]} error   [description]
   * @return {[type]}         [description]
   * @TODO Error handing for validation - api
   */
  export function errorHandler(FAILURE, error) {

        //let alertText = error.response.status + error.response.errorType  +'! <br/>'+ error.response.statusText;
        let alertText = error.response.statusText;
        Alert.error(alertText, {
            timeout: 1000,
            html: true
        });
  }
