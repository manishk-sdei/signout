/**
 * @action        : DepartmentsActions
 * @description   : Handles all departments
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         DEPARTMENT_CONST,
         GET_DEPARTMENTS_API,
         TOKEN_BEARER
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE, error ) {

    return {
      type: FAILURE,
      payload: {
        status: error.response.status,
        statusText: error.response.statusText
      }
    }
  }

  /**
   * [getDepartmens Returns a list of departments]
   * @param  {[type]} token [description]
   * @return {[type]}       [JSON]
   */
  export function getDepartmens( token ) {

      return function( dispatch ) {

        dispatch( getRequest( DEPARTMENT_CONST.GET_DEPARTMENT_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_DEPARTMENTS_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( DEPARTMENT_CONST.GET_DEPARTMENT_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            //console.log("catch : getDepartmens");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(DEPARTMENT_CONST.GET_DEPARTMENT_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  /**
   * [getDepartmenById Returns a single department by id]
   * @param  {[type]} departmentId [description]
   * @return {[type]}              [description]
   */
  export function getDepartmenById( departmentId, token ) {

    return function( dispatch ) {

        dispatch( getRequest( DEPARTMENT_CONST.GET_DEPARTMENT_BYID_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };
        let url = GET_DEPARTMENT_API + '/' + departmentId;

        AXIOS_INSTANCE.get( url, config )
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( DEPARTMENT_CONST.GET_DEPARTMENT_BYID_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getDepartmenById");
                                          }
                                        })
                                        .catch(function ( error ) {
                                              if(error.response.data.statusCode===401) {
                                                dispatch(tokenExpired());
                                              } else {
                                                dispatch(errorHandler(DEPARTMENT_CONST.GET_DEPARTMENT_BYID_FAILURE, {
                                                  response: {
                                                    status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                    errorType: error.response.data.error,        //Forbidden
                                                    statusText: error.response.data.message
                                                  }
                                                }));
                                              }
                                        });
      }
  }

  /**
   * [createDepartment Create a new department / Update a department]
   * @param  {[type]} tenantId [description]
   * @param  {[type]} deptId   [description]
   * @param  {[type]} token    [description]
   * @param  {[type]} formData [description]
   * @return {[type]}          [description]
   */
  export function createDepartment( tenantId, deptId, token, formData ) {

      return function( dispatch ) {

        dispatch( getRequest( DEPARTMENT_CONST.POST_DEPARTMENT_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

        const postData = qs.stringify({
                              id: deptId,
                              tenant_id: tenantId,
                              name: formData.name,
                              description: formData.description
                          });

        const url = (deptId===null) ? GET_DEPARTMENTS_API : GET_DEPARTMENTS_API+'/'+deptId;
        (deptId===null) ? AXIOS_INSTANCE.post(url, postData, config):  AXIOS_INSTANCE.put(url, postData, config)
                                       .then(checkHttpStatus)
                                       .then(parseJSON)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( DEPARTMENT_CONST.POST_DEPARTMENT_SUCCESS, response ) );
                                            dispatch(successHandler(DEPARTMENT_CONST.POST_DEPARTMENT_SUCCESS, {
                                              response: {
                                                  status: 200, //success
                                                  statusText: 'Department saved successfully.',
                                                //data: response.data
                                              }
                                            }));
                                          } catch (e) {
                                            //console.log("createDepartment");
                                          }
                                        })
                                        .catch(function ( error ) {console.log("llll : ", error);
                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch(errorHandler(DEPARTMENT_CONST.POST_DEPARTMENT_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                        });
      }
  }

  export function getUpdate( REQUEST, data ) {
  	return {
        type : REQUEST,
        payload: data
    	}
  }

  /**
   * [updateDepartment update initial form values name and description in departmentreducer]
   * @param  {[type]} data [name and description]
   * @return {[type]}      [description]
   */
  export function updateDepartment( data ) {
    return function( dispatch ) {
      dispatch( getUpdate( DEPARTMENT_CONST.UPDATE_DEPARTMENT_FORM_VALUES, data ) );
    }
  }
