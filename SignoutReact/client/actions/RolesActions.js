/**
 * @action        : RolesActions
 * @description   : Handles all role based actions
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         ROLES_CONST,
         GET_ROLES_API,
         TOKEN_BEARER
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE, error ) {
    return {
      type: FAILURE,
      payload: {
        status: error.response.status,
        statusText: error.response.statusText
      }
    }
  }

  /**
   * [getRoles description]
   * @param  {[type]} token [description]
   * @return {[type]}       [description]
   */
  export function getRoles( token ) {

      return function(dispatch) {

        dispatch(getRequest(ROLES_CONST.GET_ROLES_REQUEST));

        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_ROLES_API, config)
                                        .then(function (response) {
                                          try {
                                            dispatch(getSuccess(ROLES_CONST.GET_ROLES_SUCCESS, response.data.data));
                                          } catch (e) {
                                            dispatch(getFailure(ROLES_CONST.GET_ROLES_FAILURE, {
                                              response: {
                                                status: 403,
                                                statusText: 'Unable to fetch data.'
                                              }
                                            }));
                                          }
                                        })
                                        .catch(function (error) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(ROLES_CONST.POST_ROLES_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }


  export function createRole( roleId, formData, token ) {

      return function( dispatch ) {

        dispatch( getRequest( ROLES_CONST.POST_ROLES_REQUEST ) );
        let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

        const postData = qs.stringify({
                              id:   roleId,
                              name: formData.name,
                              description: formData.description
                          });

        const url = (roleId===null) ? GET_ROLES_API : `${GET_ROLES_API}/${roleId}`;
        let callRoleAPI = (roleId===null) ? AXIOS_INSTANCE.post(url, postData, config):  AXIOS_INSTANCE.put(url, postData, config);
        callRoleAPI.then(checkHttpStatus)
                                       .then(parseJSON)
                                        .then(function ( response ) {console.log("role-res---", response);
                                          try {
                                            dispatch(getSuccess(ROLES_CONST.POST_ROLES_SUCCESS, response.data));
                                            dispatch(successHandler('Success', {
                                              response: {
                                                status: 200, //success
                                                statusText: 'Role saved successfully.',
                                                data: response.data
                                              }
                                            }));
                                          } catch (e) {
                                            console.log("createRole catch error : ");
                                          }
                                        })
                                        .catch(function ( error ) {
                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch(errorHandler('Failure', {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                        });
      }
  }

  /**
   * [deleteRole delete a role]
   * @param  {[type]} token  [description]
   * @param  {[type]} roleId [description]
   * @return {[type]}        [description]
   */
  export function deleteRole( token, roleId ) {console.log(token, roleId);
    return function( dispatch ) {

      dispatch( getRequest( ROLES_CONST.DELETE_ROLES_REQUEST ) );
      let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

      const url = GET_ROLES_API+'/'+roleId;
      AXIOS_INSTANCE.delete(url, config)
                                     .then(checkHttpStatus)
                                     .then(parseJSON)
                                      .then(function ( response ) {
                                        try {
                                          dispatch(getSuccess(response.data));
                                          dispatch(successHandler(ROLES_CONST.DELETE_ROLES_SUCCESS, {
                                            response: {
                                              status: 200, //success
                                              statusText: 'Role deleted successfully.',
                                              data: response.data
                                            }
                                          }));
                                        } catch (e) {
                                          console.log("createRole catch error : ");
                                        }
                                      })
                                      .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(ROLES_CONST.DELETE_ROLES_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                      });
    }
  }


  export function getUpdate( REQUEST, data ) {
    return {
        type : REQUEST,
        payload: data
      }
  }

  export function updateRole( data ) {
    return function( dispatch ) {
      dispatch( getUpdate( ROLES_CONST.UPDATE_ROLE_FORM_VALUES, data ) );
    }
  }
