/**
 * @action        : TermsActions
 * @description   : Handles terms like(cities, states, countries, gender etc.)
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         TERM_CONST,
         GET_COUNTRIES_API,
         GET_LANGUAGES_API,
         GET_MERITALSTATUS_API,
         GET_RACE_API,
         GET_SEX_API,
         GET_TIMEZONES_API,
         TOKEN_BEARER
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE, error ) {

    return {
      type: FAILURE,
      payload: {
        status: error.response.status,
        statusText: error.response.statusText
      }
    }
  }

  export function getCountries( token ) {

      return function( dispatch ) {

        dispatch( getRequest( TERM_CONST.GET_COUNTRIES_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_COUNTRIES_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( TERM_CONST.GET_COUNTRIES_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getCountries");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(TERM_CONST.GET_COUNTRIES_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getLanguages( token ) {

      return function( dispatch ) {

        dispatch( getRequest( TERM_CONST.GET_LANGUAGES_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_LANGUAGES_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( TERM_CONST.GET_LANGUAGES_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getCountries");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(TERM_CONST.GET_LANGUAGES_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getMeritalStatus( token ) {

      return function( dispatch ) {

        dispatch( getRequest( TERM_CONST.GET_MERITALSTATUS_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_MERITALSTATUS_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( TERM_CONST.GET_MERITALSTATUS_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getCountries");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(TERM_CONST.GET_MERITALSTATUS_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getRace( token ) {

      return function( dispatch ) {

        dispatch( getRequest( TERM_CONST.GET_RACE_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_RACE_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( TERM_CONST.GET_RACE_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getCountries");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(TERM_CONST.GET_RACE_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getSex( token ) {

      return function( dispatch ) {

        dispatch( getRequest( TERM_CONST.GET_SEX_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_SEX_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( TERM_CONST.GET_SEX_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getCountries");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(TERM_CONST.GET_SEX_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getTimeZone( token ) {

      return function( dispatch ) {

        dispatch( getRequest( TERM_CONST.GET_TIMEZONES_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         AXIOS_INSTANCE.get(GET_TIMEZONES_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( TERM_CONST.GET_TIMEZONES_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getCountries");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(TERM_CONST.GET_TIMEZONES_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  //GET /terms/countries
  //GET /terms/languages
  //GET /terms/marital-status
  //GET /terms/race
  //GET /terms/sex
  //GET /terms/timezones
