/**
 * @action        : PatientsActions
 * @description   : Handles all patients
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         PATIENTS_CONST,
         GET_PATIENTS_API,
         TOKEN_BEARER
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE, error ) {

    return {
      type: FAILURE,
      payload: {
        status: error.response.status,
        statusText: error.response.statusText
      }
    }
  }

  export function countPatients( token ) {

      return function( dispatch ) {

          dispatch( getRequest( PATIENTS_CONST.GET_PATIENTS_REQUEST ) );
          let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         AXIOS_INSTANCE.get(GET_PATIENTS_API, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( PATIENTS_CONST.COUNT_PATIENTS_SUCCESS, response.data.data.length ) );
                                          } catch (e) {
                                            console.log("catch : getPatients");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(PATIENTS_CONST.GET_PATIENTS_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getPatientsWithPagination( token, query ) {

      return function( dispatch ) {

          dispatch( getRequest( PATIENTS_CONST.GET_PATIENTS_REQUEST ) );
          let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         AXIOS_INSTANCE.get(`${GET_PATIENTS_API}${query}`, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( PATIENTS_CONST.GET_PATIENTS_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getPatients");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(PATIENTS_CONST.GET_PATIENTS_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getPatients( token, searchArr ) {

      return function( dispatch ) {

          dispatch( getRequest( PATIENTS_CONST.GET_PATIENTS_REQUEST ) );
          let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

         let finalSearchStr = (searchArr === undefined) ? `` : qs.stringify(searchArr);
         let search = `?${finalSearchStr}`;

         AXIOS_INSTANCE.get(`${GET_PATIENTS_API}${search}`, config)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( PATIENTS_CONST.GET_PATIENTS_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getPatients");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(PATIENTS_CONST.GET_PATIENTS_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }

  export function getPatientById( patientId, token ) {

    return function( dispatch ) {

        dispatch( getRequest( PATIENTS_CONST.GET_PATIENT_BYID_REQUEST ) );
        let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };
        let url = GET_PATIENTS_API + '/' + patientId;

        AXIOS_INSTANCE.get( url, config )
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( PATIENTS_CONST.GET_PATIENT_BYID_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("catch : getDepartmenById");
                                          }
                                        })
                                        .catch(function ( error ) {
                                              if(error.response.data.statusCode===401) {
                                                dispatch(tokenExpired());
                                              } else {
                                                dispatch(errorHandler(PATIENTS_CONST.GET_PATIENT_BYID_FAILURE, {
                                                  response: {
                                                    status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                    errorType: error.response.data.error,        //Forbidden
                                                    statusText: error.response.data.message
                                                  }
                                                }));
                                              }
                                        });
      }
  }

  /**
   * [createPatient description]
   * @param  {[type]} token     [description]
   * @param  {[type]} tenantId  [description]
   * @param  {[type]} patientId [description]
   * @param  {[type]} formData  [description]
   * @return {[type]}           [description]
   */
  export function createPatient( token, tenantId, patientId, formData ) {

      return function( dispatch ) {

        dispatch( getRequest( PATIENTS_CONST.POST_PATIENTS_REQUEST ) );
        let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

        const postData = {
                            id: patientId,
                            first_name: formData.first_name,
                            middle_name: formData.middle_name,
                            last_name: formData.last_name,
                            dob: formData.dob,
                            ssn: formData.ssn,
                            mrn: formData.mrn,
                            sex: formData.sex,
                            demographics: {
                                race: formData.race,
                                martial_status: formData.marital_status,
                                is_deceased: formData.is_deceased,
                                death_datetime: formData.death_datetime,
                                phone_number: {
                                  home: formData.home_phone,
                                  office: formData.office_phone,
                                  mobile: formData.mobile_phone,
                                },
                                email_addresses: [formData.email],
                                language: formData.language,
                                address: {
                                  street_address: formData.street_address,
                                  city: formData.city,
                                  state: formData.state,
                                  zip: formData.zip,
                                  county: formData.county,
                                  country: formData.country
                                }
                            }
                          };

        const url = (patientId===null) ? `${GET_PATIENTS_API}` : `${GET_PATIENTS_API}/${patientId}`;
        let callAPICreatePatient = (patientId===null) ? AXIOS_INSTANCE.post(url, postData, config):  AXIOS_INSTANCE.patch(url, postData, config);
        callAPICreatePatient.then(checkHttpStatus)
                                       .then(parseJSON)
                                        .then(function ( response ) {
                                          try {
                                            dispatch(getSuccess(PATIENTS_CONST.POST_PATIENTS_SUCCESS, response));
                                            dispatch(successHandler('Success', {
                                              response: {
                                                  status: 200, //success
                                                  statusText: 'Patient saved successfully.',
                                              }
                                            }));
                                          } catch (e) {
                                            console.log("createPatient");
                                          }
                                        })
                                        .catch(function ( error ) {
                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch(getFailure(PATIENTS_CONST.POST_PATIENTS_FAILURE, error));
                                              dispatch(errorHandler('Failure', {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                        });
      }
  }

  export function getUpdate( REQUEST, data ) {
  	return {
        type : REQUEST,
        payload: data
    	}
  }


  export function updatePatient( data ) {
    return function( dispatch ) {
      dispatch( getUpdate( PATIENTS_CONST.UPDATE_PATIENTS_FORM_VALUES, data ) );
    }
  }


/**
 * [deletePatient Delete / Remove a patient]
 * @param  {[type]} token     [description]
 * @param  {[type]} patientId [description]
 * @return {[type]}           [description]
 */
export function deletePatient( token, patientId ) {
  return function( dispatch ) {

    dispatch( getRequest( PATIENTS_CONST.DELETE_PATIENTS_REQUEST ) );
    let config = { 'headers': { 'Authorization': `${TOKEN_BEARER}${token}` } };

    AXIOS_INSTANCE.delete(`${GET_PATIENTS_API}/${patientId}`, config)
                                   .then(checkHttpStatus)
                                   .then(parseJSON)
                                    .then(function ( response ) {
                                      try {
                                        dispatch(getSuccess(PATIENTS_CONST.DELETE_PATIENTS_SUCCESS, response));
                                        dispatch(successHandler('Success', {
                                          response: {
                                            status: 200, //success
                                            statusText: 'Patient deleted successfully.',
                                          }
                                        }));
                                      } catch (e) {
                                        console.log("catch : ");
                                      }
                                    })
                                    .catch(function ( error ) {
                                        if(error.response.data.statusCode===401) {
                                          dispatch(tokenExpired());
                                        } else {
                                          dispatch(getSuccess(PATIENTS_CONST.DELETE_PATIENTS_FAILURE, error));
                                          dispatch(errorHandler('Failure', {
                                            response: {
                                              status: error.response.data.statusCode, //error status 401 / 403 / 500
                                              errorType: error.response.data.error,        //Forbidden
                                              statusText: error.response.data.message
                                            }
                                          }));
                                        }
                                    });
  }
}
