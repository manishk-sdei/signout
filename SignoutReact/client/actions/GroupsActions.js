/**
 * @action        : GroupsActions
 * @description   : Handles all service groups
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         GROUPS_CONST,
         TOKEN_BEARER,
         GET_GROUPS_API,
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE ) {
  	return {
      	type : FAILURE,
        payload : []
    	}
  }

  /**
   * [getServiceGroups Returns a list of groups]
   * @param  {[type]} token     [description]
   * @param  {[type]} serviceId [description]
   * @return {[type]}           [description]
   */
  export function getServiceGroups( token, serviceId ) {

       return function( dispatch ) {

         dispatch( getRequest( GROUPS_CONST.GET_SERVICE_GROUPS_REQUEST ) );
         let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          let url = GET_GROUPS_API + '?serviceId=' + serviceId;

          AXIOS_INSTANCE.get(url, config)
                                         .then(function ( response ) {
                                           try {
                                             dispatch( getSuccess( GROUPS_CONST.GET_SERVICE_GROUPS_SUCCESS, response.data.data ) );
                                           } catch (e) {
                                             console.log('getServiceGroups');
                                           }
                                         })
                                         .catch(function ( error ) {

                                           if(error.response.data.statusCode===401) {
                                             dispatch(tokenExpired());
                                           } else {
                                             dispatch( getFailure( GROUPS_CONST.GET_SERVICE_GROUPS_FAILURE ) );
                                             dispatch(errorHandler(GROUPS_CONST.GET_SERVICE_GROUPS_FAILURE, {
                                               response: {
                                                 status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                 errorType: error.response.data.error,        //Forbidden
                                                 statusText: 'No data'
                                               }
                                             }));
                                           }
                                         });
       }
   }


   /**
    * [getServiceGroupById Returns a single group by id]
    * @param  {[type]} token   [description]
    * @param  {[type]} groupId [description]
    * @return {[api]}         [GET /groups/{id}]
    */
   export function getServiceGroupById( token, groupId ) {

        return function( dispatch ) {

          dispatch( getRequest( GROUPS_CONST.GET_SERVICE_GROUPS_BYID_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

           AXIOS_INSTANCE.get(`${GET_GROUPS_API}/groupId`, config)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( GROUPS_CONST.GET_SERVICE_GROUPS_BYID_SUCCESS, response.data.data ) );
                                            } catch (e) {
                                              console.log('getServiceGroups');
                                            }
                                          })
                                          .catch(function ( error ) {

                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch( getFailure( GROUPS_CONST.GET_SERVICE_GROUPS_BYID_FAILURE ) );
                                              dispatch(errorHandler(GROUPS_CONST.GET_SERVICE_GROUPS_BYID_FAILURE, {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: 'No data'
                                                }
                                              }));
                                            }
                                          });
        }
    }

   /**
    * [createServiceGroups Create a new group and Update a group]
    * @param  {[type]} token     [description]
    * @param  {[type]} serviceId [description]
    * @param  {[type]} groupId   [description]
    * @param  {[type]} formData  [description]
    * @return {[type]}           [description]
    */
   export function createServiceGroups( token, serviceId, groupId, formData ) {

       return function( dispatch ) {

         dispatch( getRequest( GROUPS_CONST.POST_SERVICE_GROUPS_REQUEST ) );
         let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         const postData = {
                               id: groupId,
                               service_id: serviceId,
                               name: formData.name,
                               description: formData.description,
                               is_on_call: formData.is_on_call
                           };

         const url = (groupId===null) ? GET_GROUPS_API : GET_GROUPS_API+'/'+groupId;
         let groupAPICall = (groupId===null) ? AXIOS_INSTANCE.post(url, postData, config) :  AXIOS_INSTANCE.put(url, postData, config);
         groupAPICall.then(checkHttpStatus)
                                        .then(parseJSON)
                                         .then(function ( response ) {console.log("mmmm---", response);
                                           try {
                                             dispatch( getSuccess( GROUPS_CONST.POST_SERVICE_GROUPS_SUCCESS, response ) );
                                             dispatch(successHandler('Success', {
                                               response: {
                                                   status: 200, //success
                                                   statusText: 'Group saved successfully.'
                                               }
                                             }));
                                           } catch (e) {
                                             //console.log("createServiceGroups");
                                           }
                                         })
                                         .catch(function ( error ) {
                                             if(error.response.data.statusCode===401) {
                                               dispatch(tokenExpired());
                                             } else {
                                               dispatch(errorHandler('Failure', {
                                                 response: {
                                                   status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                   errorType: error.response.data.error,        //Forbidden
                                                   statusText: error.response.data.message
                                                 }
                                               }));
                                             }
                                         });
       }
   }

   /**
    * [deleteServiceGroups Remove a group]
    * @param  {[type]} token   [description]
    * @param  {[type]} groupId [description]
    * @return {[api]}         [DELETE /groups/{id}]
    */
   export function deleteServiceGroups( token, groupId ) {
     return function( dispatch ) {

       dispatch( getRequest( GROUPS_CONST.DELETE_SERVICE_GROUPS_REQUEST ) );
       let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

       AXIOS_INSTANCE.delete(`${GET_GROUPS_API}/${groupId}`, config)
                                      .then(checkHttpStatus)
                                      .then(parseJSON)
                                       .then(function ( response ) {
                                         try {
                                           dispatch(getSuccess(response.data));
                                           dispatch(successHandler(GROUPS_CONST.DELETE_SERVICE_GROUPS_SUCCESS, {
                                             response: {
                                               status: 200, //success
                                               statusText: 'Group deleted successfully.',
                                               data: response.data
                                             }
                                           }));
                                         } catch (e) {
                                           console.log("createRole catch error : ");
                                         }
                                       })
                                       .catch(function ( error ) {
                                           if(error.response.data.statusCode===401) {
                                             dispatch(tokenExpired());
                                           } else {
                                             dispatch(errorHandler(GROUPS_CONST.DELETE_SERVICE_GROUPS_FAILURE, {
                                               response: {
                                                 status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                 errorType: error.response.data.error,        //Forbidden
                                                 statusText: error.response.data.message
                                               }
                                             }));
                                           }
                                       });
     }
   }

    export function getUpdate( REQUEST, data ) {
     return {
         type : REQUEST,
         payload: data
       }
    }

    export function updateGroupInitialState( data ) {
      return function( dispatch ) {
        dispatch( getUpdate( GROUPS_CONST.UPDATE_GROUP_FORM_VALUES, data ) );
      }
    }



    /**
     * [addUsersToGroup Add a user to a group]
     * @param {[type]} token    [description]
     * @param {[type]} groupId  [description]
     * @param {[type]} userId   [description]
     * @param {[api]} userName [//POST /groups/{id}/users]
     */
    export function addUsersToGroup( token, groupId, userId, userName) {

        return function( dispatch ) {

          dispatch( getRequest( GROUPS_CONST.ADD_USERS_TO_GROUP_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          const postData = {
                              user_id: userId,
                              group_id: groupId
                          };

          AXIOS_INSTANCE.post(`${GET_GROUPS_API}/${groupId}/users`, postData, config)
                                         .then(checkHttpStatus)
                                         .then(parseJSON)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( GROUPS_CONST.ADD_USERS_TO_GROUP_SUCCESS, response ));
                                              dispatch(successHandler('Success', {
                                                response: {
                                                    status: 200, //success
                                                    statusText: userName + ' user saved successfully for this group.',
                                                  //data: response.data
                                                }
                                              }));
                                            } catch (e) {
                                              console.log("createServiceSignouts");
                                            }
                                          })
                                          .catch(function ( error ) {
                                              if(error.response.data.statusCode===401) {
                                                dispatch(tokenExpired());
                                              } else {
                                                dispatch(getFailure(GROUPS_CONST.ADD_USERS_TO_GROUP_FAILURE, error));
                                                dispatch(errorHandler('Failure', {
                                                  response: {
                                                    status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                    errorType: error.response.data.error,        //Forbidden
                                                    statusText: error.response.data.message
                                                  }
                                                }));
                                              }
                                          });
        }
    }

    /**
     * [deleteGroupUsers Remove a user from a group]
     * @param  {[type]} token   [description]
     * @param  {[type]} user_id [description]
     * @return {[api]}         [DELETE /groups/{id}/users/{userId}]
     */
    export function deleteGroupUsers( token, groupId, userId ) {
      return function( dispatch ) {

        dispatch( getRequest( GROUPS_CONST.DELETE_GROUP_USER_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

        AXIOS_INSTANCE.delete(`${GET_GROUPS_API}/${groupId}/users/${userId}`, config)
                                       .then(checkHttpStatus)
                                       .then(parseJSON)
                                        .then(function ( response ) {
                                          try {
                                            dispatch(getSuccess(GROUPS_CONST.DELETE_GROUP_USER_SUCCESS, response.data));
                                            dispatch(successHandler('Success', {
                                              response: {
                                                status: 200, //success
                                                statusText: 'Group deleted successfully.',
                                                data: response.data
                                              }
                                            }));
                                          } catch (e) {
                                            console.log("createRole catch error : ");
                                          }
                                        })
                                        .catch(function ( error ) {
                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch(getSuccess(GROUPS_CONST.DELETE_GROUP_USER_FAILURE, error));
                                              dispatch(errorHandler('Failure', {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                        });
      }
    }
