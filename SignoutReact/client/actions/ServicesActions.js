/**
 * @action        : ServicesActions
 * @description   : Handles all services actions
 * @Created by    : smartData
 */

import React from 'react';
import qs from 'qs';
import { checkHttpStatus, parseJSON } from '../utils';
import { browserHistory } from 'react-router';
import { tokenExpired } from './LoginActions';
import { successHandler, errorHandler } from './ErrorHandler';

import { AXIOS_INSTANCE,
         SERVICES_CONST,
         GET_SERVICE_API,
         TOKEN_BEARER
       } from './Constants';

  /**
   * [getRequest description]
   * @param  {[type]} REQUEST [description]
   * @return {[type]}         [description]
   */
  export function getRequest( REQUEST ) {
  	return {
        type : REQUEST
    	}
  }

  /**
   * [getSuccess description]
   * @param  {[type]} SUCCESS [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [JSON]
   */
  export function getSuccess( SUCCESS, data ) {
  	return {
      	type : SUCCESS,
      	payload : data
    	}
  }

  /**
   * [getFailure description]
   * @param  {[type]} FAILURE [description]
   * @return {[type]}         [description]
   */
  export function getFailure( FAILURE ) {
  	return {
      	type : FAILURE
    	}
  }

  /**
   * [getServices Returns a list of services]
   * @param  {[type]} token [description]
   * @return {[type]}       [JSON]
   */
   export function getServices( token, selfEnrollment = "", urlFlag ) {

       return function( dispatch ) {

         dispatch( getRequest( SERVICES_CONST.GET_SERVICE_REQUEST ) );
         let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

         let getUrlFlag = "";
         if(selfEnrollment !== "") {
            getUrlFlag = `?selfEnrollment=${selfEnrollment}`;
         }

         if(urlFlag) {
           getUrlFlag = urlFlag;
         }

         let flag = 0;
          AXIOS_INSTANCE.get(`${GET_SERVICE_API}${getUrlFlag}`, config)
                                         .then(function ( response ) {
                                           try {
                                             dispatch( getSuccess( SERVICES_CONST.GET_SERVICE_SUCCESS, response.data.data ) );
                                           } catch (e) {
                                             dispatch(getFailure(SERVICES_CONST.GET_SERVICE_FAILURE, {
                                               response: {
                                                 status: 403,
                                                 statusText: 'Unable to fetch data.'
                                               }
                                             }));
                                           }
                                         })
                                         .catch(function ( error ) {
                                           if(flag > 0) {
                                             // Handle expire token
                                             if(error.response.data.statusCode===401) {// Handle expire token
                                               dispatch(tokenExpired());
                                             }
                                           }
                                           flag = flag + 1;
                                         });

       }
   }

  /**
   * [getServiceById Returns a single service by id]
   * @param  {[type]} serviceId [description]
   * @return {[type]}              [description]
   */
  export function getServiceById( token, serviceId ) {

    return function( dispatch ) {

        dispatch( getRequest( SERVICES_CONST.GET_SERVICE_BYID_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

        let url = GET_SERVICE_API + '/' + serviceId;

        AXIOS_INSTANCE.get( url, config )
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( SERVICES_CONST.GET_SERVICE_BYID_SUCCESS, response.data.data ) );
                                          } catch (e) {
                                            console.log("getServiceById");
                                          }
                                        })
                                        .catch(function ( error ) {
                                          if(error.response.data.statusCode===401) {
                                            dispatch(tokenExpired());
                                          } else {
                                            dispatch(errorHandler(SERVICES_CONST.GET_SERVICE_BYID_FAILURE, {
                                              response: {
                                                status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                errorType: error.response.data.error,        //Forbidden
                                                statusText: error.response.data.message
                                              }
                                            }));
                                          }
                                        });
      }
  }


  export function createService( serviceId, token, formData ) {

      return function( dispatch ) {

        dispatch( getRequest( SERVICES_CONST.GET_SERVICE_REQUEST ) );
        let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

        const postData = {
                              id: serviceId,
                              department_id: formData.departments,
                              name: formData.name,
                              description: formData.description,
                              is_self_enrollment: (formData.is_self_enrollment === undefined) ? false : formData.is_self_enrollment
                          };

        const url = (serviceId===null) ? `${GET_SERVICE_API}` : `${GET_SERVICE_API}/${serviceId}`;
        (serviceId===null) ? AXIOS_INSTANCE.post(url, postData, config):  AXIOS_INSTANCE.put(url, postData, config)
                                       .then(checkHttpStatus)
                                       .then(parseJSON)
                                        .then(function ( response ) {
                                          try {
                                            dispatch( getSuccess( SERVICES_CONST.POST_SERVICE_SUCCESS, 'data' ) );
                                            dispatch(successHandler('Success', {
                                              response: {
                                                  status: 200, //success
                                                  statusText: 'Service saved successfully.',
                                                //data: response.data
                                              }
                                            }));
                                          } catch (e) {
                                            console.log("createDepartment");
                                          }
                                        })
                                        .catch(function ( error ) {
                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch( getSuccess( SERVICES_CONST.POST_SERVICE_FAILURE, error ) );
                                              dispatch(errorHandler('Failure', {
                                                response: {
                                                  status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                  errorType: error.response.data.error,        //Forbidden
                                                  statusText: error.response.data.message
                                                }
                                              }));
                                            }
                                        });
      }
  }


  export function getUpdate( REQUEST, data ) {
   return {
       type : REQUEST,
       payload: data
     }
  }

  export function updateServiceInitialState( departmentId, data ) {
    data['departmentId'] = departmentId;

    return function( dispatch ) {
      dispatch( getUpdate( SERVICES_CONST.UPDATE_SERVICE_FORM_VALUES, data ) );
    }
  }

    /**
     * [getServiceSignoutTemplates Returns a list of signout list field templates]
     * @param  {[type]} token     [access token]
     * @param  {[type]} serviceId [description]
     * @return [api]           [GET /services/{id}/signout-list-field-templates]
     */
    export function getServiceSignoutTemplates( token, serviceId ) {

        return function( dispatch ) {

          dispatch( getRequest( SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

           AXIOS_INSTANCE.get(`${GET_SERVICE_API}/${serviceId}/signout-list-field-templates`, config)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_SUCCESS, response.data.data ) );
                                            } catch (e) {
                                              console.log('getSignoutList');
                                            }
                                          })
                                          .catch(function ( error ) {

                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch( getFailure( SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_FAILURE ) );
                                              // dispatch(errorHandler('Failure', {
                                              //   response: {
                                              //     status: error.response.data.statusCode, //error status 401 / 403 / 500
                                              //     errorType: error.response.data.error,        //Forbidden
                                              //     statusText: 'No data'
                                              //   }
                                              // }));
                                            }
                                          });
        }
    }

    export function getServiceSignoutTemplatesEditValues( token, serviceId ) {

        return function( dispatch ) {

          dispatch( getRequest( SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_EDIT_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

           AXIOS_INSTANCE.get(`${GET_SERVICE_API}/${serviceId}/signout-list-field-templates`, config)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_EDIT_SUCCESS, response.data.data ) );
                                            } catch (e) {
                                              console.log('getSignoutList');
                                            }
                                          })
                                          .catch(function ( error ) {

                                            if(error.response.data.statusCode===401) {
                                              dispatch(tokenExpired());
                                            } else {
                                              dispatch( getFailure( SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_EDIT_FAILURE ) );
                                              // dispatch(errorHandler('Failure', {
                                              //   response: {
                                              //     status: error.response.data.statusCode, //error status 401 / 403 / 500
                                              //     errorType: error.response.data.error,        //Forbidden
                                              //     statusText: 'No data'
                                              //   }
                                              // }));
                                            }
                                          });
        }
    }


    /**
     * [createServiceSignoutTemplates Note: This is a bulk PUT endpoint. You MUST post an array of ALL field templates at once.]
     * Update signout list field templates for a service.
     * - For existing field templates, include the id in order to UPDATE.
     * - For new field templates, do not include the id.
     * - To delete a field template, simply omit it from the array.
     * @param  {[type]} token     [description]
     * @param  {[type]} serviceId [description]
     * @param  {[type]} formData  [description]
     * @return {[api]}           [/services/{id}/signout-list-field-templates]
     */
    export function createServiceSignoutTemplates( token, serviceId, formData ) {

        return function( dispatch ) {

          dispatch( getRequest( SERVICES_CONST.PUT_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_REQUEST ) );
          let config = { 'headers': { 'Authorization': TOKEN_BEARER + token } };

          const postData = {
                            data: formData
                          };

          AXIOS_INSTANCE.put(`${GET_SERVICE_API}/${serviceId}/signout-list-field-templates`, postData, config)
                                         .then(checkHttpStatus)
                                         .then(parseJSON)
                                          .then(function ( response ) {
                                            try {
                                              dispatch( getSuccess( SERVICES_CONST.PUT_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_SUCCESS, 'data' ) );
                                              dispatch(successHandler('Success', {
                                                response: {
                                                    status: 200, //success
                                                    statusText: 'Patient encounter successfully added to signout list.',
                                                }
                                              }));
                                            } catch (e) {
                                              console.log("createDepartment");
                                            }
                                          })
                                          .catch(function ( error ) {
                                              if(error.response.data.statusCode===401) {
                                                dispatch(tokenExpired());
                                              } else {
                                                dispatch( getSuccess( SERVICES_CONST.PUT_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_FAILURE, error ) );
                                                dispatch(errorHandler('Failure', {
                                                  response: {
                                                    status: error.response.data.statusCode, //error status 401 / 403 / 500
                                                    errorType: error.response.data.error,        //Forbidden
                                                    statusText: error.response.data.message
                                                  }
                                                }));
                                              }
                                          });
        }
    }
