/**
 * @reducer       : SignoutReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { SIGNOUT_CONST } from '../actions/Constants';

const initialState = {
    data: [],
    isFetching: true,
    signoutListById: [],
    isFetchingById: true,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null,
    editorValues: [],
    checkPatients: [],
    name: null,
    description: null,
    signoutEncounters: []
};

export default createReducer(initialState, {
    [SIGNOUT_CONST.GET_SIGNOUT_LIST_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_LIST_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_LIST_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_LIST_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetchingById': true
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_LIST_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'signoutListById': payload,
            'isFetchingById': false
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_LIST_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetchingById': false
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.UPDATE_SIGNOUT_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'name': payload!==undefined ? payload.name : null,
            'description': payload!==undefined ? payload.description : null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_GROUP_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_GROUP_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_GROUP_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_GROUP_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_GROUP_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_GROUP_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_ENCOUNTER_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_ENCOUNTER_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.POST_SIGNOUT_ENCOUNTER_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.GET_DRAFTJS_EDITOR_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'editorValues': payload,
        });
    },
    [SIGNOUT_CONST.PUT_SIGNOUT_ENCOUNTER_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.PUT_SIGNOUT_ENCOUNTER_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.PUT_SIGNOUT_ENCOUNTER_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_ENCOUNTERS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetchingById': true
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_ENCOUNTERS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'signoutEncounters': payload,
            'isFetchingById': false
        });
    },
    [SIGNOUT_CONST.GET_SIGNOUT_ENCOUNTERS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetchingById': false
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_ENCOUNTERS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_ENCOUNTERS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });
    },
    [SIGNOUT_CONST.DELETE_SIGNOUT_ENCOUNTERS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    }
});
