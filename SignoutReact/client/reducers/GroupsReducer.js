/**
 * @reducer       : GroupsReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { GROUPS_CONST } from '../actions/Constants';

const initialState = {
    data: [],
    groupById: [],
    isFetching: true,
    isFetchingById: true,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null,

    name: null,
    description: null,
    is_on_call: false
};

export default createReducer(initialState, {
    [GROUPS_CONST.GET_SERVICE_GROUPS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [GROUPS_CONST.GET_SERVICE_GROUPS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [GROUPS_CONST.GET_SERVICE_GROUPS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [GROUPS_CONST.GET_SERVICE_GROUPS_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetchingById': true
        });
    },
    [GROUPS_CONST.GET_SERVICE_GROUPS_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'groupById': payload,
            'isFetchingById': false
        });
    },
    [GROUPS_CONST.GET_SERVICE_GROUPS_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetchingById': false
        });
    },
    [GROUPS_CONST.POST_SERVICE_GROUPS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [GROUPS_CONST.POST_SERVICE_GROUPS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [GROUPS_CONST.POST_SERVICE_GROUPS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [GROUPS_CONST.UPDATE_GROUP_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'name': payload!==undefined ? payload.name : null,
            'description': payload!==undefined ? payload.description : null,
            'is_on_call': payload!==undefined ? payload.is_on_call : false
        });
    },
    [GROUPS_CONST.DELETE_SERVICE_GROUPS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [GROUPS_CONST.DELETE_SERVICE_GROUPS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'data': payload,
            'statusText': null
        });

    },
    [GROUPS_CONST.DELETE_SERVICE_GROUPS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [GROUPS_CONST.ADD_USERS_TO_GROUP_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [GROUPS_CONST.ADD_USERS_TO_GROUP_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [GROUPS_CONST.ADD_USERS_TO_GROUP_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [GROUPS_CONST.DELETE_GROUP_USER_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [GROUPS_CONST.DELETE_GROUP_USER_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'data': payload,
            'statusText': null
        });

    },
    [GROUPS_CONST.DELETE_GROUP_USER_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    }
});
