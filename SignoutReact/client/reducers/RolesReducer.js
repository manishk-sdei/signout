/**
 * @reducer       : RollsReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { ROLES_CONST } from '../actions/Constants';

const initialState = {
    data: null,
    isFetching: true,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null,

    name: null,
    description: null,
};

export default createReducer(initialState, {
    [ROLES_CONST.GET_ROLES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [ROLES_CONST.GET_ROLES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [ROLES_CONST.GET_ROLES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [ROLES_CONST.POST_ROLES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [ROLES_CONST.POST_ROLES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': null
        });

    },
    [ROLES_CONST.POST_ROLES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [ROLES_CONST.DELETE_ROLES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [ROLES_CONST.DELETE_ROLES_SUCCESS]: (state, payload) => {console.log(payload);
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'data': payload,
            'statusText': null
        });

    },
    [ROLES_CONST.DELETE_ROLES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [ROLES_CONST.UPDATE_ROLE_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'name': payload!==undefined ? payload.name : null,
            'description': payload!==undefined ? payload.description : null
        });
    }

});
