/**
 * @reducer       : index reducer
 * @description   :
 * @Created by    : smartData
 */

import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer/LoginReducer';
import UsersReducer from './UsersReducer';
import DepartmentsReducer from './DepartmentsReducer';
import ServicesReducer from './ServicesReducer';
import RolesReducer from './RolesReducer';
import GroupsReducer from './GroupsReducer';
import SignoutReducer from './SignoutReducer';
import PatientReducer from './PatientsReducer';
import TermsReducer from './TermsReducer';

import { reducer as formReducer } from 'redux-form';   //SAYING use redux form reducer as reducer

const rootReducer = combineReducers({
  form        : formReducer,
  login       : LoginReducer,
  users       : UsersReducer,
  departments : DepartmentsReducer,
  services    : ServicesReducer,
  roles       : RolesReducer,
  groups      : GroupsReducer,
  signouts    : SignoutReducer,
  patients    : PatientReducer,
  terms       : TermsReducer
});

export default rootReducer;
