/**
 * @reducer       : DepartmentsReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { DEPARTMENT_CONST } from '../actions/Constants';

const initialState = {
    data: [],
    getDepartmentById: null,
    isFetching: true,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null,

    name: null,
    description: null,
};

export default createReducer(initialState, {
    [DEPARTMENT_CONST.GET_DEPARTMENT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [DEPARTMENT_CONST.GET_DEPARTMENT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [DEPARTMENT_CONST.GET_DEPARTMENT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [DEPARTMENT_CONST.POST_DEPARTMENT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [DEPARTMENT_CONST.POST_DEPARTMENT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [DEPARTMENT_CONST.POST_DEPARTMENT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [DEPARTMENT_CONST.UPDATE_DEPARTMENT_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'name': payload!==undefined ? payload.name : null,
            'description': payload!==undefined ? payload.description : null
        });
    },
    [DEPARTMENT_CONST.GET_DEPARTMENT_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [DEPARTMENT_CONST.GET_DEPARTMENT_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'getDepartmentById': payload,
            'isFetching': false
        });
    },
    [DEPARTMENT_CONST.GET_DEPARTMENT_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    }


});
