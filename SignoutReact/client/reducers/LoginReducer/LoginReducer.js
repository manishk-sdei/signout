/**
 * @reducer       : login reducer
 * @description   :
 * @Created by    : smartData
 */

 import { createReducer } from '../../utils';
 import { AUTH_CONST } from '../../actions/Constants';

 const initialState = {
     token: null,
     isAuthenticated: false,
     isAuthenticating: false,
     statusCode: null,
     statusText: null,
     details: []
 };

 export default createReducer(initialState, {
     [AUTH_CONST.LOGIN_USER_REQUEST]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': true,
             'statusText': null
         });
     },
     [AUTH_CONST.LOGIN_USER_SUCCESS]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': true,
             'token': payload.token,
             'statusText': null
         });

     },
     [AUTH_CONST.LOGIN_USER_FAILURE]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': false,
             'token': null,
             'statusText': payload.statusText
         });
     },
     [AUTH_CONST.LOGOUT_USER]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticated': false,
             'token': null,
             'statusText': payload.statusText
         });
     },
     [AUTH_CONST.TOKEN_EXPIRED]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticated': false,
             'token': null,
             'statusText': payload.statusText
         });
     },
     [AUTH_CONST.FORGOT_PASSWORD_REQUEST]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': true,
             'statusText': null
         });
     },
     [AUTH_CONST.FORGOT_PASSWORD_SUCCESS]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': true,
             'statusCode': payload.statusCode,
             'statusText': payload.statusText
         });

     },
     [AUTH_CONST.FORGOT_PASSWORD_FAILURE]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': false,
             'statusCode': payload.statusCode,
             'statusText': payload.statusText
         });
     },
     [AUTH_CONST.RESET_PASSWORD_REQUEST]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': true,
             'statusText': null
         });
     },
     [AUTH_CONST.RESET_PASSWORD_SUCCESS]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': true,
             'statusCode': payload.statusCode,
             'statusText': payload.statusText
         });

     },
     [AUTH_CONST.RESET_PASSWORD_FAILURE]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': false,
             'statusCode': payload.statusCode,
             'statusText': payload.statusText,
             'details' : payload.details
         });
     },
     [AUTH_CONST.STATUS_UPDATE]: (state, payload) => {
         return Object.assign({}, state, {
             'isAuthenticating': false,
             'isAuthenticated': false,
             'statusCode': null,
             'statusText': null,
             'details' : []
         });
     }
 });
