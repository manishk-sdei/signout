/**
 * @reducer       : PatientsReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { PATIENTS_CONST } from '../actions/Constants';

const initialState = {
    data: [],
    totalPatients: null,
    getDepartmentById: null,
    isFetching: true,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null,

    first_name: null,
    middle_name: null,
    last_name: null,
    dob: null,
    mrn: null
};

export default createReducer(initialState, {
    [PATIENTS_CONST.GET_PATIENTS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [PATIENTS_CONST.GET_PATIENTS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [PATIENTS_CONST.COUNT_PATIENTS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'totalPatients': payload,
            'isFetching': false
        });
    },
    [PATIENTS_CONST.GET_PATIENTS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [PATIENTS_CONST.POST_PATIENTS_REQUEST]: (state, payload) => {console.log('reducerReq');
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [PATIENTS_CONST.POST_PATIENTS_SUCCESS]: (state, payload) => {console.log('reducerSucces');
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'statusText': payload.statusText
        });
    },
    [PATIENTS_CONST.POST_PATIENTS_FAILURE]: (state, payload) => {console.log('reducerFail');
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [PATIENTS_CONST.UPDATE_PATIENTS_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'first_name': payload!==undefined ? payload.first_name : null,
            'middle_name': payload!==undefined ? payload.middle_name : null,
            'last_name': payload!==undefined ? payload.last_name : null,
            'dob': payload!==undefined ? payload.dob : null,
            'mrn': payload!==undefined ? payload.mrn : null
        });
    },
    [PATIENTS_CONST.GET_PATIENT_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [PATIENTS_CONST.GET_PATIENT_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'getDepartmentById': payload,
            'isFetching': false
        });
    },
    [PATIENTS_CONST.GET_PATIENT_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [PATIENTS_CONST.DELETE_PATIENTS_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [PATIENTS_CONST.DELETE_PATIENTS_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'data': payload,
            'statusText': null
        });
    },
    [PATIENTS_CONST.DELETE_PATIENTS_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    }
});
