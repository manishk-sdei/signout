/**
 * @reducer       : ServicesReducer
 * @description   :
 * @Created by    : smartData
 */

import { createReducer } from '../utils';
import { SERVICES_CONST } from '../actions/Constants';

const initialState = {
    data: [],
    signoutTemplates: [],
    signoutTemplatesEdit: [],
    serviceById: null,
    isFetching: true,
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null,

    departments: [],
    name: null,
    description: null,
    is_self_enrollment: false
};

export default createReducer(initialState, {
    [SERVICES_CONST.GET_SERVICE_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [SERVICES_CONST.GET_SERVICE_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'data': payload,
            'isFetching': false
        });
    },
    [SERVICES_CONST.GET_SERVICE_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [SERVICES_CONST.POST_SERVICE_REQUEST]: (state, payload) => {console.log('reducer---SReq');
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SERVICES_CONST.POST_SERVICE_SUCCESS]: (state, payload) => {console.log('reducer---SR-Sucss');
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'payload': payload,
            'statusText': payload.statusText
        });
    },
    [SERVICES_CONST.POST_SERVICE_FAILURE]: (state, payload) => {console.log('reducer---SR-Fail');
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SERVICES_CONST.UPDATE_SERVICE_FORM_VALUES]: (state, payload) => {
        return Object.assign({}, state, {
            'departmentId': payload!==undefined ? payload.departmentId : null,
            'name': payload!==undefined ? payload.name : null,
            'description': payload!==undefined ? payload.description : null,
            'is_self_enrollment': payload!==undefined ? payload.is_self_enrollment : false
        });
    },
    [SERVICES_CONST.GET_SERVICE_BYID_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [SERVICES_CONST.GET_SERVICE_BYID_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'serviceById': payload,
            'isFetching': false
        });
    },
    [SERVICES_CONST.GET_SERVICE_BYID_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'signoutTemplates': payload,
            'isFetching': false
        });
    },
    [SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    },
    [SERVICES_CONST.PUT_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [SERVICES_CONST.PUT_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'payload': payload,
            'statusText': payload.statusText
        });
    },
    [SERVICES_CONST.PUT_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'statusText': payload.statusText
        });
    },
    [SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_EDIT_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': true
        });
    },
    [SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_EDIT_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            'signoutTemplatesEdit': payload,
            'isFetching': false
        });
    },
    [SERVICES_CONST.GET_SERVICE_SIGNOUT_LIST_FIELD_TEMPLATES_EDIT_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            'isFetching': false
        });
    }

});
