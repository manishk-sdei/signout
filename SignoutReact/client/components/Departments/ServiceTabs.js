import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import GroupList from '../Groups/GroupsList';
import SignoutList from '../SignoutList/SignoutList';
import SignoutTemplates from '../SignoutList/SignoutTemplates';
import { getServiceById } from '../../actions/ServicesActions';

class ServiceTabs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data : false,
            isActive : null,
            token : null,
            serviceId: null,
            serviceData: []
        };
        this.onTabClick = this.onTabClick.bind(this);
    }

    componentDidMount() {

        document.title = "Signout - Users";
        this.setState({ token: this.props.token });
        this.setState({ serviceId : this.props.params.serviceId });
    }

    componentWillMount() {
      //this.props.getRoles(this.props.token);
      this.setState({ token: this.props.token });
      this.setState({ serviceId : this.props.params.serviceId });
      this.props.getServiceById( this.props.token, this.props.params.serviceId );
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.service && this.props.service !== nextProps.service ) {
            let service = nextProps.service;
            this.setState( { serviceData : service } );
        }
        // else if ( nextProps.service ) {
        //     if ( nextProps.service.length > 0 ) {
        //         let service = nextProps.service;
        //         this.setState( { serviceData : service } );
        //     }
        // }
    }

    onTabClick(event) {

      if(event.target.parentNode.id === 'usertab') {
          this.setState({ isActive : 0 });
      }
      if(event.target.parentNode.id === 'roletab') {
          this.setState({ isActive : 1 });
      }
      if(event.target.parentNode.id === 'templatetab') {
          this.setState({ isActive : 2 });
      }
      event.preventDefault();
    }

    render() {
        return(
          <div id="page-content-wrapper">
        {/*
        <!-- Page Content -->*/}
        <div className="col welcome-bar ">
            {/*
            <!-- welcome bar -->*/}
            <h1 className="page-title">Service - {this.state.serviceData.name}</h1>
            <ul className="breadcrumb pull-right hidden-xs-down">
                <li><Link to="/signoutlist">Sign out</Link></li>
                <li><Link to="/departments">Departments - Services</Link></li>
                <li className="active">Group List & Signout List</li>
            </ul>
        </div>

        <div className="container-fluid mrg-top30 pd-bt30">
            <div className="col pd-off-xs">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-bg overflow-none">
                            <div className="userroll">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item" id="usertab" onClick={(event)=>this.onTabClick(event)}>
                                      <a className={ (this.state.isActive === 0 || this.state.isActive === null) ? "nav-link active" : "nav-link" } data-toggle="tab" role="tab">Groups</a>
                                    </li>
                                    <li className="nav-item" id="roletab" onClick={(event)=>this.onTabClick(event)}>
                                      <a className={ (this.state.isActive === 1) ? "nav-link active" : "nav-link" } data-toggle="tab" role="tab">Signout Lists</a>
                                    </li>
                                    <li className="nav-item" id="templatetab" onClick={(event)=>this.onTabClick(event)}>
                                      <a className={ (this.state.isActive === 2) ? "nav-link active" : "nav-link" } data-toggle="tab" role="tab">Signout Template</a>
                                    </li>
                                </ul>

                                <div className="tab-content">

                                    <div className={ (this.state.isActive === 0 || this.state.isActive === null) ? "tab-pane active" : "tab-pane" } id="adduser" role="tabpanel">
                                        <GroupList token={ this.state.token } serviceId={ this.state.serviceId } />
                                    </div>

                                    <div className={ (this.state.isActive === 1) ? "tab-pane active" : "tab-pane" } id="roll" role="tabpanel">
                                        <SignoutList token={ this.state.token } serviceId={ this.state.serviceId } />
                                    </div>

                                    <div className={ (this.state.isActive === 2) ? "tab-pane active" : "tab-pane" } id="template" role="tabpanel">
                                        <SignoutTemplates token={ this.state.token } serviceId={ this.state.serviceId } />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );

    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.services.isFetching,
         service: state.services.serviceById,
     }
}

export default connect(mapStateToProps, { getServiceById })( ServiceTabs );
