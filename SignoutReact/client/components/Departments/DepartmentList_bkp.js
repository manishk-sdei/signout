import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ModalWindow from '../Modals/AddEditDeptModal';
import ModalWindowService from '../Modals/AddEditServiceModal';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import { Tooltip } from 'reactstrap';

import AddDepartment from './AddDepartment';
import { getLoginUser } from '../../actions/UsersActions';
import { getDepartmens, updateDepartment } from '../../actions/DepartmentActions';
import { updateServiceInitialState } from '../../actions/ServicesActions';

class DepartmentList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {id: null, name: null, description: null},
            departmentsData: [],
            users : [],
            editDepartmentData: [],
            editServiceData: [],
            modal2: false,
            modal1: false,
            departmentId: null,
            token: null,
            tooltipOpen: false,
            isTenantAdmin: false
        };
        this.open_modal = this.open_modal.bind(this);
        this.open_modal2 = this.open_modal2.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Departments";
        this.setState({ token: this.props.token });
    }

    componentWillMount() {
        this.setState({ token: this.props.token });
        this.props.getLoginUser(this.props.token);
        this.props.getDepartmens(this.props.token);
    }

    componentWillReceiveProps( nextProps ) {

       if (nextProps.currentLoginUser) {
          this.setState({ isTenantAdmin : nextProps.currentLoginUser.is_tenant_admin });
       }

        if ( nextProps.departments && this.props.departments !== nextProps.departments ) {
            let departments = nextProps.departments;
            this.setState( { departmentsData : departments } );
        } else if ( nextProps.departments ) {
            if ( nextProps.departments.length > 0 ) {
                let departments = nextProps.departments;
                this.setState( { departmentsData : departments } );
            }
        }

        if ( nextProps.users && this.props.users !== nextProps.users ) {
            let users = nextProps.users;
            this.setState( { users : users } );
        } else if ( nextProps.users ) {
            if ( nextProps.users.length > 0 ) {
                let users = nextProps.users;
                this.setState( { users : users } );
            }
        }

    }

    // Open modal on add and update department
    open_modal(e, data) {

      this.props.getDepartmens(this.state.token);
      this.props.updateDepartment(data); // Update row

      if( e===true ) {
          e.preventDefault();
      } else {

          this.setState({
              modal1: !this.state.modal1,
              editDepartmentData: data,
          });
      }
    }

    // Open modal on add and update service
    open_modal2(e, departmentId, data) {
      this.props.getDepartmens(this.state.token);
      this.props.updateServiceInitialState(departmentId, data); // Update initial value
      if( e===true ) {
          e.preventDefault();
      } else {
          //this.props.updateRow(data);
          this.setState({
              modal2: !this.state.modal2,
              departmentId: departmentId,
              editServiceData: data,
          });
      }
    }

    renderLoading() {
        if(this.props.isFetching) {
          return(
            <tbody>
            <tr>
              <td>
            <div className="loader" id="loader-4">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </td>
        </tr>
      </tbody>
          );
        }
    }

    renderRow() {

      // No department - waiting timeout 3 sec
      let This = this;
      setTimeout(function(){
        if(This.state.departmentsData.length <= 0) {
          return (
            <tbody>
            <tr>
              <td></td>
              <td colSpan="3" >
                  No department.
              </td>
            </tr>
            </tbody>
          );
        }
      }, 3000);

      // No department


      if( this.state.departmentsData ) {
        return this.state.departmentsData.map(( department ) => {

          return (
            <tbody key={department.id}>
            <tr>
                <td>{ department.name }</td>
                <td>{ (department.description!='')?department.description:'n/a' }</td>
                { (this.state.isTenantAdmin === true) ?
                <td>
                  {/**<Tooltip placement="top"
                           isOpen={this.state.tooltipOpen}
                           autohide={false}
                           target="edit_department"
                           toggle={() =>  this.setState({tooltipOpen: !this.state.tooltipOpen})}>
                    Edit Department
                  </Tooltip>**/}

                    <button id="edit_department"
                            onClick={ (e)=>this.open_modal(e, department) }
                            className="action-btn"
                            data-toggle="modal"
                            data-target="#edit-patient1">
                            <i className="zmdi zmdi-edit"></i>
                    </button>
                    {/**<button className="action-btn"><i className="zmdi zmdi-delete"></i></button>**/}
                </td>
                : <td></td> }
            </tr>
            <tr>
                <td className="pd-off" colSpan="3">
                    <table className="table patient-list service-tb">
                        <thead>
                          <tr>
                              <th width="15%"></th>
                              <th width="20%">Service Name</th>
                              <th width="30%">Description</th>
                              <th width="20%">Self enrollment</th>
                              <th width="17%"></th>
                          </tr>
                        </thead>
                        <tbody>
                            { this.renderServices(department.id, department.services) }
                        </tbody>
                    </table>
                </td>
            </tr>
          </tbody>
          );
        });
      }
    }

    renderServices( departmentId, services ) {
      if(services.length <= 0) {
        return (
          <tr>
            <td></td>
            <td colSpan="3">
                No service.
            </td>
          </tr>
        );
      }

      if( services ) {
        return services.map(( service ) => {//console.log(service);

          return (
                <tr key={ service.id }>
                    <td></td>
                    <td>
                        <Link to={`/service-tabs/${service.id}`}>{ service.name }</Link>
                      </td>
                    <td>{ (service.description!='')?service.description:'n/a' }</td>
                    <td>{ service.is_self_enrollment === true ? 'Yes' : 'No' }</td>
                    { (this.state.isTenantAdmin === true) ?
                    <td>
                        <button
                                onClick={ (e)=>this.open_modal2(e, departmentId, service) }
                                className="action-btn"
                                data-toggle="modal"
                                data-target="#edit-patient1">
                                <i className="zmdi zmdi-edit"></i>
                        </button>
                        {/**<button className="action-btn"><i className="zmdi zmdi-delete"></i></button>**/}
                    </td>
                    : <td></td> }
                </tr>
          );
        });
      }
    }

    render() {
        return(
          <div id="page-content-wrapper">
              {/*
              <!-- Page Content -->*/}
              <div className="col welcome-bar ">
                  {/*
                  <!-- welcome bar -->*/}
                  <h1 className="page-title">Department List</h1>
                  <ul className="breadcrumb pull-right hidden-xs-down">
                      <li><Link to={`/signoutlist`}>Sign out</Link></li>
                      <li className="active">Department List</li>
                  </ul>
              </div>

              {/** ModalWindow - with props **/}

              <ModalWindow
                          modal_var = {this.state.modal1}
                          handle_modal = { (e) => this.open_modal(e, this.state.data ) }
                          token = { this.state.token }
                          tenantId = { this.state.users.tenant?this.state.users.tenant.id:'' }
                          department = { this.state.editDepartmentData }  />

              <ModalWindowService
                          modal_var = {this.state.modal2}
                          handle_modal = { (e) => this.open_modal2(e, null, this.state.data) }
                          token = { this.state.token }
                          tenantId = { this.state.users.tenant?this.state.users.tenant.id:'' }
                          departments = { this.state.departmentsData }
                          departmentId = { this.state.departmentId }
                          service = { this.state.editServiceData } />

              <div className="container-fluid mrg-top30 pd-bt30">
                  <div className="col pd-off-xs">
                      <div className="row">
                          <div className="col-sm-12">
                              <div className="white-bg">
                                { (this.state.isTenantAdmin === true) ? <div><ButtonM type="button"
                                         onClick={(e)=>this.open_modal(e, this.state.data)}
                                         className="add-deprt btn blue-btn add-btn"
                                         data-toggle="modal"
                                         data-target="#create-patient">
                                         <span className="hidden-xs-down">Create Department</span>
                                           <img className="hidden-sm-up"
                                                src="/client/assets/images/add-file.png" />
                                </ButtonM>
                                &nbsp;
                                <ButtonM type="button"
                                         onClick={(e)=>this.open_modal2(e, null, this.state.data)}
                                         className="add-deprt btn blue-btn add-btn"
                                         data-toggle="modal"
                                         data-target="#create-patient">
                                         <span className="hidden-xs-down">Create Service</span>
                                           <img className="hidden-sm-up"
                                                src="/client/assets/images/add-file.png" />
                                            </ButtonM></div> : "" }


                                  <div className="table-responsive">
                                      <table className="table patient-list depart-list">
                                          <thead>
                                              <tr>
                                                  <th width="25%">Department Name</th>
                                                  <th width="60%">Description</th>
                                                  { (this.state.isTenantAdmin === true) ?
                                                    <th width="15%">Action</th> : <th width="15%"></th> }
                                              </tr>
                                          </thead>
                                              { this.renderLoading() }
                                              { this.renderRow() }
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

/**
 * [mapStateToProps description]
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function mapStateToProps(state) {
     return {
         isFetching: state.departments.isFetching,
         departments: state.departments.data,
         users : state.users.data
     }
}

export default connect(mapStateToProps, { getLoginUser,
                                          getDepartmens,
                                          updateDepartment,
                                          updateServiceInitialState })(DepartmentList);
