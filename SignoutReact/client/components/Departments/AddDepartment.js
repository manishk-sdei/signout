import React, { Component } from 'react';
import { Field, reduxForm, initialize } from 'redux-form';
import { connect } from 'react-redux';

class AddDepartment extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Add/Edit Departments";
    }

    handleSubmit(creds) {
          this.props.actions.loginUser(creds, this.state.redirectTo);
    }

    render() {
      const { fields: { department_name } } = this.props;
        return(
          <form className="user-add-name" onSubmit={this.handleSubmit}>
            <div className="row form-group">
                <div className="col-sm-9 col-9">
                    <input className="inputMaterial" type="text" required {...department_name} />
                    <span className="highlight"></span> <span className="bar"></span>
                    <label>Department Name</label>
                </div>
                <div className="col-sm-3 col-3">
                   <div className="row"> <button type="submit" className="btn blue-btn sign-btn"><span>Save</span></button></div>
                </div>
            </div>
         </form>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.department_name) {
        error.department_name = 'Please enter department name.';
    }
    return error;
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
// reduxForm: first is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps

// const mapStateToProps = (state) => ({
//   isAuthenticating   : state.login.isAuthenticating,
//   statusText         : state.login.statusText,
//   isAuthenticated    : state.login.isAuthenticated
// });
//
// const mapDispatchToProps = (dispatch) => ({
//   actions : bindActionCreators( { loginUser }, dispatch )
// });

export default reduxForm({
    form: 'SaveDepartmentForm',
    fields: ['department_name'],
    validate
}, null)(AddDepartment);
