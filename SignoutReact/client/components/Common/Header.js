/**
 * @class         :	Header
 * @description   : Dashboard Header
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import CheckinModal from '../Modals/CheckinModal';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem,Button  } from 'reactstrap';
import { logoutUser } from '../../actions/index';
import { logoutAndRedirect } from '../../actions/LoginActions';
import { getLoginUser, getUserServices } from '../../actions/UsersActions';
import { LOCALS_STORAGE_AUTHTOKEN } from '../../actions/Constants';
import Ink from 'react-ink';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';

class Header extends Component {

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.open_modal = this.open_modal.bind(this);
        this.state = {
        dropdownOpen: false,
        toggleProp: 'toggled',
        wrapperToggle: '',
        modal1: false,
        first_name: '',
        last_name: '',
        currentLoginUserData: [],
        token: null,
        flag: 0
        };
    }

    componentWillMount () {
      if(LOCALS_STORAGE_AUTHTOKEN) {

        this.setState({ token :  LOCALS_STORAGE_AUTHTOKEN });
        this.props.getLoginUser(LOCALS_STORAGE_AUTHTOKEN);
      }
    }

    componentWillReceiveProps (nextProps) {

        if (nextProps.currentLoginUser && this.props.currentLoginUser !== nextProps.currentLoginUser) {
            let currentLoginUser = nextProps.currentLoginUser;
            //console.log(currentLoginUser);
            //this.props.getUserServices(LOCALS_STORAGE_AUTHTOKEN, currentLoginUser.id);
            this.setState({currentLoginUserData: currentLoginUser});
        }

        if(nextProps.userServices && this.props.userServices !== nextProps.userServices) {
            let userServices = nextProps.userServices;
            if(userServices.length > 0 && this.state.flag === 0) {
              this.setState({ flag : 1 });
              this.setState({ modal1: !this.state.modal1, token: LOCALS_STORAGE_AUTHTOKEN });
            }
        }
    }

    onClickHandler(e) {
       this.props.wrapperToggle('toggled');
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    open_modal() {

        let token = localStorage.getItem('authToken');
        this.props.getUserServices(token, this.state.currentLoginUserData.id);

        this.setState({
            modal1: !this.state.modal1,
            token: token
        });
    }

    render() {
    return (
            <div>
                <header>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-8 col-sm-6 col-md-7 header-top col-xs-8">
                                <span onClick={()=>this.onClickHandler()} className="ham ripple" id="menu-toggle"><i className="zmdi zmdi-menu"><Ink /></i></span>
                                <Link to="/signoutlist" className="logo"><img src="/client/assets/images/logo.png" alt=""/></Link>
                            </div>
                            <div className="col-4 col-sm-6 col-md-5 check-header col-xs-4">
                                <div className="dropdown btn-group user-btn user_btn1">
                                         <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                                            <DropdownToggle caret><i className="zmdi zmdi-account"></i>
                                             <span className="hidden-xs-down"> {this.state.currentLoginUserData.first_name} {this.state.currentLoginUserData.last_name} </span>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                              <DropdownItem>
                                                <i className="zmdi zmdi-account-box"></i>
                                                <Link to="/account">Account</Link>
                                              </DropdownItem>
                                              <DropdownItem onClick={this.props.logoutAndRedirect}> <i className="zmdi zmdi-power-setting"></i> <a  href="javascript:void(0)">Logout</a></DropdownItem>
                                            </DropdownMenu>
                                          </ButtonDropdown>
                                </div>
                                <div className="checkin-btn">
                                    <ButtonM type="button" onClick={this.open_modal}><Ink /><i className="fa fa-check-square-o" aria-hidden="true"></i> <span className="hidden-xs-down">Check-in</span>

                                    </ButtonM>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <CheckinModal modal_var={this.state.modal1} handle_modal = {this.open_modal} token={this.state.token} />
                <div id="overlay" className="hide"></div>
            </div>
        );
    }
}

// const mapStateToProps = (state) => ({
//   isAuthenticating   : state.login.isAuthenticating,
//   statusText         : state.login.statusText,
//   isAuthenticated    : state.login.isAuthenticated
// });

function mapStateToProps(state) {
     return {
         isAuthenticating   : state.login.isAuthenticating,
         statusText         : state.login.statusText,
         isAuthenticated    : state.login.isAuthenticated,
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data,
         userServices : state.users.userServices
     }
}

// const mapDispatchToProps = ( dispatch ) => ({
//   actions : bindActionCreators( { logoutAndRedirect, getLoginUser }, dispatch )
// });

export default connect ( mapStateToProps , { getLoginUser, logoutAndRedirect, getUserServices } )( Header );
