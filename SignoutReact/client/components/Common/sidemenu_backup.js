/**
 * @class         :	SideMenu
 * @description   : SideMenu
 * @Created by    : smartData
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import MySideMenuIcons from "./menu_items";

class SideMenu extends Component {

      static contextTypes={
          router:PropTypes.object
      }

      constructor(props) {
          super(props)
          let sidemenuArr = [
              {"url":"signoutlist","index":0,"icon":"zmdi zmdi-power","name":"Signout Lists", "classNameFor":"signoutlist"},
              {"url":"patients","index":1,"icon":"zmdi zmdi-hotel","name":"Patients",  "classNameFor":"Patients"},
              {"url":"services","index":2,"icon":"zmdi zmdi-desktop-mac","name":"Services",  "classNameFor":"Services"},
              {"url":"directories","index":3,"icon":"fa fa-folder-open","name":"Directory",  "classNameFor":"Directory"}
          ]
          let sidemenuAdminArr = [
              {"url":"dashboard","index":4,"icon":"zmdi zmdi-assignment","name":"Summary", "classNameFor":"Summary"},
              {"url":"departments","index":5,"icon":"zmdi zmdi-view-dashboard","name":"Departments",  "classNameFor":"Departments"},
              {"url":"users","index":6,"icon":"zmdi zmdi-accounts-alt","name":"Users",  "classNameFor":"Users"},
              {"url":"comingsoon","index":7,"icon":"zmdi zmdi-settings","name":"Settings",  "classNameFor":"Settings"}
          ]
          this.state = {
              activeIndex: 0,
              sidemenuArr : sidemenuArr,
              sidemenuAdminArr : sidemenuAdminArr,
          }
         this.redirectToHome = this.redirectToHome.bind(this);
      }

      //redirect to dashboard
      redirectToHome(){
          if(!this.state.isDashboard) {
              this.context.router.push('/');
          }
      }

      render() {
          const menuItems = this.state.sidemenuArr.map((menu_item, key) => {
               let active = this.context.router.isActive(menu_item.url);
              return <MySideMenuIcons key={ key } name={menu_item.name} colorMenu={menu_item.classNameFor} icon={menu_item.icon} index={menu_item.index} isActive= {active} link = {menu_item.url}/>
          });

          const menuItemsAdmin = this.state.sidemenuAdminArr.map((menu_item, key) => {
               let active = this.context.router.isActive(menu_item.url);
              return <MySideMenuIcons key={ key } name={menu_item.name} colorMenu={menu_item.classNameFor} icon={menu_item.icon} index={menu_item.index} isActive= {active} link = {menu_item.url}/>
          });

          return (
                <div id="sidebar-wrapper">
                  <h2 className="hidden-sm-up menu-title"><span>Menu</span><i className="zmdi zmdi-caret-down"></i></h2>
                  <ul className="sidebar-nav">
                      {menuItems}
                      <h3 className="submenu-heading">
            						<span className="nav-label">Administration</span>
            					</h3>
                      {menuItemsAdmin}
                  </ul>
                </div>
          );
      }
}

export default SideMenu;
