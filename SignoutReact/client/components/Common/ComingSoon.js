/**
 * @class         :	404
 * @description   : Not Found
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { Link } from 'react-router';

export default class ComingSoon extends Component {
    render() {
        return (
          <div id="page-content-wrapper">
              {/*<!-- Page Content -->*/}
              <div className="col welcome-bar ">
                  {/*<!-- welcome bar -->*/}
                  <h1 className="page-title">Coming Soon</h1>
                  <ul className="breadcrumb pull-right hidden-xs-down">
                      <li><Link to={`/signoutlist`}>Sign out</Link></li>
                      <li className="active">Coming Soon</li>
                  </ul>
              </div>
              <div className="container-fluid mrg-top30 pd-bt30">
                  <h1>Coming Soon . . . </h1>
              </div>
          </div>
        );
    }
}
