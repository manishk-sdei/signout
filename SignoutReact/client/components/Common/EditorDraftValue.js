/**
 * source : https://codepen.io/Kiwka/pen/YNYvyG
 *          https://draftjs.org/
 *          http://frontendgirl.com/8-playgrounds-for-examples-from-official-draft-js-repository-v-0-10-0/
 */

import React from 'react';
import { connect } from 'react-redux';

import { Editor,
         EditorState,
         RichUtils,
         convertFromHTML,
         ContentState,
         convertFromRaw,
         convertToRaw } from 'draft-js';
import { setDraftEditorValues } from '../../actions/SignoutActions';

 let fieldObj = [];

class RichEditor extends React.Component {
   constructor(props) {
     super(props);
     this.state = { content : [] };
     this.state = {editorState: EditorState.createEmpty()};
     this.state = { fieldValue : [] };

     this.focus = () => this.refs.editor.focus();
     //this.onChange = (editorState) => this.setState({editorState});
     let field_arr = {};

     this.onChange = (editorState) => {
        const rawDraftContentState = JSON.stringify( convertToRaw(this.state.editorState.getCurrentContent()) );

        let key = this.props.editorIndex;
        let value = rawDraftContentState;

        field_arr.signout_list_field_template_id = key;
        field_arr.field_value = value;

        if(fieldObj.length > 0) {
            let pullIndex = [];
            fieldObj.map(( data, i ) => {
              if(data.signout_list_field_template_id === key) {
                  pullIndex.push(i);
              }
            });
            _.pullAt(fieldObj, pullIndex);
        }

        fieldObj.push(field_arr);

        this.props.setDraftEditorValues(fieldObj);
        this.setState({ fieldValue : fieldObj });

        this.setState({editorState});
      }

     this.handleKeyCommand = (command) => this._handleKeyCommand(command);
     this.onTab = (e) => this._onTab(e);
     this.toggleBlockType = (type) => this._toggleBlockType(type);
     this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);

     this.updateEditorData(props.content);
   }

   updateEditorData(content) {
     let editorState;

      if (content) {

        editorState = EditorState.createEmpty();

        if(this.isJSON(content) === true) {
          const contentState = convertFromRaw( JSON.parse( content ) );
          editorState = EditorState.createWithContent(contentState);
        }

        if(this.isJSON(content) === false) {
          const blocksFromHTML = convertFromHTML(content);
          const contentState = ContentState.createFromBlockArray(blocksFromHTML);
          editorState = EditorState.createWithContent(contentState);
        }
      } else {
        editorState = EditorState.createEmpty();
      }

      this.state = { editorState };
   }

   isJSON(data) {
       var ret = true;
       try {
          JSON.parse(data);
       }catch(e) {
          ret = false;
       }
       return ret;
    }

   componentWillMount() {
     let field_arr = {};
     const rawDraftContentState = JSON.stringify( convertToRaw(this.state.editorState.getCurrentContent()) );

     let key = this.props.editorIndex;
     let value = rawDraftContentState;

     field_arr.signout_list_field_template_id = key;
     field_arr.field_value = value;

     if(fieldObj.length > 0) {
         let pullIndex = [];
         fieldObj.map(( data, i ) => {
           if(data.signout_list_field_template_id === key) {
               pullIndex.push(i);
           }
         });
         _.pullAt(fieldObj, pullIndex);
     }

     fieldObj.push(field_arr);

     this.props.setDraftEditorValues(fieldObj);
     this.setState({ fieldValue : fieldObj });

     //this.setState({editorState});
   }

   componentWillReceiveProps(nextProps) {
     this.updateEditorData(nextProps.content)
     this.setState({ content : nextProps.content });
   }

   _handleKeyCommand(command) {
     const {editorState} = this.state;
     const newState = RichUtils.handleKeyCommand(editorState, command);
     if (newState) {
       this.onChange(newState);
       return true;
     }
     return false;
   }

   _onTab(e) {
     const maxDepth = 4;
     this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
   }

   _toggleBlockType(blockType) {
     this.onChange(
       RichUtils.toggleBlockType(
         this.state.editorState,
         blockType
       )
     );
   }

   _toggleInlineStyle(inlineStyle) {
     this.onChange(
       RichUtils.toggleInlineStyle(
         this.state.editorState,
         inlineStyle
       )
     );
   }

   render() {
     const {editorState} = this.state;

     // If the user changes block type before entering any text, we can
     // either style the placeholder or hide it. Let's just hide it now.
     let className = 'RichEditor-editor';
     var contentState = editorState.getCurrentContent();
     if (!contentState.hasText()) {
       if (contentState.getBlockMap().first().getType() !== 'unstyled') {
         className += ' RichEditor-hidePlaceholder';
       }
     }

     return (

           <Editor
             id={this.props.editorIndex}
             blockStyleFn={getBlockStyle}
             customStyleMap={styleMap}
             editorState={editorState}
             handleKeyCommand={this.handleKeyCommand}
             onChange={this.onChange}
             onTab={this.onTab}
             placeholder="Enter field value..."
             ref="editor"
             spellCheck={true}
             readOnly = {true} />
     );
   }
 }

 function mapStateToProps(state) {
      return {
          isFetching: state.signouts.isFetching,
          editorValues: state.signouts.editorValues
      }
 }

 export default connect(mapStateToProps, { setDraftEditorValues })( RichEditor );

 // Custom overrides for "code" style.
 const styleMap = {
   CODE: {
     backgroundColor: 'rgba(0, 0, 0, 0.05)',
     fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
     fontSize: 16,
     padding: 2,
   },
 };

 function getBlockStyle(block) {
   switch (block.getType()) {
     case 'blockquote': return 'RichEditor-blockquote';
     default: return null;
   }
 }

 class StyleButton extends React.Component {
   constructor() {
     super();
     this.onToggle = (e) => {
       e.preventDefault();
       this.props.onToggle(this.props.style);
     };
   }

   render() {
     let className = 'RichEditor-styleButton';
     if (this.props.active) {
       className += ' RichEditor-activeButton';
     }

     return (
       <span className={className} onMouseDown={this.onToggle}>
         {this.props.label}
       </span>
     );
   }
 }

 const BLOCK_TYPES = [
  //  {label: 'H1', style: 'header-one'},
  //  {label: 'H2', style: 'header-two'},
  //  {label: 'H3', style: 'header-three'},
  //  {label: 'H4', style: 'header-four'},
  //  {label: 'H5', style: 'header-five'},
  //  {label: 'H6', style: 'header-six'},
  //  {label: 'Blockquote', style: 'blockquote'},
   {label: 'UL', style: 'unordered-list-item'},
   {label: 'OL', style: 'ordered-list-item'},
   //{label: 'Code Block', style: 'code-block'},
 ];

 const BlockStyleControls = (props) => {
   const {editorState} = props;
   const selection = editorState.getSelection();
   const blockType = editorState
     .getCurrentContent()
     .getBlockForKey(selection.getStartKey())
     .getType();

   return (
     <div className="RichEditor-controls">
       {BLOCK_TYPES.map((type) =>
         <StyleButton
           key={type.label}
           active={type.style === blockType}
           label={type.label}
           onToggle={props.onToggle}
           style={type.style}
         />
       )}
     </div>
   );
 };

 var INLINE_STYLES = [
   {label: 'Bold', style: 'BOLD'},
   {label: 'Italic', style: 'ITALIC'},
   {label: 'Underline', style: 'UNDERLINE'},
  //  {label: 'Monospace', style: 'CODE'},
 ];

 const InlineStyleControls = (props) => {
   var currentStyle = props.editorState.getCurrentInlineStyle();
   return (
     <div className="RichEditor-controls">
       {INLINE_STYLES.map(type =>
         <StyleButton
           key={type.label}
           active={currentStyle.has(type.style)}
           label={type.label}
           onToggle={props.onToggle}
           style={type.style}
         />
       )}
     </div>
   );
 };
