/**
 * @Component     :	Loader Component
 * @description   : Common Loader - To set loading in all pages
 * @Created by    : smartData
 */

import React, { Component } from 'react';

export default class Loader extends Component {

    renderLoading() {
        if(this.props.isFetching) {
          return(
            <div className="loader"></div>
          );
        }
    }
    render() {
        return (
            {this.renderLoading()}
        );
    }
}
