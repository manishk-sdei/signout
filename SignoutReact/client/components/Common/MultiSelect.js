/**
 * @class         :	Multiselect
 * @description   :
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import Multiselect from 'react-widgets/lib/Multiselect';

export default class MultiselectNew extends Component {

    componentWillReceiveProps(nextProps) {
      console.log("MultiselectNew========", this.props);
    }

    render() {
        return (
          <div className="custom-box">
          {
          (this.props.userId === null) ?
                        <Multiselect
                              {...this.props.multiselect.name}
                              placeholder={this.props.multiselect.placeholder}
                              valueField={this.props.multiselect.valueField}
                              textField={this.props.multiselect.textField}
                              data={this.props.multiselect.data}
                              defaultValue={ [] }
                              onBlur={ (e) => console.log(e.target.value) } />
                              :
                              this.props.multiselect.defaultValue.length > 0 ?
                          <Multiselect
                                {...this.props.multiselect.name}
                                placeholder={this.props.multiselect.placeholder}
                                valueField={this.props.multiselect.valueField}
                                textField={this.props.multiselect.textField}
                                data={this.props.multiselect.data}
                                defaultValue={ this.props.multiselect.defaultValue }
                                //readOnly={this.props.multiselect.defaultValue}
                                onBlur={ (e) => console.log(e.target.value) } /> :
                         ""
            }
          </div>
        );
    }
}
