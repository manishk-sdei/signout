/**
 *      I`f you work with mysql its
 *
 *      LIMIT offset, items_per_page
 *
 *      To calculate the offset u can use *
 *      $offset = ($page - 1) * $items_per_page;
 *
 *      Then replace the $page accordingly.
 *
 *      Last *
 *      $last_offset = ($totalPages - 1) * $items_per_page;
 *
 *      Previous *
 *      $previous_offset = (($currentPage - 1) - 1) * $items_per_page;
 *
 *      Next *
 *      $next_offset = (($currentPage + 1) - 1) * $items_per_page;`
 */

import React from 'react';
import { connect } from 'react-redux';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { PAGINATION_DEFAULT_LIMIT } from '../../actions/Constants';
import { getPatientsWithPagination } from '../../actions/PatientsActions';
import { getUsers } from '../../actions/UsersActions';

class Paginate extends React.Component {

  constructor(props){
      super(props);
      this.state = { active : 1 };
      this.onClickPagination = this.onClickPagination.bind(this);
  }

  onClickPagination(e, clickedPageNumber) {

      e.preventDefault();

      let nextArr = ['first', 'previous', 'next', 'last'];

      let foundMatch = _.find(nextArr, function(o) { return o === clickedPageNumber; });
      if(foundMatch) { // In case of click on text - 'first', 'previous', 'next', 'last'

        // Now Call api for the page that need to paginate Data
        if(this.props.pageName === 'TENANT_PATIENTS') { // /Patients Component

          let limit = PAGINATION_DEFAULT_LIMIT;
          let offset = '';
          let orderBy = 'id';
          let order = 'desc';
          let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;

          if(foundMatch === 'first') {
              limit = PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : 1 });
          }

          if(foundMatch === 'previous') {
              let previous = this.state.active - 1;
              let previousPage = previous >=1 ? previous : 1;

              limit = PAGINATION_DEFAULT_LIMIT;
              offset = (previousPage - 1) * PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : previous });
          }

          if(foundMatch === 'next') {
              let next = this.state.active + 1;
              limit = PAGINATION_DEFAULT_LIMIT;
              offset = (next - 1) * PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : next });
          }

          if(foundMatch === 'last') {
              limit = PAGINATION_DEFAULT_LIMIT;

              let pagesObj = this.getPagesObj();
              let lastPageNumber = pagesObj.length;

              offset = (lastPageNumber - 1) * PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : lastPageNumber });
          }

          this.props.getPatientsWithPagination(this.props.token, query);
        }

        if(this.props.pageName === 'TENANT_USERS') { // /Users Component

          let limit = PAGINATION_DEFAULT_LIMIT;
          let offset = '';
          let orderBy = 'id';
          let order = 'desc';
          let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;

          if(foundMatch === 'first') {
              limit = PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : 1 });
          }

          if(foundMatch === 'previous') {
              let previous = this.state.active - 1;
              let previousPage = previous >=1 ? previous : 1;

              limit = PAGINATION_DEFAULT_LIMIT;
              offset = (previousPage - 1) * PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : previous });
          }

          if(foundMatch === 'next') {
              let next = this.state.active + 1;
              limit = PAGINATION_DEFAULT_LIMIT;
              offset = (next - 1) * PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : next });
          }

          if(foundMatch === 'last') {
              limit = PAGINATION_DEFAULT_LIMIT;

              let pagesObj = this.getPagesObj();
              let lastPageNumber = pagesObj.length;

              offset = (lastPageNumber - 1) * PAGINATION_DEFAULT_LIMIT;
              orderBy = 'id';
              order = 'desc';
              query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
              this.setState({ active : lastPageNumber });
          }

          this.props.getUsers(this.props.token, query);
        }

      } else {// In case of click on page numbers - 1 2 3 4

        // Now Call api for the page that need to paginate Data
        if(this.props.pageName === 'TENANT_PATIENTS') { // Tenant Users page
            //$offset = ($page - 1) * $items_per_page;

            let limit = PAGINATION_DEFAULT_LIMIT;
            let offset = (clickedPageNumber - 1) * PAGINATION_DEFAULT_LIMIT;
            let orderBy = 'id';
            let order = 'desc';

            let query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
            this.props.getPatientsWithPagination(this.props.token, query);
        }

        if(this.props.pageName === 'TENANT_USERS') { // Tenant Users page
            //$offset = ($page - 1) * $items_per_page;

            let limit = PAGINATION_DEFAULT_LIMIT;
            let offset = (clickedPageNumber - 1) * PAGINATION_DEFAULT_LIMIT;
            let orderBy = 'id';
            let order = 'desc';

            let query = `?limit=${limit}&offset=${offset}&orderBy=${orderBy}&order=${order}`;
            this.props.getUsers(this.props.token, query);
        }

        this.setState({ active : clickedPageNumber });
      }
  }

  getPagesObj() {
    let totalPages = this.props.totalPages;
    let pagesObj = _.range(0, this.props.totalPages, PAGINATION_DEFAULT_LIMIT);
    return pagesObj;
  }

  renderPageNumbers() {

    let pagesObj = this.getPagesObj();

    let active = this.state.active;
      return pagesObj.map((page, index) => {let pageNumber = index + 1;
        return(
            <PaginationItem className={ `${active === pageNumber ? 'active' : ''}` }>
              {
                active === pageNumber ?
                <PaginationLink href="/" onClick={ (e) => e.preventDefault() }>
                  { pageNumber }
                </PaginationLink>
                   :
                 <PaginationLink href="/" onClick={ (e) => this.onClickPagination(e, pageNumber) }>
                   { pageNumber }
                 </PaginationLink>
              }
            </PaginationItem>
        );
      });
  }

  render() {
    return (

      <Pagination size="sm">
        <PaginationItem>
          <PaginationLink  href="/" onClick={ (e) => this.onClickPagination(e, 'first') }>First</PaginationLink>
        </PaginationItem>

        <PaginationItem>
          <PaginationLink previous href="/" onClick={ (e) => this.onClickPagination(e, 'previous') } />
        </PaginationItem>

        { this.renderPageNumbers() }

        <PaginationItem>
          <PaginationLink next href="/" onClick={ (e) => this.onClickPagination(e, 'next') } />
        </PaginationItem>

        <PaginationItem>
          <PaginationLink  href="/" onClick={ (e) => this.onClickPagination(e, 'last') }>Last</PaginationLink>
        </PaginationItem>
      </Pagination>
    );
  }
}

export default connect(null, { getPatientsWithPagination, getUsers })(Paginate);
