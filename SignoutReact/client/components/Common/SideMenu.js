/**
 * @class         :	SideMenu
 * @description   : Dashbord SideMenu
 * @Created by    : smartData
 */

import React, { Component,PropTypes } from 'react';
import Ink from 'react-ink';

export default class SideMenu extends Component {
            constructor(props) {
            super(props)
            this.state = {
                activeIndex: 0
            }
        }

        static contextTypes={
            router:PropTypes.object
        }

        handleClick(index) {

            localStorage.setItem('index', index);

            let refreshIndex = localStorage.getItem(index);
            this.setState({activeIndex: index});


            if(index===0){
                this.context.router.push('/signoutlist');
            }else if(index===1){
                this.context.router.push('/patients');
            }else if(index===2){
                this.context.router.push('/services');
            }else if(index===3){
                this.context.router.push('/directories');
            } else if(index===4){
               // this.context.router.push('/dashboard');
            }else if(index===5){
                this.context.router.push('/dashboard');
            }else if(index===6){
                this.context.router.push('/departments');
            }else if(index===7){
                this.context.router.push('/users');
            }else if(index===8){
                this.context.router.push('/dashboard');
            }
        }

        render() {

            return (
                 <div id="sidebar-wrapper">
                 <h2 className="hidden-sm-up menu-title"><span>Menu</span><i className="zmdi zmdi-caret-down"></i></h2>
                 <ul className="sidebar-nav">
						<MySideMenuIcons name="Signout Lists"  icon="zmdi zmdi-power" index={0} isActive={this.state.activeIndex===0}
						onClick={this.handleClick.bind(this)} />
                        <MySideMenuIcons name="Patients" icon="zmdi zmdi-hotel" index={1} isActive={this.state.activeIndex===1}
						onClick={this.handleClick.bind(this)} />
                        <MySideMenuIcons name="Services" icon="zmdi zmdi-desktop-mac" index={2} isActive={this.state.activeIndex===2}
						onClick={this.handleClick.bind(this)} />
                        <MySideMenuIcons name="Directory" icon="fa fa-folder-open" index={3} isActive={this.state.activeIndex===3}
						onClick={this.handleClick.bind(this)} />

					<h3 className="submenu-heading">
						<span className="nav-label">Administration</span>
					</h3>
						<MySideMenuIcons name="Summary"  icon="zmdi zmdi-assignment" index={5} isActive={this.state.activeIndex===5}
						onClick={this.handleClick.bind(this)} />
                        <MySideMenuIcons name="Departments" icon="zmdi zmdi-view-dashboard" index={6} isActive={this.state.activeIndex===6}
						onClick={this.handleClick.bind(this)} />
                        <MySideMenuIcons name="Users" icon="zmdi zmdi-accounts-alt" index={7} isActive={this.state.activeIndex===7}
						onClick={this.handleClick.bind(this)} />
          {/**<MySideMenuIcons name="Settings" icon="zmdi zmdi-settings" index={8} isActive={this.state.activeIndex===8}
						onClick={this.handleClick.bind(this)} />**/}
                    </ul>
                  </div>
            );
        }
}

class MySideMenuIcons extends React.Component {

    handleClick() {

        this.props.onClick(this.props.index)
    }

    render () {
        return <li className={this.props.isActive ? 'active' : ''} onClick={this.handleClick.bind(this)}>
               <a>
                    <Ink /><i className={this.props.icon} title="Signout Lists"></i><span className="nav-label">{this.props.name}</span>
                    {/*<!--<span className="badge bg-success">New</span>-->*/}
                </a>
            </li>
    }
}
