import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { getServices } from '../../actions/ServicesActions';

class Directories extends Component {

    constructor(props) {
        super(props);
        this.state = { services : [] };
    }

    componentDidMount() {
        document.title = "Signout - Directories";
    }

    componentWillMount() {
      this.setState({ token : this.props.token });
      let urlFlag = `?onCallUsers=true`
      this.props.getServices( this.props.token, null, urlFlag );
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.services && this.props.services !== nextProps.services ) {
            let services = nextProps.services;
            let indexArr = [];
            services.map((service, index) => {
                if(service.onCallUsers.length === 0) {
                  indexArr.push(index);
                }
            });
            _.pullAt(services, indexArr);

            this.setState( { services : services } );
        }
    }

    renderServices() {
      if(this.props.isFetching) {
        return(
          <div className="white-bg">
              <div className="loader" id="loader-4">
                <span></span>
                <span></span>
                <span></span>
              </div>
          </div>
        );
      }

      if(this.state.services.length === 0) {
          <div className="white-bg">No Service</div>
      }

      if(this.state.services) {
        return this.state.services.map(( service ) => {
            return(
              <div className="white-bg" key={ service.id }>
                  <h3 className="sub-title">On Call { service.department.name }</h3>
                  {/**<div className="form-group search-bar dir-search">
                      <input className="inputMaterial" type="text" required />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Search Patient Name and MRN.</label>
                      <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                  </div>**/}
                  <div className="table-responsive">
                      <div className="dr-wrap">
                          <h3 className="content-title blue-text">{ service.name }</h3>
                          <table className="table table-hover patient-list ">
                              {/*<!--table start-->*/}
                              <thead>
                                  <tr>
                                      <th width="25%">Name</th>
                                      <th width="25%">Role</th>
                                      <th width="25%">Mobile</th>
                                      <th width="25%">Pager</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  { this.renderOnCallUsers(service.onCallUsers) }
                              </tbody>
                          </table>
                      {/* <!--table End-->*/}
                      </div>
                  </div>
              </div>
            );
        });
      }
    }

    renderOnCallUsers(onCallUsers) {

        if(onCallUsers) {
            return onCallUsers.map(( user ) => {
                return(
                  <tr key={ user.id }>
                      <td>{ user.first_name } { user.last_name }</td>
                      <td>{ user.role ? user.role.name : 'n/a' }</td>
                      <td>{ user.phone_mobile ? user.phone_mobile : 'n/a' }</td>
                      <td>{ user.phone_pager ? user.phone_pager : 'n/a' }</td>
                  </tr>
                );
            });
        }
    }

    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar">
                    <h1 className="page-title">Directory</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><Link to={`/signoutlist`}>Sign out</Link></li>
                        <li className="active">Directory</li>
                    </ul>
                </div>
                <div className="container-fluid mrg-top30 pd-bt30">
                    <div className="col pd-off-xs">
                        <div className="row">
                            <div className="col-sm-12">
                                { this.renderServices() }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
     return {
       isFetching: state.services.isFetching,
       services: state.services.data
     }
}
export default connect(mapStateToProps, { getServices })( Directories );
