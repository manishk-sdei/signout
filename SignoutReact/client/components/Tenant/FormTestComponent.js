import React, { Component } from 'react';
import ReactDOM from 'react-dom';



class PriceMultiply extends Component {
    render() {
        return (
           <tr>
             <td>
               Adjust Price Multiply
             </td>

             <td>
               by <input type="text" name="" />
             </td>
             <td>
               <button className="btn btn-danger" onClick={this.props.removeOperation.bind(null, this.props.index)} > Remove </button>
             </td>
           </tr>
        );
    }
}

class DynamicFields extends Component {

    constructor(props){
        super(props);
        this.state = {
          operations:[]
        };
    }

    removeOperation(index) {
        var operations = this.state.operations;
        operations.splice(index,1);
        this.setState({operations:operations});
    }

    addOperation() {
         var value = 'adjust-price-multiply';
         this.setState({operations:this.state.operations.concat([value])});
    }

    renderAdjustmentRows() {

       this.state.operations.map((operation,index) =>{
              if(operation == "adjust-price-multiply"){
                    return (<table>
                      <tbody>
                        <PriceMultiply key={index} index={index} removeOperation={this.removeOperation} />
                        </tbody>
                        </table>
                    );
              }
        });
    }

    render() {
        return (
          <div>
            <button onClick={this.addOperation} > Add Operation </button>
            <div id="adjust-import-data-rows">
                 {this.renderAdjustmentRows()}
            </div>
          </div>
        );
    }
}
