import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { getLoginUser, getUserServices } from '../../actions/UsersActions';
import { getSignoutList } from '../../actions/SignoutActions';
import moment from 'moment';

class SignOutList extends Component {

    constructor(props) {
        super(props);
        this.state = { currentLoginUserData : [], userSignouts: [] }
    }

    componentDidMount() {

        document.title = "Signout - SignOutList";
        this.props.getLoginUser( this.props.token );
    }

    componentWillMount () {
        this.props.getLoginUser( this.props.token );
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.currentLoginUser && this.props.currentLoginUser !== nextProps.currentLoginUser ) {

            let currentLoginUser = nextProps.currentLoginUser;
            this.setState( { currentLoginUserData : currentLoginUser } );
            this.props.getUserServices(this.props.token, currentLoginUser.id);
            this.props.getSignoutList( this.props.token, null, currentLoginUser.id );
        }

        if ( nextProps.signouts && this.props.signouts !== nextProps.signouts ) {
            let signouts = nextProps.signouts;
            this.setState( { userSignouts : signouts } );
        }
    }

    renderRow() {

      if(this.props.isFetching) {
          return(
            <div className="loader" id="loader-4">
              <span></span>
              <span></span>
              <span></span>
            </div>
          );
      }

      if(this.state.userSignouts.length === 0) {
        return(
          <div className="col-sm-12 col-md-12 col-lg-12">
              <div className="service-box">
                  <p>No signout list</p>
              </div>
          </div>
        );
      }

      if( this.state.userSignouts ) {
        return this.state.userSignouts.map(( signout ) => {

          return (
            <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6" key={ signout.id }>
                <div className="sign-whtbox">
                    <Link to={{ pathname: `/signout-detail/${signout.id}`, query: { sid: signout.service_id } }}>
                      <h2>{ signout.name }</h2>
                      <h6>{ signout.service.name }</h6>
                      <p>Last Updated: { signout.updated_at ? moment(signout.updated_at).format("MMMM Do YYYY, h:mm a") : 'n/a' }</p>
                    </Link>
                </div>
            </div>
          );
        });
      }
    }

    render() {
        return(
                <div id="page-content-wrapper">
                    <div className="col welcome-bar">
                        <h1 className="page-title">Signout List</h1>
                        <ul className="breadcrumb pull-right hidden-xs-down">
                            <li><Link to={`/signoutlist`}>Sign out</Link></li>
                            <li className="active">Sign out List</li>
                        </ul>
                    </div>
                    <div className="container-fluid mrg-top30 pd-bt30">
                        <div className="col pd-off-xs">
                          <div className="row">
                            { this.renderRow() }
                          </div>
                        </div>
                    </div>
                </div>
        );
    }
}


function mapStateToProps( state ) {
     return {
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data,
         isFetching: state.signouts.isFetching,
         signouts: state.signouts.data
     }
}

export default connect ( mapStateToProps , { getLoginUser, getSignoutList, getUserServices } )( SignOutList );
