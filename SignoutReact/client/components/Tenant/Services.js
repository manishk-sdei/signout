import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import _ from 'lodash';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import ModalWindowConfirm from '../Modals/ConfirmDeleteModal';

import { getServices } from '../../actions/ServicesActions';
import { getLoginUser } from '../../actions/UsersActions';

class Services extends Component {

    constructor(props) {
        super(props);
        this.state = {
          token: null,
          servicesData : [],
          myServices: [],
          selfSubscribeServices: [],
          userId: null,
          modal1: false,
          serviceId: null,
          subscriptionType: null
        };
        this.open_modal_confirm = this.open_modal_confirm.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Services";
    }

    componentWillMount() {

      this.props.getServices(this.props.token, true);
      this.props.getLoginUser(this.props.token);
      this.setState({ token: this.props.token })
    }

    componentWillReceiveProps( nextProps ) {

        if(nextProps.currentLoginUser) {
              // let indexArr = [];
              // nextProps.currentLoginUser.services.map(( service, index ) => {
              //   if(service.is_self_enrollment === true) {
              //       indexArr.push(service.id);
              //   }
              // });
              this.setState({ userId: nextProps.currentLoginUser.id,
                              myServices : nextProps.currentLoginUser.services });
        }

        if ( nextProps.services && this.props.services !== nextProps.services ) {
            let services = nextProps.services;

              //_.pullAllBy(services, nextProps.currentLoginUser.services, 'id');
              this.setState({ servicesData: services });
        }
    }

    open_modal_confirm( e, serviceId, subscriptionType, service ) {

          this.props.getServices(this.state.token, true); // Update row
          this.props.getLoginUser(this.state.token);

          let data = this.state.servicesData;
          if(this.state.subscriptionType === 'unsubscribe' && service !== undefined) {

            console.log("unsubscribe---", data, service);

            data.push(service);
          }

          if(this.state.subscriptionType === 'subscribe') {


            // _.remove(data, function(n) {
            //     return (n.id === serviceId);
            // });

            let removeElement = [];
            data.map(( service, index ) => {
                if(service.id === serviceId) {
                  removeElement.push(index);
                }
            })
            _.pullAt(data, removeElement);
          }
          this.setState({servicesData : data });
          //-----------------------------------------------
          (e===true) ? e.preventDefault() : '';
          this.setState({modal1: !this.state.modal1,
                        serviceId: serviceId,
                        subscriptionType: subscriptionType
          });
    }

    renderMyservices() {

      if(this.props.isFetching) {
          return(
            <div className="loader" id="loader-4">
              <span></span>
              <span></span>
              <span></span>
            </div>
          );
      }

      if(this.state.myServices.length === 0) {
        return(
          <div className="col-sm-12 col-md-12 col-lg-12">
              <div className="service-box">
                  <p>No service</p>
              </div>
          </div>
        );
      }

      if( this.state.myServices ) {
        return this.state.myServices.map(( service ) => {

          return (
            <div className="col-sm-6 col-md-6 col-lg-4" key={ service.id }>
                <div className="service-box">
                    <h3>{ service.name } <span className="dep_name">({ service.department.name })</span></h3>
                    <p>{ service.description === "" ? "No description" : service.description }</p>
                    { service.is_self_enrollment === true ?
                    <ButtonM
                          onClick={ (e) => this.open_modal_confirm( e, service.id, 'unsubscribe', service )}
                          className="btn grey-btn active">UnSubscribe</ButtonM>
                        :<ButtonM className="btn grey-btn hide">UnSubscribe</ButtonM> }
                </div>
            </div>
          );
        });
      }
    }

    renderSelfSubscribeServices() {

      if(this.props.isFetching) {
          return(
            <div className="loader" id="loader-4">
              <span></span>
              <span></span>
              <span></span>
            </div>
          );
      }

      if(this.state.servicesData.length === 0) {
        return(
          <div className="col-sm-12 col-md-12 col-lg-12">
              <div className="service-box">
                  <p>No self subscribe service</p>
              </div>
          </div>
        );
      }

      if( this.state.servicesData ) {console.log(this.state.servicesData);
        _.pullAllBy(this.state.servicesData, this.state.myServices, 'id');
        return this.state.servicesData.map(( service ) => {
          return (
            <div className="col-sm-6 col-md-6 col-lg-4" key={ service.id }>
                <div className="service-box">
                    <h3>{ service.name } <span className="dep_name">({ service.department.name })</span></h3>
                    <p>{ service.description === "" ? "No description" : service.description }</p>
                    <ButtonM
                          onClick={ (e) => this.open_modal_confirm( e, service.id, 'subscribe', service )}
                          className="btn grey-btn active">Subscribe</ButtonM>
                </div>
            </div>
          );
        });
      }
    }

    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar">
                    <h1 className="page-title">Services</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><Link to={`/signoutlist`}>Sign out</Link></li>
                        <li className="active">Services</li>
                    </ul>
                </div>
                <div className="container-fluid mrg-top30 pd-bt30">
                    <div className="col pd-off-xs">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="white-bg pdoff-lt-rt">

                                  <ModalWindowConfirm
                                              modal_var = {this.state.modal1 }
                                              handle_modal = { this.open_modal_confirm }
                                              token = { this.state.token }
                                              confirmPage = { 'SERVICE_SUBSCRIPTION' }
                                              serviceId = { this.state.serviceId }
                                              subscriptionType = { this.state.subscriptionType }
                                              userId = { this.state.userId } />

                                    <h2 className="col content-title blue-text">My Services</h2>
                                    <div className="col-sm-12">
                                        <div className="row">
                                            { this.renderMyservices() }

                                        </div>
                                    </div>
                                    <h2 className="col content-title blue-text mar-top35">Self Subscribe</h2>
                                    <div className="col-sm-12">
                                        <div className="row">
                                            { this.renderSelfSubscribeServices() }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        );
    }
}

function mapStateToProps(state) {
  return {
      isFetching: state.services.isFetching,
      services: state.services.data,
      isFetching : state.users.isFetching,
      currentLoginUser: state.users.data,
      isAuthenticating: state.users.isAuthenticating,
      getDepartmentById: state.departments.getDepartmentById
  }
}

export default connect(mapStateToProps, { getServices, getLoginUser })( Services );
