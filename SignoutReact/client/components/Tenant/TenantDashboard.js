/**
 * @class         :	TanantDashboard
 * @description   :
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getLoginUser } from '../../actions/UsersActions';

class TanantDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {currentLoginUserData: []};
    }

    componentDidMount () {
        document.title = "Signout - Tenant Dashboard";
        this.props.getLoginUser(this.props.token);
    }

    componentWillMount () {
        this.props.getLoginUser( this.props.token );
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.currentLoginUser && this.props.currentLoginUser !== nextProps.currentLoginUser ) {
            let currentLoginUser = nextProps.currentLoginUser;
            this.setState( { currentLoginUserData : currentLoginUser } );
        } else if ( nextProps.currentLoginUser ) {
            if ( nextProps.currentLoginUser.length > 0 ) {
                let currentLoginUser = nextProps.currentLoginUser;
                this.setState( { currentLoginUserData : currentLoginUser } );
            }
        }
    }

    render() {
        return(
                <div id="page-content-wrapper">
                    <div className="col welcome-bar">
                        <h1 className="page-title">Dashboard</h1>
                        <ul className="breadcrumb pull-right hidden-xs-down">
                            <li className="active">Welcome {this.state.currentLoginUserData.first_name} {this.state.currentLoginUserData.last_name}!</li>
                        </ul>
                    </div>
                    <div className="container-fluid mrg-top30 pd-bt30">
                        <div className="col pd-off-xs">
                            <div className="row">
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">Total Hospital (Tenat)</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-4">
                                                    <div className="dashboard-icon green text-center"><img src="/client/assets/images/icon-1.png" alt="" /></div>
                                                </div>
                                                <div className="col-8 text-right dash-content">
                                                    <h3>54</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">Total User</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-4">
                                                    <div className="dashboard-icon orange text-center"><i className="zmdi zmdi-account"></i></div>
                                                </div>
                                                <div className="col-8 text-right dash-content">
                                                    <h3>1256</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
          								   </div>
          								   <div className="row">
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">Total Patient</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-4">
                                                    <div className="dashboard-icon lightgrey text-center"><i className="zmdi zmdi-hotel"></i></div>
                                                </div>
                                                <div className="col-8 text-right dash-content">
                                                    <h3>57</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <a href="#">
                                        <div className="dash-head text-center">
                                            <h2 className="head-title">To be decided</h2>
                                        </div>
                                        <div className="white-bg dashbox">
                                            <div className="row">
                                                <div className="col-4">
                                                    <div className="dashboard-icon pink text-center"><i className="zmdi zmdi-plus-square"></i></div>
                                                </div>
                                                <div className="col-8 text-right dash-content">
                                                    <h3>16</h3>
                                                    <p>Revenue today</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

function mapStateToProps( state ) {
     return {
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data
     }
}

// const mapDispatchToProps = ( dispatch ) => ({
//   actions : bindActionCreators( { getLoginUser }, dispatch )
// });

export default connect ( mapStateToProps , { getLoginUser } )( TanantDashboard );
