import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ModalWindow from '../Modals/SearchPatientModal';
import ModalWindowEncounters from '../Modals/PatientEncountersModal';
import ModalWindowConfirmDelete from '../Modals/ConfirmDeleteModal';
import CheckinModal from '../Modals/CheckinModalSignout';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';

import { getLoginUser } from '../../actions/UsersActions';
import { getServiceById, getServiceSignoutTemplates, getServiceSignoutTemplatesEditValues } from '../../actions/ServicesActions';
import { getSignoutListById, getSignoutEncounters } from '../../actions/SignoutActions';
import { getPatients } from '../../actions/PatientsActions';
import EditorDraftValue from '../Common/EditorDraftValue';

class SignoutDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: false,
            signoutId : null,
            signoutListById : [],
            currentLoginUser : [],
            patients : [],
            templateFields : [],
            templateFieldsEdit: [],
            isTenantAdmin : false,
            editPatient : [],
            encounterId : null,
            serviceId : null,
            patientsEncounters : []
        };
        this.open_modal = this.open_modal.bind(this);
        this.open_modal_encounters = this.open_modal_encounters.bind(this);
        this.open_modal_delete = this.open_modal_delete.bind(this);
        this.open_modal_checkin = this.open_modal_checkin.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Service - Signout Details";
        this.setState({ token : this.props.token });
        this.setState({ signoutId : this.props.params.signoutId });
    }

    componentWillMount() {
      this.setState({ token : this.props.token });
      this.setState({ signoutId : this.props.params.signoutId });

      this.props.getLoginUser( this.props.token );
      //@args - token, signout id
      this.props.getSignoutListById( this.props.token, this.props.params.signoutId );
      this.props.getPatients(this.props.token);

      let serviceId = this.props.location.query.sid;
      if(serviceId) {
          this.props.getServiceById( this.props.token, serviceId );
          this.props.getServiceSignoutTemplates(this.props.token, serviceId);
          this.props.getServiceSignoutTemplatesEditValues(this.props.token, serviceId);
          this.setState({ serviceId : serviceId });
      }
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.signoutListById && this.props.signoutListById !== nextProps.signoutListById ) {
            let signoutListById = nextProps.signoutListById;
            this.setState( { signoutListById : signoutListById } );

            this.setState( { patientsEncounters : signoutListById.patientsEncounters } );


            // let preSelectedArr = [];
            // if(nextProps.signoutListById && nextProps.currentLoginUser) {
            //     nextProps.currentLoginUser.groups.map(( userGroup ) => {
            //         nextProps.signoutListById.groups.map(( signoutGroup ) => {
            //             if(userGroup.id === signoutGroup.id) {
            //                 signoutGroup.active = 'active';
            //                 preSelectedArr.push(signoutGroup);
            //             }
            //         });
            //     });
            // }
            // this.setState({ preSelectedGroups : preSelectedArr });
        }

        if ( nextProps.currentLoginUser && this.props.currentLoginUser !== nextProps.currentLoginUser ) {
            let currentLoginUser = nextProps.currentLoginUser;
            this.setState( { currentLoginUser : currentLoginUser } );
        }

        if ( nextProps.patients && this.props.patients !== nextProps.patients ) {
            let patients = nextProps.patients;
            this.setState( { patients : patients } );
        }

        if(nextProps.currentLoginUser) {
           this.setState({ isTenantAdmin : nextProps.currentLoginUser.is_tenant_admin });
        }

        if ( nextProps.signoutTemplates && this.props.signoutTemplates !== nextProps.signoutTemplates ) {
            let signoutTemplates = nextProps.signoutTemplates;
            _.sortBy(signoutTemplates, ['ordinal']);
            this.setState( { templateFields : signoutTemplates } );
        }

        if ( nextProps.signoutTemplatesEdit && this.props.signoutTemplatesEdit !== nextProps.signoutTemplatesEdit ) {
            let signoutTemplatesEdit = nextProps.signoutTemplatesEdit;
            _.sortBy(signoutTemplatesEdit, ['ordinal']);
            this.setState( { templateFieldsEdit : signoutTemplatesEdit } );
        }
    }

    open_modal(data) {

        this.props.getSignoutListById( this.state.token, this.state.signoutId );
        this.setState({
            modal1: !this.state.modal1
        });
    }

    open_modal_encounters(data) {

      this.props.getSignoutListById( this.state.token, this.state.signoutId );

      if(data.id !== undefined) {
        this.props.getSignoutEncounters(this.state.token, this.state.signoutId, data.id);
      }

      //this.props.getServiceSignoutTemplatesEditValues(this.state.token, this.state.serviceId);

        this.setState({
            modal2: !this.state.modal2,
            editPatient: data
        });
    }

    open_modal_delete( e, encounterId ) {

      this.props.getSignoutListById( this.state.token, this.state.signoutId );

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal3: !this.state.modal3,
          encounterId: encounterId === undefined ? null : encounterId
      });
    }

    open_modal_checkin() {

        this.props.getLoginUser( this.props.token );
        this.props.getSignoutListById( this.props.token, this.state.signoutId );

        this.setState({
            modal4: !this.state.modal4,
        });
    }

    renderTemplateFields() {

      return this.state.templateFields.map(( template ) => {

          return(
            <div className="th firstname" style={{ width: `${Math.round(80 / this.state.templateFields.length)}%` }} key={template.id}>{ template.name }</div>
          );
      });
    }

    renderTemplateValues(encounterFieldData) {

      if(encounterFieldData === null) {
        return this.state.templateFields.map(( template ) => {
            return(
              <div className="td lastname" style={{ width: `${Math.round(80 / this.state.templateFields.length)}%` }} key={template.id}>{ template.default_value }</div>
            );
        });
      }

      if(encounterFieldData) {
        return encounterFieldData.field_values.map(( template ) => {
            return(
              <div className="td lastname" style={{ width: `${Math.round(80 / this.state.templateFields.length)}%` }} key={template.signout_list_field_template_id}>
                <EditorDraftValue content = { template.field_value } editorIndex = { template.signout_list_field_template_id } />
              </div>
            );
        });
      }
    }

      onClickDivMobileView(e, patientId) {
      e.preventDefault()

      this.state.patientsEncounters.map(( patientE ) => {
        if(patientId !== patientE.id) {
          document.getElementById(patientE.id).classList.contains("show")
            ? document.getElementById(patientE.id).classList.remove("show")
            : "";
        }
      });

      e.target.classList.contains("colpsdiv")
        ? e.target.classList.remove("colpsdiv")
        : e.target.classList.add("colpsdiv");


      document.getElementById(patientId).classList.contains("show")
        ? document.getElementById(patientId).classList.remove("show")
        : document.getElementById(patientId).classList.add("show")
    }

    renderRow() {

        if(this.props.isFetching) {
          return(
            <div className="tr">
              <div className="td accordion-xs-toggle">
                <div className="loader" id="loader-4">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </div>
            </div>
          );
        }

        if(this.state.patientsEncounters === undefined || this.state.patientsEncounters == '0') {
          return (
            <div className="tr">
              <div className="td accordion-xs-toggle">No patient encounter.</div>
            </div>
          );
        }

        if( this.state.signoutListById ) {
          return this.state.patientsEncounters.map(( patientE ) => {
            return (
              <div className="tr" key={ patientE.id }>
                  <div className="td firstname accordion-xs-toggle" style={{ width: '13%' }}
                    onClick={(e)=>this.onClickDivMobileView(e, patientE.id)}>
                    { patientE.patient.first_name } { patientE.patient.middle_name }  { patientE.patient.last_name } <br />mrn:{ patientE.patient.mrn }
                  </div>
                  <div id={patientE.id} className="accordion-xs-collapse collapse">
                    <div className="inner">
                        { this.renderTemplateValues(patientE.encounterFieldData) }
                        <div className="td lastname action-click" style={{ width: '7%' }}>
                            <button
                              onClick={(e) => this.open_modal_encounters(patientE)}
                              data-toggle="modal"
                              data-target="#edit-editor"
                              className="action-btn">
                              <i className="zmdi zmdi-edit"></i>
                            </button>
                            {/**<button className="action-btn"><i className="zmdi zmdi-accounts"></i></button>**/}
                            <button
                              onClick={ (e) => this.open_modal_delete( e, patientE.id )}
                              className="action-btn">
                              <i className="zmdi zmdi-delete"></i>
                            </button>
                        </div>
                    </div>
                  </div>
              </div>
            );
          });
        }
    }


    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar ">
                    <h1 className="page-title">{ this.state.signoutListById.name } { (this.state.signoutListById.service === undefined) ? '' : `(${this.state.signoutListById.service.name})` }</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><Link to={`/signoutlist`}>Sign out</Link></li>
                        <li className="active">Sign out Detail</li>
                    </ul>
                </div>

                <ModalWindow
                            modal_var={this.state.modal1}
                            handle_modal = {this.open_modal}
                            token = { this.state.token }
                            signoutId = { this.state.signoutId }
                            patients = { this.state.patients } />

                <ModalWindowEncounters
                            modal_var={this.state.modal2}
                            handle_modal = {this.open_modal_encounters}
                            token = { this.state.token }
                            signoutId = { this.state.signoutId }
                            editPatient = { this.state.editPatient }
                            templateFields = { this.state.templateFieldsEdit } />

                <ModalWindowConfirmDelete
                            modal_var = {this.state.modal3 }
                            handle_modal = { this.open_modal_delete }
                            token = { this.state.token }
                            confirmPage = { 'SIGNOUT_ENCOUNTER' }
                            signoutId = { this.state.signoutId }
                            encounterId = { this.state.encounterId } />

              <CheckinModal
                            modal_var={this.state.modal4}
                            handle_modal = {this.open_modal_checkin}
                            token={this.state.token}
                            signoutListById={this.state.signoutListById} />

                <div className="container-fluid mrg-top30 pd-bt30">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="white-bg">
                                    <ButtonM
                                        type="button"
                                        onClick={this.open_modal}
                                        className="btn blue-btn add-btn"
                                        data-toggle="modal"
                                        data-target="#create-patient">
                                        <span className="hidden-xs-down">ADD PATIENT</span>
                                        <img className="hidden-sm-up" src="/client/assets/images/add-file.png" />
                                      </ButtonM>

                                    <div className="print-btn">
                                      <button type="button" onClick={this.open_modal_checkin} className="internal-checkin">
                                        <i className="fa fa-check-square-o" aria-hidden="true"></i>
                                          <span className="hidden-xs-down">Check-in</span>
                                      </button>
                                        <button><i className="zmdi zmdi-print"></i><span>Print</span></button>
                                    </div>
                                    <div className="table-responsive">
                                        <div className="divtable accordion-xs">
                                            <div className="tr headings">
                                              <div className="th firstname" style={{ width: '13%' }}>Patient Name</div>
                                              { this.renderTemplateFields() }
                                              <div className="th firstname" style={{ width: '7%' }}>Action</div>
                                            </div>

                                            { this.renderRow() }
                                        </div>
                                        {/*
                                        <!--table End-->*/}
                                    </div>

                                        {/*
                                        <!--pagination-->*/}
                                    {/**<div className="pagination-wrap">
                                        <nav aria-label="Page navigation example">
                                            <ul className="pagination">
                                                <li className="page-item">
                                                    <a className="page-link" href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span className="sr-only">Previous</span> </a>
                                                </li>
                                                <li className="page-item active"><a className="page-link" href="#">1</a></li>
                                                <li className="page-item"><a className="page-link" href="#">2</a></li>
                                                <li className="page-item"><a className="page-link" href="#">3</a></li>
                                                <li className="page-item"><a className="page-link" href="#">4</a></li>
                                                <li className="page-item"><a className="page-link" href="#">5</a></li>
                                                <li className="page-item">
                                                    <a className="page-link" href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span className="sr-only">Next</span> </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>**/}
                                    {/*
                                    <!--pagination-->*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}


function mapStateToProps(state) {
     return {
         isFetching: state.signouts.isFetching,
         signoutListById: state.signouts.signoutListById,
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data,
         patients: state.patients.data,
         isFetching: state.services.isFetching,
         signoutTemplates: state.services.signoutTemplates,
         signoutTemplatesEdit: state.services.signoutTemplatesEdit
     }
}

export default connect(mapStateToProps, { getLoginUser,
                                          getPatients,
                                          getSignoutListById,
                                          getServiceById,
                                          getServiceSignoutTemplates,
                                          getServiceSignoutTemplatesEditValues,
                                          getSignoutEncounters })( SignoutDetail );
