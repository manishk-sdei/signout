/**
 * @component     : AddEditUser
 * @description   : Add users / Assign services and admin departments / Mark user as hospital admin
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { browserHistory } from 'react-router';
import { getRoles } from '../../actions/RolesActions';
import { getDepartmens } from '../../actions/DepartmentActions';
import { getServices } from '../../actions/ServicesActions';
import { getUsers,
         createUser,
         getUserById,
         updateUserFormInitialValues,
         createUserServices,
         createUserDepartments,
         removeUserServices,
         removeUserDepartments } from '../../actions/UsersActions';

import ReactPhoneInput from 'react-phone-input';
import { phoneValidator } from '../Common/validate';
import MaskedInput from 'react-maskedinput';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import Multiselect from 'react-widgets/lib/Multiselect';
import MultiselectNew from '../Common/MultiSelect';

let colors = ['orange', 'red', 'blue', 'purple'];


class AddEditUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            roles : [],
            departments : [],
            services : [],
            tenantId: null,
            userId: null,
            userById: [],

            id : null,
            first_name: null,
            last_name: null,
            employee_id: null,
            email: null,
            phone_work: null,
            phone_mobile: null,
            department: null,
            role: null,
            assign_services: [],
            assign_department_admin: [],
            is_hospital_admin: false,
            alreadyAssignedDept : [],
            alreadyAssignedServices : []
        };

        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Roles";
    }

    componentWillMount() {

      this.props.getRoles(this.props.token);
      this.props.getDepartmens(this.props.token);
      this.props.getServices(this.props.token);

      if(this.props.users) {
          this.setState({tenantId: this.props.users.tenant_id});
      }

      // Get user to edit
      if(this.props.userId) {
          this.props.getUserById(this.props.token, this.props.userId);
          this.setState({ userId : this.props.userId });
      }

      this.props.getUsers(this.props.token);
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.roles && this.props.roles !== nextProps.roles ) {
            let roles = nextProps.roles;
            this.setState( { roles : roles } );
        }

        if ( nextProps.departments && this.props.departments !== nextProps.departments ) {
            let departments = nextProps.departments;
            this.setState( { departments : departments } );
        }

        if ( nextProps.services && this.props.services !== nextProps.services ) {
            let services = nextProps.services;
            this.setState( { services : services } );
        }

        if ( nextProps.userById && this.props.userById !== nextProps.userById ) {

            let userById = nextProps.userById;
            this.props.updateUserFormInitialValues(nextProps.userById);

             // get services and departments assigned to this users
             let deptAdmin = [];
             if(userById.adminDepartments) {
               userById.adminDepartments.map((dept) => {
                   deptAdmin.push(dept.id);
               });
             }

             let serviceArr = [];
             if(userById.services) {
               userById.services.map((service) => {
                  serviceArr.push(service.id);
               });
             }

             this.setState( { userById : userById,
                             id : userById.id,
                             first_name : userById.first_name,
                             last_name : userById.last_name,
                             employee_id : userById.employee_id,
                             email : userById.email,
                             phone_work : userById.phone_work,
                             phone_mobile : userById.phone_mobile,
                             department : userById.department_id,
                             role : userById.role_id,
                             is_hospital_admin : userById.is_tenant_admin,
                             assign_department_admin : deptAdmin,
                             alreadyAssignedDept : deptAdmin,
                             assign_services : serviceArr,
                             alreadyAssignedServices : serviceArr});
        }
    }

    _onChange(e) {
        let stateChange = {};
        stateChange[e.target.name] = e.target.value;
        this.setState(stateChange);
    }

    onSubmit(formData) {

        // Assign Services
        if(formData && this.state.assign_services !== undefined) {

            if(_.isEqual(this.state.assign_services.sort(), this.state.alreadyAssignedServices.sort())) {
              console.log('equal array');
            } else {

              let insertServices = [];
              this.state.assign_services.map(( service ) => {
                    insertServices.push(service.id);
              });

              let removeServices = _.difference(this.state.alreadyAssignedServices.sort(), insertServices.sort());
              let addServices = _.difference(insertServices.sort(), this.state.alreadyAssignedServices.sort());

              // remove assigned department
              removeServices.map(( removeServiceId ) => {
                  // Call API - to remove service
                  this.props.removeUserServices(this.props.token, this.props.userId, removeServiceId);
              });

              // Assign admin departments
              addServices.map(( addServiceId ) => {
                  // Call API - to add service
                  this.props.createUserServices(this.props.token, this.props.userId, addServiceId);
              });
            }
        }

        // Assign Admin Departments
        if(formData && this.state.assign_department_admin !== undefined) {

            if(_.isEqual(this.state.assign_department_admin.sort(), this.state.alreadyAssignedDept.sort())) {
              console.log('equal array');
            } else {

              let insertDept = [];
              this.state.assign_department_admin.map(( dept ) => {
                    insertDept.push(dept.id);
              });

              let removeDepartments = _.difference(this.state.alreadyAssignedDept.sort(), insertDept.sort());
              let addDepartments = _.difference(insertDept.sort(), this.state.alreadyAssignedDept.sort());

              // remove assigned department
              removeDepartments.map(( removeDeptId ) => {
                  // Call API - to remove admin department
                  this.props.removeUserDepartments(this.props.token, this.props.userId, removeDeptId);
              });

              // Assign admin departments
              addDepartments.map(( addDeptId ) => {
                  // Call API - to add admin department
                  this.props.createUserDepartments(this.props.token, this.props.userId, addDeptId);
              });
            }
        }

        // Refine Phone Numbers
        formData.is_hospital_admin=this.state.is_hospital_admin;
        let workPhone = phoneValidator(this.state.phone_work);
        formData.phone_work = workPhone;
        let mobilePhone = phoneValidator(this.state.phone_mobile);
        formData.phone_mobile = mobilePhone;

        formData.department = this.state.department;
        formData.role = this.state.role;

        // Form data
        this.props.createUser(this.props.token, this.props.users.tenant_id, this.state.id, formData);
        setTimeout(function() {
          browserHistory.push('/users');
        }, 2000);
    }

    onChange(e) {
        let stateChange = {};
        stateChange[e.target.name] = e.target.value;
        this.setState(stateChange);
      }

    render() {

        const { fields: { first_name,
                          last_name,
                          employee_id,
                          email,
                          phone_work,
                          phone_mobile,
                          department,
                          role,
                          assign_services,
                          assign_department_admin,
                          is_hospital_admin }, handleSubmit } = this.props;
        return(
          <form noValidate onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <div className="row">
                <div className="col-sm-6">
                    <div className ={`form-group ${first_name.touched && first_name.invalid ? 'has-danger' : ''}`}>
                        <input
                              {...first_name} required
                              className="inputMaterial"
                              type="text"
                              maxLength="50"
                              value={this.props.userId === undefined ? "" : this.state.first_name}
                              onChange={(event)=>this.setState({first_name: event.target.value})} />
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>First Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {first_name.touched ? first_name.error : '' }
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className ={`form-group ${last_name.touched && last_name.invalid ? 'has-danger' : ''}`}>
                        <input
                              className="inputMaterial"
                              type="text"
                              required
                              {...last_name}
                              maxLength="50"
                              value={this.props.userId === undefined ? "" : this.state.last_name}
                              onChange={(event)=>this.setState({last_name: event.target.value})} />
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>Last Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {last_name.touched ? last_name.error : '' }
                        </div>
                    </div>
                </div>
              </div>
                <div className="row">
                  <div className="col-sm-6">
                      <div className ={`form-group ${employee_id.touched && employee_id.invalid ? 'has-danger' : ''}`}>
                          <input
                                className="inputMaterial"
                                type="text"
                                required
                                {...employee_id}
                                maxLength="50"
                                value={this.props.userId === undefined ? "" : this.state.employee_id}
                                onChange={(event)=>this.setState({employee_id: event.target.value})} />
                          <span className="highlight"></span>
                          <span className="bar"></span>
                          <label>Employee Id <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {employee_id.touched ? employee_id.error : '' }
                          </div>
                      </div>
                  </div>
                  <div className="col-sm-6">
                      <div className ={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                          <input
                                className="inputMaterial"
                                type="text"
                                required
                                {...email}
                                maxLength="80"
                                value={this.props.userId === undefined ? "" : this.state.email}
                                onChange={(event)=>this.setState({email: event.target.value})} />
                          <span className="highlight"></span>
                          <span className="bar"></span>
                          <label>Email <i className="reqfld">*</i></label>
                          <div className="error_msg_danger">
                              {email.touched ? email.error : '' }
                          </div>
                      </div>
                  </div>
              </div>

              <div className="row">
                  <div className="col-sm-6">
                      <div className="form-group">
                        <MaskedInput
                                {...phone_work}
                                value={ this.state.phone_work }
                                placeholder="(+01) XXX XXX XXXX"
                                mask="(+11) 111 111 1111"
                                name="phone_work"
                                onChange={this._onChange}
                                className="inputMaterial" />
                      </div>
                  </div>
                  <div className="col-sm-6">
                      <div className ={`form-group ${phone_mobile.touched && phone_mobile.invalid ? 'has-danger' : ''}`}>
                          <MaskedInput
                                  {...phone_mobile}
                                  value={ this.state.phone_mobile }
                                  placeholder="(+01) XXX XXX XXXX"
                                  mask="(+11) 111 111 1111"
                                  name="phone_mobile"
                                  onChange={this._onChange}
                                  className="inputMaterial" />
                          <div className="error_msg_danger">
                              { phone_mobile.touched ? phone_mobile.error : '' }
                          </div>
                      </div>
                  </div>
              </div>

              <div className="row">
                  <div className="col-sm-6">
                      <div className="form-group">
                          <div className="custom-box">
                            <Select
                                  name="department" {...department}
                                  value={ this.state.department }
                                  onChange={(event)=>this.setState({ department : event.target.value })}>
                                <Option label="Department" />
                                { this.state.departments.map( department => <option key={ department.id } value={ department.id }>{ department.name }</option>) }
                            </Select>
                          </div>
                      </div>
                  </div>

                  <div className="col-sm-6">
                      <div className="form-group">
                          <div className="custom-box">
                            <Select
                                  name="role" {...role}
                                  value={ this.state.role }
                                  onChange={(event)=>this.setState({ role : event.target.value })}>
                                  <Option label="Role" />
                                  { this.state.roles.map(role => <option value={ role.id } key={ role.id }>{ role.name }</option>) }
                            </Select>
                          </div>
                      </div>
                  </div>
              </div>

              <div className="row">
                  <div className="col-sm-6">
                      <div className="form-group">
                        <div className="custom-box">
                          <Multiselect
                                {...assign_services}
                                placeholder="Assign services"
                                valueField='id'
                                textField='name'
                                data={this.state.services}
                                value={ this.state.assign_services }
                                onChange={value => this.setState({ assign_services : value })}
                                  />
                        </div>
                      </div>
                  </div>

                  <div className="col-sm-6">
                      <div className="form-group">
                          <div className="custom-box">
                                  <Multiselect
                                        {...assign_department_admin}
                                        placeholder="Assign Department Admin"
                                        valueField='id'
                                        textField='name'
                                        data={this.state.departments}
                                        value={this.state.assign_department_admin}
                                        onChange={value => this.setState({ assign_department_admin : value })} />

                          </div>
                      </div>
                  </div>
              </div>
              <div className="row">
                  <div className="col-sm-6">
                      <div className="form-group mr-lg-off">
                          <div className="checkbox">
                              <label>
                                  <input
                                      type="checkbox"
                                      {...is_hospital_admin}
                                      checked={this.state.is_hospital_admin}
                                      onChange={(e)=>this.setState({is_hospital_admin: e.target.checked})} />
                                  <span className="cr"><i className="cr-icon fa fa-check"></i></span> Select user as hospital admin </label>
                          </div>
                      </div>
                  </div>

                  <div className="col-sm-6">
                      <div className="form-group mar-offbt">
                        <button
                            type="submit"
                            className="btn blue-btn sign-btn">
                            <span>{ this.props.isAuthenticating === true ? 'Saving...' : 'Save' }</span></button>
                      </div>
                  </div>
              </div>

          </form>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.first_name) {
        error.first_name = 'Please enter first name';
    }

    if(!values.last_name) {
        error.last_name = 'Please enter last name';
    }

    if(!values.employee_id) {
        error.employee_id = 'Please enter employee id';
    }

    if(!values.email) {
        error.email = 'Please enter email address';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(!values.email) {
            error.email = 'Please enter email address';
        }else {
            error.email = 'Please enter a valid email address';
        }
    }

    if(!values.phone_mobile) {
        error.phone_mobile = 'Please enter mobile phone number';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isFetchingRole: state.roles.isFetching,
         roles: state.roles.data,
         isFetchingDepartments: state.departments.isFetching,
         departments: state.departments.data,
         isFetchingServices: state.services.isFetching,
         services: state.services.data,
         isFetching: state.users.isFetching,
         users : state.users.data,
         userById : state.users.userById,
         isAuthenticating : state.users.isAuthenticating,
         initialValues: {
           first_name : state.users.first_name,
           last_name : state.users.last_name,
           employee_id : state.users.employee_id,
           email : state.users.email,
           phone_mobile : state.users.phone_mobile,
           phone_work : state.users.phone_work,
           department : state.users.department_id,
           role: state.users.role_id,
           is_hospital_admin: state.users.is_tenant_admin
         }
     }
}

export default reduxForm({
    form: 'AddEditUserForm',
    fields: ['first_name',
             'last_name',
             'employee_id',
             'email',
             'phone_work',
             'phone_mobile',
             'department',
             'role',
             'assign_services',
             'assign_department_admin',
             'is_hospital_admin'],
    validate
}, mapStateToProps, { getUsers,
                      getRoles,
                      getDepartmens,
                      getServices,
                      createUser,
                      getUserById,
                      updateUserFormInitialValues,
                      createUserServices,
                      createUserDepartments,
                      removeUserDepartments,
                      removeUserServices })( AddEditUser );
