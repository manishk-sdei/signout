import React, { Component } from 'react';

export default class Directories extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        document.title = "Signout - Account";
    }

    render() {
        return(
            <div id="page-content-wrapper">
                {/*<!-- Page Content -->*/}
                <div className="col welcome-bar">
                    <h1 className="page-title">Directory</h1>
                    <ul className="breadcrumb pull-right hidden-xs-down">
                        <li><a href="signoutlist.html">Sign out</a></li>
                        <li className="active">Directory</li>
                    </ul>
                </div>
                <div className="container-fluid mrg-top30 pd-bt30">
                    <div className="col pd-off-xs">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="white-bg">
                                    <h3 className="sub-title">On Call Physicians</h3>
                                    <div className="form-group search-bar dir-search">
                                        <input className="inputMaterial" type="text" required />
                                        <span className="highlight"></span> <span className="bar"></span>
                                        <label>Search Patient Name and MRN.</label>
                                        <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                                    </div>
                                    <div className="table-responsive">
                                        <div className="dr-wrap">
                                            <h3 className="content-title blue-text">Internal Medicine</h3>
                                            <table className="table table-hover patient-list ">
                                                {/*<!--table start-->*/}
                                                <thead>
                                                    <tr>
                                                        <th width="25%">Name</th>
                                                        <th width="25%">Title</th>
                                                        <th width="25%">Number</th>
                                                        <th width="25%">Pager</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Carolyn Campbell</td>
                                                        <td>Title 1</td>
                                                        <td>989-878-6785</td>
                                                        <td>657-457-9876</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer Carpenter</td>
                                                        <td>Title 2</td>
                                                        <td>345-651-9870</td>
                                                        <td>987-467-1254</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        {/* <!--table End-->*/}
                                        </div>
                                        <div className="dr-wrap">
                                            <h3 className="content-title blue-text">ICU</h3>
                                            <table className="table table-hover patient-list ">
                                                {/*<!--table start-->*/}
                                                <thead>
                                                    <tr>
                                                        <th width="25%">Name</th>
                                                        <th width="25%">Title</th>
                                                        <th width="25%">Number</th>
                                                        <th width="25%">Pager</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Carolyn Campbell</td>
                                                        <td>Title 3</td>
                                                        <td>989-878-6785</td>
                                                        <td>657-457-9876</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer Carpenter</td>
                                                        <td>Title 4</td>
                                                        <td>345-651-9870</td>
                                                        <td>987-467-1254</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {/*<!--table End-->*/}
                                        </div>
                                        <div className="dr-wrap">
                                            <h3 className="content-title blue-text">CCU</h3>
                                            <table className="table table-hover patient-list ">
                                                {/*<!--table start-->*/}
                                                <thead>
                                                    <tr>
                                                        <th width="25%">Name</th>
                                                        <th width="25%">Title</th>
                                                        <th width="25%">Number</th>
                                                        <th width="25%">Pager</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Carolyn Campbell</td>
                                                        <td>Title 5</td>
                                                        <td>989-878-6785</td>
                                                        <td>657-457-9876</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer Carpenter</td>
                                                        <td>Title 6</td>
                                                        <td>345-651-9870</td>
                                                        <td>987-467-1254</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {/*<!--table End-->*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
