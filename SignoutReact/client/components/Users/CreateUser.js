import React, { Component } from 'react';
import { Link } from 'react-router';
import AddEditUser from './AddEditUser';
import RoleList from '../Roles/RoleList';

export default class CreateUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data : false,
            isActive : true,
            token : null,
            userId: null
        };
        this.onTabClick = this.onTabClick.bind(this);
    }

    componentDidMount() {

        document.title = "Signout - Users";
        this.setState({ token: this.props.token });
    }

    componentWillMount() {
      //this.props.getRoles(this.props.token);
      this.setState({ token: this.props.token });
      this.setState({ userId : this.props.params.userId })
    }

    onTabClick(event) {
      if(this.state.isActive===true) {
        this.setState({isActive: false});
      } else {
        this.setState({isActive: true});
      }
      event.preventDefault();
    }

    render() {
        return(
          <div id="page-content-wrapper">
        {/*
        <!-- Page Content -->*/}
        <div className="col welcome-bar ">
            {/*
            <!-- welcome bar -->*/}
            <h1 className="page-title">Create User & Role List</h1>
            <ul className="breadcrumb pull-right hidden-xs-down">
                <li><Link to={`/signoutlist`}>Sign out</Link></li>
                <li className="active"><Link to={`/users`}>Users</Link></li>
                <li className="active">Create User & Role List</li>
            </ul>
        </div>

        <div className="container-fluid mrg-top30 pd-bt30">
            <div className="col pd-off-xs">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-bg overflow-none">
                            <div className="userroll">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item" id="usertab" onClick={(event)=>this.onTabClick(event)}> <a className={this.state.isActive? "nav-link active" : "nav-link"} data-toggle="tab" role="tab">Create User</a> </li>
                                    <li className="nav-item" id="roletab" onClick={(event)=>this.onTabClick(event)}> <a className={this.state.isActive? "nav-link" : "nav-link active"} data-toggle="tab" role="tab">Roles</a> </li>
                                </ul>

                                <div className="tab-content">

                                    <div className={this.state.isActive? "tab-pane active" : "tab-pane"} id="adduser" role="tabpanel">
                                        <AddEditUser token={ this.state.token } userId={ this.state.userId } />
                                    </div>

                                    <div className={this.state.isActive? "tab-pane" : "tab-pane active"} id="roll" role="tabpanel">

                                        <RoleList token={ this.state.token } />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );

    }
}

// function mapStateToProps(state) {
//      return {
//          isFetching: state.roles.isFetching,
//          roles: state.roles.data
//      }
// }
//
// export default connect(mapStateToProps, { getDepartmens, getRoles })(CreateUser);
