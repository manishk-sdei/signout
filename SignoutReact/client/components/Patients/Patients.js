import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import moment from 'moment';
import ModalWindow from '../Modals/AddEditPatientModal';
import ModalWindowConfirmDelete from '../Modals/ConfirmDeleteModal';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import { getLoginUser } from '../../actions/UsersActions';
import { countPatients, getPatientsWithPagination, updatePatient } from '../../actions/PatientsActions';
import { getCountries,
         getLanguages,
         getMeritalStatus,
         getRace,
         getSex,
         getTimeZone } from '../../actions/TermsActions';
import Pagination from '../Common/Pagination';
import { PAGINATION_DEFAULT_LIMIT } from '../../actions/Constants';

class PatientList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            patientsData: [],
            editPatientData: [],
            modal1: false,
            modal2: false,
            patientId: null,
            token: null,
            isTenantAdmin: false,
            deletePatient: null,
            totalPatients: null
        };
        this.open_modal = this.open_modal.bind(this);
        this.open_modal_delete = this.open_modal_delete.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Patients";
        this.setState({ token: this.props.token });
    }

    componentWillMount() {
        this.setState({ token: this.props.token });
        this.props.getLoginUser(this.props.token);

        this.props.countPatients(this.props.token, query);

        let limit = PAGINATION_DEFAULT_LIMIT;
        let orderBy = 'id';
        let order = 'desc';
        let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
        this.props.getPatientsWithPagination(this.props.token, query);

        this.props.getCountries(this.props.token);
        this.props.getLanguages(this.props.token);
        this.props.getMeritalStatus(this.props.token);
        this.props.getRace(this.props.token);
        this.props.getSex(this.props.token);
        this.props.getTimeZone(this.props.token);
    }

    componentWillReceiveProps( nextProps ) {

        if (nextProps.currentLoginUser) {
           this.setState({ isTenantAdmin : nextProps.currentLoginUser.is_tenant_admin });
        }

        if ( nextProps.patients && this.props.patients !== nextProps.patients ) {
            let patients = nextProps.patients;
            this.setState( { patientsData : patients } );
        }

        if(nextProps.totalPatients) {
          this.setState( { totalPatients : nextProps.totalPatients } );
        }
    }

    // Open modal on add and update Patient
    open_modal(e, data) {//console.log("ddddd--------", data);

      let limit = PAGINATION_DEFAULT_LIMIT;
      let orderBy = 'id';
      let order = 'desc';
      let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;

      this.props.countPatients(this.props.token, query);
      this.props.getPatientsWithPagination(this.props.token, query);
      this.props.updatePatient(data); // Update row

      if( e===true ) {
          e.preventDefault();
      } else {

          this.setState({
              modal1: !this.state.modal1,
              editPatientData: data,
          });
      }
    }

    open_modal_delete( e, patientId ) {

      if(this.state.deletePatient) {
        let limit = PAGINATION_DEFAULT_LIMIT;
        let orderBy = 'id';
        let order = 'desc';
        let query = `?limit=${limit}&orderBy=${orderBy}&order=${order}`;
        this.props.getPatientsWithPagination(this.props.token, query); // update row
      }

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal2: !this.state.modal2,
          deletePatient: patientId,
      });
    }

    renderRow() {

      if(this.props.isFetching) {
        return(
          <tr>
            <td>
              <div className="loader" id="loader-4">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </td>
          </tr>
        );
      }

        if(this.state.patientsData.length <= 0) {
          return (
            <tr>
              <td colSpan="5">
                  No patient.
              </td>
            </tr>
          );
        }

        if( this.state.patientsData ) {
          return this.state.patientsData.map(( patient ) => {
            return (
              <tr key= { patient.id }>
                  <td>{ patient.first_name } { patient.last_name }</td>
                  <td>{ patient.sex }</td>
                  <td>{ moment(patient.dob).format('MM/DD/YYYY') }</td>
                  <td>{ patient.mrn }</td>
                  <td>
                      <button
                              onClick={ (e)=>this.open_modal(e, patient) }
                              className="action-btn"
                              data-toggle="modal"
                              data-target="#edit-patient1">
                              <i className="zmdi zmdi-edit"></i>
                      </button>
                      <button
                              onClick={ (e) => this.open_modal_delete( e, patient.id )}
                              className="action-btn">
                              <i className="zmdi zmdi-delete"></i></button>
                  </td>
              </tr>
            );
          });
        }
    }

    render() {
        return(
          <div id="page-content-wrapper">
              {/*
              <!-- Page Content -->*/}
              <div className="col welcome-bar ">
                  {/*
                  <!-- welcome bar -->*/}
                  <h1 className="page-title">Patient List</h1>
                  <ul className="breadcrumb pull-right hidden-xs-down">
                      <li><Link to={`/signoutlist`}>Sign out</Link></li>
                      <li className="active">Patient List</li>
                  </ul>
              </div>

              {/** ModalWindow - with props **/}

              <ModalWindow
                          modal_var = {this.state.modal1}
                          handle_modal = { this.open_modal }
                          token = { this.state.token }
                          patient = { this.state.editPatientData }  />

              <ModalWindowConfirmDelete
                          modal_var = {this.state.modal2 }
                          handle_modal = { this.open_modal_delete }
                          token = { this.state.token }
                          confirmPage = { 'PATIENT_LIST' }
                          patientId = { this.state.deletePatient } />

              <div className="container-fluid mrg-top30 pd-bt30">
                  <div className="col pd-off-xs">
                      <div className="row">
                          <div className="col-sm-12">
                              <div className="white-bg">
                                  <ButtonM type="button"
                                           onClick={(e)=>this.open_modal(e, this.state.data)}
                                           className="add-deprt btn blue-btn add-btn"
                                           data-toggle="modal"
                                           data-target="#create-patient">
                                           <span className="hidden-xs-down">Create Patient</span>
                                             <img className="hidden-sm-up"
                                                  src="/client/assets/images/add-file.png" />
                                  </ButtonM>

                                  <div className="table-responsive">
                                      <table className="table patient-list">
                                          <thead>
                                              <tr>
                                                  <th width="25%">Patient Name</th>
                                                  <th width="20%">Gender</th>
                                                  <th width="20">DOB</th>
                                                  <th width="20%">MRN</th>
                                                  <th width="15%"></th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              { this.renderRow() }
                                            </tbody>
                                      </table>
                                  </div>
                                  {(this.state.patientsData.length > 0) ?
                                  <Pagination
                                            token = { this.state.token }
                                            pageName = "TENANT_PATIENTS"
                                            totalPages ={ this.state.totalPatients } /> : ''
                                  }
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.patients.isFetching,
         patients: state.patients.data,
         totalPatients: state.patients.totalPatients
     }
}

export default connect(mapStateToProps, {  getLoginUser,
                                           countPatients,
                                           getPatientsWithPagination,
                                           updatePatient,
                                           getCountries,
                                           getLanguages,
                                           getMeritalStatus,
                                           getRace,
                                           getSex,
                                           getTimeZone })(PatientList);
