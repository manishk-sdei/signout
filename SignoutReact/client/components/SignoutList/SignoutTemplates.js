import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getServiceById, getServiceSignoutTemplates, createServiceSignoutTemplates } from '../../actions/ServicesActions';

import { Collapse, Button, CardBlock, Card } from 'reactstrap';
import ModalWindow from '../Modals/SignoutTemplateModal';
import ButtonM from 'muicss/lib/react/button';

class SignoutTemplates extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            token: null,
            serviceId: null,
            isTenantAdmin: false,
            templateData: [],
            serviceData: []
        };
        this.open_modal = this.open_modal.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Service - Signout List";
        this.setState({ token : this.props.token });
        this.setState({ serviceId : this.props.serviceId });
    }

    componentWillMount() {
      this.setState({ token : this.props.token });
      this.setState({ serviceId : this.props.serviceId });

      //@args - token, service id
      this.props.getServiceById( this.props.token, this.props.serviceId );
      this.props.getServiceSignoutTemplates(this.props.token, this.props.serviceId);
    }

    componentWillReceiveProps( nextProps ) {//console.log(nextProps);

        if(nextProps.currentLoginUser) {
           this.setState({ isTenantAdmin : nextProps.currentLoginUser.is_tenant_admin });
        }

        if ( nextProps.service && this.props.service !== nextProps.service ) {
            let service = nextProps.service;
            this.setState( { serviceData : service } );
        }

        if ( nextProps.signoutTemplates && this.props.signoutTemplates !== nextProps.signoutTemplates ) {
            let signoutTemplates = nextProps.signoutTemplates;
            // Sort array by field value
            _.sortBy(signoutTemplates, ['ordinal']);
            this.setState({ templateData : signoutTemplates });
        }
    }

    open_modal(e, data) {

      //if(this.state.templateData.length > 0) {
          this.props.getServiceSignoutTemplates(this.state.token, this.state.serviceId);
      //}

      if( e===true ) {
          e.preventDefault();
      } else {
          this.setState({
              modal1: !this.state.modal1,
          });
      }
    }

    changeOrder(e, upsndown, id, order) {

        e.preventDefault();

        let tempData = this.state.templateData;
        let iflag = 0;
        tempData.map(( template, index ) => {

            if(upsndown === 'up') {

                if(template.ordinal === order) {

                    template.ordinal = template.ordinal - 1;
                    let newindex = index - 1;
                    tempData[newindex].ordinal = template.ordinal + 1;
                }
            }

            if(upsndown === 'down' && iflag === 0) {

                  if(template.ordinal === order) {

                      template.ordinal = template.ordinal + 1;
                      let newi = index + 1;
                      tempData[newi].ordinal = template.ordinal - 1;
                      iflag = iflag + 1;
                  }
              }

              template.ordinal = (template.ordinal < 0) ? 0 : template.ordinal;
        });

         this.props.createServiceSignoutTemplates(this.state.token, this.state.serviceId, tempData);
         this.props.getServiceSignoutTemplates(this.state.token, this.state.serviceId);
    }

    renderRow() {

        if(this.props.isFetching) {
          return(
            <tbody>
                <tr>
                  <td colSpan="3">
                    <div className="loader" id="loader-4">
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
              </td>
            </tr>
          </tbody>
          );
        }

        if(this.state.templateData.length == '0') {
          return (
            <tbody>
            <tr>
              <td colSpan="3">
                  No Signout template.
              </td>
            </tr>
            </tbody>
          );
        }

        if( this.state.templateData ) {
          return this.state.templateData.map(( template ) => {

            return (
              <tbody key={template.id}>
              <tr>
                  <td>{ template.name }</td>
                  <td>{ (template.default_value!='') ? template.default_value : 'n/a' }</td>
                  <td>
                      <button
                              onClick={ (e)=>this.changeOrder(e, 'up', template.id, template.ordinal) }
                              className="action-btn">
                              <i className="zmdi zmdi-long-arrow-up zmdi-hc-lg"></i>
                      </button>

                      <button
                              onClick={ (e)=>this.changeOrder(e, 'down', template.id, template.ordinal) }
                              className="action-btn">
                              <i className="zmdi zmdi-long-arrow-down zmdi-hc-lg"></i>
                      </button>
                  </td>
              </tr>
            </tbody>
            );
          });
        }
    }

    render() {
      return(
      <div className="pd-off-xs">
          {/**<div className="row">
            <div className="col-sm-12">
                <div className="white-bg">
                  <strong>Service Name : {this.state.serviceData.name}</strong>
                </div>
            </div>
          </div>**/}

          <div className="row">
              <div className="col-sm-12">
                  <div className="white-bg">
                    {/** ModalWindow - with props **/}
                    <ModalWindow
                                modal_var = {this.state.modal1 }
                                handle_modal = { (e) => this.open_modal(e, this.state.data) }
                                token = { this.props.token }
                                serviceId = { this.state.serviceId }
                                templateData = { this.state.templateData } />

                      { (this.state.isTenantAdmin === true) ?
                      <ButtonM type="button"
                               onClick={(e)=>this.open_modal(e, null)}
                               className="add-deprt btn blue-btn add-btn"
                               data-toggle="modal"
                               data-target="#create-patient">
                               <span className="hidden-xs-down">{(this.state.templateData.length > 0) ? 'UPDATE' : 'CREATE' } SIGNOUT TEMPLATE</span>
                                 <img className="hidden-sm-up"
                                      src="/client/assets/images/add-file.png" />
                      </ButtonM>
                      : "" }

                      <div className="table-responsive">
                          <table className="table patient-list depart-list">
                              <thead>
                                  <tr>
                                      <th width="35%">Field Name</th>
                                      <th width="35%">Default Value</th>
                                      <th width="30%">Field Order</th>
                                  </tr>
                              </thead>
                                  { this.renderRow() }
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.services.isFetching,
         service: state.services.serviceById,
         signoutTemplates: state.services.signoutTemplates,
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data
     }
}

export default connect(mapStateToProps, { getServiceById,
                                          getServiceSignoutTemplates,
                                          createServiceSignoutTemplates })( SignoutTemplates );
