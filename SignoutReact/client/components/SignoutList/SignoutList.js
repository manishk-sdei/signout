import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSignoutList, updateSignoutInitialState, getSignoutListById } from '../../actions/SignoutActions';
import { getServiceById } from '../../actions/ServicesActions';
import { getServiceGroups } from '../../actions/GroupsActions';

import { Collapse, Button, CardBlock, Card } from 'reactstrap';
import ModalWindow from '../Modals/AddEditSignoutModal';
import ModalWindowAddGroup from '../Modals/AddGroupToSignoutModal';
import ModalWindowConfirmDelete from '../Modals/ConfirmDeleteModal';
import ButtonM from 'muicss/lib/react/button';

class SignoutList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            modal2: false,
            modal3: false,
            serviceId: null,
            signoutId: null,
            token: null,
            signoutsData: [],
            serviceData: [],
            groupsData: [],
            editSignouts: [],
            data: {id: null, name: null, description: null},
            addGroupSignoutId: null,
            collapse: false,
            collapseId: null,
            signoutGroupsData: [],
            flagSignoutId: null,
            assignedGroups: [],
            isTenantAdmin: false
        };
        this.toggle = this.toggle.bind(this);
        this.open_modal = this.open_modal.bind(this);
        this.open_modal_delete = this.open_modal_delete.bind(this);
        this.open_modal_addgroup = this.open_modal_addgroup.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Service - Signout List";
        this.setState({ token : this.props.token });
        this.setState({ serviceId : this.props.serviceId });
    }

    componentWillMount() {
      this.setState({ token : this.props.token });
      this.setState({ serviceId : this.props.serviceId });

      //@args - token, service id
      this.props.getSignoutList( this.props.token, this.props.serviceId );
      this.props.getServiceById( this.props.token, this.props.serviceId );
      this.props.getServiceGroups( this.props.token, this.props.serviceId );
    }

    componentWillReceiveProps( nextProps ) {

        if(nextProps.currentLoginUser) {
           this.setState({ isTenantAdmin : nextProps.currentLoginUser.is_tenant_admin });
        }

        if ( nextProps.service && this.props.service !== nextProps.service ) {
            let service = nextProps.service;
            this.setState( { serviceData : service } );
        }
        // else if ( nextProps.service ) {
        //     if ( nextProps.service.length > 0 ) {
        //         let service = nextProps.service;
        //         this.setState( { serviceData : service } );
        //     }
        // }

        if ( nextProps.signouts && this.props.signouts !== nextProps.signouts ) {
            let signouts = nextProps.signouts;
            this.setState( { signoutsData : signouts } );
        }
        // else if ( nextProps.signouts ) {
        //     if ( nextProps.signouts.length > 0 ) {
        //         let signouts = nextProps.signouts;
        //         this.setState( { signoutsData : signouts } );
        //     }
        // }

        if ( nextProps.groups && this.props.groups !== nextProps.groups ) {
            let groups = nextProps.groups;
            this.setState( { groupsData : groups } );
        }
        // else if ( nextProps.groups ) {
        //     if ( nextProps.groups.length > 0 ) {
        //         let groups = nextProps.groups;
        //         this.setState( { groupsData : groups } );
        //     }
        // }

        if ( nextProps.signoutListById && this.props.signoutListById !== nextProps.signoutListById ) {
            let groupUsers = nextProps.signoutListById.groups;
            this.setState( { signoutGroupsData : groupUsers } );
        }
    }

    open_modal(e, data) {

      if(this.state.serviceId) {
          this.props.getSignoutList( this.state.token, this.state.serviceId ); // Update row
      }

      if( e===true ) {
          e.preventDefault();
      } else {

        if(data) {
          this.props.updateSignoutInitialState(data); // Update initial value
        }

          this.setState({
              modal1: !this.state.modal1,
              editSignouts: data,
          });
      }
    }

    open_modal_delete( e, signoutId, groupId ) {

      if(this.state.serviceId) {
          this.props.getSignoutList( this.state.token, this.state.serviceId ); // Update row
      }

      if(this.state.flagSignoutId && this.state.flagGroupId) {
        this.props.getSignoutListById( this.props.token, this.state.flagSignoutId );
      }

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal2: !this.state.modal2,
          deleteSignout: signoutId,
          deleteGroup: groupId,
          flagSignoutId: signoutId,
          flagGroupId: groupId
      });
    }

    open_modal_addgroup( e, signoutId, groups ) {
      //this.props.getSignoutList( this.state.token, this.state.serviceId ); // Update row

      if(this.state.flagSignoutId) {
        this.props.getSignoutListById( this.props.token, this.state.flagSignoutId );
      }

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal3: !this.state.modal3,
          addGroupSignoutId: signoutId,
          flagSignoutId: signoutId,
          assignedGroups: groups
      });
    }

    renderLoading() {
        if(this.props.isFetching) {
          return(
            <tbody>
            <tr>
              <td>
                <div classcName="loader" id="loader-4">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </td>
        </tr>
      </tbody>
          );
        }
    }

    toggle(e, signoutId) {
      e.preventDefault();

        this.props.getSignoutListById( this.props.token, signoutId );
        this.setState({ signoutGroupsData: [] });
        this.setState({ collapseId: signoutId });

        if(this.state.collapseId === signoutId ) {
          this.setState({ collapse: false });
        } else {
          this.setState({ collapse: true });
        }
    }

    renderRow() {

        if(this.state.signoutsData.length == '0') {
          return (
            <tbody>
            <tr>
              <td colSpan="3" >
                  No Signouts.
              </td>
            </tr>
            </tbody>
          );
        }

      if( this.state.signoutsData ) {
        return this.state.signoutsData.map(( signout ) => {

          return (
            <tbody key={signout.id}>
            <tr>
                <td><a href="" onClick={ (e) => this.toggle(e, signout.id)}>{ signout.name }</a></td>
                <td>{ (signout.description!='')?signout.description:'n/a' }</td>
                { (this.state.isTenantAdmin === true) ?
                <td>
                    <button
                      onClick={ (e)=>this.open_modal(e, signout) }
                      className="action-btn"
                      data-toggle="modal"
                      data-target="#edit-patient1">
                      <i className="zmdi zmdi-edit"></i>
                    </button>
                    <button
                      onClick={ (e) => this.open_modal_delete( e, signout.id, null )}
                      className="action-btn">
                      <i className="zmdi zmdi-delete"></i>
                    </button>
                </td>
                : <td></td> }
            </tr>
            <tr>
                <td className="pd-off" colSpan="3">
                  <Collapse isOpen={ this.state.collapseId === signout.id  ? this.state.collapse : false}>
                <Card>
                  <CardBlock>
                    <table className="table patient-list service-tb">
                        <thead>
                          <tr>
                              <th width="25%"></th>
                              <th width="60%">Group Name</th>
                              <th width="15%"></th>
                          </tr>
                        </thead>
                        <tbody>
                          { this.renderSignoutGroups( signout.id ) }
                          <tr>
                              <td></td>
                              <td colSpan="3">
                                { (this.state.isTenantAdmin === true) ?
                                <button
                                      onClick={ (e) => this.open_modal_addgroup( e, signout.id, this.state.signoutGroupsData )}
                                      className="action-btn"
                                      data-toggle="modal"
                                      data-target="#edit-patient1">
                                      <i className="zmdi zmdi-plus-circle"></i> Add Group
                                </button>
                                : "" }
                              </td>
                          </tr>
                        </tbody>
                    </table>
                  </CardBlock>
               </Card>
             </Collapse>
                </td>
            </tr>
          </tbody>
          );
        });
      }
    }

    renderSignoutGroups(signoutId) {

        if(this.props.isFetchingById) {
          return(
                <tr>
                  <td></td>
                  <td colSpan="3">
                    <div className="loader" id="loader-4">
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                </td>
                </tr>
          );
        }

        if(this.state.signoutGroupsData.length == '0') {
          return (
            <tr>
              <td></td>
              <td colSpan="3" >No Group.</td>
            </tr>
          );
        }

      if( this.state.signoutGroupsData ) {
        return this.state.signoutGroupsData.map(( group ) => {

          return (
            <tr key={group.id}>
                <td></td>
                <td> { group.name } </td>
                <td>
                  { (this.state.isTenantAdmin === true) ?
                  <button
                    onClick={ (e) => this.open_modal_delete( e, signoutId, group.id )}
                    className="action-btn">
                    <i className="zmdi zmdi-delete"></i>
                  </button>
                  : "" }
                </td>
            </tr>
          );
        });
      }
    }

    render() {
      return(
      <div className="pd-off-xs">
          {/**<div className="row">
            <div className="col-sm-12">
                <div className="white-bg">
                  <strong>Service Name : {this.state.serviceData.name}</strong>
                </div>
            </div>
          </div>**/}

          <div className="row">
              <div className="col-sm-12">
                  <div className="white-bg">
                    {/** ModalWindow - with props **/}
                    <ModalWindow
                                modal_var = {this.state.modal1 }
                                handle_modal = { (e) => this.open_modal(e, this.state.data) }
                                token = { this.props.token }
                                serviceId = { this.state.serviceId }
                                signout = { this.state.editSignouts } />
                    <ModalWindowAddGroup
                                modal_var = {this.state.modal3 }
                                handle_modal = { this.open_modal_addgroup }
                                token = { this.props.token }
                                signoutId = { this.state.addGroupSignoutId }
                                groups = { this.state.groupsData }
                                assignedGroups = { this.state.assignedGroups } />
                    <ModalWindowConfirmDelete
                                modal_var = {this.state.modal2 }
                                handle_modal = { this.open_modal_delete }
                                token = { this.state.token }
                                confirmPage = { 'SIGNOUT_LIST' }
                                signoutId = { this.state.deleteSignout }
                                groupId = { this.state.deleteGroup } />

                      { (this.state.isTenantAdmin === true) ?
                      <ButtonM type="button"
                               onClick={(e)=>this.open_modal(e, null)}
                               className="add-deprt btn blue-btn add-btn"
                               data-toggle="modal"
                               data-target="#create-patient">
                               <span className="hidden-xs-down">CREATE SIGNOUTS</span>
                                 <img className="hidden-sm-up"
                                      src="/client/assets/images/add-file.png" />
                      </ButtonM>
                      : "" }

                      <div className="table-responsive">
                          <table className="table patient-list depart-list">
                              <thead>
                                  <tr>
                                      <th width="25%">Signout Name</th>
                                      <th width="60%">Description</th>
                                      <th width="15%">{ (this.state.isTenantAdmin === true) ? 'Action' : "" }</th>
                                  </tr>
                              </thead>
                                  { this.renderRow() }
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.signouts.isFetching,
         signouts: state.signouts.data,
         isFetchingById: state.signouts.isFetchingById,
         signoutListById: state.signouts.signoutListById,
         isFetching: state.services.isFetching,
         service: state.services.serviceById,
         isFetching: state.groups.isFetching,
         groups: state.groups.data,
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data
     }
}

export default connect(mapStateToProps, { getServiceById,
                                          getSignoutList,
                                          getServiceGroups,
                                          updateSignoutInitialState,
                                          getSignoutListById })( SignoutList );
