/**
 * @class         :	AuthenticatedComponent
 * @description   : Handles application Login authentication
 * @Created by    : smartData
 */

 import React from 'react';
 import {connect} from 'react-redux';
 import { bindActionCreators } from 'redux';
 import { browserHistory } from 'react-router';
 import { loginUserSuccess } from '../actions/LoginActions';
 import { LOCALS_STORAGE_AUTHTOKEN } from '../actions/Constants';

 export function requireAuths(Component) {

     class AuthenticatedComponent extends React.Component {

         componentWillMount () {
             this.checkAuth(this.props.isAuthenticated);
         }

         componentWillReceiveProps (nextProps) {
             this.checkAuth(nextProps.isAuthenticated);
         }

         checkAuth (isAuthenticated) {

             //@TODO Token lost on refresh page - token comes here once login user
             // 1. Token lost on refresh page
              //console.log("authtoken", this.props.token);
             if (!isAuthenticated) {

                 let token = LOCALS_STORAGE_AUTHTOKEN;

                 if(token) {
                       this.props.loginUserSuccess(token);
                 }else {
                     let redirectAfterLogin = this.props.location.pathname;
                     browserHistory.push('/')
                 }
             }
         }

         render () {
             return (
                 <div>
                     {this.props.isAuthenticated === true
                         ? <Component {...this.props}/>
                         : null
                     }
                 </div>
             )
         }
     }

     const mapStateToProps = ( state ) => ({
         token: state.login.token,
         userName: state.login.userName,
         isAuthenticated: state.login.isAuthenticated,
         currentLoginUser: state.users.data
     });

     return connect( mapStateToProps, { loginUserSuccess } )( AuthenticatedComponent );
 }
