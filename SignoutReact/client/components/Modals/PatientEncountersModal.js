import React, { Component, PropTypes } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';
import { browserHistory } from 'react-router';

import { reduxForm } from 'redux-form';
import MyEditor from '../Common/EditorDraft';
import { updateSignoutEncounters } from '../../actions/SignoutActions';

class PatientEncounterModal extends Component {

    constructor(props) {
        super(props);
        this.state = { firstName : null,
                       middleName : null,
                       lastName : null,
                       mrn: null,
                       patientData : [],
                       templateFields : [],
                       editorIndex : null,
                       version : null,
                       external_encounter_id : null };
        this.manage_modal = this.manage_modal.bind(this);
    }

    static propTypes = {
      onChange: PropTypes.func
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.editPatient && this.props.editPatient !== nextProps.editPatient ) {
            let editPatientData = nextProps.editPatient;

            this.setState( { patientData : editPatientData,
                             firstName : editPatientData.patient ? editPatientData.patient.first_name : '',
                             middleName : editPatientData.patient ? editPatientData.patient.middle_name : '',
                             lastName : editPatientData.patient ? editPatientData.patient.last_name : '',
                             mrn : editPatientData.patient ? editPatientData.patient.mrn : ''} );
        }

        if ( nextProps.templateFields && this.props.templateFields !== nextProps.templateFields ) {
            let templateFieldsData = nextProps.templateFields;

            this.setState( { templateFields : templateFieldsData } );
        }

        if ( nextProps.signoutEncounters && this.props.signoutEncounters !== nextProps.signoutEncounters ) {
            let signoutEncounters = nextProps.signoutEncounters;

            this.setState( { version : signoutEncounters.version,
                             external_encounter_id : signoutEncounters.external_encounter_id} );
        }
    }

    onSubmit(formData) {

          let newArr = [];
          this.state.templateFields.map((template) => {
              newArr.push({ field_value : template.default_value, signout_list_field_template_id : template.id })
          });
          let editorArr = this.props.editorValues;
          let arrResult = _.map(newArr, function(obj) {
                              return _.assign(obj, _.find(editorArr, {
                                  signout_list_field_template_id: obj.signout_list_field_template_id
                              }));
                          });

          this.props.updateSignoutEncounters(this.props.token,
                                             this.state.patientData,
                                             this.state.version,
                                             arrResult,
                                             formData);
          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    renderTemplateEditors() {

        let newArr = [];
        this.state.templateFields.map((template) => {
            newArr.push({ name : template.name, field_value : template.default_value, signout_list_field_template_id : template.id })
        });
        let editorArr = this.props.signoutEncounters.field_data;
        let arrResult = _.map(newArr, function(obj) {
                            return _.assign(obj, _.find(editorArr, {
                                signout_list_field_template_id: obj.signout_list_field_template_id
                            }));
                        });

        //console.log("show--", arrResult);

        return arrResult.map( ( field ) => {
            return (
              <div className="col-md-4 col-sm-6" key={ field.signout_list_field_template_id }>
                  <p className="editor-heading">{ field.name }</p>
                  <div className="editor-content">
                    {this.renderEditor(field)}
                  </div>
              </div>
            )
        });
    }

    renderEditor(field) {
      return(
        <MyEditor content = { field.field_value }
                  editorIndex = { field.signout_list_field_template_id } />
      );
    }

    render() {
      const { fields: { external_encounter_id }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editSignoutTemplates check-pop edit-pt-editor" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title"> Signout List</span>
              </ModalHeader>
              <ModalBody>
                <div className="row">
                  <div className="row col-sm-8">
                    <h6>Patient Name: { this.state.firstName } { this.state.middleName } { this.state.lastName }</h6>
                  </div>
                  <div>
                    <h6>MRN: { this.state.mrn }</h6>
                  </div>
                </div>
                <div className="row col-sm-12 form-group">
                    <input
                          className="inputMaterial"
                          type="text" required {...external_encounter_id}
                          value={  this.state.external_encounter_id }
                          onChange={()=>this.setState({external_encounter_id: event.target.value})} />
                    <span className="highlight"></span> <span className="bar"></span>
                    <label>External encounter id </label>
                </div>
                <div className="row">
                  { this.renderTemplateEditors() }
                </div>
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.signouts.signouts,
         editorValues: state.signouts.editorValues,
         signoutEncounters : state.signouts.signoutEncounters
     }
}

export default reduxForm({
    form: 'PatientEncounterForm',
    fields: ['external_encounter_id'],

}, mapStateToProps, { updateSignoutEncounters })( PatientEncounterModal );
