import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';

import { reduxForm } from 'redux-form';
import { createServiceSignouts } from '../../actions/SignoutActions';

class AddEditSignoutModal extends Component {

    constructor(props) {
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          id: null,
          name: null,
          description: null,
          serviceId: null
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        this.setState({ serviceId : this.props.serviceId });

        if ( nextProps.signout && this.props.signout !== nextProps.signout ) {
            let signout = nextProps.signout;
            this.setState( { id : signout.id } );
            this.setState( { name : signout.name } );
            this.setState( { description : signout.description } );
        }
        // else if ( nextProps.signout ) {
        //     if ( nextProps.signout.length > 0 ) {
        //         let signout = nextProps.signout;
        //         this.setState( { id : signout.id } );
        //         this.setState( { name : signout.name } );
        //         this.setState( { description : signout.description } );
        //     }
        // }
    }

    onSubmit( formData ) {

          // @args - token, serviceId, signoutId, formData
          this.props.createServiceSignouts(this.props.token, this.state.serviceId, this.state.id, formData);
          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    render() {
        const { fields: { name, description }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>

              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Signout</span>
              </ModalHeader>

              <ModalBody>
	                <div className="row form-group">
                    <div className ={`col-sm-12 form-group ${name.touched && name.invalid ? 'col-sm-12 has-danger' : ''}`}>

                      <input
                            className="inputMaterial"
                            type="text" required {...name}
                            value={  this.state.name }
                            initialValues={ this.state.name }
                            onChange={()=>this.setState({name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Signout Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {name.touched && name.error ? name.error : false }
                        </div>
                    </div>
		              </div>
                  <div className="row form-group">
                      <div className="col-sm-12">
                        <textarea
                                  className="inputMaterial" required
                                  type="text" {...description}
                                  value={ this.state.description  }
                                  onChange={()=>this.setState({description: event.target.value})}>
                        </textarea>
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Description</label>
                      </div>
  							  </div>
              </ModalBody>

              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
              </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.name) {
        error.name = 'Please enter signout name';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticated: state.signouts.isAuthenticated,
         isAuthenticating: state.signouts.isAuthenticating,
         statusText : state.signouts.statusText,
         initialValues : {
           name: state.signouts.name,
           description: state.signouts.description
         }
     }
}

export default reduxForm({
    form: 'AddEditSignoutForm',
    fields: ['name', 'description'],
    validate
}, mapStateToProps, { createServiceSignouts })( AddEditSignoutModal );
