import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import Table from './table';
import { updateUserGroups } from '../../actions/UsersActions';
import { LOCALS_STORAGE_AUTHTOKEN } from '../../actions/Constants';

class CheckinModal extends Component {

    constructor(props){
    super(props);
        this.state = {  userServices: [],
                        toggleActive : false,
                        onCallGroups : [],
                        selectedGroups : [],
                        selectedGroup : null,
                        selectedGroupSignoutList : [],
                        footerGroups : [],
                      };
        this.manage_modal = this.manage_modal.bind(this);
        this.onClickGroup = this.onClickGroup.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps(nextProps) {

      if(nextProps.userServices && this.props.userServices !== nextProps.userServices) {

          let userServices = nextProps.userServices;
          let selectedGroups = [];
          let selectedGroupNames = [];
          let indexArr = [];
          userServices.map((service, index) => {

              if(service.groups.length > 0) {
                service.groups.map((group) => {
                  if(group.user_in_group === true) {
                      selectedGroups.push(group.id);
                      selectedGroupNames.push(group);
                  }
                });
              }

              if(service.groups.length === 0) {
                indexArr.push(index);
              }
          });

          // Remove service - if no groups assigned for the service
          _.pullAt(userServices, indexArr);

          this.setState({ userServices : userServices });
          this.setState({ footerGroups : selectedGroupNames });
          this.setState({ selectedGroups : selectedGroups });
      }
    }

    renderServices() {

      if(this.props.isFetching) {
        return(
              <div className="medi-floor">
                  <div className="loader" id="loader-4">
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
              </div>
        );
      }

      if(this.state.userServices.length === 0) {
        return(
          <div className="medi-floor">
              No service
          </div>
        );
      }

      if(this.state.userServices) {
        return this.state.userServices.map((service) => {
            return(
              <div key={service.id}>
              <div className="medi-floor mar-top35">
                  <h6>{ service.name } ({service.department.name})</h6>
                  <ul>
                      {this.renderServiceGroups(service.groups)}
                  </ul>
              </div>
              <div className="medi-floor">
                  <h6>On Call Groups:</h6>
                  <ul>
                      {this.renderServiceOnCallGroups(service.groups)}
                  </ul>
              </div>
            </div>
            );
        });
      }
    }

    onClickGroup(e, group) {
        e.preventDefault();

        e.target.parentNode.className = (e.target.parentNode.className === "active") ? "inactive" : "active";

        if(e.target.parentNode.className === "active") {


          // Add group to array
          this.state.selectedGroups.push(group.id);

          // Add footer group
          let arr = this.state.footerGroups;
          arr.push(group);
          this.setState({ footerGroups : arr });

          this.setState({ selectedGroup : group.name });
          this.setState({ selectedGroupSignoutList : group.signoutLists });
        }

        if(e.target.parentNode.className === "inactive") {

          // Remove group to array
          _.pull(this.state.selectedGroups, group.id);

          // remove footer group
          let arr = this.state.footerGroups;
          arr.map(( footerGroup, index ) => {
              if(footerGroup.id === group.id) {
                  _.pullAt(arr, [index]);
              }
          });
          this.setState({ footerGroups : arr });

          this.setState({ selectedGroup : null });
          this.setState({ selectedGroupSignoutList : [] });
        }
        this.setState({ selectedGroups : this.state.selectedGroups });
    }

    renderServiceOnCallGroups(groups) {

      if(groups.length === 0) {
        return(
            <li>
                <span>No group</span>
            </li>
        );
      }

      return groups.map((group) => {
          if(group.is_on_call === true) {
            return(
                <li className={group.user_in_group === true  ? "active" : "inactive"}
                    key={group.id}
                    onClick={(e)=>this.onClickGroup(e, group)}>
                    <button>{group.name}</button>
                </li>
            );
          }
      });
    }

    renderServiceGroups(groups) {

      if(groups.length === 0) {
        return(
            <li>
                <span>No group</span>
            </li>
        );
      }

      return groups.map((group) => {
          if(group.is_on_call === false) {
            return(
                <li className={group.user_in_group === true  ? "active" : "inactive"}
                    key={group.id}
                    onClick={(e)=>this.onClickGroup(e, group)}>
                    <button>{group.name}</button>
                </li>
            );
          }
      });
    }

    renderFooterGroups() {
      return this.state.footerGroups.map(( group ) => {
          return group.name + " ";
      });
    }

    renderFooterSignouts() {
      return this.state.footerGroups.map(( group ) => {
        if(group.signoutLists.length > 0) {
          return  group.signoutLists.map(( signoutList ) => {
              return signoutList.name + " ";
            });
        }
      });
    }

    onAccept(e) {
        e.preventDefault();

        let token = this.props.token === null ? localStorage.getItem('authToken') : this.props.token;

        // API call to add groups to the user.
        this.props.updateUserGroups( token, this.props.currentLoginUser.id, this.state.selectedGroups )
        let This = this;
        setTimeout(function(){
            This.props.handle_modal('false');
        }, 1000);
    }

    render() {
        return (
        <Modal isOpen={ this.props.modal_var } toggle={this.manage_modal} className="modal_data modal_data1 check-pop modal-lg">
            <ModalHeader toggle={this.manage_modal}>
                <span className="modal-title" id="exampleModalLabel">Check In<span> Verify the Group that you are assigned to today</span></span>
            </ModalHeader>
            <ModalBody>
              {
                /**

                <div className="medi-floor">
                    <h6>On Call Groups:</h6>
                    <ul>
                        {this.renderServiceOnCallGroups()}
                    </ul>
                </div>
                **/ }
                {this.renderServices()}
            </ModalBody>
            <ModalFooter>
                <p>Selected Groups: {this.renderFooterGroups()}
                  <span>this will allow you access to the following list: {this.renderFooterSignouts()}</span>
                </p>
              <ButtonM type="button" data-ripple="" className="btn blue-btn" onClick={(e)=>this.onAccept(e)}>Accept</ButtonM>
            </ModalFooter>
        </Modal>
        );
    }

}

function mapStateToProps( state ) {
     return {
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data,
         userServices : state.users.userServices
     }
}

export default connect(mapStateToProps, { updateUserGroups })(CheckinModal);
