import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';
import Multiselect from 'react-widgets/lib/Multiselect';

import { reduxForm } from 'redux-form';
import { addUsersToGroup } from '../../actions/GroupsActions';

class AddUsersToGroupModal extends Component {

    constructor(props) {
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          groupId: null,
          users: [],
          assignedUsers: [],
          value: []
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {
        if(nextProps.groupId) {
            this.setState({ groupId : nextProps.groupId });
        }

        if(nextProps.users) {

          if(nextProps.assignedUsers) {
            let usersData = [];

            nextProps.users.map(( user, index ) => {
                nextProps.assignedUsers.map(( assignedUser ) => {

                    if(user.id === assignedUser.id) {
                        delete nextProps.users[index];
                    }
                });
            });

            this.setState({ users: nextProps.users });
          } else {
                this.setState({ users: nextProps.users });
          }
        }
    }

    onSubmit( formData ) {

        if(formData.users) {
            formData.users.map(( user ) => {
                document.getElementById('addusertogroup').disabled=true;
                  // @args - token, groupId, formData
                  let userName = user.first_name + ' ' + user.last_name;
                  this.props.addUsersToGroup(this.props.token, this.state.groupId, user.id, userName);
                  let This = this;
                  setTimeout(function(){
                      This.props.handle_modal('false');
                  }, 1000);
            });
        }

    }

    render() {

        const { fields: { users }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>

              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Add Users</span>
              </ModalHeader>

              <ModalBody>
                <div className="row">
                <div className="col-sm-12">
                <div className="form-group">
                    <div className="custom-box">
                      <Multiselect
                            {...users}
                            placeholder="Select Users"
                            data={this.state.users}
                            valueField='id'
                            defaultValue={[]}
                            onBlur={ (e) => console.log(e.target.value) }
                            textField={ user => user.first_name + ' ' + user.last_name + ' (' + user.email + ')'  } />
                    </div>
                    <div className="error_msg_danger">
                        {users.touched && users.error ? users.error : false }
                    </div>
                </div>
                </div>
                </div>
              </ModalBody>

              <ModalFooter>
                <ButtonM
                  disabled={ this.props.isAuthenticating }
                  type="submit"
                  id="addusertogroup"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
              </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(values.users !== undefined && values.users.length === 0) {
      error.users = 'Please select users';
    }

    if(!values.users) {
        error.users = 'Please select users';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticating: state.groups.isAuthenticating,
     }
}

export default reduxForm({
    form: 'AddUsersToGroupForm',
    fields: ['users'],
    validate
}, mapStateToProps, { addUsersToGroup })( AddUsersToGroupModal );
