import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import { reduxForm } from 'redux-form';
import { createServiceSignoutTemplates } from '../../actions/ServicesActions';

class SignoutTemplateModal extends Component {

    constructor(props){
        super(props);

        this.state = {
          updateLabel: 'Create',
          serviceId: null,
          id: [],
          data: [],
          inputs: [{type : 'input-0', id: "", field_name: "", field_value : ""}],
          removedElements : [],
        };
        this.manage_modal = this.manage_modal.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        this.setState({ serviceId : this.props.serviceId });
        if ( nextProps.templateData && this.props.templateData !== nextProps.templateData ) {
            let templates = nextProps.templateData;
            let inputsNew = [];
            templates.map((template) => {
              let newInput = _.uniqueId('input-');
              let inputArr = {type : newInput, id: template.id, field_name: template.name, field_value : template.default_value};
              inputsNew.push(inputArr)
            });
            this.setState({ updateLabel : templates.length > 0 ? 'Update' : 'Create' });
            this.setState({ inputs: inputsNew });
        }
    }

    appendInput(e) {
        e.preventDefault();
        //let newInput = `input-${this.state.inputs.length}`;
        let newInput = _.uniqueId('input-');
        let inputArr = {type : newInput, id: "", field_name: "", field_value : ""};
        this.setState({ inputs: this.state.inputs.concat([inputArr]) });
    }

    removeInput(e, input) {
        e.preventDefault();
        console.log(e.target.parentNode.parentNode.parentNode);
        document.getElementById("dynamicInput").removeChild(e.target.parentNode.parentNode.parentNode);
        //let inpArr = this.state.inputs;
        //let remainInputArr = _.pull(inpArr, input);

        this.setState({ removedElements: this.state.removedElements.concat([input]) });
    }

    onSubmit() {

          let resFormData = _.difference(this.state.inputs, this.state.removedElements);
          let data = [];
          let order = 1;
          resFormData.map((res) => {
              if(res.id !== "") {
                let tempData = {
                                  id: res.id,
                                  service_id: this.state.serviceId,
                                  name: res.field_name,
                                  default_value: res.field_value,
                                  ordinal: order
                                };
                                data.push(tempData);
              } else {
                let tempData = {
                                  service_id: this.state.serviceId,
                                  name: res.field_name,
                                  default_value: res.field_value,
                                  ordinal: order
                                };
                                data.push(tempData);
              }

              order = order + 1;
          });

          this.props.createServiceSignoutTemplates(this.props.token, this.state.serviceId, data);
          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    setValues(event, input, field_type, value) {

        event.preventDefault();

        if(field_type === 'field_name') {
          input.field_name = value;
        }
        if(field_type === 'field_value') {
          input.field_value = value;
        }
        this.setState({input});
    }

    render() {

        const { fields: { field_name, field_value }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{this.state.updateLabel} Signout Template</span>
              </ModalHeader>
              <ModalBody>

                <div className="patient-list">

                       <div id="dynamicInput">
                         {
                           this.state.inputs.map((input) => {
                               return (
                                 <div className="row form-group">
                                   <div className ={`col-sm-4 form-group ${field_name.touched && field_name.invalid ? 'col-sm-4 has-danger' : ''}`}>

                                     <input
                                           className="inputMaterial"
                                           type="text" required {...field_name}
                                           value={  input.field_name }
                                           onChange={ (event)=> this.setValues(event, input, 'field_name', event.target.value) } />
                                     <span className="highlight"></span> <span className="bar"></span>
                                     <label>Field Name <i className="reqfld">*</i> </label>
                                       <div className="error_msg_danger">
                                           {field_name.touched && field_name.error ? field_name.error : false }
                                       </div>
                                   </div>
                                   <div className ="col-sm-6 form-group">
                                     {/**<input
                                           className="inputMaterial"
                                           type="text" required {...field_value
                                           value={  input.field_value }
                                           onChange={ (event)=> this.setValues(event, input, 'field_value', event.target.value) } />**/}

                                     <textarea
                                               type="text" required
                                               className="inputMaterial"
                                               {...field_value}
                                               value={  input.field_value }
                                               onChange={ (event)=> this.setValues(event, input, 'field_value', event.target.value) }>
                                     </textarea>

                                     <span className="highlight"></span> <span className="bar"></span>
                                     <label>Default value </label>
                                   </div>
                                   <div className ="col-sm-2 form-group">
                                     { (input.type === 'input-0') ? ""
                                     : <button type="button" className="action-btn" onClick={ (e) => this.removeInput(e, input) }><i className="zmdi zmdi-delete"></i></button> }
                                   </div>
                                 </div>
                               );
                           })
                         }
                       </div>

                   <button type="button" className="link-popup" onClick={ (e) => this.appendInput(e) }>ADD NEW FIELD</button>
                </div>

              </ModalBody>
              <ModalFooter>

                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
        				<ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.field_name) {
        error.field_name = 'Please enter service name';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticating: state.services.isAuthenticating,
         statusText : state.services.statusText
     }
}

export default reduxForm({
    form: 'serviceTemplateForm',
    fields: ['field_name', 'field_value'],

}, mapStateToProps, { createServiceSignoutTemplates })( SignoutTemplateModal );
