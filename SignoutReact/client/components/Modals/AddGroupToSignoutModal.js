import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';
import Multiselect from 'react-widgets/lib/Multiselect';

import { reduxForm } from 'redux-form';
import { addSignoutGroups } from '../../actions/SignoutActions';

class AddGroupToSignoutModal extends Component {

    constructor(props) {
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          signoutId: null,
          groups: [],
          assignedGroups: []
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {//console.log("ggg---", nextProps);

        this.setState({ signoutId : this.props.signoutId });

        // if(nextProps.groups) {
        //   this.setState({ groups: nextProps.groups });
        // }

        if(nextProps.groups) {

          if(nextProps.assignedGroups) {
            let usersData = [];

            nextProps.groups.map(( group, index ) => {
                nextProps.assignedGroups.map(( assignedUser ) => {

                    if(group.id === assignedUser.id) {
                        delete nextProps.groups[index];
                    }
                });
            });

            this.setState({ groups: nextProps.groups });
          } else {
                this.setState({ groups: nextProps.groups });
          }
        }
    }

    onSubmit( formData ) {

        if(formData.groups) {
            formData.groups.map(( group ) => {

                  // @args - token, groupId, formData
                  this.props.addSignoutGroups(this.props.token, this.state.signoutId, group.id, group.name);
                  let This = this;
                  setTimeout(function(){
                      This.props.handle_modal('false');
                  }, 1000);
            });
        }

    }

    render() {
        const { fields: { groups }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>

              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Add Groups</span>
              </ModalHeader>

              <ModalBody>
                <div className="row">
                <div className="col-sm-12">
                <div className="form-group">
                    <div className="custom-box">
                      <Multiselect
                            {...groups}
                            placeholder="Select Groups"
                            data={this.state.groups}
                            valueField='id'
                            defaultValue={[]}
                            onBlur={ (e) => console.log(e.target.value) }
                            textField="name" />
                    </div>
                </div>
                </div>
                </div>
              </ModalBody>

              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
              </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.groups) {
        error.groups = 'Please select groups';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticated: state.groups.isAuthenticated,
         isAuthenticating: state.groups.isAuthenticating,
         statusText : state.groups.statusText,
     }
}

export default reduxForm({
    form: 'AddGroupsToSignoutListForm',
    fields: ['groups'],
    validate
}, mapStateToProps, { addSignoutGroups })( AddGroupToSignoutModal );
