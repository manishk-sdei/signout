import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';

import { reduxForm } from 'redux-form';
import { createRole } from '../../actions/RolesActions';

class AddEditRoleModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          id: null,
          name: null,
          description: null,
          roleData: []
        };
    }

    componentDidMount() {
        this.setState({ roleData: this.props.role });
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.role && this.props.role !== nextProps.role ) {
            let role = nextProps.role;
            this.setState( { id : role.id } );
            this.setState( { name : role.name } );
            this.setState( { description : role.description } );
        }
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    onSubmit(formData) {
      //e.preventDefault();
      //console.log("Post Submitted : ", formData, this.state.id);
      let roleId = (this.state.id==undefined) ? null : this.state.id;
      this.props.createRole(roleId, formData, this.props.token);
      let This = this;
      setTimeout(function(){
          This.props.handle_modal('false');
      }, 1000);
    }

    render() {
        const { fields: { name, description }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Add/Edit Role</span>
              </ModalHeader>
              <ModalBody>
	                <div className="row form-group">
                    <div className ={`col-sm-12 form-group ${name.touched && name.invalid ? 'col-sm-12 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text" required {...name}
                            value={  this.state.name }
                            initialValues={ this.state.name }
                            onChange={()=>this.setState({name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Role Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {name.touched ? name.error : '' }
                        </div>
                    </div>
		              </div>
                  <div className="row form-group">
                      <div className="col-sm-12">
                        <textarea
                                  className="inputMaterial" required
                                  type="text" {...description}
                                  value={ this.state.description  }
                                  onChange={()=>this.setState({description: event.target.value})}>
                        </textarea>
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Description</label>
                      </div>
  							  </div>
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
        				  <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.name) {
        error.name = 'Please enter name';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticating: state.roles.isAuthenticating,
         initialValues : {
           name: state.roles.name,
           description: state.roles.description
         }
     }
}

export default reduxForm({
    form: 'AddEditRoleForm',
    fields: ['name', 'description'],
    validate
}, mapStateToProps, { createRole })( AddEditRoleModal );
