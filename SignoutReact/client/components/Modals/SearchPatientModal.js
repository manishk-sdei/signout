import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import { reduxForm } from 'redux-form';
import moment from 'moment';
import { createSignoutEncounters } from '../../actions/SignoutActions';
import { getPatients } from '../../actions/PatientsActions';
import MaskedInput from 'react-maskedinput';

class PatientEncountersModal extends Component {

    constructor(props){
        super(props);
        this.state = { searchByFirstName : null,
                       searchByLastName : null,
                       searchByMrn : null,
                       searchByDob : null,
                       patients : [],
                       checkPatients : [],
                       check : false };
        this.manage_modal = this.manage_modal.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps(nextProps) {
          if ( nextProps.patients && this.props.patients !== nextProps.patients ) {
              let patients = nextProps.patients;
              this.setState( { patients : patients } );
          }
    }

    toggleCheck(e, value) {
        e.preventDefault();
        let toggleCheck = (value === false) ? true : false;
        console.log(toggleCheck);
    }

    onSubmit(formData ) {

          console.log("submit---", this.state.checkPatients, this.props.signoutId);

          this.state.checkPatients.map(( data ) => {

              this.props.createSignoutEncounters(this.props.token, this.props.signoutId, data);
          });

          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    changeCheckState(cid, value) {

        let removeFlag = 0;
        this.state.checkPatients.map((val, index) => {
            if(val.patient_id === value) {
              removeFlag = 1;
              this.state.checkPatients.splice(index, 1);
            }
        });

        if(removeFlag === 0) {
            this.state.checkPatients.push({ id : cid, patient_id : value });
        }
        this.setState({ name : this.state.checkPatients });
    }

    searchPatient( searchFieldName, searchVal ) {

//--------------
      let searchArr = { first_name : null,
                        last_name : null,
                        mrn : null,
                        dob : null };

      if(searchFieldName === 'first_name' && searchVal !== '') {
        searchArr.first_name = searchVal;
        this.setState({ searchByFirstName : searchVal });
      }
      if(searchFieldName === 'first_name' && searchVal === '') {
        this.setState({ searchByFirstName : null });
      }
      if(this.state.searchByFirstName !== null && searchFieldName !== 'first_name') {
        searchArr.first_name = this.state.searchByFirstName;
      }

      if(searchFieldName === 'last_name' && searchVal !== '') {
        searchArr.last_name = searchVal;
        this.setState({ searchByLastName : searchVal });
      }
      if(searchFieldName === 'last_name' && searchVal === '') {
        this.setState({ searchByLastName : null });
      }
      if(this.state.searchByLastName !== null && searchFieldName !== 'last_name') {
        searchArr.last_name = this.state.searchByLastName;
      }

      if(searchFieldName === 'mrn' && searchVal !== '') {
        searchArr.mrn = searchVal;
        this.setState({ searchByMrn : searchVal });
      }
      if(searchFieldName === 'mrn' && searchVal === '') {
        this.setState({ searchByMrn : null });
      }
      if(this.state.searchByMrn !== null && searchFieldName !== 'mrn') {
        searchArr.mrn = this.state.searchByMrn;
      }

      if(searchFieldName === 'dob' && searchVal !== '') {

        let dateDob = moment(searchVal).format('YYYY-MM-DD');
        if(dateDob !== "Invalid date") {
          searchArr.dob = moment(searchVal).format('YYYY-MM-DD');
          this.setState({ searchByDob : searchVal });
        }
      }
      if(searchFieldName === 'dob' && searchVal === '') {
        this.setState({ searchByDob : null });
      }
      if(this.state.searchByDob !== null && searchFieldName !== 'dob') {
        searchArr.dob = this.state.searchByDob;
      }
//--------------
console.log(searchArr);
      // Call api to search
       let result = _.omitBy(searchArr, _.isNil);
       let searchObj = { search : result };
       console.log(searchObj.search.length);
       this.props.getPatients( this.props.token, searchObj );
    }

    renderLoading() {
        if(this.props.isFetching) {
          return(
            <tr>
              <td colSpan="3">
                <div className="loader" id="loader-4">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
            </td>
          </tr>
          );
        }
    }

    renderNoPatient(patients) {
      if(patients.length === 0) {
        return(
          <tr>
              <td colSpan="4">No patient.</td>
          </tr>
        );
      }
    }

    render() {
      const { fields: { name : [] }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data wd600 check-pop add-pt">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Add Patient</span>
              </ModalHeader>
              <ModalBody>

                    <div className="row">
                        <div className="search-bar col-md-5">
                            <input required
                                   type="text"
                                   className="inputMaterial"
                                   value = {this.state.searchByFirstName}
                                   onChange = { (e) => this.searchPatient('first_name', e.target.value) }/>
                            <span className="highlight"></span> <span className="bar"></span>
                            <label>Search by first name...</label>
                            <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                        </div>
                        <div className="search-bar col-md-5">
                            <input required
                                   type="text"
                                   className="inputMaterial"
                                   value = {this.state.searchByLastName}
                                   onChange = { (e) => this.searchPatient('last_name', e.target.value) }/>
                            <span className="highlight"></span> <span className="bar"></span>
                            <label>Search by last name...</label>
                            <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                        </div>
      						</div>

                  <div className="row">
                    <div className="search-bar col-md-3">
                        <input required
                               type="text"
                               className="inputMaterial"
                               value = {this.state.searchByMrn}
                               onChange = { (e) => this.searchPatient('mrn', e.target.value) }/>
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Search by mrn...</label>
                        <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                    </div>
                      <div className="search-bar col-md-4">
                        <MaskedInput
                                className="inputMaterial dob"
                                placeholder="MM/DD/YYYY"
                                mask="11/11/1111"
                                name="card"
                                size="8"
                                value = {this.state.searchByDob}
                                onChange = { (e) => this.searchPatient('dob', e.target.value) } />

                          <span className="highlight"></span> <span className="bar"></span>
                          <label>Search patient by dob...</label>
                          <button className="search-icon"><i className="zmdi zmdi-search "></i></button>
                      </div>
                      <div className="crt-patient">
                          <button className="link-popup" data-toggle="modal" data-target="#create-patient">
                            <Link to={`/patients`}>Patients > Create Patient</Link>
                          </button>
                      </div>
                </div>

      						<div className="row">
                        <div className="col-sm-12">
                            <div className="table-responsive">
                                <table className="table table-hover add-pt-table table-bordered">
                                    {/*
                                    <!--table start-->*/}
                                    <thead>
                                        <tr>
                                            <th width="7%">&nbsp;</th>
                                            <th>Name</th>
                                            <th>Mrn</th>
                                            <th>DOB</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { this.renderLoading() }
                                        { this.renderNoPatient(this.state.patients) }
                                        {
                                          this.state.patients.map(( patient, index ) => {//console.log(patient);
                                              return (
                                                <tr key={ patient.id }>
                                                    <td>
                                                        <div className="checkbox">
                                                            <label>
                                                                <input
                                                                  {...name}
                                                                  type="checkbox"
                                                                  value={ patient.id }
                                                                  onChange = { (e) => this.changeCheckState(`${index}-checkbox`, e.target.value) } />
                                                                <span className="cr"><i className="cr-icon fa fa-check"></i></span> </label>
                                                        </div>
                                                    </td>
                                                    <td>{ patient.first_name } { patient.middle_name } { patient.last_name }</td>
                                                    <td>{ patient.mrn }</td>
                                                    <td>{ moment(patient.dob).format('MM/DD/YYYY') }</td>
                                                </tr>
                                              )
                                          })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

              </ModalBody>
              <ModalFooter>
            			<ButtonM className="blue-btn">Save</ButtonM>
            			<ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
            </form>
          </Modal>
        );
    }
}

function validate(values) {console.log(values);

    const error = {};

    if(!values.name) {
        error.name = 'Please select at least one patient';
    }
    return error;
}

function mapStateToProps(state) {
     return {

         isFetching : state.patients.isFetching
     }
}

export default reduxForm({
    form: 'SignoutEncounterForm',
    fields: ['name':[]],

}, mapStateToProps, { createSignoutEncounters, getPatients })( PatientEncountersModal );
