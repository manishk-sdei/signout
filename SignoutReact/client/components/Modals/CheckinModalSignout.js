import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import Table from './table';
import { addUsersToGroup, deleteGroupUsers } from '../../actions/GroupsActions';

class CheckinModal extends Component {

    constructor(props){
    super(props);
        this.state = {  toggleActive : false,
                        onCallGroups : [],
                        selectedGroups : [],
                        signoutListById : [],
                        checkinService : [],
                        currentLoginUser: [],
                        signoutGroups: []
                      };
        this.manage_modal = this.manage_modal.bind(this);
        this.onClickGroup = this.onClickGroup.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps(nextProps) {

      if ( nextProps.signoutListById && this.props.signoutListById !== nextProps.signoutListById ) {
          let signoutListById = nextProps.signoutListById;
          //console.log(signoutListById);
          this.setState( { signoutListById : signoutListById } );

          //let signoutListGroups = _.map(signoutListById.groups, o => _.extend({is_selected: false}, o));
          this.setState( { signoutGroups : signoutListById.groups } );
      }

      if(nextProps.currentLoginUser && this.props.currentLoginUser !== nextProps.currentLoginUser) {

          let currentLoginUser = nextProps.currentLoginUser;
          this.setState({ currentLoginUser : currentLoginUser });
      }

      if(nextProps.checkinService && this.props.checkinService !== nextProps.checkinService) {

          let checkinService = nextProps.checkinService;
          this.setState({ checkinService : checkinService });
      }
      let preSelectedArr = [];
      if(nextProps.signoutListById && nextProps.currentLoginUser) {
          nextProps.currentLoginUser.groups.map(( userGroup ) => {
              nextProps.signoutListById.groups.map(( signoutGroup ) => {
                  if(userGroup.id === signoutGroup.id) {
                      signoutGroup.active = 'active';
                      preSelectedArr.push(signoutGroup);
                  }
              });
          });
      }

      this.setState({ selectedGroups : preSelectedArr });
    }

    renderServices() {

      if(this.props.isFetching) {
        return(
              <div className="medi-floor">
                  <div className="loader" id="loader-4">
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
              </div>
        );
      }

      if(this.state.checkinService.length === 0) {
        return(
          <div className="medi-floor">
              No service
          </div>
        );
      }

      if(this.state.checkinService) {
            return(
              <div key={this.state.checkinService.id}>
              <div className="medi-floor mar-top35">
                  <h6>{ this.state.checkinService.name } ({this.state.checkinService.department.name})</h6>
                  <ul>
                      {this.renderServiceGroups(this.state.signoutListById.groups)}
                  </ul>
              </div>
              <div className="medi-floor">
                  <h6>On Call Groups:</h6>
                  <ul>
                      {this.renderServiceOnCallGroups(this.state.signoutListById.groups)}
                  </ul>
              </div>
            </div>
            );
      }
    }

    onClickGroup(e, group) {

      e.preventDefault();

      e.target.parentNode.className = (e.target.parentNode.className === "active") ? "inactive" : "active";
      console.log(this.state.signoutGroups);
      // On active
      if(e.target.parentNode.className === "active") {

          let index = _.findIndex(this.state.signoutGroups, { id: group.id });
          this.state.signoutGroups[index].is_selected = true;

          // Add footer group
          this.state.selectedGroups.push(group);
          this.setState({ selectedGroups : this.state.selectedGroups });
      }

      // On inactive
      if(e.target.parentNode.className === "inactive") {

          let index = _.findIndex(this.state.signoutGroups, { id: group.id });
          this.state.signoutGroups[index].is_selected = false;

          // remove footer group
          let indexArr = [];
          this.state.selectedGroups.map(( footerGroup, index ) => {
              if(footerGroup.id === group.id) {
                  indexArr.push(index);
              }
          });
          _.pullAt(this.state.selectedGroups, indexArr);
          this.setState({ selectedGroups : this.state.selectedGroups });
      }
    }

    renderServiceOnCallGroups(groups) {

      if(groups.length === 0) {
        return(
            <li>
                <span>No group</span>
            </li>
        );
      }

      return groups.map((group) => {
          if(group.is_on_call === true) {//console.log(group.active);
            return(
                <li className={ (group.active !== undefined && group.active === 'active') ? "active" : "inactive" }
                    key={group.id}
                    onClick={(e)=>this.onClickGroup(e, group)}>
                    <button>{group.name}</button>
                </li>
            );
          }
      });
    }

    renderServiceGroups(groups) {

      if(groups.length === 0) {
        return(
            <li>
                <span>No group</span>
            </li>
        );
      }

      return groups.map((group) => {
          if(group.is_on_call === false) {//console.log(group.active);
            return(
                <li className={ (group.active !== undefined && group.active === 'active') ? "active" : "inactive" }
                    key={group.id}
                    onClick={(e)=>this.onClickGroup(e, group)}>
                    <button>{group.name}</button>
                </li>
            );
          }
      });
    }

    renderFooterGroups() {
      if(this.state.selectedGroups.length > 0) {
        return this.state.selectedGroups.map(( group ) => {
            return group.name + " ";
        });
      }
    }

    renderFooterSignouts() {
        return this.state.signoutListName;
    }

    onAccept(e) {

      e.preventDefault();

      let currentUserId = this.state.currentLoginUser.id;
      let currentUserName = this.state.currentLoginUser.first_name + ' ' + this.state.currentLoginUser.last_name;

      if(this.state.signoutGroups.length > 0) {
          this.state.signoutGroups.map((group) => {

              // add Field -
              // group.active = 'active' means groups already assigned to user  - POST /groups/{id}/users
              if(group.active === undefined && group.is_selected === true) {
                    // API call to assign current Login User to selected groups
                    this.props.addUsersToGroup(this.props.token, group.id, currentUserId, currentUserName);
              }

              // Remove Field - Remove user from a group - DELETE /groups/{id}/users/{userId}
              if(group.active === 'active' && group.is_selected === false) {
                  // API call to remove current Login User to the groups that are not selected.
                  this.props.deleteGroupUsers(this.props.token, group.id, currentUserId);
              }
          });

          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
      }
    }

    render() {
        return (
        <Modal isOpen={ this.props.modal_var } toggle={this.manage_modal} className="modal_data modal_data1 check-pop modal-lg">
            <ModalHeader toggle={this.manage_modal}>
                <span className="modal-title" id="exampleModalLabel">Check In<span> Verify the Group that you are assigned to today</span></span>
            </ModalHeader>
            <ModalBody>
              {
                /**

                <div className="medi-floor">
                    <h6>On Call Groups:</h6>
                    <ul>
                        {this.renderServiceOnCallGroups()}
                    </ul>
                </div>
                **/ }
                {this.renderServices()}
            </ModalBody>
            <ModalFooter>
                <p>Selected Groups: {this.renderFooterGroups()}
                  <span>this will allow you access to the following list: {this.renderFooterSignouts()}</span>
                </p>
              <ButtonM type="button" data-ripple="" className="btn blue-btn" onClick={(e)=>this.onAccept(e)}>Accept</ButtonM>

            </ModalFooter>
        </Modal>
        );
    }

}

function mapStateToProps( state ) {
     return {
         isFetching: state.users.isFetching,
         currentLoginUser: state.users.data,
         isFetching: state.services.isFetching,
         checkinService : state.services.serviceById
     }
}

export default connect(mapStateToProps, { addUsersToGroup, deleteGroupUsers })(CheckinModal);
