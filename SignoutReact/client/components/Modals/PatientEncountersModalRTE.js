import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';

import { reduxForm } from 'redux-form';
import RichTextEditor from 'react-rte';
import { getPatients, createPatient } from '../../actions/PatientsActions';

class AddEditPatientModal extends Component {

    constructor(props){
        super(props);
        this.state = { patientData : [], templateFields : [], value: RichTextEditor.createEmptyValue() };
        this.manage_modal = this.manage_modal.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {
      //console.log("PE--", nextProps.templateFields);

        if ( nextProps.editPatient && this.props.editPatient !== nextProps.editPatient ) {
            let editPatientData = nextProps.editPatient;

            this.setState( { patientData : editPatientData } );
        }

        if ( nextProps.templateFields && this.props.templateFields !== nextProps.templateFields ) {
            let templateFieldsData = nextProps.templateFields;

            this.setState( { templateFields : templateFieldsData } );
        }
    }

    onSubmit(formData) {
          console.log(formData);
          // this.props.createPatient(this.props.tenantId, this.state.id, this.props.token, formData);
          // let This = this;
          // setTimeout(function(){
          //     This.props.handle_modal('false');
          // }, 1000);
    }

    onChange(value) {
      this.setState({value : value});
      if (this.props.onChange) {
        // Send the changes up to the parent component as an HTML string.
        // This is here to demonstrate using `.toString()` but in a real app it
        // would be better to avoid generating a string on each change.
        this.props.onChange(
          value.toString('html')
        );
      }
    }

    renderTemplateEditors() {

        // The toolbarConfig object allows you to specify custom buttons, reorder buttons and to add custom css classes.
        // Supported inline styles: https://github.com/facebook/draft-js/blob/master/docs/Advanced-Topics-Inline-Styles.md
        // Supported block types: https://github.com/facebook/draft-js/blob/master/docs/Advanced-Topics-Custom-Block-Render.md#draft-default-block-render-map
        const toolbarConfig = {
          display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS'],
          INLINE_STYLE_BUTTONS: [
            {label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
            {label: 'Italic', style: 'ITALIC'},
            {label: 'Underline', style: 'UNDERLINE'}
          ],
          BLOCK_TYPE_BUTTONS: [
            {label: 'UL', style: 'unordered-list-item'},
            {label: 'OL', style: 'ordered-list-item'}
          ]
        };

        return this.state.templateFields.map( ( field, index ) => {
            return (
              <div className="col-md-4 col-sm-6" key={ field.id }>
                  <p className="editor-heading">{ field.name }</p>
                  <div className="editor-content">
                    <RichTextEditor
                              toolbarConfig={toolbarConfig}
                              value={this.state.value}
                              onChange={this.onChange}
                              placeholder="Tell a story..."
                            />
                  </div>
              </div>
            );
        });
    }

    render() {
      const { fields: { field_value }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form className="editSignoutTemplates check-pop edit-pt-editor" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Blackwell Signout List</span>
              </ModalHeader>
              <ModalBody>
                <div className="row">
                    <h6>Patient Name: 5709-1</h6>
                </div>
                <div className="row">
                    { this.renderTemplateEditors() }
                </div>
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function mapStateToProps(state) {
     return {
         isAuthenticated: state.patients.isAuthenticated,
         isAuthenticating: state.patients.isAuthenticating,
         statusText : state.patients.statusText,
     }
}

export default reduxForm({
    form: 'AddEditPatientForm',
    fields: ['field_value': []],

}, null, { getPatients, createPatient })( AddEditPatientModal );
