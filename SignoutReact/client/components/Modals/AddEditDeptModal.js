import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';

import { reduxForm } from 'redux-form';
import { createDepartment } from '../../actions/DepartmentActions';

class AddEditDepartmentModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          deptData: [],
          id: null,
          name: null,
          description: null
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.department && this.props.department !== nextProps.department ) {
            let department = nextProps.department;
            this.setState( { id : department.id } );
            this.setState( { name : department.name } );
            this.setState( { description : department.description } );
        } else if ( nextProps.department ) {
            if ( nextProps.department.length > 0 ) {
                let department = nextProps.department;
                this.setState( { id : department.id } );
                this.setState( { name : department.name } );
                this.setState( { description : department.description } );
            }
        }
    }

    onSubmit(formData) {

          this.props.createDepartment(this.props.tenantId, this.state.id, this.props.token, formData);
          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    render() {
        const { fields: { name, description }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Department</span>
              </ModalHeader>
              <ModalBody>
	                <div className="row form-group">
                    <div className ={`col-sm-12 form-group ${name.touched && name.invalid ? 'col-sm-12 has-danger' : ''}`}>

                      <input
                            className="inputMaterial"
                            type="text" required {...name}
                            value={  this.state.name }
                            initialValues={ this.state.name }
                            onChange={()=>this.setState({name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Department Name <i className="reqfld">*</i> </label>
                        <div className="error_msg_danger">
                            {name.touched && name.error ? name.error : false }
                        </div>
                    </div>
		              </div>
                  <div className="row form-group">
                      <div className="col-sm-12">
                        <textarea
                                  className="inputMaterial" required
                                  type="text" {...description}
                                  value={ this.state.description  }
                                  onChange={()=>this.setState({description: event.target.value})}>
                        </textarea>
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Description</label>
                      </div>
  							  </div>
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.name) {
        error.name = 'Please enter department name';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticated: state.departments.isAuthenticated,
         isAuthenticating: state.departments.isAuthenticating,
         statusText : state.departments.statusText,
         initialValues : {
           name: state.departments.name,
           description: state.departments.description
         }
     }
}

export default reduxForm({
    form: 'AddEditDepartmentForm',
    fields: ['name', 'description'],
    validate
}, mapStateToProps, { createDepartment })( AddEditDepartmentModal );
