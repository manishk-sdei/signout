import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';

import { deleteServiceGroups, deleteGroupUsers } from '../../actions/GroupsActions';
import { deleteServiceSignouts, deleteSignoutGroup, deleteSignoutEncounters } from '../../actions/SignoutActions';
import { deletePatient } from '../../actions/PatientsActions';
import { createUserServices, removeUserServices } from '../../actions/UsersActions';
import { deleteRole } from '../../actions/RolesActions';

class ConfirmDeleteModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          id: null,
          actionButton: null
        };
        this.deleteRow = this.deleteRow.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps(nextProps) {

      if(nextProps.subscriptionType) {
        this.setState({ actionButton : nextProps.subscriptionType === 'subscribe' ? 'Subscribe' : 'UnSubscribe' });
      } else {
        this.setState({ actionButton : 'Delete' });
      }
    }

    deleteRow(e) {
      e.preventDefault();

      if (this.props.confirmPage === 'ROLE_LIST') {
        if(this.props.roleId) {
          this.props.deleteRole( this.props.token, this.props.roleId );
        }
      }

      if (this.props.confirmPage === 'SERVICE_SUBSCRIPTION') {

        if(this.props.subscriptionType === 'subscribe') {
            this.props.createUserServices( this.props.token, this.props.userId, this.props.serviceId );
        }

        if(this.props.subscriptionType === 'unsubscribe') {
            this.props.removeUserServices(this.props.token, this.props.userId, this.props.serviceId);
        }
      }

      if (this.props.confirmPage === 'GROUP_LIST') {

        if(this.props.userId === null) {
            this.props.deleteServiceGroups( this.props.token, this.props.groupId );
        }

        if(this.props.userId && this.props.groupId) {
            this.props.deleteGroupUsers( this.props.token, this.props.groupId,  this.props.userId);
        }
      }

      if (this.props.confirmPage === 'SIGNOUT_LIST') {

        if((this.props.groupId === null) && this.props.signoutId) {
          this.props.deleteServiceSignouts( this.props.token, this.props.signoutId );
        }

        if(this.props.signoutId && this.props.groupId) {
            this.props.deleteSignoutGroup( this.props.token, this.props.signoutId, this.props.groupId );
        }
      }

      if (this.props.confirmPage === 'SIGNOUT_ENCOUNTER') {
        if(this.props.signoutId && this.props.encounterId) {
            
            this.props.deleteSignoutEncounters( this.props.token, this.props.signoutId, this.props.encounterId );
        }
      }

      if (this.props.confirmPage === 'PATIENT_LIST') {

        if(this.props.patientId) {
          this.props.deletePatient( this.props.token, this.props.patientId );
        }
      }

      this.closeModal();
    }

    closeModal() {
      // Close Modal Window
      let This = this;
      setTimeout(function(){
          This.props.handle_modal('false');
      }, 1000);
    }

    render() {
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-sm delete-modal check-pop patientedit-form">

              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">Confirm</span>
              </ModalHeader>
              <ModalBody>
                Are you sure?
              </ModalBody>
              <ModalFooter>
                <ButtonM
                  disabled={
                    this.props.isDeleteGroup ? true : false}
                  onClick={ (e) => this.deleteRow(e) }
                  type="submit"
                  className="blue-btn">{ this.state.actionButton }</ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>

          </Modal>
        );
    }
}

function mapStateToProps(state) {
     return {
         isDeleteGroup: state.groups.isAuthenticating,
         isDeleteSignout: state.signouts.isAuthenticating
     }
}

export default connect(mapStateToProps, {
                               deleteRole,
                               deleteServiceGroups,
                               deleteGroupUsers,
                               deleteServiceSignouts,
                               deleteSignoutGroup,
                               deletePatient,
                               createUserServices,
                               removeUserServices,
                               deleteSignoutEncounters })( ConfirmDeleteModal );
