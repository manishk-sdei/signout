/**
 * @TODO Add patient copy functionality to copy a patient data and add as another patient.
 */

import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { reduxForm } from 'redux-form';

import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import ButtonM from 'muicss/lib/react/button';
import moment from 'moment';
import MaskedInput from 'react-maskedinput';

import { createPatient } from '../../actions/PatientsActions';

import { phoneValidator } from '../Common/validate';

class AddEditPatientModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
            countriesLists : [],
            languagesLists : [],
            meritalstatusLists : [],
            raceLists : [],
            sexLists : [],
            timezonesLists : [],

            patientData: [],

            //  id: null,
            //  first_name: null,
            //  middle_name: null,
            //  last_name: null,
            //  mrn: null,
            //  sex: null,
            //  ssn: null,
            //  dob: null,
            //  race: null,
            //  marital_status: null,
            //  is_deceased: false,
            //  death_datetime: null,
            //  home_phone: null,
            //  office_phone: null,
            //  mobile_phone: null,
            //  email: '',
            //  language: null,
            //  street_address: null,
            //  city: null,
            //  state: null,
            //  zip: null,
            //  county: null,
            //  country: null,
        };
        this._onChange = this._onChange.bind(this);
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.countries && this.props.countries !== nextProps.countries ) {
            this.setState({ countriesLists : nextProps.countries.list });
        }

        if ( nextProps.languages && this.props.languages !== nextProps.languages ) {
            this.setState({ languagesLists : nextProps.languages.list });
        }

        if ( nextProps.meritalstatus && this.props.meritalstatus !== nextProps.meritalstatus ) {
            this.setState({ meritalstatusLists : nextProps.meritalstatus.list });
        }

        if ( nextProps.race && this.props.race !== nextProps.race ) {
            this.setState({ raceLists : nextProps.race.list });
        }

        if ( nextProps.sex && this.props.sex !== nextProps.sex ) {
            this.setState({ sexLists : nextProps.sex.list });
        }

        if ( nextProps.patient && this.props.patient !== nextProps.patient ) {
            let patient = nextProps.patient;

              this.setState( { id : patient.id,
                               first_name : patient.first_name,
                               middle_name : patient.middle_name,
                               last_name : patient.last_name,
                               dob : patient.dob ? moment(patient.dob).format('MM/DD/YYYY') : '',
                               ssn : patient.ssn,
                               mrn : patient.mrn,
                               sex : patient.sex } );

              if(patient.demographics) {
                    this.setState( { race : patient.demographics.race,
                                     marital_status : patient.demographics.martial_status,
                                     is_deceased : patient.demographics.is_deceased,
                                     death_datetime : patient.demographics.death_datetime === undefined ? undefined : moment(patient.demographics.death_datetime).format('MM/DD/YYYY HH:MM:SS'),
                                     home_phone : patient.demographics.phone_number.home,
                                     office_phone : patient.demographics.phone_number.office,
                                     mobile_phone : patient.demographics.phone_number.mobile,
                                     email :  patient.demographics.email_addresses[0],
                                     language : patient.demographics.language,
                                     street_address : patient.demographics.address.street_address,
                                     city : patient.demographics.address.city,
                                     state : patient.demographics.address.state,
                                     zip : patient.demographics.address.zip,
                                     county : patient.demographics.address.county,
                                     country : patient.demographics.address.country } );
              }
        }

        // Remove field state on Create patient
        if(nextProps.patient !== undefined && nextProps.patient.length === 0) {

          this.setState( {first_name: '',
                          middle_name: '',
                          last_name: '',
                          mrn: '',
                          sex: '',
                          ssn: '',
                          dob: '',
                          race: '',
                          marital_status: '',
                          is_deceased: false,
                          death_datetime: '',
                          home_phone: '',
                          office_phone: '',
                          mobile_phone: '',
                          email: '',
                          language: '',
                          street_address: '',
                          city: '',
                          state: '',
                          zip: '',
                          county: '',
                          country: '' } );

      }
    }

    onSubmit(formData) {

        let mobilePhone = formData.mobile_phone === undefined ? this.state.mobile_phone : phoneValidator(formData.mobile_phone);
        formData.mobile_phone = mobilePhone;
        let officePhone = formData.office_phone === undefined ? this.state.office_phone : phoneValidator(formData.office_phone);
        formData.office_phone = officePhone;
        let homePhone = formData.home_phone === undefined ? this.state.home_phone : phoneValidator(formData.home_phone);
        formData.home_phone = homePhone;

        let dob = formData.dob === undefined ? '' : moment(formData.dob).format('YYYY-MM-DD');
        formData.dob = dob;

        let patientId = this.state.id === undefined ? null : this.state.id;

        let email = (formData.email === undefined) ?  this.state.email === undefined ? '' : this.state.email : formData.email;
        formData.email = email;

        formData.is_deceased = this.state.is_deceased === null ? false : this.state.is_deceased;
        let death_datetime = formData.death_datetime === undefined ? this.state.death_datetime === null ? undefined : moment(this.state.death_datetime).format() : moment(formData.death_datetime).format();//YYYY-MM-DD HH:MM:SS
        formData.death_datetime = death_datetime;

         let ssn = (formData.ssn === undefined) ?  this.state.ssn : formData.ssn;
         formData.ssn = ssn;

         let city = (formData.city === undefined) ? this.state.city : formData.city;
         formData.city = city;
         let state = (formData.state === undefined) ? this.state.state : formData.state;
         formData.state = state;
         let street_address = (formData.street_address === undefined) ? this.state.street_address : formData.street_address;
         formData.street_address = street_address;
         let zip = (formData.zip === undefined) ? this.state.zip : formData.zip;
         formData.zip = zip;

        formData.sex = this.state.sex;
        formData.race = this.state.race;
        formData.marital_status = this.state.marital_status;
        formData.language = this.state.language;
        formData.country = this.state.country;

        this.props.createPatient(this.props.token, null, patientId, formData);
        let This = this;
        setTimeout(function(){
            This.props.handle_modal('false');
        }, 1000);
    }

    _onChange(e) {
        let stateChange = {};
        stateChange[e.target.name] = e.target.value;
        this.setState(stateChange);
      }

    render() {

        const { fields: { first_name,
                 middle_name,
                 last_name,
                 mrn,
                 sex,
                 ssn,
                 dob,
                 race,
                 marital_status,
                 is_deceased,
                 death_datetime,
                 home_phone,
                 office_phone,
                 mobile_phone,
                 email,
                 language,
                 street_address,
                 city,
                 state,
                 zip,
                 county,
                 country }, handleSubmit } = this.props;

        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Patient</span>
              </ModalHeader>
              <ModalBody>
                <div className="row">

                    <div className ={`col-sm-4 form-group ${first_name.touched && first_name.invalid ? 'col-sm-4 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text"
                            maxLength="50"
                            required {...first_name}
                            value={  this.state.first_name }
                            initialValues={ this.state.first_name }
                            onChange={(event)=>this.setState({first_name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>First Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {first_name.touched && first_name.error ? first_name.error : false }
                        </div>
                    </div>

                    <div className="col-sm-4 form-group">
                      <input
                            className="inputMaterial"
                            type="text" required {...middle_name}
                            value={  this.state.middle_name }
                            initialValues={ this.state.middle_name }
                            onChange={(event)=>this.setState({middle_name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Middle Name</label>
                    </div>

                    <div className ={`col-sm-4 form-group ${last_name.touched && last_name.invalid ? 'col-sm-4 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text" required {...last_name}
                            value={  this.state.last_name }
                            initialValues={ this.state.last_name }
                            onChange={(event)=>this.setState({last_name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Last Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {last_name.touched && last_name.error ? last_name.error : false }
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className={`col-sm-4 form-group ${dob.touched && dob.invalid ? 'has-danger' : ''}`}>
                        <MaskedInput
                                {...dob}
                                placeholder="MM/DD/YYYY"
                                className="inputMaterial"
                                mask="11/11/1111"
                                name="card"
                                size="8"
                                onChange={this._onChange}
                                value={  this.state.dob } />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>D.O.B</label>
                        <div className="error_msg_danger">
                            {dob.touched && dob.error ? dob.error : false }
                        </div>
                    </div>
                    <div className="col-sm-4 form-group">
                        <div className="custom-box">
                        <Select name="sex" {...sex}
                                defaultValue={ this.state.sex }
                                onChange={(event)=>this.setState({sex: event.target.value})}>
                            <Option label="Gender" />
                            { this.state.sexLists.map( val =>
                                <option key={ val.code }
                                        value={ val.display }>
                                { val.display }
                            </option>) }
                        </Select>
                        </div>
                    </div>
                    <div className="col-sm-4 form-group">
                        <div className="custom-box">
                            <Select
                                  name="meritalStatus"
                                  {...marital_status}
                                  value={ this.state.marital_status }
                                  onChange={(event)=>this.setState({marital_status: event.target.value})}>
                                <Option label="Marital Status" />
                                { this.state.meritalstatusLists.map( val =>
                                    <option key={ val.code }
                                            value={ val.display }>
                                    { val.display }
                                </option>) }
                            </Select>
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className ={`col-sm-4 form-group ${mrn.touched && mrn.invalid ? 'col-sm-4 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text" required {...mrn}
                            value={  this.state.mrn }
                            initialValues={ this.state.mrn }
                            onChange={(event)=>this.setState({mrn: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Mrn Id <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {mrn.touched && mrn.error ? mrn.error : false }
                        </div>
                    </div>

                    <div className="col-sm-4 form-group">
                        <div className="custom-box">
                            <Select name="race" {...race}
                                    defaultValue={ this.state.race }
                                    onChange={(event)=>this.setState({race: event.target.value})}>
                                <Option label="Race" />
                                { this.state.raceLists.map( val =>
                                    <option key={ val.code }
                                            value={ val.display }>
                                    { val.display }
                                </option>) }
                            </Select>
                        </div>
                    </div>

                    <div className="col-sm-4 form-group">
                        <div className="custom-box">
                            <Select
                                name="language"
                                {...language}
                                defaultValue={ this.state.language }
                                onChange={(event)=>this.setState({language: event.target.value})}>
                                <Option label="Language" />
                                { this.state.languagesLists.map( val =>
                                    <option key={ val.code }
                                            value={ val.display }>
                                    { val.display }
                                </option>) }
                            </Select>
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-sm-4 form-group">
                        <MaskedInput
                              {...ssn}
                              placeholder="XXX-XX-XXXX"
                              className="inputMaterial"
                              mask="111-11-1111"
                              name="ssn" size="8"
                              onChange={this._onChange}
                              value={ this.state.ssn } />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>SSN</label>
                    </div>

                    <div className="col-sm-4 form-group">
                        <div className="checkbox hgt">
                            <label>
                              <input
                                  {...is_deceased}
                                  type="checkbox"
                                  checked={this.state.is_deceased}
                                  onChange = { (e) => this.setState({ is_deceased : e.target.checked }) } />
                              <span className="cr"><i className="cr-icon fa fa-check"></i></span> Is deceased </label>
                        </div>
                    </div>

                    <div className={`col-sm-4 form-group
                         ${this.state.is_deceased === true ? 'show' : 'hide'}
                         ${death_datetime.touched && death_datetime.invalid ? 'has-danger' : ''}`}>
                        <MaskedInput
                                {...death_datetime}
                                placeholder="MM/DD/YYYY HH:MM:SS"
                                className="inputMaterial"
                                mask="11/11/1111 11:11:11"
                                name="card"
                                size="8"
                                onChange={this._onChange}
                                value={  this.state.death_datetime } />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>D.O.D</label>
                        <div className="error_msg_danger">
                            {death_datetime.touched && death_datetime.error ? death_datetime.error : false }
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-12 form-group popup-title">Contact Info</div>
                </div>

                <div className="row">
                      <div className ="col-sm-12 form-group">
                        <input
                            {...email} required
                            type="text"
                            className="inputMaterial"
                            value={ this.state.email }
                            onChange = { (event) => this.setState({ email: event.target.value }) } />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Email</label>
                          <div className="error_msg_danger">
                              {email.touched ? email.error : '' }
                          </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-4 form-group">
                        <MaskedInput
                                {...home_phone}
                                value={ this.state.home_phone }
                                placeholder="(+01) XXX XXX XXXX"
                                mask="(+11) 111 111 1111"
                                name="home_phone"
                                onChange={this._onChange}
                                className="inputMaterial" />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Home No.</label>
                    </div>
                    <div className="col-sm-4 form-group">
                        <MaskedInput
                                {...office_phone}
                                value={ this.state.office_phone }
                                placeholder="(+01) XXX XXX XXXX"
                                mask="(+11) 111 111 1111"
                                name="office_phone"
                                onChange={this._onChange}
                                className="inputMaterial" />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Office No.</label>
                    </div>
                    <div className="col-sm-4 form-group">
                        <MaskedInput
                                {...mobile_phone}
                                value={ this.state.mobile_phone }
                                placeholder="(+01) XXX XXX XXXX"
                                mask="(+11) 111 111 1111"
                                name="mobile_phone"
                                onChange={this._onChange}
                                className="inputMaterial" />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Mobile No.</label>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-12 form-group">
                        <textarea
                              {...street_address} required
                              type="text"
                              className="inputMaterial"
                              value={ this.state.street_address }
                              onChange={(event)=>this.setState({street_address: event.target.value})}></textarea><span className="highlight"></span> <span className="bar"></span>
                        <label>Street Address</label>
                    </div>
                </div>

                <div className="row">

                    <div className="col-sm-4 form-group">
                        <div className=" custom-box">
                          <input
                              {...city} required
                              type="text"
                              className="inputMaterial"
                              value={ this.state.city }
                              onChange={(event)=>this.setState({city: event.target.value})} />
                          <span className="highlight"></span> <span className="bar"></span>
                          <label>City</label>
                        </div>
                    </div>

                    <div className="col-sm-4 form-group">
                        <div className="custom-box">
                            <input
                                {...state} required
                                type="text"
                                className="inputMaterial"
                                value={ this.state.state }
                                onChange={(event)=>this.setState({state: event.target.value})} />
                            <span className="highlight"></span> <span className="bar"></span>
                            <label>State</label>
                        </div>
                    </div>

                    <div className="col-sm-4 form-group">
                        <div className="custom-box">
                            <Select
                                {...country}
                                name="country"
                                defaultValue={ this.state.country }
                                onChange={(event)=>this.setState({country: event.target.value})}>
                                <Option label="Country" />
                                { this.state.countriesLists.map( val =>
                                    <option key={ val.id }
                                            value={ val.id }>
                                    { val.name }
                                </option>) }
                            </Select>
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-sm-4 form-group">
                        <input
                            className="inputMaterial"
                            {...zip} required
                            type="text"
                            value={ this.state.zip }
                            onChange={(event)=>this.setState({zip: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Zip Code</label>
                    </div>

                    {/**<div className="col-sm-4 form-group">
                        <input
                          {...county} required
                          type="text"
                          className="inputMaterial"
                          value={ this.state.county }
                          onChange={(event)=>this.setState({county: event.target.value})} />
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>County</label>
                    </div>**/}
                </div>

              </ModalBody>
              <ModalFooter>
                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.first_name) {
        error.first_name = 'Please enter first name';
    }

    if(!values.last_name) {
        error.last_name = 'Please enter last name';
    }

    if(!values.dob) {
        error.dob = 'Please enter date of birth(DOB)';
    }
    if(values.dob) {
        if(moment(values.dob).format('YYYY-MM-DD') === 'Invalid date') {
            error.dob = 'Please enter valid date';
        }
    }

    if(moment(values.death_datetime).format('YYYY-MM-DD HH:MM:SS') === 'Invalid date') {
        error.death_datetime = 'Please enter valid date';
    }

    if(!values.mrn) {
        error.mrn = 'Please enter mrn id';
    }
    // if(!values.email) {
    //     error.email = 'Please enter email address';
    // }
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        if(values.email) {
            error.email = 'Please enter a valid email address';
        }
    }
    // if(!values.mobile_phone) {
    //     error.mobile_phone = 'Please enter mobile number';
    // }
    return error;
}

function mapStateToProps(state) {
     return {
          isFetching : state.terms.isFetching,
          countries : state.terms.countries,
          languages : state.terms.languages,
          meritalstatus : state.terms.meritalstatus,
          race : state.terms.race,
          sex : state.terms.sex,
          timezones : state.timezones,

         isAuthenticating : state.patients.isAuthenticating,
         initialValues : {
           first_name: state.patients.first_name,
           middle_name: state.patients.middle_name,
           last_name: state.patients.last_name,
           dob: state.patients.dob,
           mrn: state.patients.mrn
         }
     }
}

export default reduxForm({
    form: 'AddEditPatientForm',
    fields: ['first_name',
             'middle_name',
             'last_name',
             'mrn',
             'sex',
             'ssn',
             'dob',
             'race',
             'marital_status',
             'is_deceased',
             'death_datetime',
             'home_phone',
             'office_phone',
             'mobile_phone',
             'email',
             'language',
             'street_address',
             'city',
             'state',
             'zip',
             'county',
             'country'
            ],
    validate
}, mapStateToProps, { createPatient })( AddEditPatientModal );
