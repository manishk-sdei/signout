import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';
import { reduxForm } from 'redux-form';
import { createServiceGroups } from '../../actions/GroupsActions';

class AddEditGroupModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          id: null,
          name: null,
          description: null,
          is_on_call: false,
          serviceId: null
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    componentWillReceiveProps( nextProps ) {

        this.setState({ serviceId : this.props.serviceId });
        if ( nextProps.group && this.props.group !== nextProps.group ) {
            let group = nextProps.group;
            this.setState( { id : group.id } );
            this.setState( { name : group.name } );
            this.setState( { description : group.description } );
            this.setState( { is_on_call: group.is_on_call } );
        }
    }

    onSubmit( formData ) {

          // @args - token, serviceId, groupId, formData
          this.props.createServiceGroups(this.props.token, this.state.serviceId, this.state.id, formData);
          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    render() {
        const { fields: { name, description, is_on_call }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Group</span>
              </ModalHeader>
              <ModalBody>
	                <div className="row form-group">
                    <div className ={`col-sm-12 form-group ${name.touched && name.invalid ? 'col-sm-12 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text" required {...name}
                            value={  this.state.name }
                            initialValues={ this.state.name }
                            onChange={()=>this.setState({name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Group Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {name.touched && name.error ? name.error : false }
                        </div>
                    </div>
		              </div>
                  <div className="row form-group">
                      <div className="col-sm-12">
                        <textarea
                                  className="inputMaterial" required
                                  type="text" {...description}
                                  value={ this.state.description  }
                                  onChange={()=>this.setState({description: event.target.value})}>
                        </textarea>
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Description</label>
                      </div>
  							  </div>

                  <div className="row form-group">
                      <div className="col-sm-12">
                        <div className="checkbox">
                            <label>
                              <input
                                  {...is_on_call}
                                  type="checkbox" />
                                <span className="cr"><i className="cr-icon fa fa-check"></i></span> is on call </label>
                        </div>
                      </div>
  							  </div>
              </ModalBody>
              <ModalFooter>

                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
                <ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.name) {
        error.name = 'Please enter group name';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticated: state.groups.isAuthenticated,
         isAuthenticating: state.groups.isAuthenticating,
         statusText : state.groups.statusText,
         initialValues : {
           name: state.groups.name,
           description: state.groups.description,
           is_on_call: state.groups.is_on_call
         }
     }
}

export default reduxForm({
    form: 'AddEditGroupForm',
    fields: ['name', 'description', 'is_on_call'],
    validate
}, mapStateToProps, { createServiceGroups })( AddEditGroupModal );
