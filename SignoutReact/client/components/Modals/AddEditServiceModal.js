import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ButtonM from 'muicss/lib/react/button';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import { reduxForm } from 'redux-form';
import { createService } from '../../actions/ServicesActions';

class AddEditServiceModal extends Component {

    constructor(props){
        super(props);
        this.manage_modal = this.manage_modal.bind(this);
        this.state = {
          id: null,
          name: null,
          description: null,
          departments: [],
          departmentId: null,
          is_self_enrollment: false
        };
    }

    manage_modal() {
        this.props.handle_modal('false');
    }

    // componentDidMount() {
    //   setTimeout(function(){
    //     this.setState({ departmentId: this.props.departmentId });
    //   }, 3000);
    // }

    componentWillReceiveProps( nextProps ) {

          this.setState({ departmentId: this.props.departmentId });

        if ( nextProps.departments && this.props.departments !== nextProps.departments ) {
            this.setState( { departments : nextProps.departments } );
        } else if ( nextProps.departments ) {
            if ( nextProps.departments.length > 0 ) {
                this.setState( { departments : nextProps.departments } );
            }
        }

        if ( nextProps.service && this.props.service !== nextProps.service ) {
            let service = nextProps.service;

            this.setState( { departmentId : nextProps.departmentId } );
            this.setState( { id : service.id } );
            this.setState( { name : service.name } );
            this.setState( { description : service.description } );
            this.setState( { is_self_enrollment : service.is_self_enrollment } );
        }
        // else if ( nextProps.service ) {
        //     if ( nextProps.service.length > 0 ) {
        //         let service = nextProps.service;
        //         this.setState( { departmentId : nextProps.departmentId } );
        //         this.setState( { id : service.id } );
        //         this.setState( { name : service.name } );
        //         this.setState( { description : service.description } );
        //     }
        // }
    }

    onSubmit(formData) {
          //console.log("here...", this.state.id, formData.is_self_enrollment);
          this.props.createService(this.state.id, this.props.token, formData);
          let This = this;
          setTimeout(function(){
              This.props.handle_modal('false');
          }, 1000);
    }

    render() {

        const { fields: { departments, name, description, is_self_enrollment }, handleSubmit } = this.props;
        return (
          <Modal isOpen={ this.props.modal_var} toggle={this.manage_modal} className="modal_data modal-lg check-pop patientedit-form">
            <form noValidate className="editroll" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <ModalHeader toggle={this.manage_modal}>
                 <span className="modal-title">{ this.state.id ? 'Edit' : 'Add' } Service</span>
              </ModalHeader>
              <ModalBody>

	                <div className="row form-group">
                    <div className ={`col-sm-12 form-group ${name.touched && name.invalid ? 'col-sm-12 has-danger' : ''}`}>
                      <input
                            className="inputMaterial"
                            type="text" required {...name}
                            value={  this.state.name }
                            onChange={()=>this.setState({name: event.target.value})} />
                      <span className="highlight"></span> <span className="bar"></span>
                      <label>Service Name <i className="reqfld">*</i></label>
                        <div className="error_msg_danger">
                            {name.touched ? name.error : '' }
                        </div>
                    </div>
		              </div>

                  <div className="row form-group">
                      <div className="col-sm-12">
                        <textarea
                                  className="inputMaterial" required
                                  type="text" {...description}
                                  value={ this.state.description  }
                                  onChange={()=>this.setState({description: event.target.value})}>
                        </textarea>
                        <span className="highlight"></span> <span className="bar"></span>
                        <label>Description</label>
                      </div>
  							  </div>

                  <div className="row form-group">
                      <div className={`col-sm-12 form-group ${departments.touched && departments.invalid ? 'col-sm-12 has-danger' : ''}`}>
                        <Select name="department" {...departments} value={ this.state.departmentId }>
                            <Option label="Department *"></Option>
                            { this.state.departments.map( department => <option key={ department.id } value={ department.id }>{ department.name }</option>) }
                        </Select>
                        <span className="highlight"></span> <span className="bar"></span>
                        <div className="error_msg_danger">
                            {departments.touched ? departments.error : '' }
                        </div>
                      </div>
                  </div>

                  <div className="row form-group">
                      <div className="col-sm-12">
                        <div className="checkbox">
                            <label>
                              <input
                                  {...is_self_enrollment}
                                  type="checkbox" />
                                <span className="cr"><i className="cr-icon fa fa-check"></i></span> is self enrollment </label>
                        </div>
                      </div>
  							  </div>

              </ModalBody>
              <ModalFooter>

                <ButtonM
                  type="submit"
                  className="blue-btn">
                  { this.props.isAuthenticating === true ? 'Saving...' : 'Save' }
                </ButtonM>
        				<ButtonM type="button" className="grey-btn mr-sm-4" onClick={ this.manage_modal }>Cancel</ButtonM>
              </ModalFooter>
                </form>
          </Modal>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.name) {
        error.name = 'Please enter service name';
    }
    if(!values.departments) {
        error.departments = 'Please select a department';
    }
    return error;
}

function mapStateToProps(state) {
     return {
         isAuthenticated: state.services.isAuthenticated,
         isAuthenticating: state.services.isAuthenticating,
         statusText : state.services.statusText,
         initialValues : {
           departments: state.services.departmentId,
           name: state.services.name,
           description: state.services.description,
           is_self_enrollment: state.services.is_self_enrollment
         }
     }
}

export default reduxForm({
    form: 'AddEditserviceForm',
    fields: ['name', 'description', 'departments', 'is_self_enrollment'],
    validate
}, mapStateToProps, { createService })( AddEditServiceModal );
