import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getRoles, updateRole } from '../../actions/RolesActions';
import ModalWindowConfirmDelete from '../Modals/ConfirmDeleteModal';
import ModalWindow from '../Modals/AddRoleModal';

class RoleList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token : null,
            rolesData: [],
            roleid: null,
            editRoleData: [],
            deleteRole: null
        };
        this.open_modal = this.open_modal.bind(this);
        this.open_modal_delete = this.open_modal_delete.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Roles";
    }

    componentWillMount() {
      this.props.getRoles(this.props.token);
      this.setState({ token : this.props.token });
    }

    componentWillReceiveProps( nextProps ) {

        if ( nextProps.roles && this.props.roles !== nextProps.roles ) {
            let roles = nextProps.roles;
            this.setState( { rolesData : roles } );
        } else if ( nextProps.roles ) {
            if ( nextProps.roles.length > 0 ) {
                let roles = nextProps.roles;
                this.setState( { rolesData : roles } );
            }
        }
    }

    open_modal(e, role) {
      this.props.getRoles(this.state.token);
      if( e===true ) {
          e.preventDefault();
      } else {
        if(role) {
          this.props.updateRole(role); // Update initial value
        }
          this.setState({
              modal1: !this.state.modal1,
              editRoleData: role
          });
      }
    }

    open_modal_delete( e, roleId ) {

      this.props.getRoles(this.state.token);

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal2: !this.state.modal2,
          deleteRole: roleId
      });
    }

    renderRow() {//console.log(rolesData);
      if( this.state.rolesData ) {
        return this.state.rolesData.map(( role ) => {//console.log(role);
          return (
            <tr key={ role.id }>
                <td>{ role.name }</td>
                <td>{ role.description }</td>
                <td>
                    <button
                      onClick={ (e)=>this.open_modal(e, role) }
                      className="action-btn"
                      data-toggle="modal"
                      data-target="#edit-patient6">
                      <i className="zmdi zmdi-edit"></i>
                    </button>
                    <button
                      onClick={ (e) => this.open_modal_delete( e, role.id )}
                      className="action-btn">
                      <i className="zmdi zmdi-delete"></i>
                    </button>
                </td>
            </tr>
          );
        });
      }
    }

    render() {
        return(
          <div className="table-responsive">
            <button type="button"
                    onClick={ this.open_modal }
                    className="ripple r-round btn blue-btn add-btn"
                    data-toggle="modal"
                    data-target="#create-patient">
                    <span className="hidden-xs-down">CREATE ROLE</span>
                    <img className="hidden-sm-up" src="../../client/assets/images/add-file.png" />
            </button>

            <ModalWindow
              modal_var = { this.state.modal1 }
              handle_modal = { this.open_modal }
              token = { this.state.token }
              role = { this.state.editRoleData } />

          <ModalWindowConfirmDelete
                      modal_var = {this.state.modal2 }
                      handle_modal = { this.open_modal_delete }
                      token = { this.state.token }
                      confirmPage = { 'ROLE_LIST' }
                      roleId = { this.state.deleteRole } />

              <table className="table table-hover patient-list roletable">
                {/*<!--table start-->*/}
                  <thead>
                      <tr>
                          <th width="30%">Role Name</th>
                          <th width="50%">Role Description</th>
                          <th width="20%">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                    {this.renderRow()}
                  </tbody>
              </table>
            {/*<!--table End-->*/}
          </div>
        );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.roles.isFetching,
         roles: state.roles.data
     }
}

export default connect(mapStateToProps, { getRoles, updateRole })( RoleList );
