/**
 * @class         :	ForgotPassword
 * @description   :
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { resetPassword, statusUpdate } from '../../actions/LoginActions';
import { Link } from 'react-router';
import { Alert } from 'reactstrap';

class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = { show_error: 'hide' }
    }

    componentDidMount() {
        document.title = "Signout - Reset Password";
        this.props.statusUpdate();
    }

    componentDidMount() {
        this.props.statusUpdate();
    }

    renderPwdWarning() {
            return (
                  (this.props.details && this.props.details.warning !== undefined && this.props.details.warning !== "" ) ?
                      <Alert color="warning">{this.props.details.warning}</Alert> : ''
            );
    }

    renderPwdSuggestions() {

        return (this.props.details && this.props.details.suggestions !== undefined) ?
          this.props.details.suggestions.map(( suggestionPoint ) => {
                return (
                    <Alert color="danger">{suggestionPoint}</Alert>
                )
        }) : '';
    }

    onSubmit(formData) {
        let resetToken = this.props.location.query.reset_token === undefined ? null : this.props.location.query.reset_token;
        this.props.resetPassword(formData, resetToken);
    }

    render() {
        const { fields: { new_password, confirm_password }, handleSubmit } = this.props;
        return(
            <div className="login-bg">
                <div className="login-wrap">
                    <div className="login-box">
                        <h1><img src="/client/assets/images/login-logo.png" className="login-logo" alt=""/></h1>
                        <div className="login-form reset-password-form">
                          <h2>RESET PASSWORD</h2>
                          <div className="col-sm-12">
                              {
                                (this.props.statusText!=null)?
                                <Alert color={this.props.statusCode === 200 ? "success" : "danger"}>{this.props.statusText}</Alert>
                                :""
                              }
                              {this.renderPwdWarning()}
                              {this.renderPwdSuggestions()}
                          </div>
                          <div className="col-sm-12">
                                <div className="tab-pane show active" id="signin" role="tabpanel">

                                    <form noValidate action="signoutlist" onSubmit={handleSubmit(this.onSubmit.bind(this))}>

                                       <div className={`form-group ${new_password.touched && new_password.invalid ? 'has-danger' : ''}`}>
                                            <input type="text" required className="form-control inputMaterial" {...new_password} />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>New Password</label>
                                            <i className="zmdi zmdi-lock-outline"></i>
                                            <div className="error_msg_danger">
                                                {new_password.touched ? new_password.error : '' }
                                            </div>
                                        </div>

                                        <div className={`form-group ${confirm_password.touched && confirm_password.invalid ? 'has-danger' : ''}`}>
                                            <input type="text" required className="form-control inputMaterial" {...confirm_password} />
                                            <span className="highlight"></span> <span className="bar"></span>
                                            <label>Confirm Password</label>
                                            <i className="zmdi zmdi-lock-outline"></i>
                                            <div className="error_msg_danger">
                                                {confirm_password.touched ? confirm_password.error : '' }
                                            </div>
                                        </div>

                                        <button className="btn blue-btn sign-btn">
                                          { this.props.isAuthenticating === true ? 'Sending...' : 'Send' }</button>
                                        <div className="frgt-password">
                                            <Link to="/" className="forgot-password">click here to Login!</Link>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function validate(values) {

    const error = {};

    if(!values.new_password) {
        error.new_password = 'Please enter new password';
    }

    if(!values.confirm_password) {
        error.confirm_password = 'Please enter confirm password';
    }

    if(values.new_password !== values.confirm_password) {
      error.confirm_password = 'New password does\'t match with confirm password';
    }
    return error;
}

function mapStateToProps(state) {

  return {
      isAuthenticating : state.login.isAuthenticating,
      statusCode : state.login.statusCode,
      statusText : state.login.statusText,
      details : state.login.details
  }
}

export default reduxForm({
    form: 'ResetPasswordForm',
    fields: ['new_password',
             'confirm_password'],
    validate
}, mapStateToProps, { resetPassword, statusUpdate })( ResetPassword );
