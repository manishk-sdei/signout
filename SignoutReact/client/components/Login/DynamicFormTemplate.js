/**
 * @class         :	ForgotPassword
 * @description   :
 * @Created by    : smartData
 */

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import _ from 'lodash';
import { Link } from 'react-router';

const FIELDS = {
  title: {
    type: 'input',
    label: 'Title for the post'
  },
  categories: {
    type: 'input',
    label: 'Enter some categories for this post'
  },
  content: {
    type: 'textarea',
    label: 'Post Content'
  }
};

class ForgotPassword extends Component {

    constructor(props) {
        super(props);
    }



    onSubmit(props) {
      console.log("Post Submitted : ", props);
      //  let response = this.props.forgotPassword(props);
      //  this.context.router.push('/');
    }

    renderField(fieldConfig, field) {
      const fieldHelper = this.props.fields[field];

      return (
        <div className={`form-group ${fieldHelper.touched && fieldHelper.invalid ? 'has-danger' : '' }`}>
          <label>{fieldConfig.label}</label>
          <fieldConfig.type type="text" className="form-control" {...fieldHelper} />
          <div className="text-help">
            {fieldHelper.touched ? fieldHelper.error : '' }
          </div>



          <div className={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
            <input type="text" required className="form-control inputMaterial" {...email} />
            <span className="highlight"></span> <span className="bar"></span>
            <label>New Password</label>
            <i className="zmdi zmdi-lock-outline"></i>
            <div className="error_msg_danger">
                {email.touched ? email.error : '' }
            </div>
        </div>

        <div className={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
            <input type="text" required className="form-control inputMaterial" {...email} />
            <span className="highlight"></span> <span className="bar"></span>
            <label>Confirm Password</label>
            <i className="zmdi zmdi-lock-outline"></i>
            <div className="error_msg_danger">
                {email.touched ? email.error : '' }
            </div>
        </div>



        </div>
      );
    }

    render() {
        const { handleSubmit } = this.props;
        return(
            <form onSubmit={handleSubmit(props => this.onSubmit(props))}>

                {_.map(FIELDS, this.renderField.bind(this))}
                <button className="btn blue-btn sign-btn">Save Password</button>
                <div className="frgt-password">
                    <Link to="/login?id=smartdata" className="forgot-password">click here to Login!</Link>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const error = {};

    _.each(FIELDS, (type, fields) => {
      if(!values[field]) {
        error[field] = `Enter a ${field}`;
      }
    });

    return error;
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
// reduxForm: first is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps

export default reduxForm({
    form: 'ForgotPasswordFormNew',
    fields: _.keys(FIELDS),
    validate
})(ForgotPassword);
