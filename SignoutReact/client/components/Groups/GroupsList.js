import React, { Component } from 'react';
import { connect } from 'react-redux';
import Link from 'react-router';
import { getServiceGroups, updateGroupInitialState, getServiceGroupById } from '../../actions/GroupsActions';
import { getServiceById } from '../../actions/ServicesActions';
import { getUsers } from '../../actions/UsersActions';

import { Collapse, Button, CardBlock, Card } from 'reactstrap';

import ModalWindow from '../Modals/AddEditGroupModal';
import ModalWindowConfirmDelete from '../Modals/ConfirmDeleteModal';
import ModalWindowAddUsers from '../Modals/AddUsersToGroupModal';
import ButtonM from 'muicss/lib/react/button';

class GroupsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            modal2: false,
            modal3: false,
            serviceId: null,
            groupId: null,
            deleteGroup: null,
            flagGroupId: null,
            deleteUser: null,
            flagUserId: null,
            userGroup: null,
            token: null,
            groupsData: [],
            serviceData: [],
            editGroups: [],
            usersData: [],
            groupUsersData: [],
            data: {id: null, name: null, description: null},
            collapse: true,
            collapseId: null,
            assignedUsers: [],
            isTenantAdmin: false
        };
        this.toggle = this.toggle.bind(this);
        this.open_modal = this.open_modal.bind(this);
        this.open_modal_delete = this.open_modal_delete.bind(this);
        this.open_modal_add_users = this.open_modal_add_users.bind(this);
    }

    componentDidMount() {
        document.title = "Signout - Service - Group List";
        this.setState({ token : this.props.token });
        this.setState({ serviceId : this.props.serviceId });
    }

    componentWillMount() {
      this.setState({ token : this.props.token });
      this.setState({ serviceId : this.props.serviceId });

      //@args - token, service id
      this.props.getServiceGroups( this.props.token, this.props.serviceId );
      this.props.getServiceById( this.props.token, this.props.serviceId );
      this.props.getUsers( this.props.token );
    }

    componentWillReceiveProps( nextProps ) {

         if(nextProps.currentLoginUser) {
            this.setState({ isTenantAdmin : nextProps.currentLoginUser.is_tenant_admin });
         }

        if ( nextProps.service && this.props.service !== nextProps.service ) {
            let service = nextProps.service;
            this.setState( { serviceData : service } );
        }

        if ( nextProps.groups && this.props.groups !== nextProps.groups ) {
            let groups = nextProps.groups;
            this.setState( { groupsData : groups } );
        }

        if ( nextProps.users && this.props.users !== nextProps.users ) {
            let users = nextProps.users;
            this.setState( { usersData : users } );
        }

        if ( nextProps.groupById && this.props.groupById !== nextProps.groupById ) {
            let groupUsers = nextProps.groupById.users;
            this.setState( { groupUsersData : groupUsers } );
        }
    }

    open_modal(e, data) {
      this.props.getServiceGroups( this.props.token, this.state.serviceId ); // Update row
      if( e===true ) {
          e.preventDefault();
      } else {

        if(data) {
          this.props.updateGroupInitialState(data); // Update initial value
        }

          this.setState({
              modal1: !this.state.modal1,
              editGroups: data,
          });
      }
    }

    open_modal_delete( e, groupId, userId ) {

      this.props.getServiceGroups( this.props.token, this.state.serviceId ); // Update row

      if(this.state.flagUserId && this.state.flagGroupId) {
        this.props.getServiceGroupById( this.props.token, this.state.flagGroupId );
      }

      (e===true) ? e.preventDefault() : '';
      this.setState({
          modal2: !this.state.modal2,
          deleteGroup: groupId,
          deleteUser: userId,
          flagGroupId: groupId,
          flagUserId: userId
      });
    }

    open_modal_add_users( e, groupId, users ) {

      //this.props.getServiceGroups( this.props.token, this.state.serviceId ); // Update row
      if(this.state.flagGroupId) {
        this.props.getServiceGroupById( this.props.token, this.state.flagGroupId );
      }

      if( e===true ) {
        e.preventDefault()
      } else {
        this.setState({
            modal3: !this.state.modal3,
            userGroup: groupId,
            flagGroupId: groupId,
            assignedUsers: users
        });
      }
    }

    toggle(e, groupId) {
      e.preventDefault();
//console.log(this.state.collapse);
      // Get assigned users for the group
    //  (this.state.collapse === true) ? false : '';

    //  if(this.state.collapse === false) {
          this.props.getServiceGroupById( this.props.token, groupId );
    //  } else {
        this.setState({ groupUsersData: [] });
  //    }

      this.setState({ collapseId: groupId });
    //  this.state.collapse === true ? this.setState({ collapse: false }) :
      //this.setState({ collapse: true });

      //---------------

      if(this.state.collapseId === groupId ) {
        this.setState({ collapse: false });
      } else {
        this.setState({ collapse: true });
      }
    }

    renderLoading() {
        if(this.props.isFetching) {
          return(
                <tbody>
                    <tr>
                      <td>
                        <div className="loader" id="loader-4">
                          <span></span>
                          <span></span>
                          <span></span>
                        </div>
                    </td>
                    </tr>
                </tbody>
          );
        }
    }

    renderRow() {

        if((this.state.groupsData.length === 0) && (this.props.isFetching === false)) {
          return (
            <tbody>
            <tr>
              <td colSpan="4" >
                  No group.
              </td>
            </tr>
            </tbody>
          );
        }

      if( this.state.groupsData ) {
        return this.state.groupsData.map(( group ) => {

          return (
            <tbody key={group.id}>
            <tr>
                <td><a href="" onClick={ (e) => this.toggle(e, group.id)}>{ group.name }</a></td>
                <td>{ (group.description!='')?group.description:'n/a' }</td>
                <td>{ group.is_on_call === false ? 'No' : 'Yes' }</td>
                { (this.state.isTenantAdmin === true) ?
                <td>
                    <button
                      onClick={ (e)=>this.open_modal(e, group) }
                      className="action-btn"
                      data-toggle="modal"
                      data-target="#edit-patient1">
                      <i className="zmdi zmdi-edit"></i>
                    </button>
                    <button
                      onClick={ (e) => this.open_modal_delete( e, group.id, null )}
                      className="action-btn">
                      <i className="zmdi zmdi-delete"></i>
                    </button>
                </td>
                : <td></td> }
            </tr>

            <tr>
                <td className="pd-off" colSpan="5">

                  <Collapse isOpen={ this.state.collapseId === group.id  ? this.state.collapse : false }>
                <Card>
                  <CardBlock>
                    <table className="table patient-list service-tb">
                        <thead>
                          <tr>
                              <th width="25%"></th>
                              <th width="25%">User Name</th>
                              <th width="25%">Email</th>
                              <th width="10%"></th>
                          </tr>
                        </thead>
                        <tbody>

                          { this.renderGroupUsers(group.id) }
                          <tr>
                              <td></td>
                              <td colSpan="3">
                                {/** agrs - groupId and already added users to this group */}
                                { (this.state.isTenantAdmin === true) ?
                                <button
                                      onClick={ (e) => this.open_modal_add_users( e, group.id, this.state.groupUsersData )}
                                      className="action-btn"
                                      data-toggle="modal"
                                      data-target="#edit-patient1">
                                      <i className="zmdi zmdi-plus-circle"></i> Add User
                              </button> : "" }</td>
                          </tr>
                        </tbody>
                    </table>
                  </CardBlock>
               </Card>
             </Collapse>
                </td>
            </tr>

          </tbody>
          );
        });
      }
    }

    renderGroupUsers(groupId) {

        if(this.props.isFetchingById) {
          return(
                <tr>
                  <td></td>
                  <td colSpan="3">
                    <div className="loader" id="loader-4">
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                </td>
                </tr>
          );
        }

        if(this.state.groupUsersData.length == '0') {
          return (
            <tr>
              <td></td>
              <td colSpan="3" >No User.</td>
            </tr>
          );
        }

      if( this.state.groupUsersData ) {
        return this.state.groupUsersData.map(( user ) => {

          return (
            <tr key={user.id}>
                <td></td>
                <td> { user.first_name } { user.last_name } </td>
                <td> { user.email } </td>
                <td>
                  { (this.state.isTenantAdmin === true) ?
                  <button
                    onClick={ (e) => this.open_modal_delete( e, groupId, user.id )}
                    className="action-btn">
                    <i className="zmdi zmdi-delete"></i>
                  </button>
                  : "" }
                </td>

            </tr>
          );
        });
      }
    }

    render() {
      return(
      <div className="pd-off-xs">

          <div className="row">
              <div className="col-sm-12">
                  <div className="white-bg">
                    {/** ModalWindow - with props **/}
                    <ModalWindow
                                modal_var = {this.state.modal1 }
                                handle_modal = { (e) => this.open_modal(e, this.state.data) }
                                token = { this.props.token }
                                serviceId = { this.state.serviceId }
                                group = { this.state.editGroups } />
                    <ModalWindowConfirmDelete
                                modal_var = {this.state.modal2 }
                                handle_modal = { this.open_modal_delete }
                                token = { this.state.token }
                                confirmPage = { 'GROUP_LIST' }
                                groupId = { this.state.deleteGroup }
                                userId = { this.state.deleteUser } />

                    <ModalWindowAddUsers
                                modal_var = {this.state.modal3 }
                                handle_modal = { this.open_modal_add_users }
                                token = { this.state.token }
                                groupId = { this.state.userGroup }
                                users = { this.state.usersData }
                                assignedUsers = { this.state.assignedUsers } />
                      { (this.state.isTenantAdmin === true) ?
                      <ButtonM type="button"
                               onClick={(e)=>this.open_modal(e, null)}
                               className="add-deprt btn blue-btn add-btn"
                               data-toggle="modal"
                               data-target="#create-patient">
                               <span className="hidden-xs-down">CREATE GROUP</span>
                                 <img className="hidden-sm-up"
                                      src="/client/assets/images/add-file.png" />
                      </ButtonM>
                      : "" }

                      <div className="table-responsive">
                          <table className="table patient-list depart-list">
                              <thead>
                                  <tr>
                                      <th width="25%">Group Name</th>
                                      <th width="40%">Description</th>
                                      <th width="20%">Is on call</th>
                                      <th width="15%">{ (this.state.isTenantAdmin === true) ? 'Action' : '' }</th>
                                  </tr>
                              </thead>
                                  { this.renderLoading() }
                                  { this.renderRow() }
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      );
    }
}

function mapStateToProps(state) {
     return {
         isFetching: state.groups.isFetching,
         groups: state.groups.data,
         isFetchingById: state.groups.isFetchingById,
         groupById: state.groups.groupById,
         isFetching: state.services.isFetching,
         service: state.services.serviceById,
         isFetching: state.users.isFetching,
         users: state.users.usersList,
         currentLoginUser: state.users.data
     }
}

export default connect(mapStateToProps, { getServiceById,
                                          getServiceGroupById,
                                          getUsers,
                                          getServiceGroups,
                                          updateGroupInitialState})( GroupsList );
